#pragma once

#include "Command.h"

class MoveUnitLeftCommandP2 : public Command
{
public:
	~MoveUnitLeftCommandP2();

	virtual void execute() {};
	virtual void execute(State &state);
};

