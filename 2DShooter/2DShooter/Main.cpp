#include <array>

#include <allegro5\display.h>
#include <allegro5\allegro.h>
#include <allegro5\allegro5.h>
#include <allegro5\allegro_image.h>
#include <allegro5\allegro_native_dialog.h>
#include <allegro5\allegro_font.h>
#include <allegro5\allegro_ttf.h>
#include <allegro5\allegro_audio.h>
#include "allegro5\allegro_acodec.h"

#include "State.h"

//*******************************************
// PROJECT FUNCTIONS
//*******************************************

void InitAllegro();										// Used to inialize everything for Allegro

int main()
{
	InitAllegro();

	State state_handler;

	state_handler.RunGameStates();

	return 0;
}

void InitAllegro()
{
	srand((unsigned)time(0));

	if (!al_init())
	{
		exit(1);
	}

	// Initlizes the ability to use images
	if (!al_init_image_addon())
	{
		printf("Error loading image addon!\n");
		exit(1);
	}

	// Initialize the ability to use fonts
	al_init_font_addon();

	// Initialize the ability to use true type fonts
	if (!al_init_ttf_addon())
	{
		printf("Error loading font addon!\n");
		exit(1);
	}

	al_install_audio();				// Allows audio to be played

	if (!al_is_audio_installed())
	{
		printf("Error loading audio addon!\n");
		exit(1);
	}

	// Allows the ability to load diffrent audio files
	if (!al_init_acodec_addon())
	{
		printf("Error loading accodec addon!\n");
		exit(1);
	}

	// How many sounds I am allowed to play (20)
	if (!al_reserve_samples(20))
	{
		printf("Error loading samples!\n");
		exit(1);
	}

	// Allows the ability to use joysticks in the game.
	if (!al_install_joystick())
	{
		printf("Error loading joystick addon!\n");
		exit(1);
	}

	// Allows the aility to use keyboard in the game
	if (!al_install_keyboard())
	{
		printf("Error loading keyboard addon!\n");
		exit(1);
	}

	// Allows the ability to use a mouse in the game
	if (!al_install_mouse())
	{
		printf("Error loading mouse addon!\n");
		exit(1);
	}
}

