#include "ConfirmPauseMenuCommand.h"

#include "State.h"
#include "ConfirmMenu.h"
#include "Globals.h"
#include "InputHandler.h"

ConfirmPauseMenuCommand::~ConfirmPauseMenuCommand()
{
}

void ConfirmPauseMenuCommand::execute(State &state)
{
	if (state.GetConfirmMenu()->GetChoice() == true)
	{
		state.SetState(COMBATPHASE);
		state.GetInputHandler()->DeleteCurrentHandler();
		state.GetInputHandler()->InitCombatPhase();
		state.GetAudioManager().PlayCursorSelect();
	}
	else
	{
		state.SetState(MAINMENU);
		state.DeleteGamePlay();
		state.InitializeMainMenu();
		state.GetAudioManager().StopCombatSong();
		state.GetAudioManager().PlayAudioMenuSong();
		state.GetAudioManager().PlayCursorSelect();
	}
}