#pragma once
#include "Command.h"

class ConfirmDoneBuildingCommand :
	public Command
{
public:
	~ConfirmDoneBuildingCommand();

	virtual void execute() {};
	virtual void execute(State &state);
};

