#pragma once

#include "Command.h"

class ExitTitleCommand : public Command
{
public:
	~ExitTitleCommand();

	virtual void execute() {};

	virtual void execute(State &state);
};

