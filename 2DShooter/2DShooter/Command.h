#pragma once

class State;

class Command
{
public:

	virtual ~Command() {}
	virtual void execute() = 0;
	virtual void execute(State& state) = 0;
};

