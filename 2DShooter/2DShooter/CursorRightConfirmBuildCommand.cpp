#include "CursorRightConfirmBuildCommand.h"

#include "State.h"
#include "ConfirmMenu.h"

CursorRightConfirmBuildCommand::~CursorRightConfirmBuildCommand()
{

}

void CursorRightConfirmBuildCommand::execute(State &state)
{
	state.GetConfirmMenu()->MoveCursorRight();
	state.GetAudioManager().PlayCursorMove();
}
