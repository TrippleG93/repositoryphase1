#pragma once

#include "SpriteAnimation.h"
#include "Render.h"
#include "Audio.h"
#include "Font.h"
#include "State.h"

class Unit;
class BulletManager;
class BuildingManager;
class Command;
class State;

class UnitManager
{
public:
	UnitManager();
	~UnitManager();

	/*
		Init
		Initialize everything needed for the unit manager class
	*/
	void Init(BuildingManager &building_manager);

	/*
		RenderUnits
		Draws the units onto the screen
	*/
	void RenderUnits(float window_scale_x, float window_scale_y);

	/*
		CreateUnit
		Creates a unit based on the building type
	*/
	void CeateUnit(int building_type, int player);

	/*
		CreateSupportUnit
		Creates the support unit if it is ready
		Only for player 1 units
	*/
	void CreateSupportUnit1(int building_type, int player);

	/*
		CreateSupportUnit2
		Creates the support unit if it is ready
		Only for player 2 units
	*/
	void CreateSupportUnit2(int building_type, int player);

	/*
		UpdateUnitManager
		Updates the posititons and animations of all the ships
	*/
	void UpdateUnitManager(BulletManager &bullet_manager, State &state);

	/*
		GetPlayer1Unit
		Returns the class of the unit belonging to player1
	*/
	Unit* GetPlayer1Unit();

	/*
		GetPlayer2Unit
		Returns the class of the unit belonging to player1
	*/
	Unit* GetPlayer2Unit();

	void DrawSupportNumber(float window_scale_x, float window_scale_y);

	void DrawReadyLaunch(float window_scale_x, float window_scale_y, 
		float position_x, float position_y);

	void DrawNotLaunchReady(float window_scale_x, float window_scale_y,
		float position_x, float position_y);

	//bool DoesSupportUnitExist(int player, int unit_type);
	bool IsSupportUnitAlive(int plaer, int unit_type);
	bool IsSupportUnitReadyToLaunch(int player, int unit_type);

	Unit* GetPlayer1SupportUnit(int unit_type);
	Unit* Getplayer2SupportUnit(int unit_type);

	size_t GetBulletCap() { return bullet_cap; }

private: 

	int spacing_unit = 50;						// Distance used between objects. Use /2, /4, etc. for smaller distances

	int bullet_cap;								// Max # of bullets that may be fired at one time
	
	int su4_damage;								// Amount of damage Support Unit 4 takes each frame

	Unit* player1_unit;
	Unit* player2_unit;

	std::map <int, Unit*> player_1_support_units;
	std::map <int, Unit*> player_2_support_units;

	std::map <int, bool> player_1_support_unit_alive;
	std::map <int, bool> player_1_support_unit_exist;

	std::map<int, bool> player_2_support_unit_alive;
	std::map<int, bool> player_2_support_unit_exist;

	std::map<int, int> player_1_support_unit_count;
	std::map<int, int> player_1_support_unit_ready;

	std::map<int, int> player_2_support_unit_count;
	std::map<int, int> player_2_support_unit_ready;

	Font emulogic_font;							// Holds the font and the size

	Render text_image_1;						// Holds the text image "ready"
	Render text_image_2;						// Holds the text image "not ready"

	Render support_unit_number_image_1W;
	Render support_unit_number_image_2W;
	Render support_unit_number_image_3W;
	Render support_unit_number_image_4W;

	int su4_fire_count_p1;
	int su4_fire_delay_p1 =20;
	int su4_fire_count_p2;
	int su4_fire_delay_p2 = 20;
};