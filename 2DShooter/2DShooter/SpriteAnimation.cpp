#include "SpriteAnimation.h"

SpriteAnimation::SpriteAnimation()
{

}

SpriteAnimation::~SpriteAnimation()
{
	Destroy();
}

void SpriteAnimation::AnimationLoopTrue()
{
	animation_loop = true;
}

void SpriteAnimation::AnimationLoopFalse()
{
	animation_loop = false;
}

void SpriteAnimation::CompleteAnimationTrue()
{
	complete_animation = true;
}

void SpriteAnimation::CompleteAnimatinFalse()
{
	complete_animation = false;
}

void SpriteAnimation::Update()
{
	if (animation_loop == true)
	{
		frame_count += 1;																// Increment to the next frame in the animation

		if (frame_count >= frame_delay)													// IF we stayed at that frame longer or equal to the delay to switch frame
		{
			if (current_frame < max_frame - 1)											// Make sure have not hit the beginning frame yet
			{
				current_frame += 1;														// Move on to the next frame
				frame_count = 0;														// The count on the frame goes to zero
			}
			else
			{
				complete_animation = true;												// The animation has been completed
				current_frame = 0;														// Reset back to beginning frame
				frame_count = 0;														// The count on that frame goes to zero
			}
		}
	}
	else if (animation_loop == false && complete_animation == false)
	{
		frame_count += 1;																// Increment to the next frame in the animation

		if (frame_count >= frame_delay)													// IF we stayed at that frame longer or equal to the delay to switch frame
		{
			if (current_frame < max_frame - 1)											// Make sure have not hit the beginning frame yet
			{
				current_frame += 1;														// Move on to the next frame
				frame_count = 0;														// The count on the frame goes to zero
			}
			else
			{
				complete_animation = true;												// The animation has been completed
				current_frame = 0;														// Reset back to beginning frame
				frame_count = 0;														// The count on that frame goes to zero
			}
		}
	}
}

void SpriteAnimation::AddIndividualFrame(int source_x, int source_y, int width, int height)
{
	Sprite sprite(source_x, source_y, width, height);
	animations[temp_max_frame] = sprite;

	temp_max_frame += 1;															// Adds 1 to the max frame count for the soon to be animation set
}

void SpriteAnimation::AddIndividualFrame(int source_x, int source_y, int width, int height, int pos_x, int pos_y)
{
	Sprite sprite(source_x, source_y, width, height, pos_x, pos_y);
	animations[temp_max_frame] = sprite;

	temp_max_frame += 1;															// Adds 1 to the max frame count for the soon to be animation set
}

void SpriteAnimation::AddIndividualFrame(int source_x, int source_y, int width, int height, int pos_x, int pos_y, float scale_x, float scale_y)
{
	Sprite sprite(source_x, source_y, width, height, pos_x, pos_y, scale_x, scale_y);
	animations[temp_max_frame] = sprite;

	temp_max_frame += 1;															// Adds 1 to the max frame count for the soon to be animation set
}

void SpriteAnimation::CombineFrames(int frame_delays, bool loop)
{
	animation_loop = loop;
	complete_animation = false;
	max_frame = temp_max_frame;
	temp_max_frame = 0;
	frame_delay = frame_delays;
	current_frame = 0;
	frame_count = 0;
}

void SpriteAnimation::Destroy()
{
	animations.clear();
	max_frame = 0;
	temp_max_frame = 0;
	frame_delay = 0;
	current_frame = 0;
	frame_count = 0;
}

Sprite & SpriteAnimation::GetCurrentFrame()
{
	return animations[current_frame];
}