#include "CursorRightBuildMenuCommand.h"

#include "State.h"
#include "BuildMenu.h"

CursorRightBuildMenuCommand::~CursorRightBuildMenuCommand()
{
}

void CursorRightBuildMenuCommand::execute(State &state)
{
	state.GetBuildMenu()->MoveCursorRight();
	state.GetAudioManager().PlayCursorMove();
}