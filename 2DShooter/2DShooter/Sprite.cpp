#include "Sprite.h"

Sprite::Sprite()
{

}

Sprite::Sprite(int source_x, int source_y, int width, int height) :
	source_x(source_x),
	source_y(source_y),
	width(width),
	height(height),
	x_position(0),
	y_position(0),
	scale_x(1),
	scale_y(1)
{
}

Sprite::Sprite(int source_x, int source_y, int width, int height, int x_pos, int y_pos) :
	source_x(source_x),
	source_y(source_y),
	width(width),
	height(height),
	x_position(x_pos),
	y_position(y_pos),
	scale_x(1),
	scale_y(1)
{
}

Sprite::Sprite(int source_x, int source_y, int width, int height, int x_pos, int y_pos, float scale_x, float scale_y) :
	source_x(source_x),
	source_y(source_y),
	width(width),
	height(height),
	x_position(x_pos),
	y_position(y_pos),
	scale_x(scale_x),
	scale_y(scale_y)
{

}

Sprite::~Sprite()
{
}

void Sprite::SetXPosition(int value)
{
	x_position = value;
}

void Sprite::SetYPosition(int value)
{
	y_position = value;
}

void Sprite::SetScaleX(float value)
{
	scale_x = value;
}

void Sprite::SetScaleY(float value)
{
	scale_y = value;
}