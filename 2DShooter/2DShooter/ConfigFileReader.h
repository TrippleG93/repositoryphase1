#pragma once

#include <allegro5\allegro.h>
#include <string>

class ConfigFileReader
{
public:
	ConfigFileReader();
	ConfigFileReader(std::string file_path);
	~ConfigFileReader();

	/*
	GetConfigValue
	Returns the value from the configuration file
	based on the section and the key
	This version returns characters grouped together
	*/
	const char *GetConfigValueCharacters(std::string section, std::string key);

	/*
	GetConfigValueIntegers
	Returns the value from the configuration value
	based on the section and the key
	This version returns an integer
	*/
	int GetConfigValueIntegers(std::string section, std::string key);

	/*
	GetConfigValueIntegers
	Returns the value from the configuration value
	based on the section and the key
	This version returns an float
	*/
	double GetConfigValueDouble(std::string section, std::string key);

	/*
	LoadConfigFile
	Takes in a string as the file path and
	loads the configuration file
	*/
	void LoadConfigFile();

private:

	ALLEGRO_CONFIG * cfg;									// Hold the configuration file 
	std::string file_path;									// Holds the file path of the file

};

