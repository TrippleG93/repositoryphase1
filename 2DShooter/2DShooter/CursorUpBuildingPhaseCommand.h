#pragma once

#include "Command.h"

class CursorUpBuildingPhaseCommand : public Command
{
public:

	~CursorUpBuildingPhaseCommand();

	virtual void execute() {};
	virtual void execute(State& state);
};

