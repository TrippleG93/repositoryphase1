#include "CursorDownBuildMenuCommand.h"

#include "State.h"
#include "BuildMenu.h"
#include "BuildPhase.h"
#include "BuildingManager.h"

CursorDownBuildMenuCommand::~CursorDownBuildMenuCommand()
{
}


void CursorDownBuildMenuCommand::execute(State &state)
{
	if (state.GetBuildPhase()->GetCurrentBuildPlayer() == 1)
	{
		if (state.GetBuildPhase()->GetBuildingManager()->DoesMainShipBuildingExist(1) == false)
		{

		}
		else
		{
			state.GetBuildMenu()->MoveCursorDown();
			state.GetAudioManager().PlayCursorMove();
		}
	}
	else if (state.GetBuildPhase()->GetBuildingManager()->DoesMainShipBuildingExist(2) == true)
	{
		state.GetBuildMenu()->MoveCursorDown();
		state.GetAudioManager().PlayCursorMove();
	}

}