#pragma once

#include "Command.h"

class MoveUnitLeftCommand : public Command
{
public:
	~MoveUnitLeftCommand();

	virtual void execute() {};
	virtual void execute(State &state);
};

