#pragma once

#include "SpriteAnimation.h"
#include "Render.h"
#include "Audio.h"
#include "Font.h"
#include "ConfigFileReader.h"

class TitleScreen
{
public:
	TitleScreen();
	~TitleScreen();

	/* SetUpTitleScreen
	* Loads everything needed for the start menu
	*/
	void SetUpTitleScreen();

	/* RenderTitleScreen
	* Draws everything for the title screen
	*/
	void RenderTitleScreen(float scale_x, float scale_y);

	/* UpdateTitleScreen
	* Updates animations and everything else
	*/
	void UpdateTitleScreen();


private:

	SpriteAnimation vehicle_animation;
	
	SpriteAnimation button_start_animation;
	Render button_start_image;


	Render start_text;
	int start_text_x;										// Holds the x position of the title start image
	int start_text_y;										// Holds the y position of the title start image

	Render title_text;
	int title_text_x;										// Holds the x position of the title text image
	int title_text_y;										// Holds the y position of the title text image

	Render vehicle_image;
	Render background_image;

	Font emulogic_font24;
	Font emulogic_font72;
};
