#include "CreateSupportUnit4P2.h"

#include "State.h"
#include "CombatPhase.h"
#include "UnitManager.h"


CreateSupportUnit4P2::~CreateSupportUnit4P2()
{
	
}

void CreateSupportUnit4P2::execute(State &state)
{
	state.GetCombatPhase()->GetUnitManager()->CreateSupportUnit2(8, 2);
}