#include "ConfirmMenu.h"

#include "ConfigFileReader.h"
#include "Globals.h"

ConfirmMenu::ConfirmMenu()
{
}


ConfirmMenu::~ConfirmMenu()
{
}

void ConfirmMenu::SetUpConfirmBuildPhase()
{
	ConfigFileReader file("Content/Text Files/Menus/ConfirmBuildMenu.cfg");

	choice = true;

	cursor_left = file.GetConfigValueIntegers("cursor", "left");
	current_position = cursor_left;
	cursor_right = file.GetConfigValueIntegers("cursor", "right");

	cursor_image.LoadBitmap("Content/Images/Others/Cursor.png");
	cursor_animation.AddIndividualFrame(file.GetConfigValueIntegers("cursor 1", "source x"),
		file.GetConfigValueIntegers("cursor 1", "source y"),
		file.GetConfigValueIntegers("cursor 1", "width"),
		file.GetConfigValueIntegers("cursor 1", "height"),
		file.GetConfigValueIntegers("cursor", "position x"),
		file.GetConfigValueIntegers("cursor", "position y"),
		file.GetConfigValueIntegers("cursor 1", "scale x"),
		file.GetConfigValueIntegers("cursor 1", "scale y"));
	cursor_animation.AddIndividualFrame(file.GetConfigValueIntegers("cursor 2", "source x"),
		file.GetConfigValueIntegers("cursor 2", "source y"),
		file.GetConfigValueIntegers("cursor 2", "width"),
		file.GetConfigValueIntegers("cursor 2", "height"),
		file.GetConfigValueIntegers("cursor", "position x"),
		file.GetConfigValueIntegers("cursor", "position y"),
		file.GetConfigValueIntegers("cursor 2", "scale x"),
		file.GetConfigValueIntegers("cursor 2", "scale y"));
	cursor_animation.AddIndividualFrame(file.GetConfigValueIntegers("cursor 3", "source x"),
		file.GetConfigValueIntegers("cursor 3", "source y"),
		file.GetConfigValueIntegers("cursor 3", "width"),
		file.GetConfigValueIntegers("cursor 3", "height"),
		file.GetConfigValueIntegers("cursor", "position x"),
		file.GetConfigValueIntegers("cursor", "position y"),
		file.GetConfigValueIntegers("cursor 3", "scale x"),
		file.GetConfigValueIntegers("cursor 3", "scale y"));
	cursor_animation.AddIndividualFrame(file.GetConfigValueIntegers("cursor 4", "source x"),
		file.GetConfigValueIntegers("cursor 4", "source y"),
		file.GetConfigValueIntegers("cursor 4", "width"),
		file.GetConfigValueIntegers("cursor 4", "height"),
		file.GetConfigValueIntegers("cursor", "position x"),
		file.GetConfigValueIntegers("cursor", "position y"),
		file.GetConfigValueIntegers("cursor 4", "scale x"),
		file.GetConfigValueIntegers("cursor 4", "scale y"));
	cursor_animation.AddIndividualFrame(file.GetConfigValueIntegers("cursor 5", "source x"),
		file.GetConfigValueIntegers("cursor 5", "source y"),
		file.GetConfigValueIntegers("cursor 5", "width"),
		file.GetConfigValueIntegers("cursor 5", "height"),
		file.GetConfigValueIntegers("cursor", "position x"),
		file.GetConfigValueIntegers("cursor", "position y"),
		file.GetConfigValueIntegers("cursor 5", "scale x"),
		file.GetConfigValueIntegers("cursor 5", "scale y"));
	cursor_animation.AddIndividualFrame(file.GetConfigValueIntegers("cursor 6", "source x"),
		file.GetConfigValueIntegers("cursor 6", "source y"),
		file.GetConfigValueIntegers("cursor 6", "width"),
		file.GetConfigValueIntegers("cursor 6", "height"),
		file.GetConfigValueIntegers("cursor", "position x"),
		file.GetConfigValueIntegers("cursor", "position y"),
		file.GetConfigValueIntegers("cursor 6", "scale x"),
		file.GetConfigValueIntegers("cursor 6", "scale y"));
	cursor_animation.AddIndividualFrame(file.GetConfigValueIntegers("cursor 7", "source x"),
		file.GetConfigValueIntegers("cursor 7", "source y"),
		file.GetConfigValueIntegers("cursor 7", "width"),
		file.GetConfigValueIntegers("cursor 7", "height"),
		file.GetConfigValueIntegers("cursor", "position x"),
		file.GetConfigValueIntegers("cursor", "position y"),
		file.GetConfigValueIntegers("cursor 7", "scale x"),
		file.GetConfigValueIntegers("cursor 7", "scale y"));
	cursor_animation.AddIndividualFrame(file.GetConfigValueIntegers("cursor 8", "source x"),
		file.GetConfigValueIntegers("cursor 8", "source y"),
		file.GetConfigValueIntegers("cursor 8", "width"),
		file.GetConfigValueIntegers("cursor 8", "height"),
		file.GetConfigValueIntegers("cursor", "position x"),
		file.GetConfigValueIntegers("cursor", "position y"),
		file.GetConfigValueIntegers("cursor 8", "scale x"),
		file.GetConfigValueIntegers("cursor 8", "scale y"));

	cursor_animation.CombineFrames(file.GetConfigValueIntegers("cursor", "frame"), true);

	background_image.LoadBitmap("Content/Images/Others/ConfirmBackground.png");
	background_image_x = file.GetConfigValueIntegers("background image", "position x");
	background_image_y = file.GetConfigValueIntegers("background image", "position y");

	emulogic_font24.CreateFont("Content/Fonts/Gravedigger.ttf", file.GetConfigValueIntegers("font", "size"));
	emulogic_font20.CreateFont("Content/Fonts/Gravedigger.ttf", file.GetConfigValueIntegers("font2", "size"));

	yes_text_image.CreateTextRender(emulogic_font24.ObtainFont(),
		al_map_rgb(file.GetConfigValueIntegers("yes text", "text r"),
			file.GetConfigValueIntegers("yes text", "text g"),
			file.GetConfigValueIntegers("yes text", "text b")), "Yes");
	yes_position_x = file.GetConfigValueIntegers("yes text", "position x");
	yes_position_y = file.GetConfigValueIntegers("yes text", "position y");

	no_text_image.CreateTextRender(emulogic_font24.ObtainFont(),
		al_map_rgb(file.GetConfigValueIntegers("no text", "text r"),
			file.GetConfigValueIntegers("no text", "text g"),
			file.GetConfigValueIntegers("no text", "text b")), "No");
	no_position_x = file.GetConfigValueIntegers("no text", "position x");
	no_position_y = file.GetConfigValueIntegers("no text", "position y");

	title_text_image.CreateTextRender(emulogic_font20.ObtainFont(),
		al_map_rgb(file.GetConfigValueIntegers("title text", "text r"),
			file.GetConfigValueIntegers("title text", "text g"),
			file.GetConfigValueIntegers("title text", "text b")), "READY FOR COMBAT?");
	title_position_x = file.GetConfigValueIntegers("title text", "position x");
	title_position_y = file.GetConfigValueIntegers("title text", "position y");

	error_text_image.CreateTextRender(emulogic_font20.ObtainFont(),
		al_map_rgb(file.GetConfigValueIntegers("error text", "text r"),
			file.GetConfigValueIntegers("error text", "text g"),
			file.GetConfigValueIntegers("error text", "text b")), "CHOOSE A MAIN SHIP");
	error_position_x = file.GetConfigValueIntegers("error text", "position x");
	error_position_y = file.GetConfigValueIntegers("error text", "position y");

}

void ConfirmMenu::SetUpQuitMenu()
{
	ConfigFileReader file("Content/Text Files/Menus/ConfirmBuildMenu.cfg");

	choice = true;

	cursor_image.LoadBitmap("Content/Images/Others/Cursor.png");
	cursor_animation.AddIndividualFrame(file.GetConfigValueIntegers("cursor 1", "source x"),
		file.GetConfigValueIntegers("cursor 1", "source y"),
		file.GetConfigValueIntegers("cursor 1", "width"),
		file.GetConfigValueIntegers("cursor 1", "height"),
		file.GetConfigValueIntegers("cursor pause", "position x"),
		file.GetConfigValueIntegers("cursor pause", "position y"),
		file.GetConfigValueIntegers("cursor 1", "scale x"),
		file.GetConfigValueIntegers("cursor 1", "scale y"));
	cursor_animation.AddIndividualFrame(file.GetConfigValueIntegers("cursor 2", "source x"),
		file.GetConfigValueIntegers("cursor 2", "source y"),
		file.GetConfigValueIntegers("cursor 2", "width"),
		file.GetConfigValueIntegers("cursor 2", "height"),
		file.GetConfigValueIntegers("cursor pause", "position x"),
		file.GetConfigValueIntegers("cursor pause", "position y"),
		file.GetConfigValueIntegers("cursor 2", "scale x"),
		file.GetConfigValueIntegers("cursor 2", "scale y"));
	cursor_animation.AddIndividualFrame(file.GetConfigValueIntegers("cursor 3", "source x"),
		file.GetConfigValueIntegers("cursor 3", "source y"),
		file.GetConfigValueIntegers("cursor 3", "width"),
		file.GetConfigValueIntegers("cursor 3", "height"),
		file.GetConfigValueIntegers("cursor pause", "position x"),
		file.GetConfigValueIntegers("cursor pause", "position y"),
		file.GetConfigValueIntegers("cursor 3", "scale x"),
		file.GetConfigValueIntegers("cursor 3", "scale y"));
	cursor_animation.AddIndividualFrame(file.GetConfigValueIntegers("cursor 4", "source x"),
		file.GetConfigValueIntegers("cursor 4", "source y"),
		file.GetConfigValueIntegers("cursor 4", "width"),
		file.GetConfigValueIntegers("cursor 4", "height"),
		file.GetConfigValueIntegers("cursor pause", "position x"),
		file.GetConfigValueIntegers("cursor pause", "position y"),
		file.GetConfigValueIntegers("cursor 4", "scale x"),
		file.GetConfigValueIntegers("cursor 4", "scale y"));
	cursor_animation.AddIndividualFrame(file.GetConfigValueIntegers("cursor 5", "source x"),
		file.GetConfigValueIntegers("cursor 5", "source y"),
		file.GetConfigValueIntegers("cursor 5", "width"),
		file.GetConfigValueIntegers("cursor 5", "height"),
		file.GetConfigValueIntegers("cursor pause", "position x"),
		file.GetConfigValueIntegers("cursor pause", "position y"),
		file.GetConfigValueIntegers("cursor 5", "scale x"),
		file.GetConfigValueIntegers("cursor 5", "scale y"));
	cursor_animation.AddIndividualFrame(file.GetConfigValueIntegers("cursor 6", "source x"),
		file.GetConfigValueIntegers("cursor 6", "source y"),
		file.GetConfigValueIntegers("cursor 6", "width"),
		file.GetConfigValueIntegers("cursor 6", "height"),
		file.GetConfigValueIntegers("cursor pause", "position x"),
		file.GetConfigValueIntegers("cursor pause", "position y"),
		file.GetConfigValueIntegers("cursor 6", "scale x"),
		file.GetConfigValueIntegers("cursor 6", "scale y"));
	cursor_animation.AddIndividualFrame(file.GetConfigValueIntegers("cursor 7", "source x"),
		file.GetConfigValueIntegers("cursor 7", "source y"),
		file.GetConfigValueIntegers("cursor 7", "width"),
		file.GetConfigValueIntegers("cursor 7", "height"),
		file.GetConfigValueIntegers("cursor pause", "position x"),
		file.GetConfigValueIntegers("cursor pause", "position y"),
		file.GetConfigValueIntegers("cursor 7", "scale x"),
		file.GetConfigValueIntegers("cursor 7", "scale y"));
	cursor_animation.AddIndividualFrame(file.GetConfigValueIntegers("cursor 8", "source x"),
		file.GetConfigValueIntegers("cursor 8", "source y"),
		file.GetConfigValueIntegers("cursor 8", "width"),
		file.GetConfigValueIntegers("cursor 8", "height"),
		file.GetConfigValueIntegers("cursor pause", "position x"),
		file.GetConfigValueIntegers("cursor pause", "position y"),
		file.GetConfigValueIntegers("cursor 8", "scale x"),
		file.GetConfigValueIntegers("cursor 8", "scale y"));

	cursor_animation.CombineFrames(file.GetConfigValueIntegers("cursor", "frame"), true);

	cursor_left = file.GetConfigValueIntegers("cursor pause", "left");
	current_position = cursor_left;
	cursor_right = file.GetConfigValueIntegers("cursor pause", "right");

	work.LoadBitmap("Content/Images/Others/Pause.png");
	background_image_x = 0;
	background_image_y = 0;

	emulogic_font24.CreateFont("Content/Fonts/Gravedigger.ttf", file.GetConfigValueIntegers("font", "size"));

	yes_text_image.CreateTextRender(emulogic_font24.ObtainFont(),
		al_map_rgb(file.GetConfigValueIntegers("yes text", "text r"),
			file.GetConfigValueIntegers("yes text", "text g"),
			file.GetConfigValueIntegers("yes text", "text b")), "Resume Game");
	yes_position_x = file.GetConfigValueIntegers("Resume Game", "position x");
	yes_position_y = file.GetConfigValueIntegers("Resume Game", "position y");

	no_text_image.CreateTextRender(emulogic_font24.ObtainFont(),
		al_map_rgb(file.GetConfigValueIntegers("no text", "text r"),
			file.GetConfigValueIntegers("no text", "text g"),
			file.GetConfigValueIntegers("no text", "text b")), "Quit Game");
	no_position_x = file.GetConfigValueIntegers("Quit Game", "position x");
	no_position_y = file.GetConfigValueIntegers("Quit Game", "position y");

	title_text_image.CreateTextRender(emulogic_font24.ObtainFont(),
		al_map_rgb(file.GetConfigValueIntegers("title text", "text r"),
			file.GetConfigValueIntegers("title text", "text g"),
			file.GetConfigValueIntegers("title text", "text b")), "Game Paused");
	title_position_x = file.GetConfigValueIntegers("Game Paused", "position x");
	title_position_y = file.GetConfigValueIntegers("Game Paused", "position y");

}

void ConfirmMenu::UpdateConfirmMenu()
{
	cursor_animation.Update();
	cursor_animation.GetCurrentFrame().SetXPosition(current_position);
}

void ConfirmMenu::UpdateQuitMenu()
{
	cursor_animation.Update();
	cursor_animation.GetCurrentFrame().SetXPosition(current_position);
}

void ConfirmMenu::RenderConfirmMenu(float window_scale_x, float window_scale_y)
{
	ALLEGRO_COLOR tinted = { 1.0f, 1.0f, 1.0f, 0.4f };

	background_image.DrawTintedScaledImage(tinted, 0, 0,
		background_image.GetBitmapWidth(),
		background_image.GetBitmapHeight(),
		0 * window_scale_x,
		0 * window_scale_y,
		globals::GAME_WIDTH * window_scale_x,
		2* globals::GAME_HEIGHT * window_scale_y,
		NULL); 

	background_image.DrawScaledImage(0, 0,
		(float)background_image.GetBitmapWidth(),
		(float)background_image.GetBitmapHeight(),
		background_image_x * window_scale_x,
		background_image_y * window_scale_y,
		(float)background_image.GetBitmapWidth() * window_scale_x,
		(float)background_image.GetBitmapHeight() * window_scale_y,
		NULL);

	if (true/*DoesMainShipBuildingExist()*/) { // TODO: Link this function 
		yes_text_image.DrawScaledImage(0, 0,
			(float)yes_text_image.GetBitmapWidth(),
			(float)yes_text_image.GetBitmapHeight(),
			yes_position_x * window_scale_x,
			yes_position_y * window_scale_y,
			(float)yes_text_image.GetBitmapWidth() * window_scale_x,
			(float)yes_text_image.GetBitmapHeight() * window_scale_y,
			NULL);

		no_text_image.DrawScaledImage(0, 0, 
			(float)no_text_image.GetBitmapWidth(),
			(float)no_text_image.GetBitmapHeight(),
			no_position_x * window_scale_x,
			no_position_y * window_scale_y,
			(float)no_text_image.GetBitmapWidth() * window_scale_x,
			(float)no_text_image.GetBitmapHeight() * window_scale_y,
			NULL);

		title_text_image.DrawScaledImage(0, 0, 
			(float)title_text_image.GetBitmapWidth(),
			(float)title_text_image.GetBitmapHeight(),
			title_position_x * window_scale_x,
			title_position_y * window_scale_y,
			(float)title_text_image.GetBitmapWidth() * window_scale_x,
			(float)title_text_image.GetBitmapHeight() * window_scale_y,
			NULL);

		cursor_image.DrawScaledImage((float)cursor_animation.GetCurrentFrame().GetSourceX(),
			(float)cursor_animation.GetCurrentFrame().GetSourceY(),
			(float)cursor_animation.GetCurrentFrame().GetWidth(),
			(float)cursor_animation.GetCurrentFrame().GetHeight(),
			cursor_animation.GetCurrentFrame().GetPositionX() * window_scale_x,
			cursor_animation.GetCurrentFrame().GetPositionY() * window_scale_y,
			(float)cursor_animation.GetCurrentFrame().GetWidth() * window_scale_x * cursor_animation.GetCurrentFrame().GetScaleX(),
			(float)cursor_animation.GetCurrentFrame().GetHeight() * window_scale_y * cursor_animation.GetCurrentFrame().GetScaleY(),
			NULL);
	}
	else {
		error_text_image.DrawScaledImage(0, 0,
			(float)error_text_image.GetBitmapWidth(),
			(float)error_text_image.GetBitmapHeight(),
			error_position_x * window_scale_x,
			error_position_y * window_scale_y,
			(float)error_text_image.GetBitmapWidth() * window_scale_x,
			(float)error_text_image.GetBitmapHeight() * window_scale_y,
			NULL);
	}
	
}

void ConfirmMenu::RenderQuitMenu(float window_scale_x, float window_scale_y)
{

	work.DrawScaledImage(0, 0,
		(float)work.GetBitmapWidth(),
		(float)work.GetBitmapHeight(),
		00,
		0,
		(float)work.GetBitmapWidth() * window_scale_x,
		(float)work.GetBitmapHeight() * window_scale_y,
		NULL);

	yes_text_image.DrawScaledImage(0, 0,
		(float)yes_text_image.GetBitmapWidth(),
		(float)yes_text_image.GetBitmapHeight(),
		yes_position_x * window_scale_x,
		yes_position_y * window_scale_y,
		(float)yes_text_image.GetBitmapWidth() * window_scale_x,
		(float)yes_text_image.GetBitmapHeight() * window_scale_y,
		NULL);

	no_text_image.DrawScaledImage(0, 0,
		(float)no_text_image.GetBitmapWidth(),
		(float)no_text_image.GetBitmapHeight(),
		no_position_x * window_scale_x,
		no_position_y * window_scale_y,
		(float)no_text_image.GetBitmapWidth() * window_scale_x,
		(float)no_text_image.GetBitmapHeight() * window_scale_y,
		NULL);

	title_text_image.DrawScaledImage(0, 0,
		(float)title_text_image.GetBitmapWidth(),
		(float)title_text_image.GetBitmapHeight(),
		title_position_x * window_scale_x,
		title_position_y * window_scale_y,
		(float)title_text_image.GetBitmapWidth() * window_scale_x,
		(float)title_text_image.GetBitmapHeight() * window_scale_y,
		NULL);

	cursor_image.DrawScaledImage((float)cursor_animation.GetCurrentFrame().GetSourceX(),
		(float)cursor_animation.GetCurrentFrame().GetSourceY(),
		(float)cursor_animation.GetCurrentFrame().GetWidth(),
		(float)cursor_animation.GetCurrentFrame().GetHeight(),
		cursor_animation.GetCurrentFrame().GetPositionX() * window_scale_x,
		cursor_animation.GetCurrentFrame().GetPositionY() * window_scale_y,
		(float)cursor_animation.GetCurrentFrame().GetWidth() * window_scale_x * cursor_animation.GetCurrentFrame().GetScaleX(),
		(float)cursor_animation.GetCurrentFrame().GetHeight() * window_scale_y * cursor_animation.GetCurrentFrame().GetScaleY(),
		NULL);
}

void ConfirmMenu::MoveCursorLeft()
{
	if (choice == false)
	{
		choice = true;
		current_position = cursor_left;
	}
}

void ConfirmMenu::MoveCursorRight()
{
	if (choice == true)
	{
		choice = false;
		current_position = cursor_right;
	}
}

bool ConfirmMenu::GetChoice()
{
	return choice;
}