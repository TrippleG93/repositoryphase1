#include "TitleScreen.h"

#include "Globals.h"


TitleScreen::TitleScreen()
{
	SetUpTitleScreen();
}


TitleScreen::~TitleScreen()
{
	//	title_intro_song.StopSong();
}

void TitleScreen::SetUpTitleScreen()
{
	ConfigFileReader file("Content/Text Files/Menus/TitleScreen.cfg");

	// Background
	background_image.LoadBitmap("Content/Images/Combat Phase Backgrounds/PC Computer - Space Pirates and Zombies - Backgrounds 22/Building_Background.png");

	// Vehicle on title screen
	vehicle_image.LoadBitmap("Content/Images/Ships/destroyer.png");
	vehicle_animation.Destroy();
	vehicle_animation.AddIndividualFrame(file.GetConfigValueIntegers("Vehicle", "source x"),
		file.GetConfigValueIntegers("Vehicle", "source y"),
		file.GetConfigValueIntegers("Vehicle", "width"),
		file.GetConfigValueIntegers("Vehicle", "height"),
		file.GetConfigValueIntegers("Vehicle", "position x"),
		file.GetConfigValueIntegers("Vehicle", "position y"),
		file.GetConfigValueDouble("Vehicle", "scale x"),
		file.GetConfigValueDouble("Vehicle", "scale y"));
	vehicle_animation.CombineFrames(file.GetConfigValueIntegers("Vehicle", "frames"), true);

	// "PRESS ENTER TO START" text
	button_start_image.LoadBitmap("Content/Images/Others/xbox_buttons.png");
	button_start_animation.AddIndividualFrame(file.GetConfigValueIntegers("Button Start", "source x"),
		file.GetConfigValueIntegers("Button Start", "source y"),
		file.GetConfigValueIntegers("Button Start", "width"),
		file.GetConfigValueIntegers("Button Start", "height"),
		file.GetConfigValueIntegers("Button Start", "position x"),
		file.GetConfigValueIntegers("Button Start", "position y"),
		file.GetConfigValueDouble("Button Start", "scale x"),
		file.GetConfigValueDouble("Button Start", "scale y"));
	button_start_animation.CombineFrames(file.GetConfigValueIntegers("Button Start", "frames"), true);

	emulogic_font24.CreateFont("Content/Fonts/Gravedigger.ttf", file.GetConfigValueIntegers("Start Text","text size"));
	start_text.CreateTextRender(emulogic_font24.ObtainFont(),
		al_map_rgb(file.GetConfigValueIntegers("Start Text", "text r"),
			file.GetConfigValueIntegers("Start Text", "text g"),
			file.GetConfigValueIntegers("Start Text", "text r")),
		"PRESS   ENTER /           TO  START");
	start_text_x = file.GetConfigValueIntegers("Start Text", "position x");
	start_text_y = file.GetConfigValueIntegers("Start Text", "position y");


	// "2D SHOOTER" text
	emulogic_font72.CreateFont("Content/Fonts/Gravedigger.ttf", file.GetConfigValueIntegers("Title Text", "text size"));
	title_text.CreateTextRender(emulogic_font72.ObtainFont(),
		al_map_rgb(file.GetConfigValueIntegers("Title Text", "text r"),
			file.GetConfigValueIntegers("Title Text", "text g"),
			file.GetConfigValueIntegers("Title Text", "text r")),
		"PHASE  ONE");
	title_text_x = file.GetConfigValueIntegers("Title Text", "position x");
	title_text_y = file.GetConfigValueIntegers("Title Text", "position y");
}

void TitleScreen::UpdateTitleScreen()
{
	vehicle_animation.Update();
}

void TitleScreen::RenderTitleScreen(float scale_x, float scale_y)
{
	// Background
	background_image.DrawScaledImage(0, 0,
		background_image.GetBitmapWidth(),
		background_image.GetBitmapHeight(),
		0 * scale_x,
		0 * scale_y,
		background_image.GetBitmapWidth() * scale_x,
		background_image.GetBitmapHeight() * scale_y,
		NULL);

	// Title Screen Text
	title_text.DrawScaledImage(0, 0,
		title_text.GetBitmapWidth(),
		title_text.GetBitmapHeight(),
		(title_text_x - title_text.GetBitmapWidth() / 2)* scale_x,
		title_text_y * scale_y,
		title_text.GetBitmapWidth() * scale_x,
		title_text.GetBitmapHeight() * scale_y,
		NULL);

	// Ship
	vehicle_image.DrawScaledImage(vehicle_animation.GetCurrentFrame().GetSourceX(),
		vehicle_animation.GetCurrentFrame().GetSourceY(),
		vehicle_animation.GetCurrentFrame().GetWidth(),
		vehicle_animation.GetCurrentFrame().GetHeight(),
		vehicle_animation.GetCurrentFrame().GetPositionX()* scale_x ,
		vehicle_animation.GetCurrentFrame().GetPositionY() * scale_y,
		vehicle_animation.GetCurrentFrame().GetWidth() * scale_x * vehicle_animation.GetCurrentFrame().GetScaleX(),
		vehicle_animation.GetCurrentFrame().GetHeight() * scale_y * vehicle_animation.GetCurrentFrame().GetScaleY(),
		NULL);

	// "Press Enter" text
	start_text.DrawScaledImage(0, 0,
		start_text.GetBitmapWidth(),
		start_text.GetBitmapHeight(),
		(start_text_x - start_text.GetBitmapWidth() / 2) * scale_x,
		start_text_y * scale_y,
		start_text.GetBitmapWidth() * scale_x,
		start_text.GetBitmapHeight() * scale_y,
		NULL);

	// "Press (start) text"
	button_start_image.DrawScaledImage(button_start_animation.GetCurrentFrame().GetSourceX(),
		button_start_animation.GetCurrentFrame().GetSourceY(),
		button_start_animation.GetCurrentFrame().GetWidth(),
		button_start_animation.GetCurrentFrame().GetHeight(),
		button_start_animation.GetCurrentFrame().GetPositionX()* scale_x,
		button_start_animation.GetCurrentFrame().GetPositionY() * scale_y,
		button_start_animation.GetCurrentFrame().GetWidth() * scale_x * button_start_animation.GetCurrentFrame().GetScaleX(),
		button_start_animation.GetCurrentFrame().GetHeight() * scale_y * button_start_animation.GetCurrentFrame().GetScaleY(),
		NULL);
}
