#pragma once
#include "Input.h"

#include <map>
#include <queue>

class Command;								// Forward declaring the command class

class InputHandler : public Input
{
public:
	InputHandler();
	~InputHandler();

	/*
	InitTitleHandler
	Updates all the controlls for the title screen
	*/
	void InitTitleHandler();

	/*
	TitleInputHandler
	Performs the action of inputs for the title class
	*/
	Command* TitleInputHandler();

	/*
	InitMainMenuHandler
	Updates all the controlls for the main menu
	*/
	void InitMainMenuHandler();

	/*
	MainMenuInputHandler()
	Performs the action of inputs for the main menu class
	*/
	Command* MainMenuInputHandler();

	/*
	InitGameplay
	Updates all the controls for the game play class
	*/
	void InitBuildPhase();

	/*
	BuildPhaseInputHandler
	Performs the action of inputs for the Build phase
	*/
	Command* BuildPhaseInputHandler();

	/*
	InitBuildMenu
	Updates all the controls for the build menu class
	*/
	void InitBuildMenu();

	/*
	BuildMenuInputHandler
	Performs the action of inputs for the build phase
	*/
	Command* BuildMenuInputHandler();

	/*
		InitCOnfirmBuildMenu
		Updates all the controls for the confirm menu
	*/
	void InitConfirmBuildMenu();

	/*
		ConfirmBuildMenuInputHandler
		Performs the action of inputs for the confim menu
	*/
	Command* ConfirmBuildMenuInputHandler();

	/*
		InitComabatPhase
		Initilize everything needed for combat phase input
	*/
	void InitCombatPhase();

	/*
		CombatPhaseInputHandler
		Performs the action of inputs for the Combat Phase class
	*/
	Command* CombatPhaseInputHandler();

	/*
	InitComabatPhaseP2
	Initilize everything needed for combat phase input from P2
	*/
	void InitCombatPhaseP2();

	/*
	CombatPhaseInputHandlerP2
	Performs the action of inputs for the Combat Phase class from P2
	*/
	Command* CombatPhaseInputHandlerP2();

	/*
		InitOptionMenu
		Get the controls for the option menu 
	*/
	void InitOptionMenu();

	/*
		OptionMenuInputHandler
		Perfroms the action of inputs for the option menu
	*/
	Command* OptionMemuInputHandler();

	/*
		RunAComputerCommand
		Returns one command that the computer has created
	*/
	Command* RunABuildComputerCommands();
	
	/*
		GetComputerCommands
		Returns the queue that holds the computer Commands
	*/
	std::queue <Command*> &GetComputerCommands();

	/*
		InitMapPhaseCommands
		Initialize the map phase commands
	*/
	void InitMapPhaseCommands();

	/*
		MapPhaseInputHandler
		returns one command that the player has entered
	*/
	Command* MapPhaseInputHandler();

	void InitEndGameCommands();

	Command* EndGameInputHandler();

	void InitPauseMenuCommands();

	Command* PauseMenuInputHandler();

	/*
		DeleteComputerCommand
		Deletes one command the computer has made
	*/
	void DeleteComputerCommand();

	/*
		DeleteCurrentHandler
		Removes all the inputs tied to controls
	*/
	void DeleteCurrentHandler();

private:
	// Build Phase
	int frame_count_build;										// Holds how many frames in build phase			
	int frame_delay_build;										// Holds how many frames must pass till input
	 
	std::map <int, Command*> controller;
	std::map <int, Command*> key_board;
	std::queue <Command*> computer_commands;
};

