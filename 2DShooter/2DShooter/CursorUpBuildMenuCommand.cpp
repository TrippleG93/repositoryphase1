#include "CursorUpBuildMenuCommand.h"

#include "State.h"
#include "BuildMenu.h"

CursorUpBuildMenuCommand::~CursorUpBuildMenuCommand()
{
}

void CursorUpBuildMenuCommand::execute(State &state)
{
	state.GetBuildMenu()->MoveCursorUp();
	state.GetAudioManager().PlayCursorMove();
}