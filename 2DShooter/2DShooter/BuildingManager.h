#pragma once

#include <map>

class Building;										// Forward declaring the building class

class BuildingManager
{
public:
	BuildingManager();
	~BuildingManager();

	/*
		DoesBuildingExist
		returns true if that player owns a 
		building at that spot
	*/
	bool DoesBuildingExist(int player, int location);

	/*
		CreateBuilding
		Creates a building at that location 
		for that specific player
	*/
	void CreateBuilding(int player, int location, int building_type, int row, int column);

	/*
		GetBuilding
		Obtains the building from the specific
		player at that location
	*/
	Building* GetBuilding(int player, int location);

	/*
		UpdateBuilding
		Update the animations of the buildings 
		and everything else
	*/
	void UpdateBuildings(int player);

	/*
		RenderBuildings
		Draws all the buildings onto the screen
	*/
	void RenderBuildings(int player, float window_scale_x, float window_scale_y);

	/*
		DoesMainShipBuildingExist
		Returns true if the player has already purchased a main 
		ship building to prevent another one from happening
	*/
	bool DoesMainShipBuildingExist(int player);

	/*
		DoesBuildingTypeExist
		Returns true if the building has already been 
		created by that player
	*/
	bool DoesBuildingTypeExist(int player, int type);

	/*
		GetMainBuildingType
		Obtain the building that makes the main unit
	*/
	int GetMainBuildingType(int player);

	/*
		DeleteBuildings
		Delete all the buildings in the game
	*/
	void DeleteBuildings();

private:
	std::map <int, Building*> player1_buildings;	// Holds what buildings the first player owns
	std::map <int, Building*> player2_buildings;	// Holds what buildings the second player owns
};

