#pragma once

#include "SpriteAnimation.h"
#include "Render.h"
#include "Audio.h"
#include "Font.h"


class MapPhase
{
public:
	MapPhase();
	~MapPhase();

	void SetUpMapPhase();

	void Update();

	void RenderMapPhase(float window_scale_x, float window_scale_y);

private:

	int planet_left_middle;
	int planet_left_right;
	int planet_left_left;

	Render MapBackGround;

	Render planets;

	SpriteAnimation planet1;
	SpriteAnimation planet2;
	SpriteAnimation planet3;
	SpriteAnimation planet4;
	SpriteAnimation planet5;
	SpriteAnimation planet6;
	SpriteAnimation planet7;
	SpriteAnimation planet8;
	SpriteAnimation planet9;

};

