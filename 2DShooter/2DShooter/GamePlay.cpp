#include "GamePlay.h"

#include "CombatPhase.h"
#include "BuildPhase.h"
#include "Player.h"
#include "ComputerPlayer.h"
#include "BuildingManager.h"
#include "BulletManager.h"
#include "UnitManager.h"
#include "State.h"


GamePlay::GamePlay()
{
	build_phase_round = true;
	computer_player = true;
	computer_player_turn = false;
	current_build_player = 1;

	building_phase = new BuildPhase();
	combat_phase = new CombatPhase();
	player1 = new Player();
	computer = new ComputerPlayer();
	building_manager = new BuildingManager();
	unit_manager = new UnitManager();
	bullet_manager = new BulletManager();
}


GamePlay::~GamePlay()
{
	delete(building_phase);
	delete(combat_phase);
	delete(player1);
	delete(computer);
	delete(building_manager);
	delete(unit_manager);
	delete(bullet_manager);
}

void GamePlay::RenderGamePlay(float window_scale_x, float window_scale_y)
{
	if (build_phase_round == true)
	{
		// Draw the build phase
		building_phase->RenderBuildingPhase(window_scale_x, window_scale_y);
		building_manager->RenderBuildings(GetCurrentBuildPlayer(), window_scale_x, window_scale_y);
		player1->DrawPlayerInfo(window_scale_x, window_scale_y);
	}
	else // if build phase is over and combat phase begins
	{
		combat_phase->RenderCombatPhase(window_scale_x, window_scale_y);
		unit_manager->RenderUnits(window_scale_x, window_scale_y);
		bullet_manager->RenderBulletManager(window_scale_x, window_scale_y);
	}
}

void GamePlay::ForceExit()
{
	game_over = true;
}

void GamePlay::Update()
{
	if (build_phase_round == true)
	{
		building_phase->UpdateBuildingPhase();
	}
	else // if in combat phase 
	{
		combat_phase->UpdateCombatPhase();
		unit_manager->UpdateUnitManager(*bullet_manager);
		bullet_manager->UpdateBullet();
		if (computer_player)
		{
			computer->UpdateComputerPlayer();

		}
	}
}

BuildPhase* GamePlay::GetBuildingPhase()
{
	return building_phase;
}

CombatPhase* GamePlay::GetComabatPhase()
{
	return combat_phase;
}

Player* GamePlay::GetPlayer()
{
	return player1;
}

ComputerPlayer* GamePlay::GetComputerPlayer()
{
	return computer;
}

BuildingManager* GamePlay::GetBuildingManager()
{
	return building_manager;
}

int GamePlay::GetCurrentBuildPlayer()
{
	return current_build_player;
}

void GamePlay::NextBuildPlayer(State &state)
{
	if(current_build_player < PLAYERS_PLAYING)
	{
		current_build_player += 1;
		if (DoesComputerPlayerExist() == true)
		{
			computer_player_turn = true;
			computer->ChooseBuildingsToBuy(state);
		}
	}
	else
	{
		current_build_player = 1;
		if (DoesComputerPlayerExist() == true)
		{
			computer_player_turn = false;
		}
	}
}

UnitManager* GamePlay::GetUnitManager()
{
	return unit_manager;
}

bool GamePlay::DoesComputerPlayerExist()
{
	return computer_player;
}

bool GamePlay::IsItComputerTurn()
{
	return computer_player_turn;
}

void GamePlay::SwitchToCombatPhase()
{
	build_phase_round = false;
	unit_manager->CeateUnit(building_manager->GetMainBuildingType(1), 1);
	unit_manager->CeateUnit(building_manager->GetMainBuildingType(2), 2);
}

void GamePlay::SwitchToBuildPhase()
{
	build_phase_round = true;
}

BulletManager* GamePlay::GetBulletManager()
{
	return bullet_manager;
}