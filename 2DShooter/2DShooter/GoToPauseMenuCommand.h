#pragma once
#include "Command.h"

class GoToPauseMenuCommand :
	public Command
{
public:

	~GoToPauseMenuCommand();

	virtual void execute() {};
	virtual void execute(State &state);
};

