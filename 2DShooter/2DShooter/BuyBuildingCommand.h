#pragma once

#include "Command.h"

class BuyBuildingCommand : public Command
{
public:

	~BuyBuildingCommand();

	virtual void execute() {};
	virtual void execute(State &state);
	void BuyBuilding(State &state);
};

