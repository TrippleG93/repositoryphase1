#include "ConfirmDoneBuildingCommand.h"

#include "State.h"
#include "ConfirmMenu.h"
#include "Globals.h"

#include "InputHandler.h"
#include "BuildPhase.h"
#include "BuildingManager.h"
#include "Player.h"

ConfirmDoneBuildingCommand::~ConfirmDoneBuildingCommand()
{

}

void ConfirmDoneBuildingCommand::execute(State &state)
{

	if (state.GetConfirmMenu()->GetChoice() == true && state.GetBuildPhase()->IsItComputerPlayerTurn() == false)
	{
		if (state.GetBuildPhase()->GetCurrentBuildPlayer() == 1 && 
			state.GetBuildPhase()->GetBuildingManager()->DoesMainShipBuildingExist(state.GetBuildPhase()->GetCurrentBuildPlayer()) == true)
		{
			state.SetState(BUILDPHASE);
			state.DeleteConfirmBuildMenu();
			state.GetBuildPhase()->NextBuildPlayer(state);
			state.GetBuildPhase()->ResetCursor();
			state.GetInputHandler()->InitBuildPhase();
			state.GetAudioManager().PlayCursorSelect();
			state.GetPlayer()->ResetPurchase();
		}
		else if (state.GetBuildPhase()->GetBuildingManager()->DoesMainShipBuildingExist(state.GetBuildPhase()->GetCurrentBuildPlayer()) == true)
		{
			//state.SetState(COMBATPHASE);
			//state.DeleteConfirmBuildMenu();
			state.SetState(BUILDPHASE);
			state.DeleteConfirmBuildMenu();
			state.GetBuildPhase()->NextBuildPlayer(state);
			state.GetBuildPhase()->ResetCursor();
			state.GetInputHandler()->InitBuildPhase();
			state.GetAudioManager().PlayCursorSelect();
			state.GetPlayer()->ResetPurchase();
			// Call the input handler for the comabt phase
		}
	}
	else if (state.GetConfirmMenu()->GetChoice() == true && state.GetBuildPhase()->IsItComputerPlayerTurn() == true)
	{
		//state.InitMapPhase();
		//state.GetBuildPhase()->NextBuildPlayer(state);
		state.GetAudioManager().StopBuildSong();
		state.GetAudioManager().LoadCombatSong();
		state.GetAudioManager().PlayCombatSong();
		state.SetState(COMBATPHASE);
		state.InitCombatPhase();
		state.GetBuildPhase()->NextBuildPlayer(state);
		state.GetBuildPhase()->ResetCursor();
		state.GetInputHandler()->InitCombatPhase();
		state.GetAudioManager().PlayCursorSelect();
	}
	else
	{
		state.SetState(BUILDPHASE);
		state.DeleteConfirmBuildMenu();
		state.GetInputHandler()->InitBuildPhase();
		state.GetAudioManager().PlayCursorSelect();
	}

	state.GetInputHandler()->ClearKeyBoardInputs();
	if (state.GetInputHandler()->GetHowManyControllers() != 0)
	{
		state.GetInputHandler()->ClearControllerInput(1);			// Clear the keyboard input
	}
}
