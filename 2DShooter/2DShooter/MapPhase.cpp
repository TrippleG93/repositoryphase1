#include "MapPhase.h"

#include "ConfigFileReader.h"


MapPhase::MapPhase()
{
	SetUpMapPhase();
}


MapPhase::~MapPhase()
{
}

void MapPhase::SetUpMapPhase()
{
	planet1.Destroy();
	planet2.Destroy();
	planet3.Destroy();
	planet4.Destroy();
	planet5.Destroy();
	planet6.Destroy();
	planet7.Destroy();
	planet8.Destroy();
	planet9.Destroy();

	ConfigFileReader file("Content/Text Files/map.cfg");

	MapBackGround.LoadBitmap("Content/Images/Combat Phase Backgrounds/PC Computer - Space Pirates and Zombies - Backgrounds 22/Map.png");
	planets.LoadBitmap("Content/Images/MapDetails/hjm-planets_alpha.png");

	planet1.AddIndividualFrame(file.GetConfigValueIntegers("planet 1", "source x"),
		file.GetConfigValueIntegers("planet 1", "source y"),
		file.GetConfigValueIntegers("planet 1", "width"),
		file.GetConfigValueIntegers("planet 1", "height"),
		file.GetConfigValueIntegers("planet 1", "position x"),
		file.GetConfigValueIntegers("planet 1", "position y"),
		file.GetConfigValueDouble("planet 1", "scale x"),
		file.GetConfigValueDouble("planet 1", "scale y"));
	planet1.CombineFrames(1, false);

	planet2.AddIndividualFrame(file.GetConfigValueIntegers("planet 2", "source x"),
		file.GetConfigValueIntegers("planet 2", "source y"),
		file.GetConfigValueIntegers("planet 2", "width"),
		file.GetConfigValueIntegers("planet 2", "height"),
		file.GetConfigValueIntegers("planet 2", "position x"),
		file.GetConfigValueIntegers("planet 2", "position y"),
		file.GetConfigValueDouble("planet 2", "scale x"),
		file.GetConfigValueDouble("planet 2", "scale y"));
	planet2.CombineFrames(1, false);

	planet3.AddIndividualFrame(file.GetConfigValueIntegers("planet 3", "source x"),
		file.GetConfigValueIntegers("planet 3", "source y"),
		file.GetConfigValueIntegers("planet 3", "width"),
		file.GetConfigValueIntegers("planet 3", "height"),
		file.GetConfigValueIntegers("planet 3", "position x"),
		file.GetConfigValueIntegers("planet 3", "position y"),
		file.GetConfigValueDouble("planet 3", "scale x"),
		file.GetConfigValueDouble("planet 3", "scale y"));
	planet3.CombineFrames(1, false);

	planet4.AddIndividualFrame(file.GetConfigValueIntegers("planet 4", "source x"),
		file.GetConfigValueIntegers("planet 4", "source y"),
		file.GetConfigValueIntegers("planet 4", "width"),
		file.GetConfigValueIntegers("planet 4", "height"),
		file.GetConfigValueIntegers("planet 4", "position x"),
		file.GetConfigValueIntegers("planet 4", "position y"),
		file.GetConfigValueDouble("planet 4", "scale x"),
		file.GetConfigValueDouble("planet 4", "scale y"));
	planet4.CombineFrames(1, false);

	planet5.AddIndividualFrame(file.GetConfigValueIntegers("planet 5", "source x"),
		file.GetConfigValueIntegers("planet 5", "source y"),
		file.GetConfigValueIntegers("planet 5", "width"),
		file.GetConfigValueIntegers("planet 5", "height"),
		file.GetConfigValueIntegers("planet 5", "position x"),
		file.GetConfigValueIntegers("planet 5", "position y"),
		file.GetConfigValueDouble("planet 5", "scale x"),
		file.GetConfigValueDouble("planet 5", "scale y"));
	planet5.CombineFrames(1, false);

	planet6.AddIndividualFrame(file.GetConfigValueIntegers("planet 6", "source x"),
		file.GetConfigValueIntegers("planet 6", "source y"),
		file.GetConfigValueIntegers("planet 6", "width"),
		file.GetConfigValueIntegers("planet 6", "height"),
		file.GetConfigValueIntegers("planet 6", "position x"),
		file.GetConfigValueIntegers("planet 6", "position y"),
		file.GetConfigValueDouble("planet 6", "scale x"),
		file.GetConfigValueDouble("planet 6", "scale y"));
	planet6.CombineFrames(1, false);

	planet7.AddIndividualFrame(file.GetConfigValueIntegers("planet 7", "source x"),
		file.GetConfigValueIntegers("planet 7", "source y"),
		file.GetConfigValueIntegers("planet 7", "width"),
		file.GetConfigValueIntegers("planet 7", "height"),
		file.GetConfigValueIntegers("planet 7", "position x"),
		file.GetConfigValueIntegers("planet 7", "position y"),
		file.GetConfigValueDouble("planet 7", "scale x"),
		file.GetConfigValueDouble("planet 7", "scale y"));
	planet7.CombineFrames(1, false);

	planet8.AddIndividualFrame(file.GetConfigValueIntegers("planet 8", "source x"),
		file.GetConfigValueIntegers("planet 8", "source y"),
		file.GetConfigValueIntegers("planet 8", "width"),
		file.GetConfigValueIntegers("planet 8", "height"),
		file.GetConfigValueIntegers("planet 8", "position x"),
		file.GetConfigValueIntegers("planet 8", "position y"),
		file.GetConfigValueDouble("planet 8", "scale x"),
		file.GetConfigValueDouble("planet 8", "scale y"));
	planet8.CombineFrames(1, false);

	planet9.AddIndividualFrame(file.GetConfigValueIntegers("planet 9", "source x"),
		file.GetConfigValueIntegers("planet 9", "source y"),
		file.GetConfigValueIntegers("planet 9", "width"),
		file.GetConfigValueIntegers("planet 9", "height"),
		file.GetConfigValueIntegers("planet 9", "position x"),
		file.GetConfigValueIntegers("planet 9", "position y"),
		file.GetConfigValueDouble("planet 9", "scale x"),
		file.GetConfigValueDouble("planet 9", "scale y"));
	planet9.CombineFrames(1, false);

}

void MapPhase::Update()
{

}

void MapPhase::RenderMapPhase(float window_scale_x, float window_scale_y)
{
	MapBackGround.DrawScaledImage(0, 0,
		MapBackGround.GetBitmapWidth(),
		MapBackGround.GetBitmapHeight(),
		0, 0,
		MapBackGround.GetBitmapWidth() * window_scale_x,
		MapBackGround.GetBitmapHeight() * window_scale_y,
		NULL);

	planets.DrawScaledImage(planet1.GetCurrentFrame().GetSourceX(),
		planet1.GetCurrentFrame().GetSourceY(),
		planet1.GetCurrentFrame().GetWidth(),
		planet1.GetCurrentFrame().GetHeight(),
		planet1.GetCurrentFrame().GetPositionX() * window_scale_x,
		planet1.GetCurrentFrame().GetPositionY() * window_scale_y,
		planet1.GetCurrentFrame().GetWidth() * planet1.GetCurrentFrame().GetScaleX() * window_scale_x,
		planet1.GetCurrentFrame().GetHeight() * planet1.GetCurrentFrame().GetScaleY() * window_scale_y,
		NULL);

	planets.DrawScaledImage(planet2.GetCurrentFrame().GetSourceX(),
		planet2.GetCurrentFrame().GetSourceY(),
		planet2.GetCurrentFrame().GetWidth(),
		planet2.GetCurrentFrame().GetHeight(),
		planet2.GetCurrentFrame().GetPositionX() * window_scale_x,
		planet2.GetCurrentFrame().GetPositionY() * window_scale_y,
		planet2.GetCurrentFrame().GetWidth() * planet2.GetCurrentFrame().GetScaleX() * window_scale_x,
		planet2.GetCurrentFrame().GetHeight() * planet2.GetCurrentFrame().GetScaleY() * window_scale_y,
		NULL);

	planets.DrawScaledImage(planet3.GetCurrentFrame().GetSourceX(),
		planet3.GetCurrentFrame().GetSourceY(),
		planet3.GetCurrentFrame().GetWidth(),
		planet3.GetCurrentFrame().GetHeight(),
		planet3.GetCurrentFrame().GetPositionX() * window_scale_x,
		planet3.GetCurrentFrame().GetPositionY() * window_scale_y,
		planet3.GetCurrentFrame().GetWidth() * planet3.GetCurrentFrame().GetScaleX() * window_scale_x,
		planet3.GetCurrentFrame().GetHeight() * planet3.GetCurrentFrame().GetScaleY() * window_scale_y,
		NULL);

	planets.DrawScaledImage(planet4.GetCurrentFrame().GetSourceX(),
		planet4.GetCurrentFrame().GetSourceY(),
		planet4.GetCurrentFrame().GetWidth(),
		planet4.GetCurrentFrame().GetHeight(),
		planet4.GetCurrentFrame().GetPositionX() * window_scale_x,
		planet4.GetCurrentFrame().GetPositionY() * window_scale_y,
		planet4.GetCurrentFrame().GetWidth() * planet4.GetCurrentFrame().GetScaleX() * window_scale_x,
		planet4.GetCurrentFrame().GetHeight() * planet4.GetCurrentFrame().GetScaleY() * window_scale_y,
		NULL);

	planets.DrawScaledImage(planet5.GetCurrentFrame().GetSourceX(),
		planet5.GetCurrentFrame().GetSourceY(),
		planet5.GetCurrentFrame().GetWidth(),
		planet5.GetCurrentFrame().GetHeight(),
		planet5.GetCurrentFrame().GetPositionX() * window_scale_x,
		planet5.GetCurrentFrame().GetPositionY() * window_scale_y,
		planet5.GetCurrentFrame().GetWidth() * planet5.GetCurrentFrame().GetScaleX() * window_scale_x,
		planet5.GetCurrentFrame().GetHeight() * planet5.GetCurrentFrame().GetScaleY() * window_scale_y,
		NULL);

	planets.DrawScaledImage(planet6.GetCurrentFrame().GetSourceX(),
		planet6.GetCurrentFrame().GetSourceY(),
		planet6.GetCurrentFrame().GetWidth(),
		planet6.GetCurrentFrame().GetHeight(),
		planet6.GetCurrentFrame().GetPositionX() * window_scale_x,
		planet6.GetCurrentFrame().GetPositionY() * window_scale_y,
		planet6.GetCurrentFrame().GetWidth() * planet6.GetCurrentFrame().GetScaleX() * window_scale_x,
		planet6.GetCurrentFrame().GetHeight() * planet6.GetCurrentFrame().GetScaleY() * window_scale_y,
		NULL);

	planets.DrawScaledImage(planet7.GetCurrentFrame().GetSourceX(),
		planet7.GetCurrentFrame().GetSourceY(),
		planet7.GetCurrentFrame().GetWidth(),
		planet7.GetCurrentFrame().GetHeight(),
		planet7.GetCurrentFrame().GetPositionX() * window_scale_x,
		planet7.GetCurrentFrame().GetPositionY() * window_scale_y,
		planet7.GetCurrentFrame().GetWidth() * planet7.GetCurrentFrame().GetScaleX() * window_scale_x,
		planet7.GetCurrentFrame().GetHeight() * planet7.GetCurrentFrame().GetScaleY() * window_scale_y,
		NULL);

	planets.DrawScaledImage(planet8.GetCurrentFrame().GetSourceX(),
		planet8.GetCurrentFrame().GetSourceY(),
		planet8.GetCurrentFrame().GetWidth(),
		planet8.GetCurrentFrame().GetHeight(),
		planet8.GetCurrentFrame().GetPositionX() * window_scale_x,
		planet8.GetCurrentFrame().GetPositionY() * window_scale_y,
		planet8.GetCurrentFrame().GetWidth() * planet8.GetCurrentFrame().GetScaleX() * window_scale_x,
		planet8.GetCurrentFrame().GetHeight() * planet8.GetCurrentFrame().GetScaleY() * window_scale_y,
		NULL);

	planets.DrawScaledImage(planet9.GetCurrentFrame().GetSourceX(),
		planet9.GetCurrentFrame().GetSourceY(),
		planet9.GetCurrentFrame().GetWidth(),
		planet9.GetCurrentFrame().GetHeight(),
		planet9.GetCurrentFrame().GetPositionX() * window_scale_x,
		planet9.GetCurrentFrame().GetPositionY() * window_scale_y,
		planet9.GetCurrentFrame().GetWidth() * planet9.GetCurrentFrame().GetScaleX() * window_scale_x,
		planet9.GetCurrentFrame().GetHeight() * planet9.GetCurrentFrame().GetScaleY() * window_scale_y,
		NULL);
}