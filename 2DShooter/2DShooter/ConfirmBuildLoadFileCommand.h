#pragma once

#include "Command.h"

class ConfirmBuildLoadFileCommand : public Command
{
public:
	~ConfirmBuildLoadFileCommand();

	virtual void execute() {};
	virtual void execute(State &state);
};

