#include "ExitTitleCommand.h"

#include "State.h"
#include "Globals.h"
#include "MainMenu.h"
#include "InputHandler.h"
#include "TitleScreen.h"

ExitTitleCommand::~ExitTitleCommand()
{
}

void ExitTitleCommand::execute(State & state)
{
	state.DeleteTitleScreen();
	state.InitializeMainMenu();										// Create the main menu object
	state.SetState(MAINMENU);										// Change th estate to main menu
	state.GetInputHandler()->ClearKeyBoardInputs();
	if (state.GetInputHandler()->GetHowManyControllers() != 0)
	{
		state.GetInputHandler()->ClearControllerInput(1);			// Clear the keyboard input
	}
	state.GetAudioManager().PlayCursorSelect();
}
