#include "BuyBuildingCommand.h"

#include "State.h"

#include "Player.h"
#include "BuildMenu.h"
#include "Globals.h"
#include "InputHandler.h"
#include "BuildingManager.h"
#include "BuildPhase.h"
#include "ComputerPlayer.h"
#include "Player.h"

BuyBuildingCommand::~BuyBuildingCommand()
{
}

void BuyBuildingCommand::execute(State &state)
{
	if (state.GetBuildPhase()->GetCurrentBuildPlayer() == 1)
	{
		if (state.GetBuildPhase()->GetBuildingManager()->DoesBuildingTypeExist(
			state.GetBuildPhase()->GetCurrentBuildPlayer(),
			state.GetBuildMenu()->GetBuildingType()) == false &&
			state.GetPlayer()->CanPlayerPurchase() == true)
		{
			if (state.GetBuildMenu()->GetBuildingType() <= 4)
			{
				if (state.GetBuildPhase()->GetBuildingManager()->DoesMainShipBuildingExist(state.GetBuildPhase()->GetCurrentBuildPlayer()) == false)
				{
					BuyBuilding(state);
					state.GetPlayer()->SetMainShip();
					state.GetAudioManager().PlayCursorSelect();
				}
			}
			else
			{
				BuyBuilding(state);

				state.GetAudioManager().PlayCursorSelect();
			}
		}

	}
	else if (state.GetBuildPhase()->GetCurrentBuildPlayer() == 2 && 
		state.GetBuildPhase()->DoesComputerExist() == false)
	{

	}
	else
	{
		BuyBuilding(state);
		state.GetAudioManager().PlayCursorSelect();
	}
}

void BuyBuildingCommand::BuyBuilding(State &state)
{
	state.GetBuildPhase()->GetBuildingManager()->CreateBuilding(state.GetBuildPhase()->GetCurrentBuildPlayer(),
		state.GetBuildPhase()->GetCursorLocation(),
		state.GetBuildMenu()->GetBuildingType(),
		state.GetBuildPhase()->GetCursorRow(),
		state.GetBuildPhase()->GetCursorColumn());
	state.GetBuildPhase()->SetBuildingSpot(state.GetBuildPhase()->GetCursorLocation(),
		state.GetBuildMenu()->GetBuildingType());
	if (state.GetBuildPhase()->GetCurrentBuildPlayer() == 1)
	{
		state.GetPlayer()->Purchased();
	}
	else if (state.GetBuildPhase()->GetCurrentBuildPlayer() == 1)
	{

	}
	else
	{
		state.GetComputerPlayer()->Purchased();
	}

	state.SetState(BUILDPHASE);									// Change the state to the buildphase state
	state.DeleteBuildMenu();									// Delete the build menu and its controls
	state.GetInputHandler()->InitBuildPhase();					// Change the conrols back to the build phase_
	state.GetInputHandler()->ClearKeyBoardInputs();				// Clear all inputs
	if (state.GetInputHandler()->GetHowManyControllers() != 0)
	{
		state.GetInputHandler()->ClearControllerInput(1);
	}
}

