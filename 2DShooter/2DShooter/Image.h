#pragma once

#include <string>

struct ALLEGRO_BITMAP;									// Forward declaring the ALLEGRO_BITMAP variable
struct ALLEGRO_FONT;									// Forward declaring the ALLEGRO_FONT variable
struct ALLEGRO_COLOR;									// Forward declaring the ALLGERO_COLOR variable

class Image
{
public:
	Image();
	~Image();

	/* LoadBitmap
	* Load bitmap from the file path
	*/
	void LoadBitmap(std::string file_path);

	/* DestroyBitmap
	* Destroy the image associated with the bitmap
	*/
	void DestroyBitmap();

	/* LoadSubBitmap
	* Creates a sub-bitmap of the parent, at the specified coordinates and of the specified size.
	* x_pos - the position of where you want to start at on the image
	* y_pos - the position of where you want to start at on the image
	* width - how wide the image is from the x & y position
	* height - how long the image is from the x & y position
	*/
	void LoadSubBitmap(int x_pos, int y_pos, int width, int height);

	/* GetBitmapWidth
	* obtain the width of a specific image
	*/
	int GetBitmapWidth();

	/* GetBitmapHeight
	* obtain the height of a sepecific image
	*/
	int GetBitmapHeight();

	/* CreateTextRender
	* changes the text int variable font into an image variable
	* image_name - used to associate the newly created bitmap in the mappinng variable
	* font - holds the specific font style that the text will be in
	* color - holds what color the text will be in
	* text - will be converted to an actual bitmap variable
	*/
	void CreateTextRender(ALLEGRO_FONT *font, ALLEGRO_COLOR color, std::string text);

	/* UpdateTextImage
	* used to update the text image to a new text
	* This one is specifically used for interger values to be added
	* to the text
	*/
	void UpdateTextImage(ALLEGRO_FONT *font, std::string text, int value);

	/*  ObtainImage
	* returns the image based on the name given for that image
	*/
	ALLEGRO_BITMAP *GetImage();

private:
	ALLEGRO_BITMAP * bitmap;
};

