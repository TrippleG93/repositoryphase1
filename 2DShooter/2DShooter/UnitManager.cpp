#include "UnitManager.h"

#include "Unit.h"
#include "BulletManager.h"
#include "Bullet.h"
#include "ConfigFileReader.h"
#include "Globals.h"
#include "BuildingManager.h"
#include "FireCommandSU4.h"
#include "FireCommandSU4P2.h"
#include "State.h"

#include <vector>
#include <iostream>
#include <cstdlib>

UnitManager::UnitManager()
{
}


UnitManager::~UnitManager()
{
	for (const auto& player_1_support_unit : player_1_support_units)
	{
		if (player_1_support_unit.second != nullptr)
		{
			delete(player_1_support_unit.second);
		}
	}
	player_1_support_units.clear();

	for (const auto& player_2_support_unit : player_2_support_units)
	{
		if (player_2_support_unit.second != nullptr)
		{
			delete(player_2_support_unit.second);
		}
	}
	player_2_support_units.clear();

	player_1_support_unit_alive.clear();
	player_1_support_unit_exist.clear();

	player_2_support_unit_alive.clear();
	player_2_support_unit_exist.clear();

	player_1_support_unit_count.clear();
	player_1_support_unit_ready.clear();

	player_2_support_unit_count.clear();
	player_2_support_unit_ready.clear();
	delete(player1_unit);
	delete(player2_unit);
}

void UnitManager::Init(BuildingManager &building_manager)
{
	ConfigFileReader file("Content/Text Files/Units/UnitManager.cfg");

	emulogic_font.CreateFont("Content/Fonts/Gravedigger.ttf", file.GetConfigValueIntegers("font", "size") );

	// Checks to see if any support units were built
	for (int type= 5; type < 9; type++)
	{
		if (building_manager.DoesBuildingTypeExist(1, type) == true)
		{
			player_1_support_unit_alive[type] = false;
			player_1_support_unit_exist[type] = true;
			player_1_support_unit_count[type] = 0;
			std::string test = "support unit " + std::to_string(type);
			player_1_support_unit_ready[type] = file.GetConfigValueIntegers(test, "ready");
		}
		else
		{
			player_1_support_unit_alive[type] = false;
			player_1_support_unit_exist[type] = false;
			player_1_support_unit_count[type] = 0;
			player_1_support_unit_count[type] = 0;
		}

		if (building_manager.DoesBuildingTypeExist(2, type) == true)
		{
			player_2_support_unit_alive[type] = false;
			player_2_support_unit_exist[type] = true;
			player_2_support_unit_count[type] = 0;
			std::string test = "support unit " + std::to_string(type);
			player_2_support_unit_ready[type] = file.GetConfigValueIntegers(test, "ready");
		}
		else
		{
			player_2_support_unit_alive[type] = false;
			player_2_support_unit_exist[type] = false;
			player_2_support_unit_count[type] = 0;
			player_2_support_unit_count[type] = 0;
		}
	}

	bullet_cap = file.GetConfigValueIntegers("bullet", "bullet cap");
	su4_damage = file.GetConfigValueIntegers("support unit 8", "damage");

	text_image_1.CreateTextRender(emulogic_font.ObtainFont(), al_map_rgb(
		file.GetConfigValueIntegers("text 1", "text r"),
		file.GetConfigValueIntegers("text 1", "text g"),
		file.GetConfigValueIntegers("text 1", "text b")),
		file.GetConfigValueCharacters("text 1", "text"));

	text_image_2.CreateTextRender(emulogic_font.ObtainFont(), al_map_rgb(
		file.GetConfigValueIntegers("text 2", "text r"),
		file.GetConfigValueIntegers("text 2", "text g"),
		file.GetConfigValueIntegers("text 2", "text b")),
		file.GetConfigValueCharacters("text 2", "text"));

	support_unit_number_image_1W.CreateTextRender(emulogic_font.ObtainFont(), al_map_rgb(255, 255, 255), "1");
	support_unit_number_image_2W.CreateTextRender(emulogic_font.ObtainFont(), al_map_rgb(255, 255, 255), "2");
	support_unit_number_image_3W.CreateTextRender(emulogic_font.ObtainFont(), al_map_rgb(255, 255, 255), "3");
	support_unit_number_image_4W.CreateTextRender(emulogic_font.ObtainFont(), al_map_rgb(255, 255, 255), "4");
}

void UnitManager::RenderUnits(float window_scale_x, float window_scale_y)
{
	player1_unit->RenderUnit(window_scale_x, window_scale_y);
	player2_unit->RenderUnit(window_scale_x, window_scale_y);

	DrawSupportNumber(window_scale_x, window_scale_y);

	for (const auto& player_1_support_unit : player_1_support_units)
	{
		if (player_1_support_unit.second != nullptr)
		{
			player_1_support_unit.second->RenderUnit(window_scale_x, window_scale_y);
		}
	}
	for (const auto& player_2_support_unit : player_2_support_units)
	{
		if (player_2_support_unit.second != nullptr)
		{
			player_2_support_unit.second->RenderUnit(window_scale_x, window_scale_y);
		}

	}
}

void UnitManager::CeateUnit(int building_type, int player)
{
	if (player == 1)
	{
		player1_unit = new Unit(building_type, player, 1280 / 2, 600);
	}
	else
	{
		player2_unit = new Unit(building_type, player, 1280 / 2, 100);
	}
}

Unit* UnitManager::GetPlayer1Unit()
{
	return player1_unit;
}

Unit* UnitManager::GetPlayer2Unit()
{
	return player2_unit;
}

void UnitManager::UpdateUnitManager(BulletManager &bullet_manager, State &state)
{
	player1_unit->UpdateUnit(player2_unit->GetXPosition(), player2_unit->GetYPosition());
	player2_unit->UpdateUnit(player1_unit->GetXPosition(), player1_unit->GetYPosition());

	
	// Checks to see if player 1 is taking any bullets from player 2
	for (std::vector<Bullet*>::size_type i = 0; i < bullet_manager.GetPlayer2Bullets().size(); i++)
	{
		if (player1_unit->CollisionDetection(player1_unit->GetSprite(), 
			bullet_manager.GetPlayer2Bullets().at(i)->GetSprite()) == true && 
			bullet_manager.GetPlayer2Bullets().at(i)->DidBulletHitSomething() == false)
		{
			bullet_manager.GetPlayer2Bullets().at(i)->BulletHitSomething();
			player1_unit->TakeDamage(bullet_manager.GetPlayer2Bullets().at(i)->GetDamage(),
				player2_unit->GetDamageBoost());
		}
		for (const auto& player_1_support_unit : player_1_support_units)
		{
			if (player_1_support_unit.second != nullptr)
			{
				if (player_1_support_unit.second->CollisionDetection(player_1_support_unit.second->GetSprite(),
					bullet_manager.GetPlayer2Bullets().at(i)->GetSprite()) == true &&
					bullet_manager.GetPlayer2Bullets().at(i)->DidBulletHitSomething() == false)
				{
					bullet_manager.GetPlayer2Bullets().at(i)->BulletHitSomething();
					player_1_support_unit.second->TakeDamage(bullet_manager.GetPlayer2Bullets().at(i)->GetDamage(),
						player2_unit->GetDamageBoost());
				}
			}	
		}
	}

	// Checks to see if player 2 is taking any bullets from player 1
	for (std::vector<Bullet*>::size_type i = 0; i < bullet_manager.GetPlayer1Bullets().size(); i++)
	{
		if (player2_unit->CollisionDetection(player2_unit->GetSprite(),
			bullet_manager.GetPlayer1Bullets().at(i)->GetSprite()) == true &&
			bullet_manager.GetPlayer1Bullets().at(i)->DidBulletHitSomething() == false)
		{
			bullet_manager.GetPlayer1Bullets().at(i)->BulletHitSomething();
			player2_unit->TakeDamage(bullet_manager.GetPlayer1Bullets().at(i)->GetDamage(),
				player1_unit->GetDamageBoost());
		}
		for (const auto& player_2_support_unit : player_2_support_units)
		{
			if (player_2_support_unit.second != nullptr)
			{
				if (player_2_support_unit.second->CollisionDetection(player_2_support_unit.second->GetSprite(),
					bullet_manager.GetPlayer1Bullets().at(i)->GetSprite()) == true &&
					bullet_manager.GetPlayer1Bullets().at(i)->DidBulletHitSomething() == false)
				{
					bullet_manager.GetPlayer1Bullets().at(i)->BulletHitSomething();
					player_2_support_unit.second->TakeDamage(bullet_manager.GetPlayer1Bullets().at(i)->GetDamage(),
						player1_unit->GetDamageBoost());
				}
			}

		}
	}

	for (int i = 5; i < 9; i++)
	{
		if (player_1_support_unit_exist[i] == true &&
			player_1_support_unit_count[i] <= player_1_support_unit_ready[i])
		{
			player_1_support_unit_count[i]++;
		}
		if (player_2_support_unit_exist[i] == true &&
			player_2_support_unit_count[i] <= player_2_support_unit_ready[i])
		{
			player_2_support_unit_count[i]++;
		}
	}

	if (player_1_support_unit_alive[5] == true)
	{
		player_1_support_units[5]->SuicideAi(player2_unit->GetXPosition() + player2_unit->GetSprite().GetWidth()  * player2_unit->GetSprite().GetScaleX() / 2,
			player2_unit->GetYPosition());
		if (player2_unit->CollisionDetection(player2_unit->GetSprite(), player_1_support_units[5]->GetSprite()) == true)
		{
			player2_unit->TakeDamage(25, 0);
			player_1_support_units[5]->TakeDamage(1000, 0);
		}
	}
	if (player_2_support_unit_alive[5] == true)
	{
		player_2_support_units[5]->SuicideAi(player1_unit->GetXPosition() + player1_unit->GetSprite().GetWidth() * player1_unit->GetSprite().GetScaleX() / 2,
			player1_unit->GetYPosition());
		if (player1_unit->CollisionDetection(player1_unit->GetSprite(), player_2_support_units[5]->GetSprite()) == true)
		{
			player1_unit->TakeDamage(25, 0);
			player_2_support_units[5]->TakeDamage(1000, 0);
		}
	}

	if (player_1_support_unit_alive[6] == true)
	{
		player_1_support_units[6]->ProtectAi(player1_unit->GetXPosition()+player1_unit->GetSprite().GetWidth()*player1_unit->GetSprite().GetScaleX()/2,
			player1_unit->GetYPosition());
	}
	if (player_2_support_unit_alive[6] == true)
	{
		player_2_support_units[6]->ProtectAi(player2_unit->GetXPosition() + player2_unit->GetSprite().GetWidth()*player2_unit->GetSprite().GetScaleX() / 2,
			player2_unit->GetYPosition());
	}

	if (player_1_support_unit_alive[7] == true)
	{
		player_1_support_units[7]->HealAi(player1_unit->GetXPosition(),
			player1_unit->GetYPosition());
		if (player1_unit->CollisionDetection(player1_unit->GetSprite(), player_1_support_units[7]->GetSprite()) == true)
		{
			player1_unit->Heal(50);
			player_1_support_units[7]->TakeDamage(1000, 0);
		}
	}
	if (player_2_support_unit_alive[7] == true)
	{
		player_2_support_units[7]->HealAi(player2_unit->GetXPosition(),
			player2_unit->GetYPosition());
		if (player2_unit->CollisionDetection(player2_unit->GetSprite(), player_2_support_units[7]->GetSprite()) == true)
		{
			player2_unit->Heal(50);
			player_2_support_units[7]->TakeDamage(1000, 0);
		}
	}

	if (player_1_support_unit_alive[8] == true)
	{
		// Slowly die
		player_1_support_units[8]->TakeDamage(su4_damage, 0);

		player_1_support_units[8]->SentryAi(100, globals::GAME_HEIGHT-200);
		if (player2_unit->CollisionDetection(player2_unit->GetSprite(), player_1_support_units[8]->GetSprite()) == true)
		{
			player2_unit->TakeDamage(25, 0);
			player_1_support_units[8]->TakeDamage(1000, 0);
		}

		//=============================
		// SUPPORT UNIT 4 'FIRE' LOGIC
		//=============================
		if (su4_fire_count_p1 > su4_fire_delay_p1)
		{
			FireCommandSU4 *fire = new FireCommandSU4;
			fire->execute(state);
			delete(fire);

			su4_fire_count_p1 = 0;
			su4_fire_delay_p1 = rand() % 40 + 15;
		}
		else
		{
			su4_fire_count_p1 += 1;
		}
		//=============================

	}
	if (player_2_support_unit_alive[8] == true)
	{

		// Slowly die
		player_2_support_units[8]->TakeDamage(su4_damage, 0);

		player_2_support_units[8]->SentryAi(100, globals::GAME_HEIGHT - 200);
		if (player1_unit->CollisionDetection(player1_unit->GetSprite(), player_2_support_units[8]->GetSprite()) == true)
		{
			player1_unit->TakeDamage(25, 0);
			player_2_support_units[8]->TakeDamage(1000, 0);
		}

		//=============================
		// SUPPORT UNIT 4 'FIRE' LOGIC
		//=============================
		if (su4_fire_count_p2 > su4_fire_delay_p2)
		{
			FireCommandSU4P2 *fire = new FireCommandSU4P2;
			fire->execute(state);
			delete(fire);

			su4_fire_count_p2 = 0;
			su4_fire_delay_p2 = rand() % 40 + 15;
		}
		else
		{
			su4_fire_count_p2 += 1;
		}
		//=============================
	}

	for (const auto& player_1_support_unit : player_1_support_units)
	{
		if (player_1_support_unit.second != nullptr)
		{
			// Check to see if the unit died
			if (player_1_support_unit.second->IsUnitDead() == true)
			{
				player_1_support_unit_alive[player_1_support_unit.first] = false;
				player_1_support_unit_count[player_1_support_unit.first] = 0;
				delete(player_1_support_units[player_1_support_unit.first]);
				player_1_support_units[player_1_support_unit.first] = nullptr;
			}
			else
			{
				player_1_support_unit.second->UpdateUnit(player2_unit->GetXPosition(), player2_unit->GetYPosition());
			}
		}
	}
	for (const auto& player_2_support_unit : player_2_support_units)
	{
		if (player_2_support_unit.second != nullptr)
		{
			// Check to see if the unit died
			if (player_2_support_unit.second->IsUnitDead() == true)
			{
				player_2_support_unit_alive[player_2_support_unit.first] = false;
				player_2_support_unit_count[player_2_support_unit.first] = 0;
				delete(player_2_support_units[player_2_support_unit.first]);
				player_2_support_units[player_2_support_unit.first] = nullptr;
			}
			else
			{
				player_2_support_unit.second->UpdateUnit(player1_unit->GetXPosition(), player1_unit->GetYPosition());
			}
		}
	}
}

void UnitManager::DrawSupportNumber(float window_scale_x, float window_scale_y)
{
	// P1 SUPPORT UNITS
	support_unit_number_image_1W.DrawScaledImage(0, 0,
		(float)support_unit_number_image_1W.GetBitmapWidth(),
		(float)support_unit_number_image_1W.GetBitmapHeight(),
		1 * spacing_unit * window_scale_x,
		(globals::GAME_HEIGHT - spacing_unit / 2) * window_scale_y,
		(float)support_unit_number_image_1W.GetBitmapWidth() * window_scale_x,
		(float)support_unit_number_image_1W.GetBitmapHeight() * window_scale_y,
		NULL);
	if (player_1_support_unit_exist[5] == true &&
		player_1_support_unit_count[5] > player_1_support_unit_ready[5] &&
		player_1_support_unit_alive[5] == false)
	{
		DrawReadyLaunch(window_scale_x, window_scale_y,
			1 * spacing_unit +(float)support_unit_number_image_1W.GetBitmapWidth() + 10,
			(globals::GAME_HEIGHT - spacing_unit / 2));
	}
	else
	{
		DrawNotLaunchReady(window_scale_x, window_scale_y,
			1 * spacing_unit + (float)support_unit_number_image_1W.GetBitmapWidth() + 10,
			(globals::GAME_HEIGHT - spacing_unit / 2));
	}

	support_unit_number_image_2W.DrawScaledImage(0, 0,
		(float)support_unit_number_image_2W.GetBitmapWidth(),
		(float)support_unit_number_image_2W.GetBitmapHeight(),
		3 * spacing_unit * window_scale_x,
		(globals::GAME_HEIGHT - spacing_unit / 2) * window_scale_y,
		(float)support_unit_number_image_2W.GetBitmapWidth() * window_scale_x,
		(float)support_unit_number_image_2W.GetBitmapHeight() * window_scale_y,
		NULL);
	if (player_1_support_unit_exist[6] == true &&
		player_1_support_unit_count[6] > player_1_support_unit_ready[6] &&
		player_1_support_unit_alive[6] == false)
	{
		DrawReadyLaunch(window_scale_x, window_scale_y,
			3 * spacing_unit + (float)support_unit_number_image_2W.GetBitmapWidth() + 10,
			(globals::GAME_HEIGHT - spacing_unit / 2));
	}
	else
	{
		DrawNotLaunchReady(window_scale_x, window_scale_y,
			3 * spacing_unit + (float)support_unit_number_image_2W.GetBitmapWidth() + 10,
			(globals::GAME_HEIGHT - spacing_unit / 2));
	}

	support_unit_number_image_3W.DrawScaledImage(0, 0,
		(float)support_unit_number_image_3W.GetBitmapWidth(),
		(float)support_unit_number_image_3W.GetBitmapHeight(),
		5 * spacing_unit * window_scale_x,
		(globals::GAME_HEIGHT - spacing_unit / 2) * window_scale_y,
		(float)support_unit_number_image_3W.GetBitmapWidth() * window_scale_x,
		(float)support_unit_number_image_3W.GetBitmapHeight() * window_scale_y,
		NULL);
	if (player_1_support_unit_exist[7] == true &&
		player_1_support_unit_count[7] > player_1_support_unit_ready[7] &&
		player_1_support_unit_alive[7] == false)
	{
		DrawReadyLaunch(window_scale_x, window_scale_y,
			5 * spacing_unit + (float)support_unit_number_image_3W.GetBitmapWidth() + 10,
			(globals::GAME_HEIGHT - spacing_unit / 2));
	}
	else
	{
		DrawNotLaunchReady(window_scale_x, window_scale_y,
			5 * spacing_unit + (float)support_unit_number_image_3W.GetBitmapWidth() + 10,
			(globals::GAME_HEIGHT - spacing_unit / 2));
	}

	support_unit_number_image_4W.DrawScaledImage(0, 0,
		(float)support_unit_number_image_4W.GetBitmapWidth(),
		(float)support_unit_number_image_4W.GetBitmapHeight(),
		7 * spacing_unit * window_scale_x,
		(globals::GAME_HEIGHT - spacing_unit / 2) * window_scale_y,
		(float)support_unit_number_image_4W.GetBitmapWidth() * window_scale_x,
		(float)support_unit_number_image_4W.GetBitmapHeight() * window_scale_y,
		NULL);
	if (player_1_support_unit_exist[8] == true &&
		player_1_support_unit_count[8] > player_1_support_unit_ready[8] &&
		player_1_support_unit_alive[8] == false)
	{
		DrawReadyLaunch(window_scale_x, window_scale_y,
			7 * spacing_unit + (float)support_unit_number_image_4W.GetBitmapWidth() + 10,
			(globals::GAME_HEIGHT - spacing_unit / 2));
	}
	else
	{
		DrawNotLaunchReady(window_scale_x, window_scale_y,
			7 * spacing_unit + (float)support_unit_number_image_4W.GetBitmapWidth() + 10,
			(globals::GAME_HEIGHT - spacing_unit / 2));
	}

	// P2 SUPPORT UNITS
	support_unit_number_image_1W.DrawScaledImage(0, 0,
		(float)support_unit_number_image_1W.GetBitmapWidth(),
		(float)support_unit_number_image_1W.GetBitmapHeight(),
		(globals::GAME_WIDTH - 10 * spacing_unit) * window_scale_x,
		(spacing_unit / 2) * window_scale_y,
		(float)support_unit_number_image_1W.GetBitmapWidth() * window_scale_x,
		(float)support_unit_number_image_1W.GetBitmapHeight() * window_scale_y,
		NULL);
	if (player_2_support_unit_exist[5] == true &&
		player_2_support_unit_count[5] > player_2_support_unit_ready[5] &&
		player_2_support_unit_alive[5] == false)
	{
		DrawReadyLaunch(window_scale_x, window_scale_y,
			(globals::GAME_WIDTH - 10 * spacing_unit) + (float)support_unit_number_image_1W.GetBitmapWidth() + 10,
			(spacing_unit / 2));
	}
	else
	{
		DrawNotLaunchReady(window_scale_x, window_scale_y,
			(globals::GAME_WIDTH - 10 * spacing_unit) + (float)support_unit_number_image_1W.GetBitmapWidth() + 10,
			(spacing_unit / 2));
	}

	support_unit_number_image_2W.DrawScaledImage(0, 0,
		(float)support_unit_number_image_2W.GetBitmapWidth(),
		(float)support_unit_number_image_2W.GetBitmapHeight(),
		(globals::GAME_WIDTH - 8 * spacing_unit) * window_scale_x,
		(spacing_unit / 2) * window_scale_y,
		(float)support_unit_number_image_2W.GetBitmapWidth() * window_scale_x,
		(float)support_unit_number_image_2W.GetBitmapHeight() * window_scale_y,
		NULL);
	if (player_2_support_unit_exist[6] == true &&
		player_2_support_unit_count[6] > player_2_support_unit_ready[6] &&
		player_2_support_unit_alive[6] == false)
	{
		DrawReadyLaunch(window_scale_x, window_scale_y,
			(globals::GAME_WIDTH - 8 * spacing_unit) + (float)support_unit_number_image_2W.GetBitmapWidth() + 10,
			(spacing_unit / 2));
	}
	else
	{
		DrawNotLaunchReady(window_scale_x, window_scale_y,
			(globals::GAME_WIDTH - 8 * spacing_unit) + (float)support_unit_number_image_2W.GetBitmapWidth() + 10,
			(spacing_unit / 2));
	}

	support_unit_number_image_3W.DrawScaledImage(0, 0,
		(float)support_unit_number_image_3W.GetBitmapWidth(),
		(float)support_unit_number_image_3W.GetBitmapHeight(),
		(globals::GAME_WIDTH - 6 * spacing_unit) * window_scale_x,
		(spacing_unit / 2) * window_scale_y,
		(float)support_unit_number_image_3W.GetBitmapWidth() * window_scale_x,
		(float)support_unit_number_image_3W.GetBitmapHeight() * window_scale_y,
		NULL);
	if (player_2_support_unit_exist[7] == true &&
		player_2_support_unit_count[7] > player_2_support_unit_ready[7] &&
		player_2_support_unit_alive[7] == false)
	{
		DrawReadyLaunch(window_scale_x, window_scale_y,
			(globals::GAME_WIDTH - 6 * spacing_unit) + (float)support_unit_number_image_3W.GetBitmapWidth() + 10,
			(spacing_unit / 2));
	}
	else
	{
		DrawNotLaunchReady(window_scale_x, window_scale_y,
			(globals::GAME_WIDTH - 6 * spacing_unit) + (float)support_unit_number_image_3W.GetBitmapWidth() + 10,
			(spacing_unit / 2));
	}

	support_unit_number_image_4W.DrawScaledImage(0, 0,
		(float)support_unit_number_image_4W.GetBitmapWidth(),
		(float)support_unit_number_image_4W.GetBitmapHeight(),
		(globals::GAME_WIDTH - 4 * spacing_unit) * window_scale_x,
		(spacing_unit / 2) * window_scale_y,
		(float)support_unit_number_image_4W.GetBitmapWidth() * window_scale_x,
		(float)support_unit_number_image_4W.GetBitmapHeight() * window_scale_y,
		NULL);
	if (player_2_support_unit_exist[8] == true &&
		player_2_support_unit_count[8] > player_2_support_unit_ready[8] &&
		player_2_support_unit_alive[8] == false)
	{
		DrawReadyLaunch(window_scale_x, window_scale_y,
			(globals::GAME_WIDTH - 4 * spacing_unit) + (float)support_unit_number_image_4W.GetBitmapWidth() + 10,
			(spacing_unit / 2));
	}
	else
	{
		DrawNotLaunchReady(window_scale_x, window_scale_y,
			(globals::GAME_WIDTH - 4 * spacing_unit) + (float)support_unit_number_image_4W.GetBitmapWidth() + 10,
			(spacing_unit / 2));
	}
}

void UnitManager::DrawReadyLaunch(float window_scale_x, float window_scale_y, 
	float position_x, float position_y)
{
	text_image_1.DrawScaledImage(0, 0,
		text_image_1.GetBitmapWidth(),
		text_image_1.GetBitmapHeight(),
		position_x * window_scale_x,
		position_y * window_scale_y,
		text_image_1.GetBitmapWidth() * window_scale_x,
		text_image_1.GetBitmapHeight() * window_scale_y,
		NULL);
}

void UnitManager::DrawNotLaunchReady(float window_scale_x, float window_scale_y, 
	float position_x, float position_y)
{
	text_image_2.DrawScaledImage(0, 0,
		text_image_2.GetBitmapWidth(),
		text_image_2.GetBitmapHeight(),
		position_x * window_scale_x,
		position_y * window_scale_y,
		text_image_2.GetBitmapWidth() * window_scale_x,
		text_image_2.GetBitmapHeight() * window_scale_y,
		NULL);
}

void UnitManager::CreateSupportUnit1(int building_type, int player)
{
	if (player == 1 && player_1_support_unit_exist[building_type] == true &&
		player_1_support_unit_count[building_type] > player_1_support_unit_ready[building_type] &&
		player_1_support_unit_alive[building_type] == false)
	{
		int random_integer = rand() % 1200;
		player_1_support_units[building_type] = new Unit(building_type, player, random_integer, 640);
		player_1_support_unit_alive[building_type] = true;
	}
}

void UnitManager::CreateSupportUnit2(int building_type, int player)
{
	if (player == 2 && player_2_support_unit_exist[building_type] == true &&
		player_2_support_unit_count[building_type] > player_2_support_unit_ready[building_type] &&
		player_2_support_unit_alive[building_type] == false)
	{
		int random_integer = rand() % 1200;
		player_2_support_units[building_type] = new Unit(building_type, player, random_integer, 50);
		player_2_support_unit_alive[building_type] = true;
	}
}

bool UnitManager::IsSupportUnitReadyToLaunch(int player, int unit_type)
{
	if (player == 1)
	{
		if (player_1_support_unit_ready[unit_type] <= player_1_support_unit_count[unit_type])
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	else
	{
		if (player_2_support_unit_ready.count(unit_type) != 0)
		{
			if (player_2_support_unit_ready[unit_type] <= player_2_support_unit_count[unit_type])
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else
			return false;
	}
}

bool UnitManager::IsSupportUnitAlive(int player, int unit_type)
{
	if (player == 1)
	{
		if (player_1_support_unit_alive.count(unit_type) != 0)
		{
			return player_1_support_unit_alive[unit_type];
		}
		else
			return false;
	}
	else
	{
		if (player_2_support_unit_alive.count(unit_type) != 0)
		{
			return player_2_support_unit_alive[unit_type];
		}
		else
			return false;
	}
}

Unit* UnitManager::GetPlayer1SupportUnit(int unit_type)
{
	return player_1_support_units[unit_type];
}

Unit* UnitManager::Getplayer2SupportUnit(int unit_type)
{
	return player_2_support_units[unit_type];
}