#pragma once

enum States
{
	SHUTDOWN, TITLE, MAINMENU, TUTORIAL, BUILDPHASE, COMBATPHASE, BUILDMENU, CONFIRMMENU, OPTIONMENU, MAPPHASE, ENDGAMEMENU, PAUSEMENU
};

enum DIRECTION
{
	UP, DOWN, LEFT, RIGHT
};

namespace globals
{
	const int GAME_WIDTH = 1280;	// Game dimension x direction
	const int GAME_HEIGHT = 720;	// Game dimension y direction

	// Scale unit images
	const int UNIT_SCALE_3 = 3;		
	const int UNIT_SCALE_4 = 4;
	const int UNIT_SCALE_5 = 5;
	const int SPRITE_SCALE = 2;		// Scale all images by multiplying it by 2

	// Ship velocity 
	const int UNIT_SPEED_2 = 2;		
	const int UNIT_SPEED_3 = 3;	
	const int UNIT_SPEED_4 = 4;	

	const int COST_MAIN_BUILDING = 500;			
	const int COST_SUPPORT_BUILDING = 750;
	const int COST_BUFF_BUILDING = 1000;
}