#include "CreateSupportUnit3P1.h"

#include "State.h"
#include "CombatPhase.h"
#include "UnitManager.h"

CreateSupportUnit3P1::~CreateSupportUnit3P1()
{
}

void CreateSupportUnit3P1::execute(State &state)
{
	state.GetCombatPhase()->GetUnitManager()->CreateSupportUnit1(7, 1);
}

