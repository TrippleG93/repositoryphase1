#include "ConfigFileReader.h"



ConfigFileReader::ConfigFileReader()
{
	file_path = "";
}

ConfigFileReader::ConfigFileReader(std::string file_path) :
	file_path(file_path)
{
	LoadConfigFile();
}


ConfigFileReader::~ConfigFileReader()
{
	al_destroy_config(cfg);
}

void ConfigFileReader::LoadConfigFile()
{
	cfg = al_load_config_file(file_path.c_str());

	if (cfg == NULL)
	{
		printf("FILE %s WAS NEVER READ PROPERLY\n", file_path.c_str());
	}
}

const char *ConfigFileReader::GetConfigValueCharacters(std::string section, std::string key)
{
	return al_get_config_value(cfg, section.c_str(), key.c_str());
}

int ConfigFileReader::GetConfigValueIntegers(std::string section, std::string key)
{
	return atoi(al_get_config_value(cfg, section.c_str(), key.c_str()));
}

double ConfigFileReader::GetConfigValueDouble(std::string section, std::string key)
{
	return atof(al_get_config_value(cfg, section.c_str(), key.c_str()));
}