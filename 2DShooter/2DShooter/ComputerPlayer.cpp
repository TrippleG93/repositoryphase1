#include <random>
#include <string>
#include <cstdlib> 
#include <ctime> 
#include <iostream>
#include <fstream>

#include "ComputerPlayer.h"
#include "Globals.h"
#include "State.h"
#include "InputHandler.h"
#include "Unit.h"
#include "UnitManager.h"
#include "BulletManager.h"
#include "Bullet.h"
#include "Player.h"
#include "CombatPhase.h"
#include "ConfigFileReader.h"
#include "BuildPhase.h"
#include "BuildingManager.h"

// BuildPhaseCommands
#include "GoToBuildMenuCommand.h"
#include "FinishBuildingCommand.h"
#include "CursorDownBuildingPhaseCommand.h"
#include "CursorRightBuildingPhaseCommand.h"
#include "CursorLeftBuildingPhaseCommand.h"

// BuildMenuCommands
#include "BuyBuildingCommand.h"
#include "CursorRightBuildMenuCommand.h"
#include "CursorDownBuildMenuCommand.h"
#include "CursorRightBuildMenuCommand.h"

// ConfirmDoneBuildingCommands
#include "ConfirmDoneBuildingCommand.h"

// CombatPhaseCommands
#include "MoveUnitLeftCommandP2.h"
#include "MoveUnitRightCommandP2.h"
#include "FireCommandP2.h"
#include "CreateSupportUnit1P2.h"
#include "CreateSupportUnit2P2.h"
#include "CreateSupportUnit3P2.h"
#include "CreateSupportUnit4P2.h"


ComputerPlayer::ComputerPlayer()
{
	SetUpComputerPlayer();
}

ComputerPlayer::~ComputerPlayer()
{
}

void ComputerPlayer::SetUpComputerPlayer()
{
	main_ship_building = false;
	frame_delay_fireP2 = 60;
	frame_delay_wander = 30;
	choose_to_evade = true;
	follow_delay = 300;
	evade_dist_to_bullet = 150;

	emulogic_font12.CreateFont("Content/Fonts/Gravedigger.ttf", 12);
	emulogic_font18.CreateFont("Content/Fonts/Gravedigger.ttf", 18);

	ConfigFileReader file("Content/Text Files/Player/player.cfg");

	ResetPurchase();

	purchases_left.CreateTextRender(emulogic_font12.ObtainFont(), al_map_rgb(255, 255, 255), "Purchases left: " + std::to_string(purchases));
	int position_x = file.GetConfigValueIntegers("purchases", "position x");
	int position_y = file.GetConfigValueIntegers("purchases", "position y");

	ConfigFileReader file2("Content/Text Files/Player/player.cfg");

	heart_image.LoadBitmap("Content/Images/Others/49-gaming-hearts-lives-game-damage-512.png");
	heart_animation.AddIndividualFrame(file2.GetConfigValueIntegers("heart image", "source x"),
		file2.GetConfigValueIntegers("heart image", "source y"),
		file2.GetConfigValueIntegers("heart image", "width"),
		file2.GetConfigValueIntegers("heart image", "height"),
		0, 0,
		file2.GetConfigValueDouble("heart image", "scale x"),
		file2.GetConfigValueDouble("heart image", "scale y"));

	lost_animation.AddIndividualFrame(file2.GetConfigValueIntegers("lost heart image", "source x"),
		file2.GetConfigValueIntegers("lost heart image", "source y"),
		file2.GetConfigValueIntegers("lost heart image", "width"),
		file2.GetConfigValueIntegers("lost heart image", "height"),
		0, 0,
		file2.GetConfigValueDouble("lost heart image", "scale x"),
		file2.GetConfigValueDouble("lost heart image", "scale y"));


	lives = file2.GetConfigValueIntegers("lives", "amount");
	max_lives = lives;
}


void ComputerPlayer::ChooseBuildingsToBuy(State &state)
{
	if (state.GetBuildPhase()->GetBuildingManager()->DoesMainShipBuildingExist(2) == false)
	{
		BuyFirstMainBuilding(state);
		BuyNextBuilding(state);
	}
	else
	{
		BuyNextBuilding(state);
	}
}

void ComputerPlayer::BuyFirstMainBuilding(State &state)
{
	if (state.GetBuildPhase()->GetBuildingManager()->DoesMainShipBuildingExist(2) == false)
	{
		int building_type = (rand() % 4) + 1;

		if (state.GetBuildPhase()->GetBuildingManager()->DoesBuildingTypeExist(1, building_type) == true)
		{
			if (building_type + 1 > 4)
			{
				building_type = 1;
			}
			else
			{
				building_type += 1;
			}
		}

		// First going to "Enter the build Menu"
		state.GetInputHandler()->GetComputerCommands().push(new GoToBuildMenuCommand);

		int current_type = 1;

		// Going to head to the location of the building in the build menu
		while (building_type != current_type)
		{
			state.GetInputHandler()->GetComputerCommands().push(new CursorRightBuildMenuCommand());
			current_type += 1;
		}

		// Now buy the building
		state.GetInputHandler()->GetComputerCommands().push(new BuyBuildingCommand());
		state.GetInputHandler()->GetComputerCommands().push(new CursorRightBuildingPhaseCommand());

	}
}

void ComputerPlayer::BuyNextBuilding(State &state)
{
		int pick = rand() % 4 + 5;
		// If spot is taken, pick the next one
		while (state.GetBuildPhase()->GetBuildingManager()->DoesBuildingExist(2, pick) == true)
		{
			pick += 1;
			if (pick == 9)
				pick = 5;
		}

	int cursor_location = 1;

	int building_type = 7;// (rand() % 8) + 5;

	int counter = 0;

	while (state.GetBuildPhase()->GetBuildingManager()->DoesBuildingTypeExist(2, building_type) == true)
	{
		if (building_type >= 12)
		{
			building_type = 5;
		}
		building_type += 1;
		counter += 1;
		if (counter == 8)
		{
			break;
		}
	}

	if (counter == 8)
	{
		state.GetInputHandler()->GetComputerCommands().push(new FinishBuildingCommand());
		state.GetInputHandler()->GetComputerCommands().push(new ConfirmDoneBuildingCommand());
	}
	else
	{
		if (state.GetBuildPhase()->GetBuildingManager()->DoesBuildingExist(2, 4) == true)
		{
			state.GetInputHandler()->GetComputerCommands().push(new CursorDownBuildingPhaseCommand());
			cursor_location += 4;
		}
		if (state.GetBuildPhase()->GetBuildingManager()->DoesBuildingExist(2, 8) == true)
		{
			state.GetInputHandler()->GetComputerCommands().push(new CursorDownBuildingPhaseCommand());
			cursor_location += 8;
		}

		while (state.GetBuildPhase()->GetBuildingManager()->DoesBuildingExist(2, cursor_location) == true)
		{
			state.GetInputHandler()->GetComputerCommands().push(new CursorRightBuildingPhaseCommand());
			cursor_location += 1;
		}

		// First going to "Enter the build Menu"
		state.GetInputHandler()->GetComputerCommands().push(new GoToBuildMenuCommand);

		int current_type = 0;

		if (building_type > 8 )
		{
			state.GetInputHandler()->GetComputerCommands().push(new CursorDownBuildMenuCommand());
			state.GetInputHandler()->GetComputerCommands().push(new CursorDownBuildMenuCommand());
			current_type = building_type - 9;
		}
		else if (building_type > 4)
		{
			state.GetInputHandler()->GetComputerCommands().push(new CursorDownBuildMenuCommand());
			current_type = building_type - 5;
		}

		for (int i = 0; i < current_type; i++)
		{
			state.GetInputHandler()->GetComputerCommands().push(new CursorRightBuildMenuCommand());
		}

		// Now buy the building
		state.GetInputHandler()->GetComputerCommands().push(new BuyBuildingCommand());


		state.GetInputHandler()->GetComputerCommands().push(new FinishBuildingCommand());
		state.GetInputHandler()->GetComputerCommands().push(new ConfirmDoneBuildingCommand());
	}

	
}

void ComputerPlayer::SetStats(int building_type) {
	// COMPUTER PLAYER STATS

	ConfigFileReader file("Content/Text Files/Units/Unit.cfg");

	std::string ship;

	switch (building_type) {
		case(1):
		ship = "ship type 1";
		break;
	case(2):
		ship = "ship type 2";
		break;
	case(3):
		ship = "ship type 3";
		break;
	case(4):
		ship = "ship type 4";
		break;
	};

	// Read in stats from file
	ComputerPlayerConfigStats.frame_delay_fireP2_min = file.GetConfigValueIntegers(ship, "frame_delay_fireP2_min");
	ComputerPlayerConfigStats.frame_delay_fireP2_max = file.GetConfigValueIntegers(ship, "frame_delay_fireP2_max") - ComputerPlayerConfigStats.frame_delay_fireP2_min;
	ComputerPlayerConfigStats.wander_min = file.GetConfigValueIntegers(ship, "wander_min");
	ComputerPlayerConfigStats.wander_max = file.GetConfigValueIntegers(ship, "wander_max") - ComputerPlayerConfigStats.wander_min;
	ComputerPlayerConfigStats.wander_often = file.GetConfigValueIntegers(ship, "wander_often");
	ComputerPlayerConfigStats.approach_dist = file.GetConfigValueIntegers(ship, "approach_dist");
	ComputerPlayerConfigStats.follow_dist = file.GetConfigValueIntegers(ship, "follow_dist");
	ComputerPlayerConfigStats.follow_delay_min = file.GetConfigValueIntegers(ship, "follow_delay_min");
	ComputerPlayerConfigStats.follow_delay_max = file.GetConfigValueIntegers(ship, "follow_delay_max") - ComputerPlayerConfigStats.follow_delay_min;
	ComputerPlayerConfigStats.evade_min = file.GetConfigValueIntegers(ship, "evade_min");
	ComputerPlayerConfigStats.evade_max = file.GetConfigValueIntegers(ship, "evade_max")- ComputerPlayerConfigStats.evade_min;
	ComputerPlayerConfigStats.evade_often = file.GetConfigValueIntegers(ship, "evade_often");
	ComputerPlayerConfigStats.stuck_delay = file.GetConfigValueIntegers(ship, "stuck_delay");

}


void ComputerPlayer::UpdateComputerPlayer()
{
	action_text.CreateTextRender(emulogic_font18.ObtainFont(), al_map_rgb(255, 0, 0), std::to_string(frame_count_moveP2));

	action_text.DrawScaledImage(0, 0,
		(float)action_text.GetBitmapWidth(),
		(float)action_text.GetBitmapHeight(),
		SPACING_UNIT,
		(SPACING_UNIT * 2),
		(float)action_text.GetBitmapWidth(),
		(float)action_text.GetBitmapHeight(),
		NULL);


}

// COMPUTER AI STATE MACHINE
Command* ComputerPlayer::AICombatPhaseCommands(State &state)
{
	// Updated player positions
	x_position_player_1 = state.GetCombatPhase()->GetUnitManager()->GetPlayer1Unit()->GetXPosition() +
		state.GetCombatPhase()->GetUnitManager()->GetPlayer1Unit()->GetSprite().GetWidth() *
		state.GetCombatPhase()->GetUnitManager()->GetPlayer1Unit()->GetSprite().GetScaleX() / 2;

	x_position_player_2 = state.GetCombatPhase()->GetUnitManager()->GetPlayer2Unit()->GetXPosition() +
		state.GetCombatPhase()->GetUnitManager()->GetPlayer2Unit()->GetSprite().GetWidth() *
		state.GetCombatPhase()->GetUnitManager()->GetPlayer2Unit()->GetSprite().GetScaleX() / 2;

	y_position_player_2 = state.GetCombatPhase()->GetUnitManager()->GetPlayer2Unit()->GetYPosition();
	position_diff = abs(x_position_player_1 - x_position_player_2);

	frame_count_moveP2 += 1;
	if (frame_count_moveP2 >= INT32_MAX)
		frame_count_moveP2 = 0;

	//*******************************************
	//Try and fire support ships
	//*******************************************
	if (state.GetCombatPhase()->GetUnitManager()->IsSupportUnitReadyToLaunch(2, 5) == true && 
		state.GetCombatPhase()->GetUnitManager()->IsSupportUnitAlive(2,5) == false)
	{
		return new CreateSupportUnit1P2();
	}
	else if (state.GetCombatPhase()->GetUnitManager()->IsSupportUnitReadyToLaunch(2, 6) == true &&
		state.GetCombatPhase()->GetUnitManager()->IsSupportUnitAlive(2, 6) == false)
	{
		return new CreateSupportUnit2P2();
	}
	else if (state.GetCombatPhase()->GetUnitManager()->IsSupportUnitReadyToLaunch(2, 7) == true &&
		state.GetCombatPhase()->GetUnitManager()->IsSupportUnitAlive(2, 7) == false)
	{
		return new CreateSupportUnit3P2();
	}
	else if (state.GetCombatPhase()->GetUnitManager()->IsSupportUnitReadyToLaunch(2, 8) == true &&
		state.GetCombatPhase()->GetUnitManager()->IsSupportUnitAlive(2, 8) == false)
	{
		return new CreateSupportUnit4P2();
	}

	
	//*******************************************
	// WHEN PLAYER 2 SHOULD FIRE
	//*******************************************
	std::vector<Bullet*>::size_type i = 0;

	if (position_diff < ComputerPlayerConfigStats.approach_dist)
	{
		if (Fire(state) == true)
		{
			return new FireCommandP2();
		}
	}


	//*******************************************
	// WHEN PLAYER 1 SUPPORT UNITS ARE LAUNCHED
	//*******************************************
	if (state.GetCombatPhase()->GetUnitManager()->IsSupportUnitAlive(1, 8)) 
	{
		// Only attack these support units directly
		action = ATTACK_SU;
		unit = 8;
		// use the evade_X variable to find how close support unit 1 is
		if (Fire(state) == true && abs(x_position_player_2 - state.GetCombatPhase()->GetUnitManager()->GetPlayer1SupportUnit(8)->GetXPosition()) < ComputerPlayerConfigStats.approach_dist)
		{
			return new FireCommandP2();
		}
		goto attack_su;
	}


	//*******************************************
	// WHERE PLAYER 2 SHOULD MOVE
	//*******************************************
	// BULLET NEAR PLAYER 2
	for (i = 0; i < state.GetCombatPhase()->GetBulletManager()->GetPlayer1Bullets().size(); i++)
	{
		if (choose_to_evade
			&& ((state.GetCombatPhase()->GetBulletManager()->GetPlayer1BulletY(i) - y_position_player_2) < evade_dist_to_bullet)
			&& (abs(state.GetCombatPhase()->GetBulletManager()->GetPlayer1BulletX(i) - x_position_player_2) < evade_dist_to_bullet / 2))
		{
			action = EVADE;
			evade_x = state.GetCombatPhase()->GetBulletManager()->GetPlayer1BulletX(i);
			goto evade;
		}
		else
		{
			choose_to_evade = (rand() % 10) < ComputerPlayerConfigStats.evade_often;
		}
	}
	// SUICIDE UNIT NEAR PLAYER 2
	if(state.GetCombatPhase()->GetUnitManager()->IsSupportUnitAlive(1,5))
	{
		// Either evade or attack support unit 1
		if ((state.GetCombatPhase()->GetUnitManager()->GetPlayer1SupportUnit(5)->GetYPosition() - y_position_player_2) > evade_dist_to_bullet)
		{
			// Only attack these support units directly
			action = ATTACK_SU;
			unit = 5;
			goto attack_su;
		}
		else if (choose_to_evade
			&& abs(state.GetCombatPhase()->GetUnitManager()->GetPlayer1SupportUnit(5)->GetXPosition() - x_position_player_2) < evade_dist_to_bullet)
		{
			action = EVADE;
			evade_x = state.GetCombatPhase()->GetUnitManager()->GetPlayer1SupportUnit(5)->GetXPosition();
			goto evade;
		}
		else 
		{
			action = SEEK;
			goto seek;
		}
	}


	// AT PLAYER 1 OR AT THE EDGE
	if (position_diff < 3 || x_position_player_2 <= (0 + 1) || x_position_player_2 >= (globals::GAME_WIDTH -1))
	{
		action = FOLLOW;
	}
	// NOT CLOSE TO PLAYER 1
	else //if (position_diff > approach_dist)
	{
		if (choose_to_seek)
			action = SEEK;
		else
		{
			action = WANDER;
		}
	}

	switch (action) {
	case STAY:
		return NULL;
	case SEEK:
/*goto*/seek:
		return Seek(state);
	case APPROACH:
		//return Approach(state);
	case EVADE:
/*goto*/evade:
		return Evade(state, evade_x);
	case FOLLOW:
		return Follow(state);
	case WANDER:
		return Wander(state);
	case FLEE:
		//return Flee(state);
	case PURSUE:
		//return Pursue(state);
	case ATTACK_SU:
/*goto*/attack_su:		
		return AttackSupportUnit(state, unit);
	default:
		return Wander(state);
	};

	return NULL;
}

Command* ComputerPlayer::Seek(State &state)
{
	// Seek left/right
	if (x_position_player_2 < x_position_player_1)
	{
		return new MoveUnitRightCommandP2();
	}
	else
	{
		return new MoveUnitLeftCommandP2();
	}

	return NULL;
}

Command* ComputerPlayer::Evade(State &state, int x_position_bullet)
{

	if (abs(x_position_bullet - x_position_player_2) <= (evade_dist_to_bullet / 2 - 5))
	{
		// Caught at the edges of the screen
		if (x_position_bullet < state.GetCombatPhase()->GetUnitManager()->GetPlayer2Unit()->GetSprite().GetWidth())
			return new MoveUnitRightCommandP2();
		else if(x_position_bullet > globals::GAME_WIDTH-state.GetCombatPhase()->GetUnitManager()->GetPlayer2Unit()->GetSprite().GetWidth())
			return new MoveUnitLeftCommandP2();

		if (x_position_player_2 < x_position_bullet)
		{
			return new MoveUnitLeftCommandP2();
		}
		else if (x_position_player_2 >= x_position_bullet)
		{
			return new MoveUnitRightCommandP2();
		}
	}
	else if (abs(x_position_bullet - x_position_player_2) <= (evade_dist_to_bullet / 2 - 2))
	{
		state.GetCombatPhase()->GetUnitManager()->GetPlayer2Unit()->IdleHorizontal();
		return NULL;
	}
	// If we are nearly finished with evading this bullet, randomly choose a different distance for the next bullet
	else
	{
		evade_dist_to_bullet = (rand() % ComputerPlayerConfigStats.evade_max) + ComputerPlayerConfigStats.evade_min;
	}

	return NULL;
}

bool ComputerPlayer::Fire(State &state)
{
	frame_count_fireP2 += 1;

	if (frame_count_fireP2 > frame_delay_fireP2)
	{
		frame_count_fireP2 = 0;

		// While Player 2 is above Player 1, how long must he wait until firing again
		frame_delay_fireP2 = (rand() % ComputerPlayerConfigStats.frame_delay_fireP2_max) + ComputerPlayerConfigStats.frame_delay_fireP2_min;

		return true;
	}

	return false;;
}

Command* ComputerPlayer::Approach(State &state)
{
	// This function's purpose is to alter incoming velocity
	if (x_position_player_2 < x_position_player_1)
	{
		return new MoveUnitRightCommandP2();
	}
	else
	{
		return new MoveUnitLeftCommandP2();
	}

	return NULL;
}

Command* ComputerPlayer::Wander(State &state)
{
	if (position_diff > globals::GAME_WIDTH/4)
	{
		choose_to_seek = true; // = SEEK
		// move right
		if(x_position_player_1 > x_position_player_2)
			return new MoveUnitRightCommandP2();
		// move left
		else
			return new MoveUnitLeftCommandP2();
	}

	// bool move_l_r:	left=false, right=true
	if (frame_count_moveP2 < frame_delay_wander)
	{	
		// move right
		if (move_l_r)
			return new MoveUnitRightCommandP2();
		// move left
		else
			return new MoveUnitLeftCommandP2();
	}
	else
	{
		frame_count_moveP2 = 0;
		frame_delay_wander = (rand() % ComputerPlayerConfigStats.wander_max) + ComputerPlayerConfigStats.wander_min;
		move_l_r = frame_delay_wander & 1;
		choose_to_seek = position_diff % 10 < ComputerPlayerConfigStats.wander_often;
		action = STAY;
	}

	return NULL;
}

Command* ComputerPlayer::Follow(State &state)
{
	if (frame_count_moveP2 >= follow_delay)
	{
		frame_count_moveP2 = 0;
		follow_delay = (rand() % ComputerPlayerConfigStats.follow_delay_max) + ComputerPlayerConfigStats.follow_delay_min;
		for (int i = 0; i < ComputerPlayerConfigStats.wander_min; i++)
			return Wander(state);
		return NULL;
	}


	state.GetCombatPhase()->GetUnitManager()->GetPlayer2Unit()->IdleHorizontal();
	return NULL;
}

Command* ComputerPlayer::AttackSupportUnit(State &state, int unit) 
{
	int x_diff = x_position_player_2 - (state.GetCombatPhase()->GetUnitManager()->GetPlayer1SupportUnit(unit)->GetXPosition() + 
		(state.GetCombatPhase()->GetUnitManager()->GetPlayer1SupportUnit(unit)->GetSprite().GetWidth() * 
		state.GetCombatPhase()->GetUnitManager()->GetPlayer1SupportUnit(unit)->GetSprite().GetScaleX()/2));

	// use the evade_X variable to find how close support unit 1 is
	if (Fire(state) &&  abs(x_diff) < ComputerPlayerConfigStats.approach_dist)
	{
		return new FireCommandP2();
	}

	if (x_diff > 2)
	{
		return new MoveUnitLeftCommandP2();
	}
	else if (x_diff < -2)
	{
		return new MoveUnitRightCommandP2();
	}
	else
		state.GetCombatPhase()->GetUnitManager()->GetPlayer2Unit()->IdleHorizontal();

	return NULL;

}

void ComputerPlayer::ResetPurchase()
{
	ConfigFileReader file("Content/Text Files/Player/player.cfg");

	if (has_main_ship == false)
	{
		purchases = file.GetConfigValueIntegers("purchases", "no main ship");
	}
	else
	{
		purchases = file.GetConfigValueIntegers("purchases", "yes main ship");
	}
}

void ComputerPlayer::Purchased()
{
	purchases -= 1;
	purchases_left.CreateTextRender(emulogic_font12.ObtainFont(), al_map_rgb(0, 0, 0), "Purchases left: " + std::to_string(purchases));
}

bool ComputerPlayer::CanPlayerPurchase()
{
	if (purchases - 1 < 0)
	{
		return false;
	}
	else
		return true;
}

void ComputerPlayer::Update()
{
	if (ready_to_lose_life == true && timer < show_lives_count)
	{
		if (timer > time_to_lose_count && just_lost_life == false)
		{
			just_lost_life = true;
			LoseALife();
		}
		timer += 1;
		if (timer >= show_lives_count)
		{
			done_losing_life = true;
		}
	}
}

void ComputerPlayer::LoseALife()
{
	lives -= 1;
}

bool ComputerPlayer::IsDead()
{
	if (lives <= 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}

void ComputerPlayer::SetTimers()
{
	ConfigFileReader file("Content/Text Files/Player/player.cfg");

	ready_to_lose_life = false;
	just_lost_life = false;
	done_losing_life = false;

	show_lives_count = file.GetConfigValueIntegers("timer", "show lives");
	time_to_lose_count = file.GetConfigValueIntegers("timer", "time to lose");
	timer = 0;
}

void ComputerPlayer::DrawHeart(int position_x, int position_y, float window_scale_x, float window_scale_y)
{
	for (int counter = 0; counter < max_lives; counter++)
	{
		if (counter < lives)
		{
			heart_image.DrawScaledImage(heart_animation.GetCurrentFrame().GetSourceX(),
				heart_animation.GetCurrentFrame().GetSourceY(),
				heart_animation.GetCurrentFrame().GetWidth(),
				heart_animation.GetCurrentFrame().GetHeight(),
				position_x + (counter * heart_animation.GetCurrentFrame().GetWidth() * heart_animation.GetCurrentFrame().GetScaleX()) * window_scale_x,
				position_y * window_scale_y,
				heart_animation.GetCurrentFrame().GetWidth() * heart_animation.GetCurrentFrame().GetScaleX() * window_scale_x,
				heart_animation.GetCurrentFrame().GetHeight() * heart_animation.GetCurrentFrame().GetScaleY() * window_scale_y,
				NULL);
		}
		else
		{
			heart_image.DrawScaledImage(lost_animation.GetCurrentFrame().GetSourceX(),
				lost_animation.GetCurrentFrame().GetSourceY(),
				lost_animation.GetCurrentFrame().GetWidth(),
				lost_animation.GetCurrentFrame().GetHeight(),
				position_x + (counter * lost_animation.GetCurrentFrame().GetWidth() * lost_animation.GetCurrentFrame().GetScaleX()) * window_scale_x,
				position_y * window_scale_y,
				lost_animation.GetCurrentFrame().GetWidth() * lost_animation.GetCurrentFrame().GetScaleX() * window_scale_x,
				lost_animation.GetCurrentFrame().GetHeight() * lost_animation.GetCurrentFrame().GetScaleY() * window_scale_y,
				NULL);
		}
	}
}
