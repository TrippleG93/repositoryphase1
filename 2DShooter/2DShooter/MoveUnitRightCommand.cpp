#include "MoveUnitRightCommand.h"

#include "State.h"

#include "UnitManager.h"
#include "Unit.h"
#include "CombatPhase.h"

MoveUnitRightCommand::~MoveUnitRightCommand()
{
}

void MoveUnitRightCommand::execute(State &state)
{
	state.GetCombatPhase()->GetUnitManager()->GetPlayer1Unit()->MoveRight();
}
