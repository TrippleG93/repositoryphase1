#include "CursorUpOptionMenuCommand.h"

#include "State.h"
#include "OptionMenu.h"

CursorUpOptionMenuCommand::~CursorUpOptionMenuCommand()
{
}

void CursorUpOptionMenuCommand::execute(State &state)
{
	state.GetOptionMenu()->MoveCursorUp();
	state.GetAudioManager().PlayCursorMove();
}