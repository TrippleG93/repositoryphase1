#pragma once
#include "allegro5\allegro.h"
#include <string>

class Display
{
public:
	Display();
	~Display();

	/* CreateDisplay
	* Creates a screen with specific width and height
	* as parameters
	*/
	void CreateDisplay(int width, int height);

	/* Destroy
	* Will destroy the display variable
	*/
	void Destroy();

	/* SetDisplayFlags
	* Initialize all the flags that can be used
	* that will alter the display based on what
	* the user wants
	*/
	void SetDisplayFlags();

	/* DisplayFullScreenMode
	* Makes the screen full screen with a window
	*/
	void DisplayFullScreenWindowMode();

	/* DisplayFullScreenMode
	* Makes the screen full screen with no window
	*/
	void DisplayFullScreenMode();

	/* DisplayWindowMode
	* Makes the screen window mode
	* Resizable and maximizable
	*/
	void DisplayWindowMode();

	/* SetDisplayPosition
	* Choose the position of where the display
	* will start off on the screen
	*/
	void SetDisplayPosition(int position_x, int position_y);

	/* GetDisplayWidth
	* Return the width of the display screen
	*/
	int GetDisplayWidth();

	/* GetDisplayHeight
	* Return the height of the display screen
	*/
	int GetDisplayHeight();

	/* SetWindowTitle
	* Change the name of the title of the top
	* window bar for the game
	*/
	void SetWindowTitle(std::string name);

	/* SetNewWindowTitle
	* Set the title that will be used when a
	* new display is created.
	*/
	void SetNewWindowTitle(std::string name);

	/* FlipDisplay
	* Everything on the backbuffer gets
	* shown on the main display
	*/
	void FlipDisplay();
	// Flip display

	ALLEGRO_DISPLAY *GetDisplay() { return display; }
	float GetDisplayScaleX() { return display_scale_x; }
	float GetDisplayScaleY() { return display_scale_y; }

private:
	ALLEGRO_DISPLAY * display;
	float display_scale_x;
	float display_scale_y;
};

