#include "BackToBuildPhaseCommand.h"

#include "State.h"
#include "Globals.h"
#include "InputHandler.h"

BackToBuildPhaseCommand::~BackToBuildPhaseCommand()
{
}

void BackToBuildPhaseCommand::execute(State &state)
{
	state.SetState(BUILDPHASE);
	state.DeleteBuildMenu();
	state.GetInputHandler()->InitBuildPhase();
	state.GetInputHandler()->ClearKeyBoardInputs();
	if (state.GetInputHandler()->GetHowManyControllers() != 0)
	{
		state.GetInputHandler()->ClearControllerInput(1);			// Clear the keyboard input
	}
	state.GetAudioManager().PlayCursorMove();
}