#include "CreateSupportUnit3P2.h"

#include "State.h"
#include "CombatPhase.h"
#include "UnitManager.h"

CreateSupportUnit3P2::~CreateSupportUnit3P2()
{
}

void CreateSupportUnit3P2::execute(State &state)
{
	state.GetCombatPhase()->GetUnitManager()->CreateSupportUnit2(7, 2);
}