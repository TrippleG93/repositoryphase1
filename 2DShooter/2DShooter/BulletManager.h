#pragma once

#include <vector>

class Bullet;
class UnitManager;

class BulletManager
{
public:
	BulletManager();

	~BulletManager();

	/*
		UpdateBullet
	*/
	void UpdateBullet();

	void RenderBulletManager(float window_scale_x, float window_scale_y);

	/*
		Creates the bullet for the player
	*/
	void CreateBullet(int player, int type, UnitManager &unit_manager);

	/*
		GetPlayer1Bulllets
		Returns the vector containing player one bullets fired
	*/
	std::vector <Bullet*> GetPlayer1Bullets();

	/*
		GetPlayer2Bullets
		Returns the vectors containing player two bullets fired
	*/
	std::vector <Bullet*> GetPlayer2Bullets();

	int GetPlayer1BulletX(std::vector<Bullet*>::size_type i);
	int GetPlayer1BulletY(std::vector<Bullet*>::size_type i);

private:

	std::vector <Bullet*> player1_bullets;
	std::vector <Bullet*> player2_bullets;

	int player1_reload_count;									// How many frames have passed 
	int player1_reload_delay = 120;								// Wait X frames before reload
	int player2_reload_count;									// How many frames have passed 
	int player2_reload_delay = 120;								// Wait X frames before reload

	int count_laser = 0;
};

