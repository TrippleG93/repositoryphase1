#pragma once
#include "Command.h"

class GoToBuildMenuCommand : public Command
{
public:
	~GoToBuildMenuCommand();

	virtual void execute() {};
	virtual void execute(State &state);
};

