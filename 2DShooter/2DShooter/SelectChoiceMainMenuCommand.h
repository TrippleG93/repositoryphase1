#pragma once

#include "Command.h"

class SelectChoiceMainMenuCommand : public Command
{
public:

	~SelectChoiceMainMenuCommand();

	virtual void execute() {};
	virtual void execute(State& state);
};

