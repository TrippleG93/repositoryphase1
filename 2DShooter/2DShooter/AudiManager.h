#pragma once

#include "AudiManager.h"
#include "Audio.h"

class AudiManager
{
public:
	AudiManager();
	~AudiManager();

	/*
		LoadMenu
		Loads all audio for the main menu,
		title screen and option menu
	*/
	void LoadMenuSong();

	void PlayAudioMenuSong();

	void StopMenuSong();

	void LoadBuildSong();

	void PlayerBuildSong();

	void StopBuildSong();

	void LoadCombatSong();

	void PlayCombatSong();

	void StopCombatSong();

	void LoadEndGameWinner();

	void PlayEndGameWinnerSong();

	void StopEndGameWinnerSong();

	void LoadEndGameLoser();

	void PlayEndGameLoserSong();

	void StopEndGameLoserSong();

	void LoadCursorMove();

	void PlayCursorMove();

	void LoadCursorSelect();

	void PlayCursorSelect();


private:
	Audio menu_song;		// Holds the audio for the main menu, tilte menu, option menu.
	Audio combat_song;		// Holds the audio for the combat phase and the pause menu
	Audio build_song;		// Holds the audio for the build phase

	Audio winner_song;		// Hold the song if they won the game
	Audio loser_song;

	Audio cursor_move;		// Holds the sound effect of the cursor moving
	Audio cursor_select;	// Holds the sound effect of the cursor selecting something

	Audio firing;
	Audio taking_damage;
};

