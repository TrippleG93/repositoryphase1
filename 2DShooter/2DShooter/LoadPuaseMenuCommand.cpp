#include "LoadPuaseMenuCommand.h"

#include "State.h"
#include "ConfirmMenu.h"


LoadPuaseMenuCommand::~LoadPuaseMenuCommand()
{
}

void LoadPuaseMenuCommand::execute(State &state)
{
	state.GetConfirmMenu()->SetUpQuitMenu();
}
