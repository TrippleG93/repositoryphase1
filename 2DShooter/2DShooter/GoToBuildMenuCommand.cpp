#include "GoToBuildMenuCommand.h"

#include "State.h"
#include "Globals.h"
#include "InputHandler.h"
#include "BuildingManager.h"

#include "BuildPhase.h"

GoToBuildMenuCommand::~GoToBuildMenuCommand()
{
}

void GoToBuildMenuCommand::execute(State &state)
{
	if (state.GetBuildPhase()->GetBuildingManager()->DoesBuildingExist(state.GetBuildPhase()->GetCurrentBuildPlayer(),
		state.GetBuildPhase()->GetCursorLocation()) == false)
	{
		state.GetInputHandler()->DeleteCurrentHandler();		// Removes the control scheme of the build phase
		state.SetState(BUILDMENU);								// Change the state to the build menu
		state.InitializeBuildMenu();							// Create the build menu class and set controls for it
		state.GetInputHandler()->ClearKeyBoardInputs();			// Clear the keyboard inputs 
		if (state.GetInputHandler()->GetHowManyControllers() != 0)
		{
			state.GetInputHandler()->ClearControllerInput(1);	// Clear the controller inputs
		}
		state.GetAudioManager().PlayCursorSelect();
	}
}