#include "Building.h"
#include "Globals.h"

#include "ConfigFileReader.h"

Building::Building()
{
	building_type = -1;
}

Building::Building(int type, int row, int column) :
	building_type(type),
	row(row),
	column(column)
{
	SetUpBuilding();
}

Building::~Building()
{
}

void Building::SetUpBuilding()
{
	ConfigFileReader file("Content/Text Files/Buildings/Buildings.cfg");

	upgrade_level = 0;

	switch (building_type)
	{
		// !!! SEE BOTTOM OF CLASS FOR CASES 1-4 BUILDINGS
	case 1:
		ship_image.LoadBitmap("Content/Images/Ships/destroyer.png");
		ship_animation.AddIndividualFrame(file.GetConfigValueIntegers("ship type 1", "source x"),
			file.GetConfigValueIntegers("ship type 1", "source y"),
			file.GetConfigValueIntegers("ship type 1", "width"),
			file.GetConfigValueIntegers("ship type 1", "height"),
			file.GetConfigValueIntegers("ship type 1", "position x"),
			file.GetConfigValueIntegers("ship type 1", "position y"),
			file.GetConfigValueDouble("ship type 1", "scale x"),
			file.GetConfigValueDouble("ship type 1", "scale y"));
		ship_animation.CombineFrames(file.GetConfigValueIntegers("ship type 1", "frame"), false);
		is_ship = true;
		break;
	case 2:
		ship_image.LoadBitmap("Content/Images/Ships/blue1.png");
		ship_animation.AddIndividualFrame(file.GetConfigValueIntegers("ship type 2", "source x"),
			file.GetConfigValueIntegers("ship type 2", "source y"),
			file.GetConfigValueIntegers("ship type 2", "width"),
			file.GetConfigValueIntegers("ship type 2", "height"),
			file.GetConfigValueIntegers("ship type 2", "position x"),
			file.GetConfigValueIntegers("ship type 2", "position y"),
			file.GetConfigValueDouble("ship type 2", "scale x"),
			file.GetConfigValueDouble("ship type 2", "scale y"));
		ship_animation.CombineFrames(file.GetConfigValueIntegers("ship type 1", "frame"), false);
		is_ship = true;
		break;
	case 3:
		ship_image.LoadBitmap("Content/Images/Ships/smallgreen.png");
		ship_animation.AddIndividualFrame(file.GetConfigValueIntegers("ship type 3", "source x"),
			file.GetConfigValueIntegers("ship type 3", "source y"),
			file.GetConfigValueIntegers("ship type 3", "width"),
			file.GetConfigValueIntegers("ship type 3", "height"),
			file.GetConfigValueIntegers("ship type 3", "position x"),
			file.GetConfigValueIntegers("ship type 3", "position y"),
			file.GetConfigValueDouble("ship type 3", "scale x"),
			file.GetConfigValueDouble("ship type 3", "scale y"));
		ship_animation.CombineFrames(file.GetConfigValueIntegers("ship type 3", "frame"), false);
		is_ship = true;
		break;
	case 4:
		ship_image.LoadBitmap("Content/Images/Ships/topdownfighter.png");
		ship_animation.AddIndividualFrame(file.GetConfigValueIntegers("ship type 4", "source x"),
			file.GetConfigValueIntegers("ship type 4", "source y"),
			file.GetConfigValueIntegers("ship type 4", "width"),
			file.GetConfigValueIntegers("ship type 4", "height"),
			file.GetConfigValueIntegers("ship type 4", "position x"),
			file.GetConfigValueIntegers("ship type 4", "position y"),
			file.GetConfigValueDouble("ship type 4", "scale x"),
			file.GetConfigValueDouble("ship type 4", "scale y"));
		ship_animation.CombineFrames(file.GetConfigValueIntegers("ship type 4", "frame"), false);
		is_ship = true;
		break;
	case 5:
		ship_image.LoadBitmap("Content/Images/Ships/tribase-u2-d0.png");
		ship_animation.AddIndividualFrame(file.GetConfigValueIntegers("ship type 5", "source x"),
			file.GetConfigValueIntegers("ship type 5", "source y"),
			file.GetConfigValueIntegers("ship type 5", "width"),
			file.GetConfigValueIntegers("ship type 5", "height"),
			file.GetConfigValueIntegers("ship type 5", "position x"),
			file.GetConfigValueIntegers("ship type 5", "position y"),
			file.GetConfigValueDouble("ship type 5", "scale x"),
			file.GetConfigValueDouble("ship type 5", "scale y"));
		ship_animation.CombineFrames(file.GetConfigValueIntegers("ship type 5", "frame"), false);
		is_ship = true;
		break;
	case 6:
		ship_image.LoadBitmap("Content/Images/Ships/ship6.png");
		ship_animation.AddIndividualFrame(file.GetConfigValueIntegers("ship type 6", "source x"),
			file.GetConfigValueIntegers("ship type 6", "source y"),
			file.GetConfigValueIntegers("ship type 6", "width"),
			file.GetConfigValueIntegers("ship type 6", "height"),
			file.GetConfigValueIntegers("ship type 6", "position x"),
			file.GetConfigValueIntegers("ship type 6", "position y"),
			file.GetConfigValueDouble("ship type 6", "scale x"),
			file.GetConfigValueDouble("ship type 6", "scale y"));
		ship_animation.CombineFrames(file.GetConfigValueIntegers("ship type 6", "frame"), false);
		is_ship = true;
		break;
	case 7:
		ship_image.LoadBitmap("Content/Images/Ships/redship4.png");
		ship_animation.AddIndividualFrame(file.GetConfigValueIntegers("ship type 7", "source x"),
			file.GetConfigValueIntegers("ship type 7", "source y"),
			file.GetConfigValueIntegers("ship type 7", "width"),
			file.GetConfigValueIntegers("ship type 7", "height"),
			file.GetConfigValueIntegers("ship type 7", "position x"),
			file.GetConfigValueIntegers("ship type 7", "position y"),
			file.GetConfigValueDouble("ship type 7", "scale x"),
			file.GetConfigValueDouble("ship type 7", "scale y"));
		ship_animation.CombineFrames(file.GetConfigValueIntegers("ship type 7", "frame"), false);
		is_ship = true;
		break;
	case 8:
		ship_image.LoadBitmap("Content/Images/Ships/Spacestation-by-MillionthVector.png");
		ship_animation.AddIndividualFrame(file.GetConfigValueIntegers("ship type 8", "source x"),
			file.GetConfigValueIntegers("ship type 8", "source y"),
			file.GetConfigValueIntegers("ship type 8", "width"),
			file.GetConfigValueIntegers("ship type 8", "height"),
			file.GetConfigValueIntegers("ship type 8", "position x"),
			file.GetConfigValueIntegers("ship type 8", "position y"),
			file.GetConfigValueDouble("ship type 8", "scale x"),
			file.GetConfigValueDouble("ship type 8", "scale y"));
		ship_animation.CombineFrames(file.GetConfigValueIntegers("ship type 8", "frame"), false);
		is_ship = true;
		break;
	case 9:
		building_image.LoadBitmap("Content/Images/Buildings/Mobile - Breath of Fire 6 - Item Boosters.png");
		building_animation.AddIndividualFrame(file.GetConfigValueIntegers("building type 1", "source x"),
			file.GetConfigValueIntegers("building type 1", "source y"),
			file.GetConfigValueIntegers("building type 1", "width"),
			file.GetConfigValueIntegers("building type 1", "height"),
			file.GetConfigValueIntegers("building type 1", "position x"),
			file.GetConfigValueIntegers("building type 1", "position y"),
			file.GetConfigValueDouble("building type 1", "scale x"),
			file.GetConfigValueDouble("building type 1", "scale y"));
		building_animation.CombineFrames(file.GetConfigValueIntegers("building type 1", "frame"), false);
		is_ship = false;
		break;
	case 10:
		building_image.LoadBitmap("Content/Images/Buildings/Mobile - Breath of Fire 6 - Item Boosters.png");
		building_animation.AddIndividualFrame(file.GetConfigValueIntegers("building type 2", "source x"),
			file.GetConfigValueIntegers("building type 2", "source y"),
			file.GetConfigValueIntegers("building type 2", "width"),
			file.GetConfigValueIntegers("building type 2", "height"),
			file.GetConfigValueIntegers("building type 2", "position x"),
			file.GetConfigValueIntegers("building type 2", "position y"),
			file.GetConfigValueDouble("building type 2", "scale x"),
			file.GetConfigValueDouble("building type 2", "scale y"));
		building_animation.CombineFrames(file.GetConfigValueIntegers("building type 2", "frame"), false);
		is_ship = false;
		break;
	case 11:
		building_image.LoadBitmap("Content/Images/Buildings/Mobile - Breath of Fire 6 - Item Boosters.png");
		building_animation.AddIndividualFrame(file.GetConfigValueIntegers("building type 3", "source x"),
			file.GetConfigValueIntegers("building type 3", "source y"),
			file.GetConfigValueIntegers("building type 3", "width"),
			file.GetConfigValueIntegers("building type 3", "height"),
			file.GetConfigValueIntegers("building type 3", "position x"),
			file.GetConfigValueIntegers("building type 3", "position y"),
			file.GetConfigValueDouble("building type 3", "scale x"),
			file.GetConfigValueDouble("building type 3", "scale y"));
		building_animation.CombineFrames(file.GetConfigValueIntegers("building type 3", "frame"), false);
		is_ship = false;
		break;
	case 12:
		building_image.LoadBitmap("Content/Images/Buildings/Mobile - Breath of Fire 6 - Item Boosters.png");
		building_animation.AddIndividualFrame(file.GetConfigValueIntegers("building type 4", "source x"),
			file.GetConfigValueIntegers("building type 4", "source y"),
			file.GetConfigValueIntegers("building type 4", "width"),
			file.GetConfigValueIntegers("building type 4", "height"),
			file.GetConfigValueIntegers("building type 4", "position x"),
			file.GetConfigValueIntegers("building type 4", "position y"),
			file.GetConfigValueDouble("building type 4", "scale x"),
			file.GetConfigValueDouble("building type 4", "scale y"));
		building_animation.CombineFrames(file.GetConfigValueIntegers("building type 4", "frame"), false);
		is_ship = false;
		break;
	default:
		break;
	}
}

void Building::UpdateBuilding()
{
	building_animation.Update();
}

void Building::RenderBuilding(float window_scale_x, float window_scale_y)
{
	if(is_ship == false)
		building_image.DrawScaledImage((float)building_animation.GetCurrentFrame().GetSourceX(),
			(float)building_animation.GetCurrentFrame().GetSourceY(),
			(float)building_animation.GetCurrentFrame().GetWidth(),
			(float)building_animation.GetCurrentFrame().GetHeight(),
			(building_animation.GetCurrentFrame().GetPositionX() + (75 + ((column - 1) * 200))) * window_scale_x,
			(building_animation.GetCurrentFrame().GetPositionY() + (75 + ((row - 1) * 200))) * window_scale_y,
			(float)building_animation.GetCurrentFrame().GetWidth() * window_scale_x * building_animation.GetCurrentFrame().GetScaleX(),
			(float)building_animation.GetCurrentFrame().GetHeight() * window_scale_y * building_animation.GetCurrentFrame().GetScaleY(),
			NULL);
	else 
		ship_image.DrawScaledImage((float)ship_animation.GetCurrentFrame().GetSourceX(),
			(float)ship_animation.GetCurrentFrame().GetSourceY(),
			(float)ship_animation.GetCurrentFrame().GetWidth(),
			(float)ship_animation.GetCurrentFrame().GetHeight(),
			(ship_animation.GetCurrentFrame().GetPositionX() + (115 + ((column - 1) * 200))) * window_scale_x,
			(ship_animation.GetCurrentFrame().GetPositionY() + (50 + ((row - 1) * 200))) * window_scale_y,
			(float)ship_animation.GetCurrentFrame().GetWidth() * window_scale_x * ship_animation.GetCurrentFrame().GetScaleX(),
			(float)ship_animation.GetCurrentFrame().GetHeight() * window_scale_y * ship_animation.GetCurrentFrame().GetScaleY(),
			NULL);

}

void Building::LoadBuilding()
{

}

int Building::GetBuildingType()
{
	return building_type;
}


/* CASES 1-4 BUILDINGS

case 1:
building_image.LoadBitmap("Content/Images/Buildings/econU_buildings_1.png");
building_animation.AddIndividualFrame(0, 22, 255, 138);
building_animation.CombineFrames(10, false);
break;
case 2:
building_image.LoadBitmap("Content/Images/Buildings/econU_buildings_1.png");
building_animation.AddIndividualFrame(257, 20, 254, 140);
building_animation.CombineFrames(10, false);
break;
case 3:
building_image.LoadBitmap("Content/Images/Buildings/econU_buildings_1.png");
building_animation.AddIndividualFrame(513, 21, 254, 139);
building_animation.CombineFrames(10, false);
break;
case 4:
building_image.LoadBitmap("Content/Images/Buildings/econU_buildings_1.png");
building_animation.AddIndividualFrame(769, 24, 254, 136);
building_animation.CombineFrames(10, false);
break;

*/