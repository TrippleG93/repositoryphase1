#pragma once

#include "Command.h"

class CursorLeftBuildMenuCommand : public Command
{
public:

	~CursorLeftBuildMenuCommand();

	virtual void execute() {};
	virtual void execute(State &state);
};

