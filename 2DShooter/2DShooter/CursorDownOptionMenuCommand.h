#pragma once

#include "Command.h"

class CursorDownOptionMenuCommand: public Command
{
public:
	~CursorDownOptionMenuCommand();

	virtual void execute() {};
	virtual void execute(State &state);
};

