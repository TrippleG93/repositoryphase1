#include "BuildPhase.h"

#include "Globals.h"
#include "BuildingManager.h"
#include "State.h"
#include "ComputerPlayer.h"
#include "ConfigFileReader.h"
#include "Font.h"

BuildPhase::BuildPhase()
{
	SetUpBuildingPhase();
}

BuildPhase::~BuildPhase()
{
	delete(building_manager);
}

void BuildPhase::SetUpBuildingPhase()
{
	computer_player = true;
	computer_player_turn = false;
	current_build_player = 1;

	building_manager = new BuildingManager();

	cursor_row = 1;
	cursor_column = 1;

	cursor1_choice = 1;

	for (int i = 0; i < 12; i++)
	{
		building_spots[i] = -1;							// -1 means that a buildng does not eist
	}
	ConfigFileReader file("Content/Text Files/Menus/BuildMenu.cfg");

	platform_image.LoadBitmap("Content/Images/Buildings/Floating_Platform.png");

	platform_animation.AddIndividualFrame(37, 4, 98, 93);
	platform_animation.AddIndividualFrame(149, 4, 98, 93);
	platform_animation.AddIndividualFrame(262, 4, 98, 93);
	platform_animation.AddIndividualFrame(373, 4, 98, 93);
	platform_animation.CombineFrames(10, true);

	emulogic_font18.CreateFont("Content/Fonts/Gravedigger.ttf", 18);

	title_text_image.CreateTextRender(emulogic_font18.ObtainFont(), al_map_rgb(255, 255, 255), "Building Phase");

	space_image.LoadBitmap("Content/Images/Combat Phase Backgrounds/PC Computer - Space Pirates and Zombies - Backgrounds 22/background_blue_5.png");

	cursor_image.LoadBitmap("Content/Images/Others/Cursor.png");

	cursor1_animation.AddIndividualFrame(5, 6, 22, 23);
	cursor1_animation.AddIndividualFrame(5, 38, 21, 21);
	cursor1_animation.AddIndividualFrame(5, 70, 19, 22);
	cursor1_animation.AddIndividualFrame(5, 102, 23, 22);
	cursor1_animation.AddIndividualFrame(5, 134, 24, 22);
	cursor1_animation.AddIndividualFrame(4, 198, 22, 20);
	cursor1_animation.AddIndividualFrame(5, 230, 24, 21);
	cursor1_animation.CombineFrames(8, true);

	cursor2_animation.AddIndividualFrame(34, 6, 22, 22);
	cursor2_animation.AddIndividualFrame(34, 38, 21, 20);
	cursor2_animation.AddIndividualFrame(34, 70, 17, 20);
	cursor2_animation.AddIndividualFrame(34, 101, 22, 21);
	cursor2_animation.AddIndividualFrame(34, 133, 22, 22);
	cursor2_animation.AddIndividualFrame(34, 165, 21, 19);
	cursor2_animation.AddIndividualFrame(34, 197, 18, 20);
	cursor2_animation.AddIndividualFrame(34, 229, 22, 20);
	cursor2_animation.CombineFrames(10, true);


	controller_home_image.LoadBitmap("Content/Images/Others/xbox_home_logo.png");
	controller_Home_animation.AddIndividualFrame(file.GetConfigValueIntegers("controller button Home", "source x"),
		file.GetConfigValueIntegers("controller button Home", "source y"),
		file.GetConfigValueIntegers("controller button Home", "width"),
		file.GetConfigValueIntegers("controller button Home", "height"),
		file.GetConfigValueIntegers("controller button Home", "position x"),
		file.GetConfigValueIntegers("controller button Home", "position y"),
		file.GetConfigValueDouble("controller button Home", "scale x"),
		file.GetConfigValueDouble("controller button Home", "scale y"));
	controller_Home_animation.CombineFrames(file.GetConfigValueIntegers("controller button Home", "frame"), false);

	controller_image.LoadBitmap("Content/Images/Others/xbox_buttons.png");
	controller_LStick_animation.AddIndividualFrame(file.GetConfigValueIntegers("controller button LStick", "source x"),
		file.GetConfigValueIntegers("controller button LStick", "source y"),
		file.GetConfigValueIntegers("controller button LStick", "width"),
		file.GetConfigValueIntegers("controller button LStick", "height"),
		file.GetConfigValueIntegers("controller button LStick", "position x"),
		file.GetConfigValueIntegers("controller button LStick", "position y"),
		file.GetConfigValueDouble("controller button LStick", "scale x"),
		file.GetConfigValueDouble("controller button LStick", "scale y"));
	controller_Home_animation.CombineFrames(file.GetConfigValueIntegers("controller button LStick", "frame"), false);

	controller_A_animation.AddIndividualFrame(file.GetConfigValueIntegers("controller button A", "source x"),
		file.GetConfigValueIntegers("controller button A", "source y"),
		file.GetConfigValueIntegers("controller button A", "width"),
		file.GetConfigValueIntegers("controller button A", "height"),
		file.GetConfigValueIntegers("controller button A", "position x"),
		file.GetConfigValueIntegers("controller button A", "position y"),
		file.GetConfigValueDouble("controller button A", "scale x"),
		file.GetConfigValueDouble("controller button A", "scale y"));
	controller_Home_animation.CombineFrames(file.GetConfigValueIntegers("controller button A", "frame"), false);

	controller_B_animation.AddIndividualFrame(file.GetConfigValueIntegers("controller button B", "source x"),
		file.GetConfigValueIntegers("controller button B", "source y"),
		file.GetConfigValueIntegers("controller button B", "width"),
		file.GetConfigValueIntegers("controller button B", "height"),
		file.GetConfigValueIntegers("controller button B", "position x"),
		file.GetConfigValueIntegers("controller button B", "position y"),
		file.GetConfigValueDouble("controller button B", "scale x"),
		file.GetConfigValueDouble("controller button B", "scale y"));
	controller_Home_animation.CombineFrames(file.GetConfigValueIntegers("controller button B", "frame"), false);

	controller_Start_animation.AddIndividualFrame(file.GetConfigValueIntegers("controller button Start", "source x"),
		file.GetConfigValueIntegers("controller button Start", "source y"),
		file.GetConfigValueIntegers("controller button Start", "width"),
		file.GetConfigValueIntegers("controller button Start", "height"),
		file.GetConfigValueIntegers("controller button Start", "position x"),
		file.GetConfigValueIntegers("controller button Start", "position y"),
		file.GetConfigValueDouble("controller button Start", "scale x"),
		file.GetConfigValueDouble("controller button Start", "scale y"));
	controller_Home_animation.CombineFrames(file.GetConfigValueIntegers("controller button Start", "frame"), false);


	emulogic_font8.CreateFont("Content/Fonts/Gravedigger.ttf", file.GetConfigValueIntegers("font 2", "size"));
	controls_1.CreateTextRender(emulogic_font8.ObtainFont(),
		al_map_rgb(file.GetConfigValueIntegers("controls text 1", "text r"),
			file.GetConfigValueIntegers("controls text 1", "text g"),
			file.GetConfigValueIntegers("controls text 1", "text b")),
		"CONTROLS:  KEYBOARD  / ");
	controls_text_1_x = file.GetConfigValueIntegers("controls text 1", "position x");
	controls_text_1_y = file.GetConfigValueIntegers("controls text 1", "position y");

	controls_2.CreateTextRender(emulogic_font8.ObtainFont(),
		al_map_rgb(file.GetConfigValueIntegers("controls text 2", "text r"),
			file.GetConfigValueIntegers("controls text 2", "text g"),
			file.GetConfigValueIntegers("controls text 2", "text b")),
		"MOVEMENT:  'W-A-S-D'  /");
	controls_text_2_x = file.GetConfigValueIntegers("controls text 2", "position x");
	controls_text_2_y = file.GetConfigValueIntegers("controls text 2", "position y");

	controls_3.CreateTextRender(emulogic_font8.ObtainFont(),
		al_map_rgb(file.GetConfigValueIntegers("controls text 3", "text r"),
			file.GetConfigValueIntegers("controls text 3", "text g"),
			file.GetConfigValueIntegers("controls text 3", "text b")),
		"SELECT:  'ENTER'  /");
	controls_text_3_x = file.GetConfigValueIntegers("controls text 3", "position x");
	controls_text_3_y = file.GetConfigValueIntegers("controls text 3", "position y");

	controls_4.CreateTextRender(emulogic_font8.ObtainFont(),
		al_map_rgb(file.GetConfigValueIntegers("controls text 4", "text r"),
			file.GetConfigValueIntegers("controls text 4", "text g"),
			file.GetConfigValueIntegers("controls text 4", "text b")),
		"CONFIRM BUILD:  'C'  /");
	controls_text_4_x = file.GetConfigValueIntegers("controls text 4", "position x");
	controls_text_4_y = file.GetConfigValueIntegers("controls text 4", "position y");

	controls_5.CreateTextRender(emulogic_font8.ObtainFont(),
		al_map_rgb(file.GetConfigValueIntegers("controls text 5", "text r"),
			file.GetConfigValueIntegers("controls text 5", "text g"),
			file.GetConfigValueIntegers("controls text 5", "text b")),
		"GO BACK:  'BACKSPACE'  /");
	controls_text_5_x = file.GetConfigValueIntegers("controls text 5", "position x");
	controls_text_5_y = file.GetConfigValueIntegers("controls text 5", "position y");

	lives_text_image.CreateTextRender(emulogic_font18.ObtainFont(), al_map_rgb(255, 255, 255), "Lives Remaining");
	lives_text_x = file.GetConfigValueIntegers("lives image", "position x");
	lives_text_y = file.GetConfigValueIntegers("lives image", "position y");
}

void BuildPhase::UpdateBuildingPhase()
{
	platform_animation.Update();
	cursor1_animation.Update();
}

void BuildPhase::RenderBuildingPhase(float window_scale_x, float window_scale_y)
{
		space_image.DrawScaledImage(0, 0,
		space_image.GetBitmapWidth(),
		space_image.GetBitmapHeight(),
		0 * window_scale_x,
		0 * window_scale_y,
		(float)space_image.GetBitmapWidth() * window_scale_x,
		(float)space_image.GetBitmapHeight() * window_scale_y,
		NULL);

	cursor_image.DrawScaledImage(cursor1_animation.GetCurrentFrame().GetSourceX(),
		cursor1_animation.GetCurrentFrame().GetSourceY(),
		cursor1_animation.GetCurrentFrame().GetHeight(),
		cursor1_animation.GetCurrentFrame().GetWidth(),
		(175 + ((cursor_column - 1) * 200)) * window_scale_x,
		(125 + ((cursor_row - 1) * 200)) * window_scale_y,
		cursor1_animation.GetCurrentFrame().GetWidth() * window_scale_x,
		cursor1_animation.GetCurrentFrame().GetHeight() * window_scale_y,
		NULL);

	title_text_image.DrawScaledImage(0, 0,
		title_text_image.GetBitmapWidth(),
		title_text_image.GetBitmapHeight(),
		400 * window_scale_x,
		10 * window_scale_y,
		title_text_image.GetBitmapWidth() * window_scale_x,
		title_text_image.GetBitmapHeight() * window_scale_y,
		NULL);

	controls_1.DrawScaledImage(0, 0,
		(float)controls_1.GetBitmapWidth(),
		(float)controls_1.GetBitmapHeight(),
		controls_text_1_x * window_scale_x,
		controls_text_1_y * window_scale_y,
		(float)controls_1.GetBitmapWidth() * window_scale_x,
		(float)controls_1.GetBitmapHeight() * window_scale_y,
		NULL); 
	
	controller_home_image.DrawScaledImage((float)controller_Home_animation.GetCurrentFrame().GetSourceX(),
		(float)controller_Home_animation.GetCurrentFrame().GetSourceY(),
		(float)controller_Home_animation.GetCurrentFrame().GetWidth(),
		(float)controller_Home_animation.GetCurrentFrame().GetHeight(),
		controller_Home_animation.GetCurrentFrame().GetPositionX() * window_scale_x,
		controller_Home_animation.GetCurrentFrame().GetPositionY() * window_scale_y,
		(float)controller_Home_animation.GetCurrentFrame().GetWidth() * window_scale_x * controller_Home_animation.GetCurrentFrame().GetScaleX(),
		(float)controller_Home_animation.GetCurrentFrame().GetHeight() * window_scale_y * controller_Home_animation.GetCurrentFrame().GetScaleY(),
		NULL);

	controls_2.DrawScaledImage(0, 0,
		(float)controls_2.GetBitmapWidth(),
		(float)controls_2.GetBitmapHeight(),
		controls_text_2_x * window_scale_x,
		controls_text_2_y * window_scale_y,
		(float)controls_2.GetBitmapWidth() * window_scale_x,
		(float)controls_2.GetBitmapHeight() * window_scale_y,
		NULL);

	controller_image.DrawScaledImage((float)controller_LStick_animation.GetCurrentFrame().GetSourceX(),
		(float)controller_LStick_animation.GetCurrentFrame().GetSourceY(),
		(float)controller_LStick_animation.GetCurrentFrame().GetWidth(),
		(float)controller_LStick_animation.GetCurrentFrame().GetHeight(),
		controller_LStick_animation.GetCurrentFrame().GetPositionX() * window_scale_x,
		controller_LStick_animation.GetCurrentFrame().GetPositionY() * window_scale_y,
		(float)controller_LStick_animation.GetCurrentFrame().GetWidth() * window_scale_x * controller_LStick_animation.GetCurrentFrame().GetScaleX(),
		(float)controller_LStick_animation.GetCurrentFrame().GetHeight() * window_scale_y * controller_LStick_animation.GetCurrentFrame().GetScaleY(),
		NULL);

	controls_3.DrawScaledImage(0, 0,
		(float)controls_3.GetBitmapWidth(),
		(float)controls_3.GetBitmapHeight(),
		controls_text_3_x * window_scale_x,
		controls_text_3_y * window_scale_y,
		(float)controls_3.GetBitmapWidth() * window_scale_x,
		(float)controls_3.GetBitmapHeight() * window_scale_y,
		NULL);

	controller_image.DrawScaledImage((float)controller_A_animation.GetCurrentFrame().GetSourceX(),
		(float)controller_A_animation.GetCurrentFrame().GetSourceY(),
		(float)controller_A_animation.GetCurrentFrame().GetWidth(),
		(float)controller_A_animation.GetCurrentFrame().GetHeight(),
		controller_A_animation.GetCurrentFrame().GetPositionX() * window_scale_x,
		controller_A_animation.GetCurrentFrame().GetPositionY() * window_scale_y,
		(float)controller_A_animation.GetCurrentFrame().GetWidth() * window_scale_x * controller_A_animation.GetCurrentFrame().GetScaleX(),
		(float)controller_A_animation.GetCurrentFrame().GetHeight() * window_scale_y * controller_A_animation.GetCurrentFrame().GetScaleY(),
		NULL);

	controls_4.DrawScaledImage(0, 0,
		(float)controls_4.GetBitmapWidth(),
		(float)controls_4.GetBitmapHeight(),
		controls_text_4_x * window_scale_x,
		controls_text_4_y * window_scale_y,
		(float)controls_4.GetBitmapWidth() * window_scale_x,
		(float)controls_4.GetBitmapHeight() * window_scale_y,
		NULL);

	controller_image.DrawScaledImage((float)controller_Start_animation.GetCurrentFrame().GetSourceX(),
		(float)controller_Start_animation.GetCurrentFrame().GetSourceY(),
		(float)controller_Start_animation.GetCurrentFrame().GetWidth(),
		(float)controller_Start_animation.GetCurrentFrame().GetHeight(),
		controller_Start_animation.GetCurrentFrame().GetPositionX() * window_scale_x,
		controller_Start_animation.GetCurrentFrame().GetPositionY() * window_scale_y,
		(float)controller_Start_animation.GetCurrentFrame().GetWidth() * window_scale_x * controller_Start_animation.GetCurrentFrame().GetScaleX(),
		(float)controller_Start_animation.GetCurrentFrame().GetHeight() * window_scale_y * controller_Start_animation.GetCurrentFrame().GetScaleY(),
		NULL);

	controls_5.DrawScaledImage(0, 0,
		(float)controls_5.GetBitmapWidth(),
		(float)controls_5.GetBitmapHeight(),
		controls_text_5_x * window_scale_x,
		controls_text_5_y * window_scale_y,
		(float)controls_5.GetBitmapWidth() * window_scale_x,
		(float)controls_5.GetBitmapHeight() * window_scale_y,
		NULL);

	controller_image.DrawScaledImage((float)controller_B_animation.GetCurrentFrame().GetSourceX(),
		(float)controller_B_animation.GetCurrentFrame().GetSourceY(),
		(float)controller_B_animation.GetCurrentFrame().GetWidth(),
		(float)controller_B_animation.GetCurrentFrame().GetHeight(),
		controller_B_animation.GetCurrentFrame().GetPositionX() * window_scale_x,
		controller_B_animation.GetCurrentFrame().GetPositionY() * window_scale_y,
		(float)controller_B_animation.GetCurrentFrame().GetWidth() * window_scale_x * controller_B_animation.GetCurrentFrame().GetScaleX(),
		(float)controller_B_animation.GetCurrentFrame().GetHeight() * window_scale_y * controller_B_animation.GetCurrentFrame().GetScaleY(),
		NULL);

	lives_text_image.DrawScaledImage(0, 0,
		lives_text_image.GetBitmapWidth(),
		lives_text_image.GetBitmapHeight(),
		lives_text_x * window_scale_x,
		lives_text_y * window_scale_y,
		lives_text_image.GetBitmapWidth() * window_scale_x,
		lives_text_image.GetBitmapHeight() * window_scale_y,
		NULL);

	int x_position = 100;
	int y_position = 150;


	for (int i = 0; i < 12; i++)
	{

		if (i % 4 == 0 && i != 0)
		{
			y_position += 200;
			x_position = 100;
		}

		platform_image.DrawScaledImage(platform_animation.GetCurrentFrame().GetSourceX(),
			platform_animation.GetCurrentFrame().GetSourceY(),
			platform_animation.GetCurrentFrame().GetWidth(),
			platform_animation.GetCurrentFrame().GetHeight(),
			x_position * window_scale_x,
			y_position * window_scale_y,
			platform_animation.GetCurrentFrame().GetWidth() * window_scale_x,
			platform_animation.GetCurrentFrame().GetHeight() * window_scale_y,
			NULL);

		x_position += 200;
	}

	building_manager->RenderBuildings(current_build_player, window_scale_x, window_scale_y);
}

void BuildPhase::MoveCursorDown()
{
	if (cursor_row + 1 < 4)
	{
		cursor_row += 1;
		cursor1_choice += 4;
	}
}

void BuildPhase::MoveCursorUp()
{
	if (cursor_row - 1 > 0)
	{
		cursor_row -= 1;
		cursor1_choice -= 4;
	}
}

void BuildPhase::MoveCursorLeft()
{
	if (cursor_column - 1 > 0)
	{
		cursor_column -= 1;
		cursor1_choice -= 1;
	}
}

void BuildPhase::MoveCursorRight()
{
	if (cursor_column + 1 < 5)
	{
		cursor_column += 1;
		cursor1_choice += 1;
	}
}

int BuildPhase::GetCursorLocation()
{
	return cursor1_choice;
}

void BuildPhase::SetBuildingSpot(int location, int type)
{
	building_spots[location] = type;
}

void BuildPhase::ResetCursor()
{
	cursor_row = 1;
	cursor_column = 1;

	cursor1_choice = 1;
}

int BuildPhase::GetCursorColumn()
{
	return cursor_column;
}

int BuildPhase::GetCursorRow()
{
	return cursor_row;
}

void BuildPhase::NextBuildPlayer(State &state)
{
	if (current_build_player < PLAYERS_PLAYING)
	{
		current_build_player += 1;
		if (computer_player == true)
		{
			computer_player_turn = true;
			state.GetComputerPlayer()->ChooseBuildingsToBuy(state);
		}
	}
	else
	{
		current_build_player = 1;
		if (computer_player == true)
		{
			computer_player_turn = false;
		}
	}
}

bool BuildPhase::IsItComputerPlayerTurn()
{
	return computer_player_turn;
}

bool BuildPhase::DoesComputerExist()
{
	return computer_player;
}

BuildingManager* BuildPhase::GetBuildingManager()
{
	return building_manager;
}