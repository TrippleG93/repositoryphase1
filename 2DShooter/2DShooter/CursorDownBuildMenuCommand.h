#pragma once

#include "Command.h"

class CursorDownBuildMenuCommand : public Command
{
public:

	~CursorDownBuildMenuCommand();

	virtual void execute() {};
	virtual void execute(State &state);
};

