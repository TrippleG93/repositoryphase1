#include "ExitEndGameMenu.h"

#include "State.h"
#include "Globals.h"
#include "InputHandler.h"
#include "EndGameMenu.h"

ExitEndGameMenu::~ExitEndGameMenu()
{
}

void ExitEndGameMenu::execute(State &state)
{
	state.DeleteEndGameMenu();
	state.DeleteGamePlay();
	state.SetState(MAINMENU);
	state.InitializeMainMenu();
	state.GetAudioManager().StopEndGameLoserSong();
	state.GetAudioManager().StopEndGameWinnerSong();
	state.GetAudioManager().PlayAudioMenuSong();

	state.GetInputHandler()->ClearKeyBoardInputs();			// Clear the keyboard inputs 
	if (state.GetInputHandler()->GetHowManyControllers() != 0)
	{
		state.GetInputHandler()->ClearControllerInput(1);	// Clear the controller inputs
	}
	state.GetAudioManager().PlayCursorSelect();
}

