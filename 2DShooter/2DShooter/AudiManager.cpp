#include "AudiManager.h"



AudiManager::AudiManager()
{
}


AudiManager::~AudiManager()
{
}

void AudiManager::LoadMenuSong()
{
	menu_song.LoadAudioFiles("Content/Audio/Songs/Hyper Potions - Time Trials Sonic Mania Trailer Song  GameChops.ogg");
}

void AudiManager::PlayAudioMenuSong()
{
	menu_song.PlaySongLoop();
}

void AudiManager::StopMenuSong()
{
	menu_song.StopSong();
}

void AudiManager::LoadBuildSong()
{
	build_song.LoadAudioFiles("Content/Audio/Songs/Hyper Potions - Surf.ogg");
}

void AudiManager::PlayerBuildSong()
{
	build_song.PlaySongLoop();
}

void AudiManager::StopBuildSong()
{
	build_song.StopSong();
}

void AudiManager::LoadCombatSong()
{
	combat_song.LoadAudioFiles("Content/Audio/Songs/Sonic Mania  Hyper Potions - Friends Segas Official Animated Opening Song GameChops.ogg");
}

void AudiManager::PlayCombatSong()
{
	combat_song.PlaySongLoop();
}

void AudiManager::StopCombatSong()
{
	combat_song.StopSong();
}

void AudiManager::LoadEndGameWinner()
{
	winner_song.LoadAudioFiles("Content/Audio/Songs/Pokemon Remix - Hyper Potions - Littleroot Town Theme - GameChops.ogg");
}

void AudiManager::PlayEndGameWinnerSong()
{
	winner_song.PlaySongLoop();
}

void AudiManager::StopEndGameWinnerSong()
{
	winner_song.StopSong();
}

void AudiManager::LoadEndGameLoser()
{
	loser_song.LoadAudioFiles("Content/Audio/Songs/Legend of Zelda  Song of Storms  Tudd General Offensive Deep House Remix  - GameChops.ogg");
}

void AudiManager::PlayEndGameLoserSong()
{
	loser_song.PlaySongLoop();
}

void AudiManager::StopEndGameLoserSong()
{
	loser_song.StopSong();
}

void AudiManager::LoadCursorSelect()
{
	cursor_select.LoadAudioFiles("Content/Audio/Songs/menu-select.wav");
}

void AudiManager::PlayCursorSelect()
{
	cursor_select.StopSong();
	cursor_select.PlaySongOnce();
}

void AudiManager::LoadCursorMove()
{
	cursor_move.LoadAudioFiles("Conent/Audio/Songs/Moving Cursor.wav");
}

void AudiManager::PlayCursorMove()
{
	cursor_move.StopSong();
	cursor_move.PlaySongOnce();
}