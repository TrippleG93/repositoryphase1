#include "MoveUnitLeftCommand.h"

#include "State.h"

#include "UnitManager.h"
#include "Unit.h"

#include "CombatPhase.h"
MoveUnitLeftCommand::~MoveUnitLeftCommand()
{

}

void MoveUnitLeftCommand::execute(State &state)
{
	state.GetCombatPhase()->GetUnitManager()->GetPlayer1Unit()->MoveLeft();
}