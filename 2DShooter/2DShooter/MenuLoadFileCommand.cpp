#include "MenuLoadFileCommand.h"

#include "State.h"
#include "MainMenu.h"

MenuLoadFileCommand::~MenuLoadFileCommand()
{
}

void MenuLoadFileCommand::execute(State &state)
{
	state.GetMainMenu()->SetUpMainMenu();
}