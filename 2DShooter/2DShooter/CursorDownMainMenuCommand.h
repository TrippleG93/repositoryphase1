#pragma once

#include "Command.h"

class CursorDownMainMenuCommand : public Command
{
public:
	~CursorDownMainMenuCommand();

	virtual void execute() {};
	virtual void execute(State& state);
};

