#include "State.h"
#include "Globals.h"
#include "Display.h"
#include "Input.h"
#include "InputHandler.h"
#include "Command.h"
#include "ComputerPlayer.h"
#include "BuildingManager.h"

#include "Font.h"
#include "Audio.h"

#include "TitleScreen.h"
#include "MainMenu.h"
#include "BuildMenu.h"
#include "ConfirmMenu.h"
#include "OptionMenu.h"
#include "EndGameMenu.h"

#include "Player.h"
#include "ComputerPlayer.h"

#include "BuildPhase.h"
#include "CombatPhase.h"
#include "MapPhase.h"

State::State()
{
	SetUpState();
}

State::~State()
{
	delete(display);
	delete(input_handler);
}

void State::SetUpState()
{
	//*******************************************
	// PROJECT Init
	//*******************************************
	state = TITLE;
	current_controller = 0;                             // Keeps track of what controller will currently make the input
	game_loop = true;                                   // Used for the game loop
	render = false;                                     // Lets the program know it needs to draw to the screen
	display = new Display();                            // Create the Display object
	input_handler = new InputHandler();                 // Create the input handler class
	input_handler->SetUpControllers();                   // Initialize the controllers in the game
	InitializeTitleScreen();
	timer = al_create_timer(1.0 / 60.0);                // Game run 60 frames a second
														//*******************************************
														// TIMER Init AND STARTUP
														//*******************************************
	event_queue = al_create_event_queue();
	al_register_event_source(event_queue, al_get_timer_event_source(timer));
	al_register_event_source(event_queue, al_get_keyboard_event_source());          // Registers the keyboard to the queue
	al_register_event_source(event_queue, al_get_mouse_event_source());             // Registers the mouse to the queue
	al_register_event_source(event_queue, al_get_joystick_event_source());          // Register the joystick to the queue                                                               
	al_register_event_source(event_queue,
	al_get_display_event_source(display->GetDisplay()));	                        // Registers the display to the queue
	al_start_timer(timer);

	//For FPS
	emulogic_font18.CreateFont("Content/Fonts/Gravedigger.ttf", 12);

	audio.LoadMenuSong();
	audio.PlayAudioMenuSong();
	audio.LoadCursorMove();
	audio.LoadCursorSelect();
}

void State::RunGameStates()
{
	//*******************************************
	// Game Loop
	//*******************************************
	while (game_loop == true)
	{
		al_wait_for_event(event_queue, &events);
		if (input_handler->GetHowManyControllers() != 0)
		{
			input_handler->UpdateControllerInput(events);
		}
		input_handler->UpdateKeyboardInput(events);
		//_input->update_Keyboard_Input(Events);
		if (events.type == ALLEGRO_EVENT_JOYSTICK_CONFIGURATION)
		{
			input_handler->ResyncControllers();
		}
		//*******************************************
		// GAME UPDATE
		//*******************************************
		if (events.type == ALLEGRO_EVENT_TIMER)
		{
			//*******************************************
			// FPS UPDATE
			//*******************************************
			game_fps = UpdateFPS();
			
			// Display delayed
			/*frame_count_fps += 1;
			if (frame_count_fps >= frame_delay_fps)
			{*/
				fps_text_image.CreateTextRender(emulogic_font18.ObtainFont(), al_map_rgb(0, 255, 0), std::to_string(game_fps));
				frame_count_fps = 0;
			//}


			render = true;
			//*******************************************
			// UPDATE ANIMATIONS HERE
			//*******************************************
			switch (state) {
			case TITLE:
				title_screen->UpdateTitleScreen();
				break;
			case MAINMENU:
				main_menu->UpdateMainMenu();
				break;
			case TUTORIAL:
				// Call update for tutorial here
				break;
			case COMBATPHASE:
				if (combat_phase->DidUnitDie() == false)
				{
					combat_phase->UpdateCombatPhase(*this);
				}
				else
				{
					player->UpdatePlayer();
					computer->Update();
				}
				break;
			case BUILDPHASE:
				build_phase->UpdateBuildingPhase();
				break;
			case BUILDMENU:
				build_menu->Update();
				build_phase->UpdateBuildingPhase();
				break;
			case CONFIRMMENU:
				build_phase->UpdateBuildingPhase();
				confirm_menu->UpdateConfirmMenu();
				break;
			case OPTIONMENU:
				option_menu->UpdateOptionMenu();
				break;
			case MAPPHASE:
				map_phase->Update();
			case ENDGAMEMENU:
				end_game_menu->UpdateEndGameMenu();
			case PAUSEMENU:
				confirm_menu->UpdateQuitMenu();
				// update call here for class call
				break;
			}
			//*******************************************
			// END UPDATE ANIMATIONS
			//*******************************************
			//====================================================================
			// UPDATE INPUT FOR STATE
			//====================================================================
			switch (state) {
			case TITLE:
				UpdateTitleState();
				break;
			case MAINMENU:
				UpdateMainMenuState();
				break;
			case BUILDPHASE:
				UpdateBuildingState();
				break;
			case COMBATPHASE:
				UpdateCombatPhaseState();
				break;
			case BUILDMENU:
				UpdateBuildMenuState();
				break;
			case CONFIRMMENU:
				UpdateConfirmMenu();
				break;
			case OPTIONMENU:
				UpdateOptionMenu();
				break;
			case MAPPHASE:
				UpdateMapPhase();
				break;
			case ENDGAMEMENU:
				UpdateEndGameMenu();
			case PAUSEMENU:
				UpdatePauseMenu();
			};
			if (input_handler->GetHowManyControllers() != 0)
			{
				input_handler->ButtonHeldEvent();
			}
			input_handler->KeyHeldEvent();
		}
		//*******************************************
		// RENDER
		//*******************************************
		if (render == true)
		{
			render = false;
			//al_draw_textf(font18, al_map_rgb(255, 0, 255), 5, 5, 0, "FPS: %i", game_fps);   //display FPS on screen
			// Begin the project render based on the state
			switch (state) {
			case TITLE:
				RenderTitleState();
				break;
			case MAINMENU:
				RenderMainMenuState();
				break;
			case BUILDPHASE:
				RenderBuildingState();
				break;
			case COMBATPHASE:
				RenderCombatPhaseState();
				break;
			case BUILDMENU:
				RenderBuildMenuState();
				break;
			case CONFIRMMENU:
				RenderConfirmMenu();
				break;
			case OPTIONMENU:
				RenderOptionMenu();
				break;
			case MAPPHASE:
				RenderMapPhase();
				break;
			case ENDGAMEMENU:
				RenderEndGameMenu();
				break;
			case PAUSEMENU:
				RenderPauseMenu();
			};
			display->FlipDisplay();
		}
	}

	

}

void State::InitializeTitleScreen()
{
	title_screen = new TitleScreen();
	input_handler->InitTitleHandler();
}

void State::UpdateTitleState()
{
	Command* command = input_handler->TitleInputHandler();
	if (command)
	{
		command->execute(*this);
	}
}

void State::RenderTitleState()
{
	title_screen->RenderTitleScreen(display->GetDisplayScaleX(), display->GetDisplayScaleY());
	//fps_text_image.DrawScaledImage(0, 0,
	//	fps_text_image.GetBitmapWidth(),
	//	fps_text_image.GetBitmapHeight(),
	//	10,
	//	10,
	//	fps_text_image.GetBitmapWidth(),
	//	fps_text_image.GetBitmapHeight(),
	//	NULL);
}

void State::DeleteTitleScreen()
{
	input_handler->DeleteCurrentHandler();
	delete(title_screen);
}

void State::InitializeMainMenu()
{
	main_menu = new MainMenu();
	input_handler->InitMainMenuHandler();
}

void State::UpdateMainMenuState()
{
	Command* command = input_handler->MainMenuInputHandler();
	if (command)
	{
		command->execute(*this);
	}
}

void State::RenderMainMenuState()
{
	main_menu->RenderMainMenu(display->GetDisplayScaleX(), display->GetDisplayScaleY());
	//fps_text_image.DrawScaledImage(0, 0,
	//	fps_text_image.GetBitmapWidth(),
	//	fps_text_image.GetBitmapHeight(),
	//	10,
	//	10,
	//	fps_text_image.GetBitmapWidth(),
	//	fps_text_image.GetBitmapHeight(),
	//	NULL);
}

void State::DeleteMainMenu()
{
	delete(main_menu);
	input_handler->DeleteCurrentHandler();
}

void State::InitializeGamePlay()
{
	build_phase = new BuildPhase();
	player = new Player();
	computer = new ComputerPlayer();
	input_handler->InitBuildPhase();
}

// Belongs with gameplay class
void State::UpdateBuildingState()
{
	if (build_phase->IsItComputerPlayerTurn() == false)
	{
		Command* command = input_handler->BuildPhaseInputHandler();
		if (command)
		{
			command->execute(*this);
		}
	}
	else
	{
		HandleComputerInput();
	}
}

void State::InitCombatPhase()
{
	combat_phase = new CombatPhase();
	combat_phase->SetUpCombatPhase(*this);
	player->SetTimers();
	computer->SetTimers();
	GetInputHandler()->InitCombatPhase();
}

void State::DeleteCombatPhase()
{
	delete(combat_phase);
	GetInputHandler()->DeleteCurrentHandler();
}

// Belongs with gameplay class
void State::RenderBuildingState()
{
	build_phase->RenderBuildingPhase(display->GetDisplayScaleX(), display->GetDisplayScaleY());
	//fps_text_image.DrawScaledImage(0, 0,
	//	fps_text_image.GetBitmapWidth(),
	//	fps_text_image.GetBitmapHeight(),
	//	10,
	//	10,
	//	fps_text_image.GetBitmapWidth(),
	//	fps_text_image.GetBitmapHeight(),
	//	NULL);
	if (build_phase->GetCurrentBuildPlayer() == 1)
	{
		player->DrawHeart(875, 75, display->GetDisplayScaleX(), display->GetDisplayScaleY());
	}
	else if (build_phase->GetCurrentBuildPlayer() == 2 && build_phase->DoesComputerExist() == true)
	{
		computer->DrawHeart(875, 75, display->GetDisplayScaleX(), display->GetDisplayScaleY());
	}
}

void State::UpdateCombatPhaseState()
{
	if (combat_phase->ReadyToStart() == true)
	{
		if (combat_phase->DidUnitDie() == false)
		{
			Command* command = input_handler->CombatPhaseInputHandler();
			Command* command2 = computer->AICombatPhaseCommands(*this);

			if (command)
			{
				command->execute(*this);
			}

			if (command2)
			{
				command2->execute(*this);
				delete(command2);
			}
		}

		if (combat_phase->DidPlayer1Die() == true && player->ReadyToLoseLife() == false)
		{
			player->StartLifeLostTimer();
		}
		else if (combat_phase->DidPlayer2Die() == true && computer->ReadyToLoseLife() == false)
		{
			computer->StartLifeLostTimer();
		}

		if (combat_phase->DidUnitDie() == true && (player->DoneLosingLife() == true || computer->DoneLosingLife()))
		{
			if (player->IsDead() == true || computer->IsDead() == true)
			{
				state = ENDGAMEMENU;
				audio.StopCombatSong();
				if (player->IsDead() == true)
				{
					InitEndGameMenu(2);
					audio.LoadEndGameLoser();
					audio.PlayEndGameLoserSong();
				}
				else
				{
					InitEndGameMenu(1);
					audio.LoadEndGameWinner();
					audio.PlayEndGameWinnerSong();
				}

			}
			else
			{
				DeleteCombatPhase();
				audio.StopCombatSong();
				audio.PlayerBuildSong();
				input_handler->InitBuildPhase();
				state = BUILDPHASE;
				player->ResetPurchase();
			}

		}
	}
	
}

// Belongs with gameplay class
void State::RenderCombatPhaseState()
{

	combat_phase->RenderCombatPhase(display->GetDisplayScaleX(), display->GetDisplayScaleY());

	//fps_text_image.DrawScaledImage(0, 0,
	//	fps_text_image.GetBitmapWidth(),
	//	fps_text_image.GetBitmapHeight(),
	//	10,
	//	10,
	//	fps_text_image.GetBitmapWidth(),
	//	fps_text_image.GetBitmapHeight(),
	//	NULL);
	if (combat_phase->DidPlayer1Die() == true)
	{
		player->DrawHeart(display->GetDisplayWidth() / 3, display->GetDisplayHeight() / 2,
			display->GetDisplayScaleX(), display->GetDisplayScaleY());
	}
	else if (combat_phase->DidPlayer2Die() && build_phase->DoesComputerExist() == true)
	{
		computer->DrawHeart(display->GetDisplayWidth() / 3, display->GetDisplayHeight() / 2,
			display->GetDisplayScaleX(), display->GetDisplayScaleY());
	}
	else if (combat_phase->DidPlayer2Die() && build_phase->DoesComputerExist() == false)
	{

	}
}

void State::DeleteGamePlay()
{
	delete(build_phase);
	delete(combat_phase);
	delete(player);
	delete(computer);
	input_handler->DeleteCurrentHandler();
}

void State::InitializeBuildMenu()
{
	build_menu = new BuildMenu();
	input_handler->InitBuildMenu();
}

void State::UpdateBuildMenuState()
{
	if (build_phase->IsItComputerPlayerTurn() == false)
	{
		Command* command = input_handler->BuildMenuInputHandler();
		if (command)
		{
			command->execute(*this);
		}
	}
	else
	{
		HandleComputerInput();
	}
}

void State::RenderBuildMenuState()
{
	RenderBuildingState();
	build_menu->DrawBuildMenu(display->GetDisplayScaleX(), display->GetDisplayScaleY());
	//fps_text_image.DrawScaledImage(0, 0,
	//	fps_text_image.GetBitmapWidth(),
	//	fps_text_image.GetBitmapHeight(),
	//	10,
	//	10,
	//	fps_text_image.GetBitmapWidth(),
	//	fps_text_image.GetBitmapHeight(),
	//	NULL);

	enum types {MAINSHIP = 1, SUPPORTSHIPS, BUFFBUILDINGS};
	int row_buffer = 0;

	if (build_phase->GetCurrentBuildPlayer() == 1)
	{
		switch (build_menu->GetCursorRow())
		{
		case MAINSHIP:
			if (build_phase->GetBuildingManager()->DoesMainShipBuildingExist(1) == true &&
				build_phase->GetCurrentBuildPlayer() == 1)
			{
				for (int i = 1; i <= 4; i++)
				{
					build_menu->DrawXImage(row_buffer, 0,
						display->GetDisplayScaleX(),
						display->GetDisplayScaleY());
					row_buffer += 235;
				}
			}
			else if (build_phase->GetBuildingManager()->DoesMainShipBuildingExist(1) == true &&
				build_phase->GetCurrentBuildPlayer() == 2)
			{
				for (int i = 1; i <= 4; i++)
				{
					build_menu->DrawXImage(row_buffer, 0,
						display->GetDisplayScaleX(),
						display->GetDisplayScaleY());
					row_buffer += 235;
				}
			}
			break;
		case SUPPORTSHIPS:
			row_buffer = 0;
			for (int i = 5; i <= 8; i++)
			{
				if (build_phase->GetCurrentBuildPlayer() == 1)
				{
					if (build_phase->GetBuildingManager()->DoesBuildingTypeExist(1, i) == true)
					{
						build_menu->DrawXImage(row_buffer, 0,
							display->GetDisplayScaleX(),
							display->GetDisplayScaleY());
					}
				}
				else
				{
					if (build_phase->GetBuildingManager()->DoesBuildingExist(2, i))
					{
						build_menu->DrawXImage(row_buffer, 0,
							display->GetDisplayScaleX(),
							display->GetDisplayScaleY());
					}
				}
				row_buffer += 235;
			}
			break;
		case BUFFBUILDINGS:
			row_buffer = 0;
			for (int i = 9; i <= 12; i++)
			{
				if (build_phase->GetCurrentBuildPlayer() == 1)
				{
					if (build_phase->GetBuildingManager()->DoesBuildingTypeExist(1, i) == true)
					{
						build_menu->DrawXImage(row_buffer, 0,
							display->GetDisplayScaleX(),
							display->GetDisplayScaleY());
					}
				}
				else
				{
					if (build_phase->GetBuildingManager()->DoesBuildingExist(2, i))
					{
						build_menu->DrawXImage(row_buffer, 0,
							display->GetDisplayScaleX(),
							display->GetDisplayScaleY());
					}
				}
				row_buffer += 235;
			}
			break;
		}
	}

	

	if (build_phase->GetCurrentBuildPlayer() == 1)
	{
		player->DrawPlayerInfo(display->GetDisplayScaleX(), display->GetDisplayScaleY());
	}
	else if (build_phase->GetCurrentBuildPlayer() == 1 && build_phase->DoesComputerExist() == false)
	{
		//
	}
	else
	{

	}
}

void State::DeleteBuildMenu()
{
	delete(build_menu);
	input_handler->DeleteCurrentHandler();
}

void State::InitializeConfirmBuildMenu()
{
	confirm_menu = new ConfirmMenu();
	confirm_menu->SetUpConfirmBuildPhase();
	input_handler->InitConfirmBuildMenu();
}

void State::UpdateConfirmMenu()
{
	if (build_phase->IsItComputerPlayerTurn() == false)
	{
		Command* command = input_handler->ConfirmBuildMenuInputHandler();
		if (command)
		{
			command->execute(*this);
		}
	}
	else
	{
		HandleComputerInput();
	}
}

void State::RenderConfirmMenu()
{
	RenderBuildingState();
	confirm_menu->RenderConfirmMenu(display->GetDisplayScaleX(), display->GetDisplayScaleY());
	//fps_text_image.DrawScaledImage(0, 0,
	//	fps_text_image.GetBitmapWidth(),
	//	fps_text_image.GetBitmapHeight(),
	//	10,
	//	10,
	//	fps_text_image.GetBitmapWidth(),
	//	fps_text_image.GetBitmapHeight(),
	//	NULL);
}

void State::DeleteConfirmBuildMenu()
{
	delete(confirm_menu);
	input_handler->DeleteCurrentHandler();
}

void State::InitializeOptionMenu()
{
	option_menu = new OptionMenu();
	input_handler->InitOptionMenu();
}

void State::UpdateOptionMenu()
{
	Command* command = input_handler->OptionMemuInputHandler();
	if (command)
	{
		command->execute(*this);
	}
}

void State::RenderOptionMenu()
{
	option_menu->RenderOptionMenu(display->GetDisplayScaleX(), display->GetDisplayScaleY());
	//fps_text_image.DrawScaledImage(0, 0,
	//	fps_text_image.GetBitmapWidth(),
	//	fps_text_image.GetBitmapHeight(),
	//	10,
	//	10,
	//	fps_text_image.GetBitmapWidth(),
	//	fps_text_image.GetBitmapHeight(),
	//	NULL);
}

void State::DeleteOptionMenu()
{
	delete(option_menu);
	input_handler->DeleteCurrentHandler();
}

TitleScreen* State::GetTitleScreen()
{
	return title_screen;
}

MainMenu* State::GetMainMenu()
{
	return main_menu;
}


BuildMenu* State::GetBuildMenu()
{
	return build_menu;
}

InputHandler* State::GetInputHandler()
{
	return input_handler;
}

ConfirmMenu* State::GetConfirmMenu()
{
	return confirm_menu;
}

OptionMenu* State::GetOptionMenu()
{
	return option_menu;
}

Display* State::GetDisplay()
{
	return display;
}

Player* State::GetPlayer()
{
	return player;
}

ComputerPlayer* State::GetComputerPlayer()
{
	return computer;
}

BuildPhase* State::GetBuildPhase()
{
	return build_phase;
}

CombatPhase* State::GetCombatPhase()
{
	return combat_phase;
}

MapPhase* State::GetMapPhase()
{
	return map_phase;
}

int State::GetState()
{
	return state;
}

void State::SetState(int new_state)
{
	state = new_state;
}

void State::SetGameLoop(bool loop)
{
	game_loop = loop;
}

void State::HandleComputerInput()
{
	Command* command = input_handler->RunABuildComputerCommands();
	if (command)
	{
		command->execute(*this);
		input_handler->DeleteComputerCommand();
	}
}

double State::UpdateFPS()
{
	/* need to zero out the ticklist array before starting */
	/* average will ramp up until the buffer is full */
	/* returns average ticks per frame over the MAXSAMPLES last frames */

	double new_time = al_get_time();
	double fps = 1.0 / (new_time - old_time);
	old_time = new_time;

	return fps;
}

void State::InitMapPhase()
{
	if (map_phase == nullptr)
	{
		state = MAPPHASE;
		map_phase = new MapPhase();
		input_handler->DeleteCurrentHandler();
		input_handler->InitMapPhaseCommands();
	}
	else
	{
		state = MAPPHASE;
		input_handler->DeleteCurrentHandler();
		input_handler->InitMapPhaseCommands();
	}
}

void State::UpdateMapPhase()
{
	Command* command = input_handler->MapPhaseInputHandler();
	if (command)
	{
		command->execute(*this);
	}
}

void State::RenderMapPhase()
{
	map_phase->RenderMapPhase(display->GetDisplayScaleX(), display->GetDisplayScaleY());
}

void State::DeleteMapPhase()
{
	delete(map_phase);
}

void State::InitEndGameMenu(int who_won)
{
	end_game_menu = new EndGameMenu(who_won);
	input_handler->DeleteCurrentHandler();
	input_handler->InitEndGameCommands();
}

void State::UpdateEndGameMenu()
{
	Command* command = input_handler->EndGameInputHandler();
	if (command)
	{
		command->execute(*this);
	}
}

void State::RenderEndGameMenu()
{
	end_game_menu->DrawEndGameMenu(display->GetDisplayScaleX(), display->GetDisplayScaleY());
}

void State::DeleteEndGameMenu()
{
	delete(end_game_menu);
	input_handler->DeleteCurrentHandler();
}

void State::InitPauseMenu()
{
	confirm_menu->SetUpQuitMenu();
	input_handler->DeleteCurrentHandler();
	input_handler->InitPauseMenuCommands();
}

void State::UpdatePauseMenu()
{
	Command* command = input_handler->PauseMenuInputHandler();
	if (command)
	{
		command->execute(*this);
	}
}

void State::RenderPauseMenu()
{
	confirm_menu->RenderQuitMenu(display->GetDisplayScaleX(), display->GetDisplayScaleY());
}

void State::DeletePauseMenu()
{
	delete(confirm_menu);
	input_handler->DeleteCurrentHandler();
}