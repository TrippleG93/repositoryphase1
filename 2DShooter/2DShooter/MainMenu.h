#pragma once

#include "SpriteAnimation.h"
#include "Render.h"
#include "Audio.h"
#include "Font.h"

class MainMenu
{
public:
	MainMenu();
	~MainMenu();

	/*
	SetUpMainMenu
	Load the images audio ect
	*/
	void SetUpMainMenu();

	/*
	RenderMainMenu
	Draw everything needed for the main menu
	*/
	void RenderMainMenu(float window_scale_x, float window_scale_y);

	/*
	UpdateMainMenu
	Update the frames inputs
	*/
	void UpdateMainMenu();

	/*
	MoveCursorUp
	Update the choice by subtracting by 1
	*/
	void MoveCursorUp();

	/*
	MoveCursorDown
	Update the choice by adding by 1
	*/
	void MoveCursorDown();

	/*
	GetChoice
	Returns the int value of the choice
	*/
	int GetChoice();

private:
	SpriteAnimation cursor_animation;

	Render background_image;
	Render cursor_image;

	Render options_text;
	int option_text_x;
	int option_text_y;

	Render start_game_text;
	int start_text_x;
	int start_text_y;

	Render quit_text;
	int quit_text_x;
	int quit_text_y;

	Render main_menu_text;
	int main_text_x;
	int main_text_y;

	Font emulogic_font24;
	Font emulogic_font26;
	Font emulogic_font76;

	int y_position_cursor;
	int choice;
};

