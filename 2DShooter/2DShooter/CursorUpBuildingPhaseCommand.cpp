#include "CursorUpBuildingPhaseCommand.h"

#include "state.h"

#include "BuildPhase.h"
#include "Globals.h"

CursorUpBuildingPhaseCommand::~CursorUpBuildingPhaseCommand()
{
}

void CursorUpBuildingPhaseCommand::execute(State &state)
{
		state.GetBuildPhase()->MoveCursorUp();
		state.GetAudioManager().PlayCursorMove();
}