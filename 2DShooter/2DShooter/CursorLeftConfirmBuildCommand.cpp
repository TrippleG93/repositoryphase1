#include "CursorLeftConfirmBuildCommand.h"

#include "State.h"
#include "ConfirmMenu.h"

CursorLeftConfirmBuildCommand::~CursorLeftConfirmBuildCommand()
{
}

void CursorLeftConfirmBuildCommand::execute(State &state)
{
	state.GetConfirmMenu()->MoveCursorLeft();
	state.GetAudioManager().PlayCursorMove();
}