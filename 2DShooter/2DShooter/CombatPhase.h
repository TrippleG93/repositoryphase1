#pragma once

#include "SpriteAnimation.h"
#include "Render.h"
#include "Audio.h"
#include "State.h"
#include "Globals.h"

class BulletManager;					// Forward declaring the bullet manager class
class UnitManager;						// Forward declaring the unit manager class
class State;							// Forward declaring the state class

class CombatPhase
{
public:
	CombatPhase();
	~CombatPhase();

	void SetUpCombatPhase(State &state);

	void RenderCombatPhase(float window_scale_x, float window_scale_y);

	void UpdateCombatPhase(State &state);

	bool DidUnitDie();

	bool DidPlayer1Die();

	bool DidPlayer2Die();

	void ResetCountDown();

	BulletManager* GetBulletManager() { return bullet_manager; }
	UnitManager* GetUnitManager() { return unit_manager; }
	bool ReadyToStart() { return begin; }

private:
	int wait_time;										// Holds how long we wait till input starts
	int go_time;										// Holds how long we show the go text
	int time_passed;									// Holds how much time has passed

	bool begin;											// Holds if we can take in input or not

	int SPACING_UNIT = 50;								// Distance used between objects. Use /2, /4, etc. for smaller distances

	bool support_ship_ready = true;						// Makes numbers green when a support ship is ready to deploy

	SpriteAnimation support_unit_animation;

	Render space_image;
	
	BulletManager* bullet_manager = nullptr;			// Holds the bullet manange class
	UnitManager* unit_manager = nullptr;				// Holds the unit_manager class

	Font emulogic_font;								// Holds the type of font and size
	Render ready_text;								// Holds the text that says ready
	Render go_text;									// Holds the text that says go

	Render lost_text_1;								// Holds the text of who lost
	int lost_text_x;
	int lost_text_y;

	Render lost_text_2;
};

