#include "FireCommand.h"

#include "State.h"

#include "BulletManager.h"

#include "CombatPhase.h"

FireCommand::~FireCommand()
{
}



void FireCommand::execute(State &state)
{
	state.GetCombatPhase()->GetBulletManager()->CreateBullet(1, 0, *state.GetCombatPhase()->GetUnitManager());
}
