#include "BuildingManager.h"

#include "Building.h"


BuildingManager::BuildingManager()
{
}


BuildingManager::~BuildingManager()
{
	DeleteBuildings();
}

bool BuildingManager::DoesBuildingExist(int player, int location)
{
	if (player == 1)
	{
		if (player1_buildings.count(location) == 0)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	else
	{
		if (player2_buildings.count(location) == 0)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
}

void BuildingManager::CreateBuilding(int player, int location, int building_type, int row, int column)
{
	if (player == 1)
	{
		player1_buildings[location] = new Building(building_type, row, column);
	}
	else
	{
		player2_buildings[location] = new Building(building_type, row, column);
	}
}

Building* BuildingManager::GetBuilding(int player, int location)
{
	if (player == 1)
	{
		return player1_buildings[location];
	}
	else
	{
		return player2_buildings[location];
	}
}

void BuildingManager::DeleteBuildings()
{
	for (auto const& x : player1_buildings)
	{
		delete(x.second);
	}

	for (auto const& x : player2_buildings)
	{
		delete(x.second);
	}
}

void BuildingManager::UpdateBuildings(int player)
{
	if (player == 1)
	{
		for (auto const& x : player1_buildings)
		{
			x.second->UpdateBuilding();
		}
	}
	else
	{
		for (auto const& x : player2_buildings)
		{
			x.second->UpdateBuilding();
		}
	}
}

void BuildingManager::RenderBuildings(int player, float window_scale_x, float window_scale_y)
{
	if (player == 1)
	{
		for (auto const& x : player1_buildings)
		{
			x.second->RenderBuilding(window_scale_x, window_scale_y);
		}
	}
	else
	{
		for (auto const& x : player2_buildings)
		{
			x.second->RenderBuilding(window_scale_x, window_scale_y);
		}
	}
}

bool BuildingManager::DoesMainShipBuildingExist(int player)
{
	if (player == 1)
	{
		for (auto const& x : player1_buildings)
		{
			if (x.second->GetBuildingType() > 0 && x.second->GetBuildingType() < 5)
			{
				return true;
			}
		}
	}
	else
	{
		for (auto const& x : player2_buildings)
		{
			if (x.second->GetBuildingType() > 0 && x.second->GetBuildingType() < 5)
			{
				return true;
			}
		}
	}

	return false;
}

bool BuildingManager::DoesBuildingTypeExist(int player, int type)
{
	if (player == 1)
	{
		for (auto const& x : player1_buildings)
		{
			if (x.second->GetBuildingType()  == type)
			{
				return true;
			}
		}
	}
	else
	{
		for (auto const& x : player2_buildings)
		{
			if (x.second->GetBuildingType() == type)
			{
				return true;
			}
		}
	}
	return false;
}

int BuildingManager::GetMainBuildingType(int player)
{
	if (player == 1)
	{
		for (auto const& x : player1_buildings)
		{
			if (x.second->GetBuildingType() < 5)
			{
				return x.second->GetBuildingType();
			}
		}
	}
	else
	{
		for (auto const& x : player2_buildings)
		{
			if (x.second->GetBuildingType() < 5)
			{
				return x.second->GetBuildingType();;
			}
		}
	}

	printf("UNIT DOES NOT EXIST FOR %d player", player);
	return -1;
}