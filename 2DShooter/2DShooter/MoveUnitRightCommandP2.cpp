#include "MoveUnitRightCommandP2.h"

#include "State.h"

#include "UnitManager.h"
#include "Unit.h"
#include "CombatPhase.h"


MoveUnitRightCommandP2::~MoveUnitRightCommandP2()
{
}

void MoveUnitRightCommandP2::execute(State &state)
{
	state.GetCombatPhase()->GetUnitManager()->GetPlayer2Unit()->MoveRight();
}
