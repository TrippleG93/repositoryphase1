#include "BackToTitleCommand.h"

#include "Globals.h"
#include "State.h"
#include "InputHandler.h"

BackToTitleCommand::~BackToTitleCommand()
{
}

void BackToTitleCommand::execute(State &state)
{
	state.DeleteMainMenu();
	state.InitializeTitleScreen();
	state.SetState(TITLE);
	state.GetInputHandler()->ClearKeyBoardInputs();
	if (state.GetInputHandler()->GetHowManyControllers() != 0)
	{
		state.GetInputHandler()->ClearControllerInput(1);			// Clear the keyboard input
	}
	state.GetAudioManager().PlayCursorSelect();
}
