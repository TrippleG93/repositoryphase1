#include "OptionLoadFileCommand.h"

#include "State.h"
#include "OptionMenu.h"

OptionLoadFileCommand::~OptionLoadFileCommand()
{
}

void OptionLoadFileCommand::execute(State &state)
{
	state.GetOptionMenu()->SetUpOptionMenu();
}