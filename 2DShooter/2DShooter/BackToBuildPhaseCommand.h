#pragma once

#include "Command.h"

class BackToBuildPhaseCommand : public Command
{
public:

	~BackToBuildPhaseCommand();

	virtual void execute() {};
	virtual void execute(State &state);
};

