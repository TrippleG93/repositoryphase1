#include "CursorRightBuildingPhaseCommand.h"

#include "Globals.h"
#include "State.h"
#include "BuildPhase.h"


CursorRightBuildingPhaseCommand::~CursorRightBuildingPhaseCommand()
{
}

void CursorRightBuildingPhaseCommand::execute(State &state)
{
	state.GetBuildPhase()->MoveCursorRight();

	state.GetAudioManager().PlayCursorMove();
}
