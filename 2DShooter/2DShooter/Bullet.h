#pragma once

#include "Render.h"
#include "SpriteAnimation.h"
#include "Audio.h"
#include "Font.h"


class Bullet
{
public:
	Bullet();
	Bullet(int player, int x_position, int y_position, int type, float boost);
	Bullet(int player, int x_position, int y_position, int type, float boost, int opp_pos_x, int opp_pos_y);
	~Bullet();

	/*
		SetUpBullet
		Load everything needed for the bullet class
	*/
	void SetUpBullet(int pos_x, int pos_y);

	/*
		UpdateBullet
		Updates the animations of the bullet
		Updates the position of the bullet
	*/
	void UpdateBullet();

	/*
		RenderBullet
		Draws everything for the bullet class
	*/
	void RenderBullet(float window_scale_x, float window_scale_y);

	/*
		GetXPosition
		returns the x position of the bullet
	*/
	int GetXPosition();

	/*
		GetYPosition
		Returns the y position of the bullet
	*/
	int GetYPosition();

	/*
	MoveLeft
	Set -2 to x_velocity to move the ship to the left
	*/
	void MoveLeft();

	/*
	MoveRight
	Set 2 to the x_velocity to move the ship to the right
	*/
	void MoveRight();

	/*
	MoveUp
	Set -2 to the y_velocity to move the ship upward
	*/
	void MoveUp();

	/*
	MoveDown
	Set 2 ti the y_velocity to move the ship downward
	*/
	void MoveDown();

	/*
	IdleHorizontal
	Set the x_velocity to 0 it will no longer move in
	the x direction
	*/
	void IdleHorizontal();

	/*
	IdleVerticle
	Set the y_velocity to 0 it will no longer move
	in the y direction
	*/
	void IdleVerticle();

	/*
	GetXVelocity
	returns the x velocity of the unit
	*/
	int GetXVelocity();

	/*
	SetXVelocity
	sets the x velocity of the unit
	*/
	void SetXVelocity(int new_x_velocity);
	void SetYVelocity(int new_y_velocity);


	/*
		GetSprite
		Obtains the sprite oject inside the sprite animation class
	*/
	Sprite GetSprite();

	/*
		DidBulletHitSomething
		Returns true if the bullet hit something
	*/
	bool DidBulletHitSomething();

	void BulletHitSomething();

	void BulletHitNothing();

	int GetDamage() { return damage; }

	int GetType() { return bullet_type; }

	int GetLaserDelay() { return delay_laser; }

	void SetBulletBoost(float set_bullet_boost);


private:
	int which_player;
	int x_velocity;								// Holds the velocity of the bullet in the x direction
	int y_velocity;								// Holds the velocity of the bullet in the y direction
	int current_x_velocity;						// Holds the current velocity of the bullet in the x direction
	int current_y_velocity;						// Holds the current velocity of the bullet in the y direction
	int damage;
	bool hit;									// Holds whether the bullet hit something or not
	int bullet_type;
	float bullet_boost;
	float angle;
	int opponent_position_x;
	int opponent_position_y;


	int count_laser = 0;
	int delay_laser = 200;


	Render bullet_image;						// Holds the image of the bullet
	SpriteAnimation bullet_animation;			// Holds the animation of the bullet

	// TODO: temp
	int display_laser = 300;
	int move_x = 500;
	int old_sqrt = 580;
};

