#include "CursorUpMainMenuCommand.h"

#include "State.h"
#include "MainMenu.h"

CursorUpMainMenuCommand::~CursorUpMainMenuCommand()
{
}

void CursorUpMainMenuCommand::execute(State& state)
{
	state.GetMainMenu()->MoveCursorUp();
	state.GetAudioManager().PlayCursorMove();
}