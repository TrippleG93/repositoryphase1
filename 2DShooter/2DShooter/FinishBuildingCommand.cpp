#include "FinishBuildingCommand.h"

#include "State.h"
#include "InputHandler.h"
#include "Globals.h"


FinishBuildingCommand::~FinishBuildingCommand()
{
}

void FinishBuildingCommand::execute(State &state)
{
	state.GetInputHandler()->DeleteCurrentHandler();			// Deletes the current controls for the build phase
	state.SetState(CONFIRMMENU);
	state.InitializeConfirmBuildMenu();
	state.GetInputHandler()->ClearKeyBoardInputs();				// Clear the keyboard inputs 
	if (state.GetInputHandler()->GetHowManyControllers() != 0)
	{
		state.GetInputHandler()->ClearControllerInput(1);		// Clear the controller inputs
	}
	state.GetAudioManager().PlayCursorSelect();
}
