#include "ConfirmChoiceOptionMenu.h"

#include "State.h"
#include "OptionMenu.h"
#include "Display.h"
#include "Globals.h"

ConfirmChoiceOptionMenu::~ConfirmChoiceOptionMenu()
{

}

void ConfirmChoiceOptionMenu::execute(State &state)
{
	const int FULL_SCREEN = 0;
	const int DEFAULT_SCREEN = 1;
	const int EXIT = 2;
	switch (state.GetOptionMenu()->GetChoice())
	{
	case FULL_SCREEN:
		state.GetDisplay()->DisplayFullScreenWindowMode();
		state.GetAudioManager().PlayCursorSelect();
		break;
	case DEFAULT_SCREEN:
		state.GetDisplay()->DisplayWindowMode();
		state.GetAudioManager().PlayCursorSelect();
		break;
	case EXIT:
		state.DeleteOptionMenu();
		state.InitializeMainMenu();
		state.SetState(MAINMENU);
		state.GetAudioManager().PlayCursorSelect();
		break;
	default:
		break;
	}
}
