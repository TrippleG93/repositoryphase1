#include "SelectChoiceMainMenuCommand.h"

#include "State.h"
#include "MainMenu.h"
#include "Globals.h"
#include "InputHandler.h"
#include "AudiManager.h"

SelectChoiceMainMenuCommand::~SelectChoiceMainMenuCommand()
{
}

void SelectChoiceMainMenuCommand::execute(State &state)
{
	const int STARTGAME = 0;
	const int OPTIONS = 1;
	const int QUIT = 2;

	switch (state.GetMainMenu()->GetChoice())
	{
	case STARTGAME:
		// Start the gameplay
		state.DeleteMainMenu();
		state.InitializeGamePlay();
		state.SetState(BUILDPHASE);
		state.GetAudioManager().StopMenuSong();
		state.GetAudioManager().LoadBuildSong();
		state.GetAudioManager().PlayerBuildSong();
		state.GetAudioManager().PlayCursorSelect();
		break;
	case OPTIONS:
		// Enter the option menu
		state.DeleteMainMenu();
		state.InitializeOptionMenu();
		state.SetState(OPTIONMENU);
		state.GetAudioManager().PlayCursorSelect();
		break;
	case QUIT:
		// Quit the game
		state.DeleteMainMenu();
		state.SetState(SHUTDOWN);
		state.SetGameLoop(false);
		state.GetAudioManager().PlayCursorSelect();
	default:
		break;
	}

	state.GetInputHandler()->ClearKeyBoardInputs();

	if (state.GetInputHandler()->GetHowManyControllers() != 0)
	{
		state.GetInputHandler()->ClearControllerInput(1);			// Clear the keyboard input
	}
}
