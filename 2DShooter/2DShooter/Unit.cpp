#include "Unit.h"

#include <iostream>
#include <fstream>
#include <string.h>

#include "Globals.h"
#include "ConfigFileReader.h"

Unit::Unit() :
	health_points(100)
{
}

Unit::Unit(int type, int player, int position_x, int position_y)
{
	SetUp(type, player, position_x, position_y);
}

Unit::~Unit()
{
}

void Unit::SetUp(int type, int player, int position_x, int position_y)
{
	ConfigFileReader file("Content/Text Files/Units/Unit.cfg");

	building_type = type;
	which_player = player;
	current_x_velocity = 0;
	current_y_velocity = 0;

	frame_count_health = 0;
	frame_delay_health = file.GetConfigValueIntegers("others", "frame delay health");
	

	bar.LoadBitmap("Content/Images/Others/bar.png");
	health.LoadBitmap("Content/Images/Others/health.png");

	bar_animation.AddIndividualFrame(file.GetConfigValueIntegers("health bar", "source x"),
		file.GetConfigValueIntegers("health bar", "source y"),
		file.GetConfigValueIntegers("health bar", "width"),
		file.GetConfigValueIntegers("health bar", "height"),
		0, 0,
		file.GetConfigValueDouble("health bar", "scale x"),
		file.GetConfigValueDouble("health bar", "scale y"));
	bar_animation.CombineFrames(10, false);

	health_animation.AddIndividualFrame(file.GetConfigValueIntegers("health image", "source x"),
		file.GetConfigValueIntegers("health image", "source y"),
		file.GetConfigValueIntegers("health image", "width"),
		file.GetConfigValueIntegers("health image", "height"),
		0, 0,
		file.GetConfigValueDouble("health image", "scale x"),
		file.GetConfigValueDouble("health image", "scale y"));
	health_animation.CombineFrames(10, false);

	bar_x_position = file.GetConfigValueIntegers("health bar", "position x");
	bar_y_position = file.GetConfigValueIntegers("health bar", "position y");
	health_x_position = file.GetConfigValueIntegers("health image", "position x");
	health_y_position = file.GetConfigValueIntegers("health image", "position y");

	//===================================================
	//Ship
	//===================================================
		std::string ship;

		switch (building_type) {
		case(ATTACK):
			unit_image.LoadBitmap("Content/Images/Ships/destroyer.png");
			ship = "ship type 1";
			break;
		case(DEFENCE):
			unit_image.LoadBitmap("Content/Images/Ships/blue1.png");
			ship = "ship type 2";
			break;
		case(SPEED):
			unit_image.LoadBitmap("Content/Images/Ships/smallgreen.png");
			ship = "ship type 3";
			break;
		case(BALANCE):
			unit_image.LoadBitmap("Content/Images/Ships/topdownfighter.png");
			ship = "ship type 4";
			break;
		}
		//if(player == 2)
		//unit_image.LoadBitmap("Content/Images/Ships/3spaceships/large.ship_.1.png");

	switch (building_type) {
	case(SUPPORT1):
		if (which_player == 1)
			unit_image.LoadBitmap("Content/Images/Ships/tribase-u2-d0-GREEN.png");
		else
			unit_image.LoadBitmap("Content/Images/Ships/tribase-u2-d0-RED.png");
	
		ship = "support type 1";
		wander_min_suicide_ai = file.GetConfigValueIntegers(ship, "wander min SU1");
		wander_max_suicide_ai = file.GetConfigValueIntegers(ship, "wander max SU1");
		break;
	case(SUPPORT2):
		if(which_player == 1)
			unit_image.LoadBitmap("Content/Images/Ships/ship6-GREEN.png");
		else
			unit_image.LoadBitmap("Content/Images/Ships/ship6-RED.png");

		ship = "support type 2";
		distance_x = file.GetConfigValueIntegers("support type 2", "distance x 1");
		distance_x2 = file.GetConfigValueIntegers("support type 2", "distance x 2");
		distance_y = file.GetConfigValueIntegers("support type 2", "distance y");
		break;
	case(SUPPORT3):
		if (which_player == 1)
			unit_image.LoadBitmap("Content/Images/Ships/redship4-GREEN.png");
		else
			unit_image.LoadBitmap("Content/Images/Ships/redship4-RED.png"); 

		ship = "support type 3";
		state = WANDER;
		heal_stops = file.GetConfigValueIntegers("support type 3", "heal stops");
		stop_amount = 0;															
		time_to_move = file.GetConfigValueIntegers("support type 3", "time to move");
		time_waiting = time_to_move + 1;
		x_pos = 0;
		y_pos = 0;
		distance_x = file.GetConfigValueIntegers("support type 3", "distance x");
		distance_x = file.GetConfigValueIntegers("support type 3", "distance x2");
		break;
	case(SUPPORT4):
		if (which_player == 1)
			unit_image.LoadBitmap("Content/Images/Ships/Spacestation-by-MillionthVector-GREEN.png");
		else
			unit_image.LoadBitmap("Content/Images/Ships/Spacestation-by-MillionthVector-RED.png");

		if (player == 1) {
			position_x = rand() % (globals::GAME_WIDTH - 200) + 100;
			position_y = globals::GAME_HEIGHT - 200;
		}
		else {
			position_x = rand() % (globals::GAME_WIDTH - 200) + 100;
			position_y = 200;
		}
		ship = "support type 4";
		break;
	};

	// Read in stats from file
	unit_animation.AddIndividualFrame(file.GetConfigValueIntegers(ship, "source x"),
		file.GetConfigValueIntegers(ship, "source y"),
		file.GetConfigValueIntegers(ship, "width"),
		file.GetConfigValueIntegers(ship, "height"),
		position_x,
		position_y,
		file.GetConfigValueDouble(ship, "scale x"),
		file.GetConfigValueDouble(ship, "scale y"));
	unit_animation.CombineFrames(file.GetConfigValueIntegers(ship, "frame"), false);
	ship_velocity_x = file.GetConfigValueIntegers(ship, "velocity x");
	ship_velocity_y = file.GetConfigValueIntegers(ship, "velocity y");
	health_points = file.GetConfigValueIntegers(ship, "health");
	damage_boost = file.GetConfigValueIntegers(ship, "damage boost");
	defence_boost = file.GetConfigValueDouble(ship, "defence boost");
	bullet_boost =  file.GetConfigValueDouble(ship, "bullet boost");

	max_health = health_points;
	//===================================================
}

void Unit::UpdateUnit(int opp_x, int opp_y)
{
	if (building_type == SUPPORT4) {
		opponent_position_x = opp_x;
		opponent_position_y = opp_y;
	}

	if (frame_count_health <= frame_delay_health)
	{
		frame_count_health += 1;
	}
	
	unit_animation.Update();

	if (unit_animation.GetCurrentFrame().GetPositionX() + current_x_velocity > 0 &&
		unit_animation.GetCurrentFrame().GetPositionX() + current_x_velocity < (globals::GAME_WIDTH - (unit_animation.GetCurrentFrame().GetWidth() * unit_animation.GetCurrentFrame().GetScaleX())))
	{
		unit_animation.GetCurrentFrame().SetXPosition(unit_animation.GetCurrentFrame().GetPositionX() + current_x_velocity);
	}

	if (unit_animation.GetCurrentFrame().GetPositionY() + current_y_velocity > 0 && 
		unit_animation.GetCurrentFrame().GetPositionY() + current_y_velocity < globals::GAME_HEIGHT)
	{
		unit_animation.GetCurrentFrame().SetYPosition(unit_animation.GetCurrentFrame().GetPositionY() + current_y_velocity);
	}
	else
	{
		TakeDamage(200, 0);
	}
}

void Unit::RenderUnit(float window_scale_x, float window_scale_y)
{
	if (which_player == 1)
	{
			unit_image.DrawScaledImage((float)unit_animation.GetCurrentFrame().GetSourceX(),
				(float)unit_animation.GetCurrentFrame().GetSourceY(),
				(float)unit_animation.GetCurrentFrame().GetWidth(),
				(float)unit_animation.GetCurrentFrame().GetHeight(),
				(float)unit_animation.GetCurrentFrame().GetPositionX() * window_scale_x,
				(float)unit_animation.GetCurrentFrame().GetPositionY() * window_scale_y,
				(float)unit_animation.GetCurrentFrame().GetWidth() * window_scale_x * unit_animation.GetCurrentFrame().GetScaleX(),
				(float)unit_animation.GetCurrentFrame().GetHeight() * window_scale_y * unit_animation.GetCurrentFrame().GetScaleY(),
				NULL);

		if (frame_count_health <= frame_delay_health)
		{
			DrawHealthbar(window_scale_x, window_scale_y);
		}
	}
	else
	{
		unit_image.DrawScaledImage((float)unit_animation.GetCurrentFrame().GetSourceX(),
			(float)unit_animation.GetCurrentFrame().GetSourceY(),
			(float)unit_animation.GetCurrentFrame().GetWidth(),
			(float)unit_animation.GetCurrentFrame().GetHeight(),
			(float)unit_animation.GetCurrentFrame().GetPositionX() * window_scale_x,
			(float)unit_animation.GetCurrentFrame().GetPositionY() * window_scale_y,
			(float)unit_animation.GetCurrentFrame().GetWidth() * window_scale_x * unit_animation.GetCurrentFrame().GetScaleX(),
			(float)unit_animation.GetCurrentFrame().GetHeight() * window_scale_y * unit_animation.GetCurrentFrame().GetScaleY(),
			ALLEGRO_FLIP_VERTICAL);

		if (frame_count_health <= frame_delay_health)
		{
			DrawHealthbar(window_scale_x, window_scale_y);
		}
	}
}

void Unit::MoveLeft()
{
	current_x_velocity = -ship_velocity_x;
}

void Unit::MoveUp()
{
	current_y_velocity = -ship_velocity_y;
}

void Unit::MoveRight()
{
	current_x_velocity = ship_velocity_x;
}

void Unit::MoveDown()
{
	current_y_velocity = ship_velocity_y;
}

void Unit::IdleHorizontal()
{
	current_x_velocity = 0;
}

void Unit::IdleVerticle()
{
	current_y_velocity = 0;
}

int Unit::GetXPosition()
{
	return unit_animation.GetCurrentFrame().GetPositionX();
}

int Unit::GetYPosition()
{
	return unit_animation.GetCurrentFrame().GetPositionY();
}

int Unit::GetXVelocity()
{
	return current_x_velocity;
}

void Unit::SetXVelocity(int new_x_velocity)
{
	current_x_velocity = new_x_velocity;
}

bool Unit::CollisionDetection(Sprite sprite_1, Sprite sprite_2)
{
	int test1 = sprite_1.GetPositionX();
	int test2 = sprite_1.GetPositionX() + sprite_1.GetWidth() * sprite_1.GetScaleX();
	int test3 = sprite_2.GetPositionX();
	int test4 = sprite_2.GetPositionX() + sprite_2.GetWidth() * sprite_2.GetScaleX();

	int test5 = sprite_1.GetPositionY();
	int test6 = sprite_1.GetPositionY() + sprite_1.GetHeight() * sprite_1.GetScaleY();
	int test7 = sprite_2.GetPositionY();

	//printf("Bullet range left%d\n", test3);

	if (test1 <= test3 && test2 >= test3)
	{
		if (test5 <= test7 && test6 >= test7)
		{
			return true;
		}
	}
	if (test1 <= test4 && test2 >= test4)
	{
		if (test5 <= test7 && test6 >= test7)
		{
			return true;
		}
	}
		
	return false;
}

bool Unit::CollisionDetectionRadius(Sprite sprite_1, Sprite sprite_2)
{
	// TODO: Still working out the kinks in this method

	int x1 = sprite_1.GetPositionX();
	int y1 = sprite_1.GetPositionY();
	int r1 = sprite_1.GetHeight() * sprite_1.GetScaleY() / 2;

	int x2 = sprite_2.GetPositionX();
	int y2 = sprite_2.GetPositionY();
	int r2 = sprite_2.GetHeight() * sprite_1.GetScaleY() / 2;

	//compare the distance to combined radii
	if (std::sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1)) < (r1 + r2))
	{		
		return true;
	}

	return false;
}

Sprite Unit::GetSprite()
{
	return unit_animation.GetCurrentFrame();
}

void Unit::DrawHealthbar(float window_scale_x, float window_scale_y)
{
	float health_percentage = (float)health_points / (float)max_health;
	if (unit_animation.GetCurrentFrame().GetPositionX() < 900)
	{
		bar.DrawScaledImage((float)bar_animation.GetCurrentFrame().GetSourceX(),
			(float)bar_animation.GetCurrentFrame().GetSourceY(),
			(float)bar_animation.GetCurrentFrame().GetWidth(),
			(float)bar_animation.GetCurrentFrame().GetHeight(),
			(float)(unit_animation.GetCurrentFrame().GetPositionX() * window_scale_x + (unit_animation.GetCurrentFrame().GetWidth() * unit_animation.GetCurrentFrame().GetScaleX()) + bar_x_position),
			(float)(unit_animation.GetCurrentFrame().GetPositionY() * window_scale_y + (unit_animation.GetCurrentFrame().GetHeight() * unit_animation.GetCurrentFrame().GetScaleY()) + bar_y_position),
			(float)bar_animation.GetCurrentFrame().GetWidth() * bar_animation.GetCurrentFrame().GetScaleX() * window_scale_x,
			(float)bar_animation.GetCurrentFrame().GetHeight() * bar_animation.GetCurrentFrame().GetScaleY() * window_scale_y,
			NULL);

		health.DrawScaledImage((float)health_animation.GetCurrentFrame().GetSourceX(),
			(float)health_animation.GetCurrentFrame().GetSourceY(),
			(float)health_animation.GetCurrentFrame().GetWidth() * health_percentage,
			(float)health_animation.GetCurrentFrame().GetHeight(),
			(float)(unit_animation.GetCurrentFrame().GetPositionX() * window_scale_x + (unit_animation.GetCurrentFrame().GetWidth() * unit_animation.GetCurrentFrame().GetScaleX()) + health_x_position),
			(float)(unit_animation.GetCurrentFrame().GetPositionY() * window_scale_x + (unit_animation.GetCurrentFrame().GetHeight() * unit_animation.GetCurrentFrame().GetScaleY()) + health_y_position),
			(float)health_animation.GetCurrentFrame().GetWidth() * health_percentage * (float)health_animation.GetCurrentFrame().GetScaleX() * window_scale_x,
			(float)health_animation.GetCurrentFrame().GetHeight() * health_animation.GetCurrentFrame().GetScaleY() * window_scale_y,
			NULL);
	}
	else
	{
		bar.DrawScaledImage((float)bar_animation.GetCurrentFrame().GetSourceX(),
			(float)bar_animation.GetCurrentFrame().GetSourceY(),
			(float)bar_animation.GetCurrentFrame().GetWidth(),
			(float)bar_animation.GetCurrentFrame().GetHeight(),
			(float)unit_animation.GetCurrentFrame().GetPositionX() * window_scale_x - bar_x_position - (bar_animation.GetCurrentFrame().GetWidth() * bar_animation.GetCurrentFrame().GetScaleX()),
			(float)unit_animation.GetCurrentFrame().GetPositionY() * window_scale_y + (unit_animation.GetCurrentFrame().GetHeight() * unit_animation.GetCurrentFrame().GetScaleY()) + bar_y_position,
			(float)bar_animation.GetCurrentFrame().GetWidth() * bar_animation.GetCurrentFrame().GetScaleX() * window_scale_x,
			(float)bar_animation.GetCurrentFrame().GetHeight() * bar_animation.GetCurrentFrame().GetScaleY() * window_scale_y,
			NULL);

		health.DrawScaledImage((float)health_animation.GetCurrentFrame().GetSourceX(),
			(float)health_animation.GetCurrentFrame().GetSourceY(),
			(float)health_animation.GetCurrentFrame().GetWidth() * health_percentage,
			(float)health_animation.GetCurrentFrame().GetHeight(),
			(float)unit_animation.GetCurrentFrame().GetPositionX() * window_scale_x - health_x_position - (health_animation.GetCurrentFrame().GetWidth() * health_animation.GetCurrentFrame().GetScaleX()),
			(float)unit_animation.GetCurrentFrame().GetPositionY() * window_scale_y + (unit_animation.GetCurrentFrame().GetHeight() * unit_animation.GetCurrentFrame().GetScaleY()) + health_y_position,
			(float)health_animation.GetCurrentFrame().GetWidth() * health_percentage * (float)health_animation.GetCurrentFrame().GetScaleX() * window_scale_x,
			health_animation.GetCurrentFrame().GetHeight() * health_animation.GetCurrentFrame().GetScaleY() * window_scale_y,
			NULL);
	}
	
}

void Unit::TakeDamage(int damage, int bonus)
{
	//return; //TODO: REMOVE-USED FOR DEBUGGING
	health_points = health_points - ((damage + bonus) + defence_boost);

	frame_count_health = 0;

	if (health_points <= 0)
	{
		dead = true;
	}
}

void Unit::Heal(int health_boost)
{
	frame_count_health = 0;

	if (health_points + health_boost > max_health)
	{
		health_points = max_health;
	}
	else
	{
		health_points = health_points + health_boost;
	}
}

void Unit::SuicideAi(int position_x, int position_y)
{
	if (abs(unit_animation.GetCurrentFrame().GetPositionY() - position_y) > 20)
	{
		wander_count_suicide_ai += 1;
		if (wander_count_suicide_ai < wander_delay_suicide_ai)
		{
			if (suicide_ai_LR_move && unit_animation.GetCurrentFrame().GetPositionX() > 0)
				MoveLeft();
			else if(unit_animation.GetCurrentFrame().GetPositionX() < globals::GAME_WIDTH)
				MoveRight();
		}
		else
		{
			suicide_ai_LR_move = (suicide_ai_LR_move == false) ? true : false;
			wander_delay_suicide_ai = rand() % wander_max_suicide_ai + wander_min_suicide_ai;
			wander_count_suicide_ai = 0;
		}
	}
	else
	{
		if ((unit_animation.GetCurrentFrame().GetPositionX() + (unit_animation.GetCurrentFrame().GetWidth() * unit_animation.GetCurrentFrame().GetScaleX() / 2)) > (position_x + distance_x2))
		{
			MoveLeft();
		}
		else if ((unit_animation.GetCurrentFrame().GetPositionX() + (unit_animation.GetCurrentFrame().GetWidth() * unit_animation.GetCurrentFrame().GetScaleX() / 2)) < (position_x - distance_x2))
		{
			MoveRight();
		}
		if(which_player == 1)
			IdleHorizontal();
	} 

	if (which_player == 2 && unit_animation.GetCurrentFrame().GetPositionY() < position_y)
	{
		MoveDown();
	}
	else if (which_player == 1 && unit_animation.GetCurrentFrame().GetPositionY() > position_y)
	{
		MoveUp();
	}
}

void Unit::ProtectAi(int player_position_x, int player_position_y)
{
	switch (which_player)
	{
	case 1:
		// Handle the y position of the unit moving
		if (unit_animation.GetCurrentFrame().GetPositionY() > player_position_y - 100)
		{
			MoveUp();
		}
		else
		{
			IdleVerticle();
		}
		// Handle the x position of the unit moving
		if (unit_animation.GetCurrentFrame().GetPositionX() + (unit_animation.GetCurrentFrame().GetWidth() * unit_animation.GetCurrentFrame().GetScaleX()) / 2 < (player_position_x + distance_x) &&
			unit_animation.GetCurrentFrame().GetPositionX() + (unit_animation.GetCurrentFrame().GetWidth() * unit_animation.GetCurrentFrame().GetScaleX()) / 2 > (player_position_x - distance_x2))
		{
			IdleHorizontal();
		}
		else if (unit_animation.GetCurrentFrame().GetPositionX() + (unit_animation.GetCurrentFrame().GetWidth() * unit_animation.GetCurrentFrame().GetScaleX()) / 2 > (player_position_x - distance_x))
		{
			MoveLeft();
		}
		else if (unit_animation.GetCurrentFrame().GetPositionX() + (unit_animation.GetCurrentFrame().GetWidth() * unit_animation.GetCurrentFrame().GetScaleX()) / 2 < (player_position_x + distance_x2))
		{
			MoveRight();
		}
		break;
	case 2:
		// Handle the y position of the unit moving
		if (unit_animation.GetCurrentFrame().GetPositionY() < player_position_y + 100)
		{
			MoveDown();
		}
		else
		{
			IdleVerticle();
		}
		// Handle the x position of the unit moving
		if (unit_animation.GetCurrentFrame().GetPositionX() + (unit_animation.GetCurrentFrame().GetWidth() * unit_animation.GetCurrentFrame().GetScaleX()) / 2 < (player_position_x + distance_x) &&
			unit_animation.GetCurrentFrame().GetPositionX() + (unit_animation.GetCurrentFrame().GetWidth() * unit_animation.GetCurrentFrame().GetScaleX()) / 2 > (player_position_x - distance_x2))
		{
			IdleHorizontal();
		}
		else if (unit_animation.GetCurrentFrame().GetPositionX() + (unit_animation.GetCurrentFrame().GetWidth() * unit_animation.GetCurrentFrame().GetScaleX()) / 2 > (player_position_x - distance_x))
		{
			MoveLeft();
		}
		else if (unit_animation.GetCurrentFrame().GetPositionX() + (unit_animation.GetCurrentFrame().GetWidth() * unit_animation.GetCurrentFrame().GetScaleX()) / 2 < (player_position_x + distance_x2))
		{
			MoveRight();
		}
		break;
	}
}

void Unit::HealAi(int player_position_x, int player_position_y)
{
	switch (state)
	{
	case WANDER:
		if (time_waiting > time_to_move)
		{
			time_waiting = 0;

			if (which_player == 1)
			{
				x_pos = (rand() % 1000);
				y_pos = (rand() % 200) + 400;
			}
			else
			{
				x_pos = (rand() % 1000);
				y_pos = (rand() % 150) + 200;
			}
		}
		if (which_player == 1)
		{
			if (unit_animation.GetCurrentFrame().GetPositionY() < y_pos)
			{
				IdleVerticle();
			}
			else
			{
				MoveUp();
			}
		}
		else
		{
			if (unit_animation.GetCurrentFrame().GetPositionY() > y_pos)
			{
				IdleVerticle();
			}
			else
			{
				MoveDown();
			}
		}

		if ((x_pos - distance_x) < unit_animation.GetCurrentFrame().GetPositionX() &&
			(x_pos + distance_x2) > unit_animation.GetCurrentFrame().GetPositionX())
		{
			IdleHorizontal();
		}
		else if (x_pos > unit_animation.GetCurrentFrame().GetPositionX())
		{
			MoveRight();
		}
		else if (x_pos < unit_animation.GetCurrentFrame().GetPositionX())
		{
			MoveLeft();
		}

		if (which_player == 1)
		{
			if (unit_animation.GetCurrentFrame().GetPositionY() < y_pos && 
				(x_pos - distance_x) < unit_animation.GetCurrentFrame().GetPositionX() &&
				(x_pos + distance_x2) > unit_animation.GetCurrentFrame().GetPositionX())
			{
				stop_amount = stop_amount + 1;
				state = WAIT;
			}
		}
		else
		{
			if (unit_animation.GetCurrentFrame().GetPositionY() > y_pos &&
				(x_pos - distance_x) < unit_animation.GetCurrentFrame().GetPositionX() &&
				(x_pos + distance_x2) > unit_animation.GetCurrentFrame().GetPositionX())
			{
				stop_amount = stop_amount + 1;
				state = WAIT;
			}
		}

		break;
	case WAIT:
		time_waiting = time_waiting + 1;
		
		if (time_waiting > time_to_move)
		{
			if (stop_amount == heal_stops)
			{
				state = HEAL;
			}
			else
			{
				state = WANDER;
			}
		}
		break;
	case HEAL:
		if (unit_animation.GetCurrentFrame().GetPositionX() + (unit_animation.GetCurrentFrame().GetWidth() * unit_animation.GetCurrentFrame().GetScaleX() / 2) > player_position_x)
		{
			MoveLeft();
		}
		else
		{
			MoveRight();
		}

		if (which_player == 1)
		{
			if (unit_animation.GetCurrentFrame().GetPositionY() < player_position_y)
			{
				MoveDown();
			}
			else if (unit_animation.GetCurrentFrame().GetPositionY() >= player_position_y)
			{
				IdleVerticle();
			}
			break;
		}
		else
		{
			if (unit_animation.GetCurrentFrame().GetPositionY() > player_position_y)
			{
				MoveUp();
			}
			else if (unit_animation.GetCurrentFrame().GetPositionY() <= player_position_y)
			{
				IdleVerticle();
			}
			break;
		}
	}
}

// Support Unit 4
void Unit::SentryAi(int position_x, int position_y)
{
	// Fire bullets from unit to opponent. Use CollisionDetectionRadius? (and Separating Axis Thm?)
}

float Unit::GetBulletBoost() 
{ 
	return bullet_boost;
}
void Unit::BoostAttack(int boost)
{
	damage_boost += boost;
}

void Unit::BoostDefense(int boost)
{
	defence_boost += boost;
}

void Unit::BoostHealth(int boost)
{
	max_health += boost;
	health_points += boost;
}

void Unit::BoostSpeed(int boost)
{
	ship_velocity_x += boost;
	ship_velocity_y += boost;
}