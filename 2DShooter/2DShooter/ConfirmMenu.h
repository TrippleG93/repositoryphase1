#pragma once

#include "Render.h"
#include "SpriteAnimation.h"
#include "Font.h"
#include "Audio.h"


class ConfirmMenu
{
public:
	ConfirmMenu();
	~ConfirmMenu();

	/*
		SetUpConfrimBuildPhase
		Its going to load everything needed for the 
		Confirm menu
	*/
	void SetUpConfirmBuildPhase();

	/*
		SetUpQuitMenu
		Its going to load everything to give the player
		the option to quit the game
	*/
	void SetUpQuitMenu();

	/*
		UpdateConfirmMenu
		Will update the animations of the game
	*/
	void UpdateConfirmMenu();

	void UpdateQuitMenu();

	/*
		RenderConfrimMenu
		Draws everything for the confirm menu
	*/
	void RenderConfirmMenu(float window_scale_x, float window_scale_y);

	void RenderQuitMenu(float window_scale_x, float window_scale_y);

	/*
		MoveCursorRight
		Moves the cursor right if 
		the choice is on yes (True)
	*/
	void MoveCursorRight();

	/*
		MoveCurosrLeft
		Moves the cursor left if
		the choice is on no (False)
	*/
	void MoveCursorLeft();

	/*
		GetChoice
		Returns true if the user chose yes
		Returns false if the user chose no
	*/
	bool GetChoice();

private:

	bool choice;								// Boolean for yes or no
	int current_position;
	int cursor_left;
	int cursor_right;

	SpriteAnimation cursor_animation;			// Holds the animation of the cursor

	Render background_image;					// Holds the backgroud image
	int background_image_x;						// Holds the x position of the image
	int background_image_y;						// Holds the y position of the image

	Render work;

	Render cursor_image;						// Holds the cursor image

	Font emulogic_font20;						// Font type emulogic size 20
	Font emulogic_font24;						// Font type emulogic size 24

	Render yes_text_image;						// Holds the text "yes" as an image
	int yes_position_x;
	int yes_position_y;

	Render no_text_image;						// Holds the text "no" as an image
	int no_position_x;
	int no_position_y;

	Render title_text_image;					// Holds the text that is the puprose of the confirm
	int title_position_x;
	int title_position_y;

	Render error_text_image;					// Holds the text that is the puprose of the confirm
	int error_position_x;
	int error_position_y;
};

