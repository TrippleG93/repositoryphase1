#include "BuildMenuLoadFileCommand.h"

#include "State.h"
#include "BuildMenu.h"

BuildMenuLoadFileCommand::~BuildMenuLoadFileCommand()
{
}


void BuildMenuLoadFileCommand::execute(State &state)
{
	state.GetBuildMenu()->SetUpBuildMenu();
}