#include "CreateSupportUnit1P1.h"

#include "State.h"
#include "CombatPhase.h"
#include "UnitManager.h"

CreateSupportUnit1P1::~CreateSupportUnit1P1()
{
}

void CreateSupportUnit1P1::execute(State &state)
{
	state.GetCombatPhase()->GetUnitManager()->CreateSupportUnit1(5, 1);
}