#pragma once
#include "allegro5\allegro.h"
#include "Font.h"
#include "Render.h"
#include "AudiManager.h"

//For FPS
#define MAXSAMPLES 100

/*
State class
Handles every state in the game
*/
class TitleScreen;                          // Forward declaring the title screen class
class MainMenu;                             // Forward declaring the main menu class
class Display;                              // Forward declaring the display class
class InputHandler;                         // Forward declaring the input class
class BuildMenu;                            // Forward declaring the build menu class
class ConfirmMenu;                          // Forward declaring the confirm menu class
class OptionMenu;                           // Forward declaring the option menu class
class EndGameMenu;							// Forward declaring the end game menu class

class Player;								// Forward declaring the player class
class ComputerPlayer;						// Forward declaring the computer player class

class BuildPhase;							// Forward declaring the build phase class
class CombatPhase;							// Forward declaring the combat phase clas
class MapPhase;								// Forward declaring the map phase class

class State

{
public:
	State();
	~State();
	/*
	RunGameStates
	Runs the game loop that plays the game
	*/
	void RunGameStates();

	/*
	InitalizeTitleScreen
	Initialze everything needed for the title class
	*/
	void InitializeTitleScreen();

	/*
	DeleteTitleScreen
	Deletes everything from input controls to the class itself
	*/
	void DeleteTitleScreen();

	/*
	InitializeMainMenu
	Initialze evrything needed for the main menu
	*/
	void InitializeMainMenu();

	/*
	DeleteMainMenu
	Deletes everything from input controls to the class itself
	*/
	void DeleteMainMenu();

	/*
	InitializeGamePlay
	Initialize everything needed for the gameplay class
	*/
	void InitializeGamePlay();

	void InitCombatPhase();

	void DeleteCombatPhase();

	/*
	DeleteGamePlay
	Deletes everything from input controls to the class itself
	*/
	void DeleteGamePlay();

	/*
	InitializeBuildMenu
	Initialzie everything needed for the build menu class
	*/
	void InitializeBuildMenu();

	/*
	DeleteBuildMenu
	Deletes everything needed for the build menu
	*/
	void DeleteBuildMenu();

	/*
	InitializeConfirmBuilMenu
	FOr the class build menu will be set up
	so that it confirms for the build menu
	*/
	void InitializeConfirmBuildMenu();

	/*
	DeleteConfirmBuildMenu
	Deletes everything that was needed for the confirm menu
	*/
	void DeleteConfirmBuildMenu();

	/*
	InitializeOptionMenu
	For the option menu class it will get initilized
	and load all the controls for it
	*/
	void InitializeOptionMenu();

	/*
	DeleteOptionMenu
	Deletes everything that was needed for the option menu
	*/
	void DeleteOptionMenu();

	/*
		InitMapPhase
		Initialize everything with the map phase class
	*/
	void InitMapPhase();

	/*
		DeleteMapPhase
		Deletes everything with the map phase class
	*/
	void DeleteMapPhase();

	void InitEndGameMenu(int who_won);

	void DeleteEndGameMenu();

	void InitPauseMenu();

	void DeletePauseMenu();

	/*
	Set State
	Changes the current state of the game
	*/
	void SetState(int new_state);

	/*
	GetTitleScreen
	Returns the title screen
	*/
	TitleScreen* GetTitleScreen();

	/*
	GetMainMenu
	Returns the main menu class
	*/
	MainMenu* GetMainMenu();

	/*
	GetBuildMenu
	Returns the build menu class
	*/
	BuildMenu* GetBuildMenu();

	/*
	GetInputHandler
	Returns the input handler class
	*/
	InputHandler* GetInputHandler();

	/*
	GetConfirmMenu
	Returns the confirm menu class
	*/
	ConfirmMenu* GetConfirmMenu();

	/*
	GetOptionMenu
	returns option menu class
	*/
	OptionMenu* GetOptionMenu();

	/*
	GetDisplay
	Returns the display class
	*/
	Display* GetDisplay();

	/*
		GetPlayer
		Returns the player class
	*/
	Player* GetPlayer();

	/*
		ComputerPlayer
		Returns the computer player class
	*/
	ComputerPlayer* GetComputerPlayer();

	/*
		Returns the buiildPhase class
	*/
	BuildPhase* GetBuildPhase();

	CombatPhase* GetCombatPhase();

	/*
		GetMapPhase
		Returns the map phase class
	*/
	MapPhase* GetMapPhase();


	AudiManager& GetAudioManager() { return audio; }

	EndGameMenu* GetEndGameMenu() { return end_game_menu; }
	/*
	GetState
	Gets the curent state of the game
	*/
	int GetState();

	/*
	SetGameLoop
	Changes the game loop of the game
	*/
	void SetGameLoop(bool loop);

	/*
	HandleComputerInput
	Has its own input handler call that
	will be in every update call to handler
	the computer inputs
	*/
	void HandleComputerInput();


	double UpdateFPS();


private:
	void SetUpState();                                      // Update the state of the game
	void UpdateTitleState();                                // Update input of the title state
	void RenderTitleState();                                // Draw everything for the title state
	void UpdateMainMenuState();                             // Update main menu state
	void RenderMainMenuState();                             // Render main menu  state
	void UpdateBuildingState();                             // Update the building state based on the controls 
	void RenderBuildingState();                             // Draw the building state
	void UpdateBuildMenuState();                            // Update the build menu state
	void RenderBuildMenuState();                            // Draw the build menu state
	void UpdateConfirmMenu();                               // Update the confirm menu state
	void RenderConfirmMenu();                               // Draw the confirm menu
	void UpdateCombatPhaseState();                          // Updates the combat phase state
	void RenderCombatPhaseState();                          // Draws the combat phase state
	void UpdateOptionMenu();                                // Updates the option menu state
	void RenderOptionMenu();                                // Draws the option menu
	void UpdateMapPhase();									// Updates the map phase
	void RenderMapPhase();									// Draws the map phase state
	void UpdateEndGameMenu();
	void RenderEndGameMenu();
	void UpdatePauseMenu();
	void RenderPauseMenu();
												

	TitleScreen * title_screen = nullptr;                   // Runs the title screen of the game
	MainMenu *main_menu = nullptr;                          // Runs the main menu of the game
	BuildMenu *build_menu = nullptr;                        // Runs the build menu of the game
	ConfirmMenu *confirm_menu = nullptr;                    // Runs the confirm menu of the game
	OptionMenu *option_menu = nullptr;                      // Runs the option menu of the game
	EndGameMenu *end_game_menu = nullptr;					// Runs the end game menu of the game

	ALLEGRO_EVENT_QUEUE *event_queue;                       // Used to hold all events from allegro
	ALLEGRO_EVENT events;                                   // Holds and individual event from allegro
	ALLEGRO_TIMER *timer;                                   // Timer that allows 60 fps to happen

	Display *display = nullptr;                             // Pointer that holds the display class
	InputHandler *input_handler = nullptr;                  // Pointer that holds the input handler class

	//=============
	// DISPLAY FPS
	Font emulogic_font18;
	Render fps_text_image;
	double game_fps;
	double old_time;
	Player* player = nullptr;								// Pointer that holds the player class
	ComputerPlayer *computer = nullptr;						// Pointer that holds the computer player class

	BuildPhase* build_phase = nullptr;						// Pointer that holds the buildphase class
	CombatPhase* combat_phase = nullptr;					// Pointer that holds the combat phase class
	MapPhase* map_phase = nullptr;							// Pointer that holds the map phase class

	int tickindex = 0;
	int frame_delay_fps = 5;
	int frame_count_fps = 0;
	//=============

	int state;                                              // Keeps track of what state we are currently in
	int current_controller;                                 // Keeps track of what controller will currently make the input

	bool game_loop;                                         // Used for the game loop
	bool render;                                            // Lets the program know it needs to draw to the screen

	AudiManager audio;
};