#include "TitleLoadFilesCommand.h"

#include "State.h"
#include "TitleScreen.h"

TitleLoadFilesCommand::~TitleLoadFilesCommand()
{
}

void TitleLoadFilesCommand::execute(State &state)
{
	state.GetTitleScreen()->SetUpTitleScreen();
}