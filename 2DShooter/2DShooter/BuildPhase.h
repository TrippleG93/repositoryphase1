#pragma once

#include "SpriteAnimation.h"
#include "Render.h"
#include "Audio.h"
#include "Font.h"

#include <map>

class BuildingManager;					// Forward declaring the building manager class
class State;							// Forward declaring the state class

class BuildPhase
{
public:
	BuildPhase();
	~BuildPhase();

	/*
	SetUpBuildingPhase
	Loads everything needed for the class
	*/
	void SetUpBuildingPhase();

	/*
	UpdateBuildingPhase
	Updates the animations
	*/
	void UpdateBuildingPhase();

	/*
	RenderBuildingPhase
	Draws everything needed for the building phase
	*/
	void RenderBuildingPhase(float window_scale_x, float window_scale_y);

	/*
		MoveCursorUp
	*/
	void MoveCursorUp();

	/*
		MoveCursorDown
	*/
	void MoveCursorDown();

	/*
		MoveCursorLeft
	*/
	void MoveCursorLeft();

	/*
		MoveCursorRight
	*/
	void MoveCursorRight();

	/*
		GetCursorLocation
	*/
	int GetCursorLocation();

	/*
		GetCursorRow
	*/
	int GetCursorRow();

	/*
		GetCursorColumn
	*/
	int GetCursorColumn();

	/*
		SetBuildingSpot
	*/
	void SetBuildingSpot(int location, int type);

	/*
		ResetCursor
		Reset the cursor at the original position
	*/
	void ResetCursor();

	bool IsItComputerPlayerTurn();

	void NextBuildPlayer(State &state);

	bool DoesComputerExist();

	BuildingManager* GetBuildingManager();

	int GetCurrentBuildPlayer() { return current_build_player; }
private:
	int SPACING_UNIT = 50;

	bool computer_player;							// Holds if a computer player exist or not
	int computer_player_turn;						// Holds if it is the computer's turn or not
	int current_build_player;						// Holds which player is currently building
	const int PLAYERS_PLAYING = 2;					// Holds the max amount of players in the game

	int cursor_row;
	int cursor_column;

	int cursor_position_x;							// Holds the x position of the main cursor
	int cursor_position_y;							// Holds the y position of the main cursor
	int cursor1_choice;								// Holds what spot the cursor is at 

	BuildingManager* building_manager = nullptr;	// Holds the building manager class

	std::map<int, int> building_spots;				// Holds if that place has a building in it and its type

	SpriteAnimation platform_animation;				// Holds the animation coordinate of the platform
	SpriteAnimation cursor1_animation;				// Holds the first cursor type animation coordinates
	SpriteAnimation cursor2_animation;				// Holds the second cursor type animation coordinates

	Font emulogic_font18;							// Holds the type of font and size
	Font emulogic_font8;							// Holds the type of font and size

	Render title_text_image;						// Contains the text image of the title
	Render platform_image;							// Contains the image of the platform
	Render space_image;								// Contains an image of space
	Render cursor_image;							// Contains the image of the cursors

	Render controller_home_image;
	Render controller_image;
	SpriteAnimation controller_Home_animation;
	SpriteAnimation controller_LStick_animation;
	SpriteAnimation controller_A_animation;
	SpriteAnimation controller_B_animation;
	SpriteAnimation controller_Start_animation;

	Render lives_text_image;						// Contains the text image of lives
	int lives_text_x;
	int lives_text_y;

	Render controls_1;
	int controls_text_1_x;
	int controls_text_1_y;

	Render controls_2;
	int controls_text_2_x;
	int controls_text_2_y;

	Render controls_3;
	int controls_text_3_x;
	int controls_text_3_y;

	Render controls_4;
	int controls_text_4_x;
	int controls_text_4_y;

	Render controls_5;
	int controls_text_5_x;
	int controls_text_5_y;
};

