#include "BuildMenu.h"

#include "ConfigFileReader.h"

BuildMenu::BuildMenu()
{
	SetUpBuildMenu();
}


BuildMenu::~BuildMenu()
{
}

void BuildMenu::SetUpBuildMenu()
{
	ConfigFileReader file("Content/Text Files/Menus/BuildMenu.cfg");

	cursor_row = 1;												// Starting position of the cursor row
	cursor_column = 1;											// Starting position of the cursor column
	location = 1;

	building_set_image1.LoadBitmap("Content/Images/Buildings/Mobile - Breath of Fire 6 - Item Boosters.png");

	arrow_image.LoadBitmap("Content/Images/Others/Triangle.png");
	arrow_animation.AddIndividualFrame(file.GetConfigValueIntegers("arrow", "source x"),
		file.GetConfigValueIntegers("arrow", "source y"),
		file.GetConfigValueIntegers("arrow", "width"),
		file.GetConfigValueIntegers("arrow", "height"),
		file.GetConfigValueIntegers("arrow", "position x"),
		file.GetConfigValueIntegers("arrow", "position y"),
		file.GetConfigValueDouble("arrow", "scale x"),
		file.GetConfigValueDouble("arrow", "scale y"));
	arrow_animation.CombineFrames(file.GetConfigValueIntegers("arrow", "frame"), false);

	ship_image1.LoadBitmap("Content/Images/Ships/destroyer.png");
	ship_type_animation1.AddIndividualFrame(file.GetConfigValueIntegers("ship type 1", "source x"),
		file.GetConfigValueIntegers("ship type 1", "source y"),
		file.GetConfigValueIntegers("ship type 1", "width"),
		file.GetConfigValueIntegers("ship type 1", "height"),
		file.GetConfigValueIntegers("ship type 1", "position x"),
		file.GetConfigValueIntegers("ship type 1", "position y"),
		file.GetConfigValueDouble("ship type 1", "scale x"),
		file.GetConfigValueDouble("ship type 1", "scale y"));
	ship_type_animation1.CombineFrames(file.GetConfigValueIntegers("ship type 1", "frame"), false);

	ship_image2.LoadBitmap("Content/Images/Ships/blue1.png");
	ship_type_animation2.AddIndividualFrame(file.GetConfigValueIntegers("ship type 2", "source x"),
		file.GetConfigValueIntegers("ship type 2", "source y"),
		file.GetConfigValueIntegers("ship type 2", "width"),
		file.GetConfigValueIntegers("ship type 2", "height"),
		file.GetConfigValueIntegers("ship type 2", "position x"),
		file.GetConfigValueIntegers("ship type 2", "position y"),
		file.GetConfigValueDouble("ship type 2", "scale x"),
		file.GetConfigValueDouble("ship type 2", "scale y"));
	ship_type_animation2.CombineFrames(file.GetConfigValueIntegers("ship type 2", "frame"), false);

	ship_image3.LoadBitmap("Content/Images/Ships/smallgreen.png");
	ship_type_animation3.AddIndividualFrame(file.GetConfigValueIntegers("ship type 3", "source x"),
		file.GetConfigValueIntegers("ship type 3", "source y"),
		file.GetConfigValueIntegers("ship type 3", "width"),
		file.GetConfigValueIntegers("ship type 3", "height"),
		file.GetConfigValueIntegers("ship type 3", "position x"),
		file.GetConfigValueIntegers("ship type 3", "position y"),
		file.GetConfigValueDouble("ship type 3", "scale x"),
		file.GetConfigValueDouble("ship type 3", "scale y"));
	ship_type_animation3.CombineFrames(file.GetConfigValueIntegers("ship type 3", "frame"), false);

	ship_image4.LoadBitmap("Content/Images/Ships/topdownfighter.png");
	ship_type_animation4.AddIndividualFrame(file.GetConfigValueIntegers("ship type 4", "source x"),
		file.GetConfigValueIntegers("ship type 4", "source y"),
		file.GetConfigValueIntegers("ship type 4", "width"),
		file.GetConfigValueIntegers("ship type 4", "height"),
		file.GetConfigValueIntegers("ship type 4", "position x"),
		file.GetConfigValueIntegers("ship type 4", "position y"),
		file.GetConfigValueDouble("ship type 4", "scale x"),
		file.GetConfigValueDouble("ship type 4", "scale y"));
	ship_type_animation4.CombineFrames(file.GetConfigValueIntegers("ship type 4", "frame"), false);

	ship_image5.LoadBitmap("Content/Images/Ships/tribase-u2-d0.png");
	ship_type_animation5.AddIndividualFrame(file.GetConfigValueIntegers("ship type 5", "source x"),
		file.GetConfigValueIntegers("ship type 5", "source y"),
		file.GetConfigValueIntegers("ship type 5", "width"),
		file.GetConfigValueIntegers("ship type 5", "height"),
		file.GetConfigValueIntegers("ship type 5", "position x"),
		file.GetConfigValueIntegers("ship type 5", "position y"),
		file.GetConfigValueDouble("ship type 5", "scale x"),
		file.GetConfigValueDouble("ship type 5", "scale y"));
	ship_type_animation5.CombineFrames(file.GetConfigValueIntegers("ship type 5", "frame"), false);

	ship_image6.LoadBitmap("Content/Images/Ships/ship6.png");
	ship_type_animation6.AddIndividualFrame(file.GetConfigValueIntegers("ship type 6", "source x"),
		file.GetConfigValueIntegers("ship type 6", "source y"),
		file.GetConfigValueIntegers("ship type 6", "width"),
		file.GetConfigValueIntegers("ship type 6", "height"),
		file.GetConfigValueIntegers("ship type 6", "position x"),
		file.GetConfigValueIntegers("ship type 6", "position y"),
		file.GetConfigValueDouble("ship type 6", "scale x"),
		file.GetConfigValueDouble("ship type 6", "scale y"));
	ship_type_animation6.CombineFrames(file.GetConfigValueIntegers("ship type 6", "frame"), false);

	ship_image7.LoadBitmap("Content/Images/Ships/redship4.png");
	ship_type_animation7.AddIndividualFrame(file.GetConfigValueIntegers("ship type 7", "source x"),
		file.GetConfigValueIntegers("ship type 7", "source y"),
		file.GetConfigValueIntegers("ship type 7", "width"),
		file.GetConfigValueIntegers("ship type 7", "height"),
		file.GetConfigValueIntegers("ship type 7", "position x"),
		file.GetConfigValueIntegers("ship type 7", "position y"),
		file.GetConfigValueDouble("ship type 7", "scale x"),
		file.GetConfigValueDouble("ship type 7", "scale y"));
	ship_type_animation7.CombineFrames(file.GetConfigValueIntegers("ship type 7", "frame"), false);

	ship_image8.LoadBitmap("Content/Images/Ships/Spacestation-by-MillionthVector.png");
	ship_type_animation8.AddIndividualFrame(file.GetConfigValueIntegers("ship type 8", "source x"),
		file.GetConfigValueIntegers("ship type 8", "source y"),
		file.GetConfigValueIntegers("ship type 8", "width"),
		file.GetConfigValueIntegers("ship type 8", "height"),
		file.GetConfigValueIntegers("ship type 8", "position x"),
		file.GetConfigValueIntegers("ship type 8", "position y"),
		file.GetConfigValueDouble("ship type 8", "scale x"),
		file.GetConfigValueDouble("ship type 8", "scale y"));
	ship_type_animation8.CombineFrames(file.GetConfigValueIntegers("ship type 8", "frame"), false);

	building_type_animation9.AddIndividualFrame(file.GetConfigValueIntegers("building type 1", "source x"),
		file.GetConfigValueIntegers("building type 1", "source y"),
		file.GetConfigValueIntegers("building type 1", "width"),
		file.GetConfigValueIntegers("building type 1", "height"),
		file.GetConfigValueIntegers("building type 1", "position x"),
		file.GetConfigValueIntegers("building type 1", "position y"),
		file.GetConfigValueDouble("building type 1", "scale x"),
		file.GetConfigValueDouble("building type 1", "scale y"));
	building_type_animation9.CombineFrames(file.GetConfigValueIntegers("building type 1", "frame"), false);

	building_type_animation10.AddIndividualFrame(file.GetConfigValueIntegers("building type 2", "source x"),
		file.GetConfigValueIntegers("building type 2", "source y"),
		file.GetConfigValueIntegers("building type 2", "width"),
		file.GetConfigValueIntegers("building type 2", "height"),
		file.GetConfigValueIntegers("building type 2", "position x"),
		file.GetConfigValueIntegers("building type 2", "position y"),
		file.GetConfigValueDouble("building type 2", "scale x"),
		file.GetConfigValueDouble("building type 2", "scale y"));
	building_type_animation10.CombineFrames(file.GetConfigValueIntegers("building type 2", "frame"), false);

	building_type_animation11.AddIndividualFrame(file.GetConfigValueIntegers("building type 3", "source x"),
		file.GetConfigValueIntegers("building type 3", "source y"),
		file.GetConfigValueIntegers("building type 3", "width"),
		file.GetConfigValueIntegers("building type 3", "height"),
		file.GetConfigValueIntegers("building type 3", "position x"),
		file.GetConfigValueIntegers("building type 3", "position y"),
		file.GetConfigValueDouble("building type 3", "scale x"),
		file.GetConfigValueDouble("building type 3", "scale y"));
	building_type_animation11.CombineFrames(file.GetConfigValueIntegers("building type 3", "frame"), false);

	building_type_animation12.AddIndividualFrame(file.GetConfigValueIntegers("building type 4", "source x"),
		file.GetConfigValueIntegers("building type 4", "source y"),
		file.GetConfigValueIntegers("building type 4", "width"),
		file.GetConfigValueIntegers("building type 4", "height"),
		file.GetConfigValueIntegers("building type 4", "position x"),
		file.GetConfigValueIntegers("building type 4", "position y"),
		file.GetConfigValueDouble("building type 4", "scale x"),
		file.GetConfigValueDouble("building type 4", "scale y"));
	building_type_animation12.CombineFrames(file.GetConfigValueIntegers("building type 4", "frame"), false);

	building_bar_image.LoadBitmap("Content/Images/Buildings/BuildingBar.png");

	cursor_image.LoadBitmap("Content/Images/Others/Cursor.png");
	cursor_animation.AddIndividualFrame(file.GetConfigValueIntegers("cursor 1", "source x"),
		file.GetConfigValueIntegers("cursor 1", "source y"),
		file.GetConfigValueIntegers("cursor 1", "width"),
		file.GetConfigValueIntegers("cursor 1", "height"),
		file.GetConfigValueIntegers("cursor", "position x"),
		file.GetConfigValueIntegers("cursor", "position y"),
		file.GetConfigValueIntegers("cursor 1", "scale x"),
		file.GetConfigValueIntegers("cursor 1", "scale y"));
	cursor_animation.AddIndividualFrame(file.GetConfigValueIntegers("cursor 2", "source x"),
		file.GetConfigValueIntegers("cursor 2", "source y"),
		file.GetConfigValueIntegers("cursor 2", "width"),
		file.GetConfigValueIntegers("cursor 2", "height"),
		file.GetConfigValueIntegers("cursor", "position x"),
		file.GetConfigValueIntegers("cursor", "position y"),
		file.GetConfigValueIntegers("cursor 2", "scale x"),
		file.GetConfigValueIntegers("cursor 2", "scale y"));
	cursor_animation.AddIndividualFrame(file.GetConfigValueIntegers("cursor 3", "source x"),
		file.GetConfigValueIntegers("cursor 3", "source y"),
		file.GetConfigValueIntegers("cursor 3", "width"),
		file.GetConfigValueIntegers("cursor 3", "height"),
		file.GetConfigValueIntegers("cursor", "position x"),
		file.GetConfigValueIntegers("cursor", "position y"),
		file.GetConfigValueIntegers("cursor 3", "scale x"),
		file.GetConfigValueIntegers("cursor 3", "scale y"));
	cursor_animation.AddIndividualFrame(file.GetConfigValueIntegers("cursor 4", "source x"),
		file.GetConfigValueIntegers("cursor 4", "source y"),
		file.GetConfigValueIntegers("cursor 4", "width"),
		file.GetConfigValueIntegers("cursor 4", "height"),
		file.GetConfigValueIntegers("cursor", "position x"),
		file.GetConfigValueIntegers("cursor", "position y"),
		file.GetConfigValueIntegers("cursor 4", "scale x"),
		file.GetConfigValueIntegers("cursor 4", "scale y"));
	cursor_animation.AddIndividualFrame(file.GetConfigValueIntegers("cursor 5", "source x"),
		file.GetConfigValueIntegers("cursor 5", "source y"),
		file.GetConfigValueIntegers("cursor 5", "width"),
		file.GetConfigValueIntegers("cursor 5", "height"),
		file.GetConfigValueIntegers("cursor", "position x"),
		file.GetConfigValueIntegers("cursor", "position y"),
		file.GetConfigValueIntegers("cursor 5", "scale x"),
		file.GetConfigValueIntegers("cursor 5", "scale y"));
	cursor_animation.AddIndividualFrame(file.GetConfigValueIntegers("cursor 6", "source x"),
		file.GetConfigValueIntegers("cursor 6", "source y"),
		file.GetConfigValueIntegers("cursor 6", "width"),
		file.GetConfigValueIntegers("cursor 6", "height"),
		file.GetConfigValueIntegers("cursor", "position x"),
		file.GetConfigValueIntegers("cursor", "position y"),
		file.GetConfigValueIntegers("cursor 6", "scale x"),
		file.GetConfigValueIntegers("cursor 6", "scale y"));
	cursor_animation.AddIndividualFrame(file.GetConfigValueIntegers("cursor 7", "source x"),
		file.GetConfigValueIntegers("cursor 7", "source y"),
		file.GetConfigValueIntegers("cursor 7", "width"),
		file.GetConfigValueIntegers("cursor 7", "height"),
		file.GetConfigValueIntegers("cursor", "position x"),
		file.GetConfigValueIntegers("cursor", "position y"),
		file.GetConfigValueIntegers("cursor 7", "scale x"),
		file.GetConfigValueIntegers("cursor 7", "scale y"));
	cursor_animation.AddIndividualFrame(file.GetConfigValueIntegers("cursor 8", "source x"),
		file.GetConfigValueIntegers("cursor 8", "source y"),
		file.GetConfigValueIntegers("cursor 8", "width"),
		file.GetConfigValueIntegers("cursor 8", "height"),
		file.GetConfigValueIntegers("cursor", "position x"),
		file.GetConfigValueIntegers("cursor", "position y"),
		file.GetConfigValueIntegers("cursor 8", "scale x"),
		file.GetConfigValueIntegers("cursor 8", "scale y"));

	cursor_animation.CombineFrames(file.GetConfigValueIntegers("cursor", "frame"), true);

	emulogic_font16.CreateFont("Content/Fonts/Gravedigger.ttf", file.GetConfigValueIntegers("font 1", "size"));
	emulogic_font8.CreateFont("Content/Fonts/Gravedigger.ttf", file.GetConfigValueIntegers("font 2", "size"));
	
	ship_names_1.CreateTextRender(emulogic_font16.ObtainFont(),
		al_map_rgb(file.GetConfigValueIntegers("ship name text 1", "text r"),
			file.GetConfigValueIntegers("ship name text 1", "text g"),
			file.GetConfigValueIntegers("ship name text 1", "text b")),
		"ATTACK                                 DEFENSE                                 SPEED                          BALANCE");
	ship_name_text_1_x = file.GetConfigValueIntegers("ship name text 1", "position x");
	ship_name_text_1_y = file.GetConfigValueIntegers("ship name text 1", "position y");

	ship_names_2.CreateTextRender(emulogic_font16.ObtainFont(),
		al_map_rgb(file.GetConfigValueIntegers("ship name text 2", "text r"),
			file.GetConfigValueIntegers("ship name text 2", "text g"),
			file.GetConfigValueIntegers("ship name text 2", "text b")),
		"KAMIKAZE                              SHIELD                                      MEDIC                             SENTRY");
	ship_name_text_2_x = file.GetConfigValueIntegers("ship name text 2", "position x");
	ship_name_text_2_y = file.GetConfigValueIntegers("ship name text 2", "position y");

	ship_names_3.CreateTextRender(emulogic_font16.ObtainFont(),
		al_map_rgb(file.GetConfigValueIntegers("ship name text 3", "text r"),
			file.GetConfigValueIntegers("ship name text 3", "text g"),
			file.GetConfigValueIntegers("ship name text 3", "text b")),
		"ATTACK                                DEFENCE                                   HEALTH                              SPEED");
	ship_name_text_3_x = file.GetConfigValueIntegers("ship name text 3", "position x");
	ship_name_text_3_y = file.GetConfigValueIntegers("ship name text 3", "position y");

	description_text_image1.CreateTextRender(emulogic_font16.ObtainFont(), 
		al_map_rgb(file.GetConfigValueIntegers("text image 1", "text r"),
			file.GetConfigValueIntegers("text image 1", "text g"), 
			file.GetConfigValueIntegers("text image 1", "text b")),
		"Main  Fighter");
	text_image_1_x = file.GetConfigValueIntegers("text image 1", "position x");
	text_image_1_y = file.GetConfigValueIntegers("text image 1", "position y");

	description_text_image2.CreateTextRender(emulogic_font16.ObtainFont(),
		al_map_rgb(file.GetConfigValueIntegers("text image 2", "text r"),
			file.GetConfigValueIntegers("text image 2", "text g"),
			file.GetConfigValueIntegers("text image 2", "text b")), "Support Units");
	text_image_2_x = file.GetConfigValueIntegers("text image 2", "position x");
	text_image_2_y = file.GetConfigValueIntegers("text image 2", "position y");

	description_text_image3.CreateTextRender(emulogic_font16.ObtainFont(),
		al_map_rgb(file.GetConfigValueIntegers("text image 3", "text r"),
			file.GetConfigValueIntegers("text image 3", "text g"),
			file.GetConfigValueIntegers("text image 3", "text b")), "UPGRADES");
	text_image_3_x = file.GetConfigValueIntegers("text image 3", "position x");
	text_image_3_y = file.GetConfigValueIntegers("text image 3", "position y");

	x_mark_image.LoadBitmap("Content/Images/No/No.png");
	x_mark_animation.AddIndividualFrame(file.GetConfigValueIntegers("x image", "source x"),
		file.GetConfigValueIntegers("x image", "source y"),
		file.GetConfigValueIntegers("x image", "width"),
		file.GetConfigValueIntegers("x image", "height"),
		file.GetConfigValueIntegers("x image", "position x"),
		file.GetConfigValueIntegers("x image", "position y"),
		file.GetConfigValueDouble("x image", "scale x"),
		file.GetConfigValueDouble("x image", "scale y"));
}

void BuildMenu::Update()
{
	cursor_animation.Update();
}

void BuildMenu::MoveCursorLeft()
{
	if (cursor_column - 1 > 0)
	{
		cursor_column -= 1;
		location -= 1;
	}
}

void BuildMenu::MoveCursorRight()
{
	if (cursor_column + 1 < 5)
	{
		cursor_column += 1;
		location += 1;
	}
}

void BuildMenu::MoveCursorUp()
{
	if (cursor_row - 1 > 0)
	{
		cursor_row -= 1;
		location -= 4;
	}
}

void BuildMenu::MoveCursorDown()
{
	if (cursor_row + 1 < 4)
	{
		cursor_row += 1;
		location += 4;
	}
}

void BuildMenu::DrawBuildMenu(float window_scale_x, float window_scale_y)
{
	building_bar_image.DrawScaledImage(0, 0,
		(float)building_bar_image.GetBitmapWidth(),
		(float)building_bar_image.GetBitmapHeight(),
		0, 0,
		(float)building_bar_image.GetBitmapWidth() * window_scale_x,
		(float)building_bar_image.GetBitmapHeight() * window_scale_y,
		NULL);

	switch (cursor_row)
	{
	case 1:
		DrawRowOne(window_scale_x, window_scale_y);
		break;
	case 2:
		DrawRowTwo(window_scale_x, window_scale_y);
		break;
	case 3:
		DrawRowThree(window_scale_x, window_scale_y);
		break;
	default:
		break;
	}

	cursor_image.DrawScaledImage((float)cursor_animation.GetCurrentFrame().GetSourceX(),
		(float)cursor_animation.GetCurrentFrame().GetSourceY(),
		(float)cursor_animation.GetCurrentFrame().GetWidth(),
		(float)cursor_animation.GetCurrentFrame().GetHeight(),
		(200 + (250 * (cursor_column - 1) ))* window_scale_x,
		625 * window_scale_y,
		(float)cursor_animation.GetCurrentFrame().GetWidth() * window_scale_x,// * 2,
		(float)cursor_animation.GetCurrentFrame().GetHeight() * window_scale_y,// * 2,
		NULL);
}

void BuildMenu::DrawRowOne(float window_scale_x, float window_scale_y)
{
	arrow_image.DrawTintedScaledImage(gray,
		(float)arrow_animation.GetCurrentFrame().GetSourceX(),
		(float)arrow_animation.GetCurrentFrame().GetSourceY(),
		(float)arrow_animation.GetCurrentFrame().GetWidth(),
		(float)arrow_animation.GetCurrentFrame().GetHeight(),
		(float)arrow_animation.GetCurrentFrame().GetPositionX() * window_scale_x,
		(float)arrow_animation.GetCurrentFrame().GetPositionY() * window_scale_y +50,
		(float)arrow_animation.GetCurrentFrame().GetWidth() * window_scale_x * arrow_animation.GetCurrentFrame().GetScaleX(),
		(float)arrow_animation.GetCurrentFrame().GetHeight() * window_scale_y * arrow_animation.GetCurrentFrame().GetScaleY(),
		ALLEGRO_FLIP_VERTICAL);

	ship_image1.DrawScaledImage((float)ship_type_animation1.GetCurrentFrame().GetSourceX(),
		(float)ship_type_animation1.GetCurrentFrame().GetSourceY(),
		(float)ship_type_animation1.GetCurrentFrame().GetWidth(),
		(float)ship_type_animation1.GetCurrentFrame().GetHeight(),
		(float)ship_type_animation1.GetCurrentFrame().GetPositionX() * window_scale_x,
		(float)ship_type_animation1.GetCurrentFrame().GetPositionY() * window_scale_y,
		(float)ship_type_animation1.GetCurrentFrame().GetWidth() * window_scale_x * ship_type_animation1.GetCurrentFrame().GetScaleX(),
		(float)ship_type_animation1.GetCurrentFrame().GetHeight() * window_scale_y * ship_type_animation1.GetCurrentFrame().GetScaleY(),
		NULL);

	ship_image2.DrawScaledImage((float)ship_type_animation2.GetCurrentFrame().GetSourceX(),
		(float)ship_type_animation2.GetCurrentFrame().GetSourceY(),
		(float)ship_type_animation2.GetCurrentFrame().GetWidth(),
		(float)ship_type_animation2.GetCurrentFrame().GetHeight(),
		(float)ship_type_animation2.GetCurrentFrame().GetPositionX() * window_scale_x,
		(float)ship_type_animation2.GetCurrentFrame().GetPositionY() * window_scale_y,
		(float)ship_type_animation2.GetCurrentFrame().GetWidth() * window_scale_x * ship_type_animation2.GetCurrentFrame().GetScaleX(),
		(float)ship_type_animation2.GetCurrentFrame().GetHeight() * window_scale_y * ship_type_animation2.GetCurrentFrame().GetScaleY(),
		NULL);

	ship_image3.DrawScaledImage((float)ship_type_animation3.GetCurrentFrame().GetSourceX(),
		(float)ship_type_animation3.GetCurrentFrame().GetSourceY(),
		(float)ship_type_animation3.GetCurrentFrame().GetWidth(),
		(float)ship_type_animation3.GetCurrentFrame().GetHeight(),
		(float)ship_type_animation3.GetCurrentFrame().GetPositionX() * window_scale_x,
		(float)ship_type_animation3.GetCurrentFrame().GetPositionY() * window_scale_y,
		(float)ship_type_animation3.GetCurrentFrame().GetWidth() * window_scale_x * ship_type_animation3.GetCurrentFrame().GetScaleX(),
		(float)ship_type_animation3.GetCurrentFrame().GetHeight() * window_scale_y * ship_type_animation3.GetCurrentFrame().GetScaleY(),
		NULL);

	ship_image4.DrawScaledImage((float)ship_type_animation4.GetCurrentFrame().GetSourceX(),
		(float)ship_type_animation4.GetCurrentFrame().GetSourceY(),
		(float)ship_type_animation4.GetCurrentFrame().GetWidth(),
		(float)ship_type_animation4.GetCurrentFrame().GetHeight(),
		(float)ship_type_animation4.GetCurrentFrame().GetPositionX() * window_scale_x,
		(float)ship_type_animation4.GetCurrentFrame().GetPositionY() * window_scale_y,
		(float)ship_type_animation4.GetCurrentFrame().GetWidth() * window_scale_x * ship_type_animation4.GetCurrentFrame().GetScaleX(),
		(float)ship_type_animation4.GetCurrentFrame().GetHeight() * window_scale_y *  ship_type_animation4.GetCurrentFrame().GetScaleY(),
		NULL);

	description_text_image1.DrawScaledImage(0, 0,
		(float)description_text_image1.GetBitmapWidth(),
		(float)description_text_image1.GetBitmapHeight(),
		(float)text_image_1_x * window_scale_x,
		(float)text_image_1_y * window_scale_y,
		(float)description_text_image1.GetBitmapWidth() * window_scale_x,
		(float)description_text_image1.GetBitmapHeight() * window_scale_y,
		NULL);

	ship_names_1.DrawScaledImage(0, 0,
		(float)ship_names_1.GetBitmapWidth(),
		(float)ship_names_1.GetBitmapHeight(),
		(float)ship_name_text_1_x * window_scale_x,
		(float)ship_name_text_1_y * window_scale_y,
		(float)ship_names_1.GetBitmapWidth() * window_scale_x,
		(float)ship_names_1.GetBitmapHeight() * window_scale_y,
		NULL);

}

void BuildMenu::DrawRowTwo(float window_scale_x, float window_scale_y)
{
	arrow_image.DrawScaledImage((float)arrow_animation.GetCurrentFrame().GetSourceX(),
		(float)arrow_animation.GetCurrentFrame().GetSourceY(),
		(float)arrow_animation.GetCurrentFrame().GetWidth(),
		(float)arrow_animation.GetCurrentFrame().GetHeight(),
		(float)arrow_animation.GetCurrentFrame().GetPositionX() * window_scale_x,
		(float)arrow_animation.GetCurrentFrame().GetPositionY() * window_scale_y,
		(float)arrow_animation.GetCurrentFrame().GetWidth() * window_scale_x * arrow_animation.GetCurrentFrame().GetScaleX(),
		(float)arrow_animation.GetCurrentFrame().GetHeight() * window_scale_y * arrow_animation.GetCurrentFrame().GetScaleY(),
		NULL);
	arrow_image.DrawTintedScaledImage(gray,
		(float)arrow_animation.GetCurrentFrame().GetSourceX(),
		(float)arrow_animation.GetCurrentFrame().GetSourceY(),
		(float)arrow_animation.GetCurrentFrame().GetWidth(),
		(float)arrow_animation.GetCurrentFrame().GetHeight(),
		(float)arrow_animation.GetCurrentFrame().GetPositionX() * window_scale_x,
		(float)arrow_animation.GetCurrentFrame().GetPositionY() * window_scale_y + 50,
		(float)arrow_animation.GetCurrentFrame().GetWidth() * window_scale_x * arrow_animation.GetCurrentFrame().GetScaleX(),
		(float)arrow_animation.GetCurrentFrame().GetHeight() * window_scale_y * arrow_animation.GetCurrentFrame().GetScaleY(),
		ALLEGRO_FLIP_VERTICAL);

	ship_image5.DrawScaledImage((float)ship_type_animation5.GetCurrentFrame().GetSourceX(),
		(float)ship_type_animation5.GetCurrentFrame().GetSourceY(),
		(float)ship_type_animation5.GetCurrentFrame().GetWidth(),
		(float)ship_type_animation5.GetCurrentFrame().GetHeight(),
		(float)ship_type_animation5.GetCurrentFrame().GetPositionX() * window_scale_x,
		(float)ship_type_animation5.GetCurrentFrame().GetPositionY() * window_scale_y,
		(float)ship_type_animation5.GetCurrentFrame().GetWidth() * window_scale_x * ship_type_animation5.GetCurrentFrame().GetScaleX(),
		(float)ship_type_animation5.GetCurrentFrame().GetHeight() * window_scale_y * ship_type_animation5.GetCurrentFrame().GetScaleY(),
		NULL);

	ship_image6.DrawScaledImage((float)ship_type_animation6.GetCurrentFrame().GetSourceX(),
		(float)ship_type_animation6.GetCurrentFrame().GetSourceY(),
		(float)ship_type_animation6.GetCurrentFrame().GetWidth(),
		(float)ship_type_animation6.GetCurrentFrame().GetHeight(),
		(float)ship_type_animation6.GetCurrentFrame().GetPositionX() * window_scale_x,
		(float)ship_type_animation6.GetCurrentFrame().GetPositionY() * window_scale_y,
		(float)ship_type_animation6.GetCurrentFrame().GetWidth() * window_scale_x * ship_type_animation7.GetCurrentFrame().GetScaleX(),
		(float)ship_type_animation6.GetCurrentFrame().GetHeight() * window_scale_y * ship_type_animation6.GetCurrentFrame().GetScaleY(),
		NULL);

	ship_image7.DrawScaledImage((float)ship_type_animation7.GetCurrentFrame().GetSourceX(),
		(float)ship_type_animation7.GetCurrentFrame().GetSourceY(),
		(float)ship_type_animation7.GetCurrentFrame().GetWidth(),
		(float)ship_type_animation7.GetCurrentFrame().GetHeight(),
		(float)ship_type_animation7.GetCurrentFrame().GetPositionX() * window_scale_x,
		(float)ship_type_animation7.GetCurrentFrame().GetPositionY() * window_scale_y,
		(float)ship_type_animation7.GetCurrentFrame().GetWidth() * window_scale_x * ship_type_animation7.GetCurrentFrame().GetScaleX(),
		(float)ship_type_animation7.GetCurrentFrame().GetHeight() * window_scale_y * ship_type_animation7.GetCurrentFrame().GetScaleY(),
		NULL);

	ship_image8.DrawScaledImage((float)ship_type_animation8.GetCurrentFrame().GetSourceX(),
		(float)ship_type_animation8.GetCurrentFrame().GetSourceY(),
		(float)ship_type_animation8.GetCurrentFrame().GetWidth(),
		(float)ship_type_animation8.GetCurrentFrame().GetHeight(),
		(float)ship_type_animation8.GetCurrentFrame().GetPositionX() * window_scale_x,
		(float)ship_type_animation8.GetCurrentFrame().GetPositionY() * window_scale_y,
		(float)ship_type_animation8.GetCurrentFrame().GetWidth() * window_scale_x * ship_type_animation8.GetCurrentFrame().GetScaleX(),
		(float)ship_type_animation8.GetCurrentFrame().GetHeight() * window_scale_y * ship_type_animation8.GetCurrentFrame().GetScaleY(),
		NULL);

	description_text_image2.DrawScaledImage(0, 0,
		(float)description_text_image2.GetBitmapWidth(),
		(float)description_text_image2.GetBitmapHeight(),
		text_image_2_x * window_scale_x,
		text_image_2_y * window_scale_y,
		(float)description_text_image2.GetBitmapWidth() * window_scale_x,
		(float)description_text_image2.GetBitmapHeight() * window_scale_y,
		NULL);

	ship_names_2.DrawScaledImage(0, 0,
		(float)ship_names_2.GetBitmapWidth(),
		(float)ship_names_2.GetBitmapHeight(),
		(float)ship_name_text_2_x * window_scale_x,
		(float)ship_name_text_2_y * window_scale_y,
		(float)ship_names_2.GetBitmapWidth() * window_scale_x,
		(float)ship_names_2.GetBitmapHeight() * window_scale_y,
		NULL);
}

void BuildMenu::DrawRowThree(float window_scale_x, float window_scale_y)
{
	arrow_image.DrawScaledImage((float)arrow_animation.GetCurrentFrame().GetSourceX(),
		(float)arrow_animation.GetCurrentFrame().GetSourceY(),
		(float)arrow_animation.GetCurrentFrame().GetWidth(),
		(float)arrow_animation.GetCurrentFrame().GetHeight(),
		(float)arrow_animation.GetCurrentFrame().GetPositionX() * window_scale_x,
		(float)arrow_animation.GetCurrentFrame().GetPositionY() * window_scale_y,
		(float)arrow_animation.GetCurrentFrame().GetWidth() * window_scale_x * arrow_animation.GetCurrentFrame().GetScaleX(),
		(float)arrow_animation.GetCurrentFrame().GetHeight() * window_scale_y * arrow_animation.GetCurrentFrame().GetScaleY(),
		NULL);

	building_set_image1.DrawScaledImage((float)building_type_animation9.GetCurrentFrame().GetSourceX(),
		(float)building_type_animation9.GetCurrentFrame().GetSourceY(),
		(float)building_type_animation9.GetCurrentFrame().GetWidth(),
		(float)building_type_animation9.GetCurrentFrame().GetHeight(),
		(float)building_type_animation9.GetCurrentFrame().GetPositionX() * window_scale_x,
		(float)building_type_animation9.GetCurrentFrame().GetPositionY() * window_scale_y,
		(float)building_type_animation9.GetCurrentFrame().GetWidth() * window_scale_x * building_type_animation9.GetCurrentFrame().GetScaleX(),
		(float)building_type_animation9.GetCurrentFrame().GetHeight() * window_scale_y * building_type_animation9.GetCurrentFrame().GetScaleY(),
		NULL);

	building_set_image1.DrawScaledImage((float)building_type_animation10.GetCurrentFrame().GetSourceX(),
		(float)building_type_animation10.GetCurrentFrame().GetSourceY(),
		(float)building_type_animation10.GetCurrentFrame().GetWidth(),
		(float)building_type_animation10.GetCurrentFrame().GetHeight(),
		(float)building_type_animation10.GetCurrentFrame().GetPositionX() * window_scale_x,
		(float)building_type_animation10.GetCurrentFrame().GetPositionY() * window_scale_y,
		(float)building_type_animation10.GetCurrentFrame().GetWidth() * window_scale_x * building_type_animation10.GetCurrentFrame().GetScaleX(),
		(float)building_type_animation10.GetCurrentFrame().GetHeight() * window_scale_y * building_type_animation10.GetCurrentFrame().GetScaleY(),
		NULL);

	building_set_image1.DrawScaledImage((float)building_type_animation11.GetCurrentFrame().GetSourceX(),
		(float)building_type_animation11.GetCurrentFrame().GetSourceY(),
		(float)building_type_animation11.GetCurrentFrame().GetWidth(),
		(float)building_type_animation11.GetCurrentFrame().GetHeight(),
		(float)building_type_animation11.GetCurrentFrame().GetPositionX() * window_scale_x,
		(float)building_type_animation11.GetCurrentFrame().GetPositionY() * window_scale_y,
		(float)building_type_animation11.GetCurrentFrame().GetWidth() * window_scale_x * building_type_animation11.GetCurrentFrame().GetScaleX(),
		(float)building_type_animation11.GetCurrentFrame().GetHeight() * window_scale_y * building_type_animation11.GetCurrentFrame().GetScaleY(),
		NULL);

	building_set_image1.DrawScaledImage((float)building_type_animation12.GetCurrentFrame().GetSourceX(),
		(float)building_type_animation12.GetCurrentFrame().GetSourceY(),
		(float)building_type_animation12.GetCurrentFrame().GetWidth(),
		(float)building_type_animation12.GetCurrentFrame().GetHeight(),
		(float)building_type_animation12.GetCurrentFrame().GetPositionX() * window_scale_x,
		(float)building_type_animation12.GetCurrentFrame().GetPositionY() * window_scale_y,
		(float)building_type_animation12.GetCurrentFrame().GetWidth() * window_scale_x * building_type_animation12.GetCurrentFrame().GetScaleX(),
		(float)building_type_animation12.GetCurrentFrame().GetHeight() * window_scale_y * building_type_animation12.GetCurrentFrame().GetScaleY(),
		NULL);

	description_text_image3.DrawScaledImage(0, 0,
		(float)description_text_image3.GetBitmapWidth(),
		(float)description_text_image3.GetBitmapHeight(),
		text_image_3_x * window_scale_x,
		text_image_3_y * window_scale_y,
		(float)description_text_image3.GetBitmapWidth() * window_scale_x,
		(float)description_text_image3.GetBitmapHeight() * window_scale_y,
		NULL);

	ship_names_3.DrawScaledImage(0, 0,
		(float)ship_names_3.GetBitmapWidth(),
		(float)ship_names_3.GetBitmapHeight(),
		(float)ship_name_text_3_x * window_scale_x,
		(float)ship_name_text_3_y * window_scale_y,
		(float)ship_names_3.GetBitmapWidth() * window_scale_x,
		(float)ship_names_3.GetBitmapHeight() * window_scale_y,
		NULL);
}

int BuildMenu::GetBuildingType()
{
	return location;
}

void BuildMenu::DrawXImage(int position_x, int position_y, float window_scale_x, float window_scale_y)
{
	x_mark_image.DrawScaledImage(x_mark_animation.GetCurrentFrame().GetSourceX(),
		x_mark_animation.GetCurrentFrame().GetSourceY(),
		x_mark_animation.GetCurrentFrame().GetWidth(),
		x_mark_animation.GetCurrentFrame().GetHeight(),
		((float)x_mark_animation.GetCurrentFrame().GetPositionX() + (float)position_x) * window_scale_x,
		((float)x_mark_animation.GetCurrentFrame().GetPositionY() + (float)position_y) * window_scale_y,
		x_mark_animation.GetCurrentFrame().GetWidth() * window_scale_x * x_mark_animation.GetCurrentFrame().GetScaleX(),
		x_mark_animation.GetCurrentFrame().GetHeight() * window_scale_y * x_mark_animation.GetCurrentFrame().GetScaleY(),
		NULL);
}