#pragma once

#include "Command.h"

class CursorDownBuildingPhaseCommand : public Command
{
public:

	~CursorDownBuildingPhaseCommand();

	virtual void execute() {};
	virtual void execute(State& state);
};

