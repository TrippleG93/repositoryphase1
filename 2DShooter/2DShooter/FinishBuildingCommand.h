#pragma once
#include "Command.h"

class FinishBuildingCommand : public Command
{
public:
	~FinishBuildingCommand();

	virtual void execute() {};
	virtual void execute(State& state);
};

