#pragma once

#include "Command.h"

class OptionLoadFileCommand : public Command
{
public:
	~OptionLoadFileCommand();

	virtual void execute() {};
	virtual void execute(State &state);
};

