#include "CreateSupportUnit2P1.h"

#include "State.h"
#include "CombatPhase.h"
#include "UnitManager.h"

CreateSupportUnit2P1::~CreateSupportUnit2P1()
{
}

void CreateSupportUnit2P1::execute(State &state)
{
	state.GetCombatPhase()->GetUnitManager()->CreateSupportUnit1(6, 1);
}
