#include "CursorLeftBuildingPhaseCommand.h"

#include "State.h"
#include "Globals.h"

#include "BuildPhase.h"

CursorLeftBuildingPhaseCommand::~CursorLeftBuildingPhaseCommand()
{

}

void CursorLeftBuildingPhaseCommand::execute(State &state)
{
	state.GetBuildPhase()->MoveCursorLeft();
	state.GetAudioManager().PlayCursorMove();
}