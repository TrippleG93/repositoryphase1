#pragma once

#include "Render.h"
#include "SpriteAnimation.h"
#include "Font.h"
#include "Audio.h"

class BuildMenu
{
public:
	BuildMenu();
	~BuildMenu();

	/*
	SetUpBuildMenu
	Load everything needed for the class
	and initialize the variables
	*/
	void SetUpBuildMenu();

	/*
	Update
	updates the animations
	*/
	void Update();

	/*
	DrawBuildMenu
	Draws everything associated with the class
	*/
	void DrawBuildMenu(float window_scale_x, float window_scale_y);

	/*
	MoveCursorRight
	Will add one to the cursor column
	*/
	void MoveCursorRight();

	/*
	MoveCursorLeft
	Will Subtract one to the cursor column
	*/
	void MoveCursorLeft();

	/*
	MoveCursorDown
	Will add one to the row cursor
	*/
	void MoveCursorDown();

	/*
	MoveCursorUp
	Will subtract one to the cursor row
	*/
	void MoveCursorUp();

	/*
		GetBuildingType
		returns the building type based off of where the 
		cursor location is at
	*/
	int GetBuildingType();

	void DrawXImage(int position_x, int position_y, float window_scale_x, float window_scale_y);

	int GetCursorRow() { return cursor_row; }

private:

	/*
		DrawRowOne
		Draws the buildings that are linked to the main ship
	*/
	void DrawRowOne(float window_scale_x, float window_scale_y);

	/*
		DrawRowTwo
		Draws the buildings that are linked to the support ship
	*/
	void DrawRowTwo(float window_scale_x, float window_scale_y);

	/*
		DrawRowThree
		Draws the buildings that are linked to the buffs for the ship
	*/
	void DrawRowThree(float window_scale_x, float window_scale_y);

	int cursor_row;									// Holds the cursor row position
	int cursor_column;								// Holds the cursor column position
	int location;									// Holds the location (Platform 5, 6, ect)

	SpriteAnimation cursor_animation;
	
	SpriteAnimation building_type_animation9;
	SpriteAnimation building_type_animation10;
	SpriteAnimation building_type_animation11;
	SpriteAnimation building_type_animation12;

	// Main ships
	SpriteAnimation ship_type_animation1;
	SpriteAnimation ship_type_animation2;
	SpriteAnimation ship_type_animation3;
	SpriteAnimation ship_type_animation4;

	// Support ship
	SpriteAnimation ship_type_animation5;
	SpriteAnimation ship_type_animation6;
	SpriteAnimation ship_type_animation7;
	SpriteAnimation ship_type_animation8;

	Render building_set_image1;
	Render building_set_image2;
	Render building_set_image3;
	Render building_set_image4;

	// Ships instead of buildings
	Render ship_image1;
	Render ship_image2;
	Render ship_image3;
	Render ship_image4;
	Render ship_image5;
	Render ship_image6;
	Render ship_image7;
	Render ship_image8;

	Render building_bar_image;
	Render cursor_image;

	Font emulogic_font16;
	Font emulogic_font8;

	Render x_mark_image;
	SpriteAnimation x_mark_animation;
	int x_mark_x;
	int x_mark_y;

	Render description_text_image1;				// Will Contain text for what row its on "Main Ship Buildings"
	int text_image_1_x;
	int text_image_1_y;

	Render description_text_image2;				// Will contain text for what row its on "Support Ship Buildings"
	int text_image_2_x;
	int text_image_2_y;

	Render description_text_image3;				// Will contain text for what row its on "Buff buildings"
	int text_image_3_x;
	int text_image_3_y;

	Render ship_names_1;
	int ship_name_text_1_x;
	int ship_name_text_1_y;

	Render ship_names_2;
	int ship_name_text_2_x;
	int ship_name_text_2_y;

	Render ship_names_3;
	int ship_name_text_3_x;
	int ship_name_text_3_y;

	Render arrow_image;
	SpriteAnimation arrow_animation;
	ALLEGRO_COLOR gray = { 1, 1, 1, 0.75 };
};

