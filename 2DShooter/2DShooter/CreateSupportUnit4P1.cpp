#include "CreateSupportUnit4P1.h"

#include "State.h"
#include "CombatPhase.h"
#include "UnitManager.h"

CreateSupportUnit4P1::~CreateSupportUnit4P1()
{
}

void CreateSupportUnit4P1::execute(State &state)
{
	state.GetCombatPhase()->GetUnitManager()->CreateSupportUnit1(8, 1);
}
