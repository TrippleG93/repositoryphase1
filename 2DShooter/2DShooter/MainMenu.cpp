#include "MainMenu.h"
#include "ConfigFileReader.h"

MainMenu::MainMenu() :
	choice(0),
	y_position_cursor(0)
{
	SetUpMainMenu();
}


MainMenu::~MainMenu()
{
	// TODO: FIGURE OUT THE MUSIC
	//	main_menu_song.StopSong();
}

void MainMenu::SetUpMainMenu()
{
	ConfigFileReader file("Content/Text Files/Menus/MainMenu.cfg");
	// Background
	background_image.LoadBitmap("Content/Images/Combat Phase Backgrounds/PC Computer - Space Pirates and Zombies - Backgrounds 22/Building_Background.png");

	emulogic_font24.CreateFont("Content/Fonts/Gravedigger.ttf", file.GetConfigValueIntegers("Font 1", "size"));
	start_game_text.CreateTextRender(emulogic_font24.ObtainFont(), 
		al_map_rgb(file.GetConfigValueIntegers("Start Text", "text r"),
			file.GetConfigValueIntegers("Start Text", "text g"),
			file.GetConfigValueIntegers("Start Text", "text b")),
		"START  GAME");
	start_text_x = file.GetConfigValueIntegers("Start Text", "position x");
	start_text_y = file.GetConfigValueIntegers("Start Text", "position y");

	quit_text.CreateTextRender(emulogic_font24.ObtainFont(), 
		al_map_rgb(file.GetConfigValueIntegers("Quit Text", "text r"),
			file.GetConfigValueIntegers("Quit Text", "text g"),
			file.GetConfigValueIntegers("Quit Text", "text b")),
		"QUIT");
	quit_text_x = file.GetConfigValueIntegers("Quit Text", "position x");
	quit_text_y = file.GetConfigValueIntegers("Quit Text", "position y");

	emulogic_font26.CreateFont("Content/Fonts/Gravedigger.ttf", file.GetConfigValueIntegers("Font 2", "size"));
	options_text.CreateTextRender(emulogic_font26.ObtainFont(),
		al_map_rgb(file.GetConfigValueIntegers("Option Text", "text r"),
			file.GetConfigValueIntegers("Option Text", "text g"),
			file.GetConfigValueIntegers("Option Text", "text b")),
		"OPTIONS");
	option_text_x = file.GetConfigValueIntegers("Option Text", "position x");
	option_text_y = file.GetConfigValueIntegers("Option Text", "position y");

	emulogic_font76.CreateFont("Content/Fonts/Gravedigger.ttf", file.GetConfigValueIntegers("Font 3", "size"));
	main_menu_text.CreateTextRender(emulogic_font76.ObtainFont(),
		al_map_rgb(file.GetConfigValueIntegers("Main Menu Text", "text r"),
			file.GetConfigValueIntegers("Main Menu Text", "text g"),
			file.GetConfigValueIntegers("Main Menu Text", "text b")), 
		"MAIN  MENU");
	main_text_x = file.GetConfigValueIntegers("Main Menu Text", "position x");
	main_text_y = file.GetConfigValueIntegers("Main Menu Text", "position y");
	
	cursor_image.LoadBitmap("Content/Images/Others/Cursor.png");
	cursor_animation.AddIndividualFrame(file.GetConfigValueIntegers("Cursor Animation 1", "source x"),
		file.GetConfigValueIntegers("Cursor Animation 1", "source y"),
		file.GetConfigValueIntegers("Cursor Animation 1", "width"),
		file.GetConfigValueIntegers("Cursor Animation 1", "height"),
		file.GetConfigValueIntegers("Cursor Animation 1", "position x"), 
		file.GetConfigValueIntegers("Cursor Animation 1", "position y"),
		file.GetConfigValueIntegers("Cursor Animation 1", "scale x"), 
		file.GetConfigValueIntegers("Cursor Animation 1", "scale y"));
	cursor_animation.AddIndividualFrame(file.GetConfigValueIntegers("Cursor Animation 2", "source x"),
		file.GetConfigValueIntegers("Cursor Animation 2", "source y"),
		file.GetConfigValueIntegers("Cursor Animation 2", "width"),
		file.GetConfigValueIntegers("Cursor Animation 2", "height"),
		file.GetConfigValueIntegers("Cursor Animation 2", "position x"),
		file.GetConfigValueIntegers("Cursor Animation 2", "position y"),
		file.GetConfigValueIntegers("Cursor Animation 2", "scale x"),
		file.GetConfigValueIntegers("Cursor Animation 2", "scale y"));
	cursor_animation.AddIndividualFrame(file.GetConfigValueIntegers("Cursor Animation 3", "source x"),
		file.GetConfigValueIntegers("Cursor Animation 3", "source y"),
		file.GetConfigValueIntegers("Cursor Animation 3", "width"),
		file.GetConfigValueIntegers("Cursor Animation 3", "height"),
		file.GetConfigValueIntegers("Cursor Animation 3", "position x"),
		file.GetConfigValueIntegers("Cursor Animation 3", "position y"),
		file.GetConfigValueIntegers("Cursor Animation 3", "scale x"),
		file.GetConfigValueIntegers("Cursor Animation 3", "scale y"));
	cursor_animation.AddIndividualFrame(file.GetConfigValueIntegers("Cursor Animation 4", "source x"),
		file.GetConfigValueIntegers("Cursor Animation 4", "source y"),
		file.GetConfigValueIntegers("Cursor Animation 4", "width"),
		file.GetConfigValueIntegers("Cursor Animation 4", "height"),
		file.GetConfigValueIntegers("Cursor Animation 4", "position x"),
		file.GetConfigValueIntegers("Cursor Animation 4", "position y"),
		file.GetConfigValueIntegers("Cursor Animation 4", "scale x"),
		file.GetConfigValueIntegers("Cursor Animation 4", "scale y"));
	cursor_animation.AddIndividualFrame(file.GetConfigValueIntegers("Cursor Animation 5", "source x"),
		file.GetConfigValueIntegers("Cursor Animation 5", "source y"),
		file.GetConfigValueIntegers("Cursor Animation 5", "width"),
		file.GetConfigValueIntegers("Cursor Animation 5", "height"),
		file.GetConfigValueIntegers("Cursor Animation 5", "position x"),
		file.GetConfigValueIntegers("Cursor Animation 5", "position y"),
		file.GetConfigValueIntegers("Cursor Animation 5", "scale x"),
		file.GetConfigValueIntegers("Cursor Animation 5", "scale y"));
	cursor_animation.AddIndividualFrame(file.GetConfigValueIntegers("Cursor Animation 6", "source x"),
		file.GetConfigValueIntegers("Cursor Animation 6", "source y"),
		file.GetConfigValueIntegers("Cursor Animation 6", "width"),
		file.GetConfigValueIntegers("Cursor Animation 6", "height"),
		file.GetConfigValueIntegers("Cursor Animation 6", "position x"),
		file.GetConfigValueIntegers("Cursor Animation 6", "position y"),
		file.GetConfigValueIntegers("Cursor Animation 6", "scale x"),
		file.GetConfigValueIntegers("Cursor Animation 6", "scale y"));
	cursor_animation.AddIndividualFrame(file.GetConfigValueIntegers("Cursor Animation 7", "source x"),
		file.GetConfigValueIntegers("Cursor Animation 7", "source y"),
		file.GetConfigValueIntegers("Cursor Animation 7", "width"),
		file.GetConfigValueIntegers("Cursor Animation 7", "height"),
		file.GetConfigValueIntegers("Cursor Animation 7", "position x"),
		file.GetConfigValueIntegers("Cursor Animation 7", "position y"),
		file.GetConfigValueIntegers("Cursor Animation 7", "scale x"),
		file.GetConfigValueIntegers("Cursor Animation 7", "scale y"));
	cursor_animation.AddIndividualFrame(file.GetConfigValueIntegers("Cursor Animation 8", "source x"),
		file.GetConfigValueIntegers("Cursor Animation 8", "source y"),
		file.GetConfigValueIntegers("Cursor Animation 8", "width"),
		file.GetConfigValueIntegers("Cursor Animation 8", "height"),
		file.GetConfigValueIntegers("Cursor Animation 8", "position x"),
		file.GetConfigValueIntegers("Cursor Animation 8", "position y"),
		file.GetConfigValueIntegers("Cursor Animation 8", "scale x"),
		file.GetConfigValueIntegers("Cursor Animation 8", "scale y"));
	cursor_animation.CombineFrames(file.GetConfigValueIntegers("Cursor Animation 1", "frame"), true);
}

void MainMenu::UpdateMainMenu()
{
	cursor_animation.Update();

	switch (choice)
	{
	case 0:
		cursor_animation.GetCurrentFrame().SetYPosition(300);
		break;
	case 1:
		cursor_animation.GetCurrentFrame().SetYPosition(350);
		break;
	case 2:
		cursor_animation.GetCurrentFrame().SetYPosition(450);
		break;
	default:
		break;
	}
}

void MainMenu::RenderMainMenu(float window_scale_x, float window_scale_y)
{
	// TODO: PUT INTO GLOBAL HEADER
	int SCALE_CURSOR = 2;

	// BACKGROUND
	background_image.DrawScaledImage(0, 0,
		background_image.GetBitmapWidth(),
		background_image.GetBitmapHeight(),
		0 * window_scale_x,
		0 * window_scale_y,
		background_image.GetBitmapWidth() * window_scale_x,
		background_image.GetBitmapHeight() * window_scale_y,
		NULL);

	// MAIN MENU
	main_menu_text.DrawScaledImage(0, 0,
		main_menu_text.GetBitmapWidth(),
		main_menu_text.GetBitmapHeight(),
		(main_text_x - main_menu_text.GetBitmapWidth() / 2)* window_scale_x,
		main_text_y * window_scale_y,
		main_menu_text.GetBitmapWidth() * window_scale_x,
		main_menu_text.GetBitmapHeight() * window_scale_y,
		NULL);

	// START GAME
	start_game_text.DrawScaledImage(0, 0,
		start_game_text.GetBitmapWidth(),
		start_game_text.GetBitmapHeight(),
		(start_text_x - start_game_text.GetBitmapWidth() / 2)* window_scale_x,
		start_text_y * window_scale_y,
		start_game_text.GetBitmapWidth() * window_scale_x,
		start_game_text.GetBitmapHeight() * window_scale_y,
		NULL);

	// OPTIONS
	options_text.DrawScaledImage(0, 0,
		options_text.GetBitmapWidth(),
		options_text.GetBitmapHeight(),
		(option_text_x - options_text.GetBitmapWidth() / 2)* window_scale_x,
		option_text_y * window_scale_y,
		options_text.GetBitmapWidth() * window_scale_x,
		options_text.GetBitmapHeight() * window_scale_y,
		NULL);

	// QUIT 
	quit_text.DrawScaledImage(0, 0,
		quit_text.GetBitmapWidth(),
		quit_text.GetBitmapHeight(),
		(quit_text_x - quit_text.GetBitmapWidth() * window_scale_x / 2)* window_scale_x,
		quit_text_y * window_scale_y,
		quit_text.GetBitmapWidth() * window_scale_x,
		quit_text.GetBitmapHeight() * window_scale_y,
		NULL);

	// CURSOR
	cursor_image.DrawScaledImage(cursor_animation.GetCurrentFrame().GetSourceX(),
		cursor_animation.GetCurrentFrame().GetSourceY(),
		cursor_animation.GetCurrentFrame().GetWidth(),
		cursor_animation.GetCurrentFrame().GetHeight(),
		cursor_animation.GetCurrentFrame().GetPositionX() * window_scale_x,
		(cursor_animation.GetCurrentFrame().GetPositionY() + 15) * window_scale_y,
		cursor_animation.GetCurrentFrame().GetWidth() * window_scale_x * cursor_animation.GetCurrentFrame().GetScaleX(),
		cursor_animation.GetCurrentFrame().GetHeight() * window_scale_y * cursor_animation.GetCurrentFrame().GetScaleY(),
		NULL);
}

void MainMenu::MoveCursorDown()
{
	if (choice + 1 <= 2)
	{
		choice += 1;
	}
}

void MainMenu::MoveCursorUp()
{
	if (choice - 1 >= 0)
	{
		choice -= 1;
	}
}

int MainMenu::GetChoice()
{
	return choice;
}