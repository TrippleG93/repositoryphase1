#include "MoveUnitLeftCommandP2.h"

#include "State.h"

#include "UnitManager.h"
#include "Unit.h"

#include "CombatPhase.h"
MoveUnitLeftCommandP2::~MoveUnitLeftCommandP2()
{

}

void MoveUnitLeftCommandP2::execute(State &state)
{
	state.GetCombatPhase()->GetUnitManager()->GetPlayer2Unit()->MoveLeft();
}