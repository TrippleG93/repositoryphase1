#include "CursorDownBuildingPhaseCommand.h"

#include "Globals.h"

#include "BuildPhase.h"
#include "State.h"

CursorDownBuildingPhaseCommand::~CursorDownBuildingPhaseCommand()
{
}

void CursorDownBuildingPhaseCommand::execute(State &state)
{
	state.GetBuildPhase()->MoveCursorDown();
	state.GetAudioManager().PlayCursorMove();
}