#include "CursorDownMainMenuCommand.h"

#include "State.h"
#include "MainMenu.h"

CursorDownMainMenuCommand::~CursorDownMainMenuCommand()
{
}

void CursorDownMainMenuCommand::execute(State &state)
{
	state.GetMainMenu()->MoveCursorDown();
	state.GetAudioManager().PlayCursorMove();
}