#include "FireCommandP2.h"

#include "State.h"

#include "BulletManager.h"

#include "CombatPhase.h"
FireCommandP2::~FireCommandP2()
{
}



void FireCommandP2::execute(State &state)
{
	state.GetCombatPhase()->GetBulletManager()->CreateBullet(2, 0, *state.GetCombatPhase()->GetUnitManager());
}
