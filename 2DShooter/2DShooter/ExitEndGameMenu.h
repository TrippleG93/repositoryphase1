#pragma once
#include "Command.h"

class ExitEndGameMenu :
	public Command
{
public:
	~ExitEndGameMenu();

	virtual void execute() {};
	virtual void execute(State &state);
};

