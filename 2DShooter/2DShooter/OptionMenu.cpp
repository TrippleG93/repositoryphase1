#include "OptionMenu.h"

#include "ConfigFileReader.h"

OptionMenu::OptionMenu()
{
	SetUpOptionMenu();
}


OptionMenu::~OptionMenu()
{
}

void OptionMenu::SetUpOptionMenu()
{
	ConfigFileReader file("Content/Text Files/Menus/OptionMenu.cfg");

	// Background
	background_image.LoadBitmap("Content/Images/Combat Phase Backgrounds/PC Computer - Space Pirates and Zombies - Backgrounds 22/Building_Background.png");

	emulogic_font24.CreateFont("Content/Fonts/Gravedigger.ttf", file.GetConfigValueIntegers("font 1", "size"));

	start_game_text.CreateTextRender(emulogic_font24.ObtainFont(), 
		al_map_rgb(file.GetConfigValueIntegers("start text", "text r"),
			file.GetConfigValueIntegers("start text", "text g"),
			file.GetConfigValueIntegers("start text", "text b")),
			"Full  Screen");
	start_text_x = file.GetConfigValueIntegers("start text", "position x");
	start_text_y = file.GetConfigValueIntegers("start text", "position y");


	options_text.CreateTextRender(emulogic_font24.ObtainFont(), 
		al_map_rgb(file.GetConfigValueIntegers("option text", "text r"),
			file.GetConfigValueIntegers("option text", "text g"),
			file.GetConfigValueIntegers("option text", "text b")),
		"Default Screen");
	option_text_x = file.GetConfigValueIntegers("option text", "position x");
	option_text_y = file.GetConfigValueIntegers("option text", "position y");

	quit_text.CreateTextRender(emulogic_font24.ObtainFont(), 
		al_map_rgb(file.GetConfigValueIntegers("quit text", "text r"),
			file.GetConfigValueIntegers("quit text", "text g"),
			file.GetConfigValueIntegers("quit text", "text b")),
		"Go Back");
	quit_text_x = file.GetConfigValueIntegers("quit text", "position x");
	quit_text_y = file.GetConfigValueIntegers("quit text", "position y");

	emulogic_font76.CreateFont("Content/Fonts/Gravedigger.ttf", file.GetConfigValueIntegers("font 2", "size"));

	main_menu_text.CreateTextRender(emulogic_font76.ObtainFont(), 
		al_map_rgb(file.GetConfigValueIntegers("main text", "text r"),
			file.GetConfigValueIntegers("main text", "text g"),
			file.GetConfigValueIntegers("main text", "text b")),
		"Options Menu");
	main_text_x = file.GetConfigValueIntegers("main text", "position x");
	main_text_y = file.GetConfigValueIntegers("main text", "position y");

	cursor_image.LoadBitmap("Content/Images/Others/Cursor.png");
	cursor_animation.AddIndividualFrame(file.GetConfigValueIntegers("cursor 1", "source x"),
		file.GetConfigValueIntegers("cursor 1", "source y"),
		file.GetConfigValueIntegers("cursor 1", "width"),
		file.GetConfigValueIntegers("cursor 1", "height"),
		file.GetConfigValueIntegers("cursor 1", "position x"),
		file.GetConfigValueIntegers("cursor 1", "position y"),
		file.GetConfigValueIntegers("cursor 1", "scale x"),
		file.GetConfigValueIntegers("cursor 1", "scale y"));
	cursor_animation.AddIndividualFrame(file.GetConfigValueIntegers("cursor 2", "source x"),
		file.GetConfigValueIntegers("cursor 2", "source y"),
		file.GetConfigValueIntegers("cursor 2", "width"),
		file.GetConfigValueIntegers("cursor 2", "height"),
		file.GetConfigValueIntegers("cursor 2", "position x"),
		file.GetConfigValueIntegers("cursor 2", "position y"),
		file.GetConfigValueIntegers("cursor 2", "scale x"),
		file.GetConfigValueIntegers("cursor 2", "scale y"));
	cursor_animation.AddIndividualFrame(file.GetConfigValueIntegers("cursor 3", "source x"),
		file.GetConfigValueIntegers("cursor 3", "source y"),
		file.GetConfigValueIntegers("cursor 3", "width"),
		file.GetConfigValueIntegers("cursor 3", "height"),
		file.GetConfigValueIntegers("cursor 3", "position x"),
		file.GetConfigValueIntegers("cursor 3", "position y"),
		file.GetConfigValueIntegers("cursor 3", "scale x"),
		file.GetConfigValueIntegers("cursor 3", "scale y"));
	cursor_animation.AddIndividualFrame(file.GetConfigValueIntegers("cursor 4", "source x"),
		file.GetConfigValueIntegers("cursor 4", "source y"),
		file.GetConfigValueIntegers("cursor 4", "width"),
		file.GetConfigValueIntegers("cursor 4", "height"),
		file.GetConfigValueIntegers("cursor 4", "position x"),
		file.GetConfigValueIntegers("cursor 4", "position y"),
		file.GetConfigValueIntegers("cursor 4", "scale x"),
		file.GetConfigValueIntegers("cursor 4", "scale y"));
	cursor_animation.AddIndividualFrame(file.GetConfigValueIntegers("cursor 5", "source x"),
		file.GetConfigValueIntegers("cursor 5", "source y"),
		file.GetConfigValueIntegers("cursor 5", "width"),
		file.GetConfigValueIntegers("cursor 5", "height"),
		file.GetConfigValueIntegers("cursor 5", "position x"),
		file.GetConfigValueIntegers("cursor 5", "position y"),
		file.GetConfigValueIntegers("cursor 5", "scale x"),
		file.GetConfigValueIntegers("cursor 5", "scale y"));
	cursor_animation.AddIndividualFrame(file.GetConfigValueIntegers("cursor 6", "source x"),
		file.GetConfigValueIntegers("cursor 6", "source y"),
		file.GetConfigValueIntegers("cursor 6", "width"),
		file.GetConfigValueIntegers("cursor 6", "height"),
		file.GetConfigValueIntegers("cursor 6", "position x"),
		file.GetConfigValueIntegers("cursor 6", "position y"),
		file.GetConfigValueIntegers("cursor 6", "scale x"),
		file.GetConfigValueIntegers("cursor 6", "scale y"));
	cursor_animation.AddIndividualFrame(file.GetConfigValueIntegers("cursor 7", "source x"),
		file.GetConfigValueIntegers("cursor 7", "source y"),
		file.GetConfigValueIntegers("cursor 7", "width"),
		file.GetConfigValueIntegers("cursor 7", "height"),
		file.GetConfigValueIntegers("cursor 7", "position x"),
		file.GetConfigValueIntegers("cursor 7", "position y"),
		file.GetConfigValueIntegers("cursor 7", "scale x"),
		file.GetConfigValueIntegers("cursor 7", "scale y"));
	cursor_animation.AddIndividualFrame(file.GetConfigValueIntegers("cursor 8", "source x"),
		file.GetConfigValueIntegers("cursor 8", "source y"),
		file.GetConfigValueIntegers("cursor 8", "width"),
		file.GetConfigValueIntegers("cursor 8", "height"),
		file.GetConfigValueIntegers("cursor 8", "position x"),
		file.GetConfigValueIntegers("cursor 8", "position y"),
		file.GetConfigValueIntegers("cursor 8", "scale x"),
		file.GetConfigValueIntegers("cursor 8", "scale y"));

	cursor_animation.CombineFrames(file.GetConfigValueIntegers("cursor 1", "frame"), true);
}

void OptionMenu::UpdateOptionMenu()
{
	cursor_animation.Update();

	switch (choice)
	{
	case 0:
		cursor_animation.GetCurrentFrame().SetYPosition(300);
		break;
	case 1:
		cursor_animation.GetCurrentFrame().SetYPosition(350);
		break;
	case 2:
		cursor_animation.GetCurrentFrame().SetYPosition(450);
		break;
	default:
		break;
	}
}

void OptionMenu::RenderOptionMenu(float window_scale_x, float window_scale_y)
{
	// TODO: PUT INTO GLOBAL HEADER
	int SCALE_CURSOR = 2;

	// BACKGROUND
	background_image.DrawScaledImage(0, 0,
		background_image.GetBitmapWidth(),
		background_image.GetBitmapHeight(),
		0 * window_scale_x,
		0 * window_scale_y,
		background_image.GetBitmapWidth() * window_scale_x,
		background_image.GetBitmapHeight() * window_scale_y,
		NULL);

	// MAIN MENU
	main_menu_text.DrawScaledImage(0, 0,
		main_menu_text.GetBitmapWidth(),
		main_menu_text.GetBitmapHeight(),
		(main_text_x - main_menu_text.GetBitmapWidth() / 2)* window_scale_x,
		main_text_y * window_scale_y,
		main_menu_text.GetBitmapWidth() * window_scale_x,
		main_menu_text.GetBitmapHeight() * window_scale_y,
		NULL);

	// START GAME
	start_game_text.DrawScaledImage(0, 0,
		start_game_text.GetBitmapWidth(),
		start_game_text.GetBitmapHeight(),
		(start_text_x - start_game_text.GetBitmapWidth() / 2)* window_scale_x,
		start_text_y * window_scale_y,
		start_game_text.GetBitmapWidth() * window_scale_x,
		start_game_text.GetBitmapHeight() * window_scale_y,
		NULL);

	// OPTIONS
	options_text.DrawScaledImage(0, 0,
		options_text.GetBitmapWidth(),
		options_text.GetBitmapHeight(),
		(option_text_x - options_text.GetBitmapWidth() / 2)* window_scale_x,
		option_text_y * window_scale_y,
		options_text.GetBitmapWidth() * window_scale_x,
		options_text.GetBitmapHeight() * window_scale_y,
		NULL);

	// QUIT 
	quit_text.DrawScaledImage(0, 0,
		quit_text.GetBitmapWidth(),
		quit_text.GetBitmapHeight(),
		(quit_text_x - quit_text.GetBitmapWidth() / 2)* window_scale_x,
		quit_text_y * window_scale_y,
		quit_text.GetBitmapWidth() * window_scale_x,
		quit_text.GetBitmapHeight() * window_scale_y,
		NULL);

	// CURSOR
	cursor_image.DrawScaledImage(cursor_animation.GetCurrentFrame().GetSourceX(),
		cursor_animation.GetCurrentFrame().GetSourceY(),
		cursor_animation.GetCurrentFrame().GetWidth(),
		cursor_animation.GetCurrentFrame().GetHeight(),
		cursor_animation.GetCurrentFrame().GetPositionX() * window_scale_x,
		(cursor_animation.GetCurrentFrame().GetPositionY() + 15) * window_scale_y,
		cursor_animation.GetCurrentFrame().GetWidth() * window_scale_x * cursor_animation.GetCurrentFrame().GetScaleX(),
		cursor_animation.GetCurrentFrame().GetHeight() * window_scale_y * cursor_animation.GetCurrentFrame().GetScaleY(),
		NULL);
}

void OptionMenu::MoveCursorDown()
{
	if (choice + 1 <= 2)
	{
		choice += 1;
	}
}

void OptionMenu::MoveCursorUp()
{
	if (choice - 1 >= 0)
	{
		choice -= 1;
	}
}

int OptionMenu::GetChoice()
{
	return choice;
}