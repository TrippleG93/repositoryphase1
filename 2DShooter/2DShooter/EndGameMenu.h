#pragma once

#include "Render.h"
#include "SpriteAnimation.h"
#include "Font.h"
#include "Audio.h"

class EndGameMenu
{
public:
	EndGameMenu();
	EndGameMenu(int winner);
	~EndGameMenu();

	void SetUpEndGameMenu();

	void UpdateEndGameMenu();

	void DrawEndGameMenu(float window_scale_x, float window_scale_y);

	int GetWhoWon() { return who_won; }

private:

	void LoadBackgroundAnimation();

	Font emulogic_font;						// Font type emulogic size 16

	Render background_image;				// Holds the background image 
	SpriteAnimation background_animation;	// Holds the background animation

	Render who_won_text;					// Holds the text of who won
	int who_won_x;
	int who_won_y;

	int who_won;							// Holds who won the game

};

