#include "ConfirmBuildLoadFileCommand.h"

#include "State.h"
#include "ConfirmMenu.h"

ConfirmBuildLoadFileCommand::~ConfirmBuildLoadFileCommand()
{
}

void ConfirmBuildLoadFileCommand::execute(State &state)
{
	state.GetConfirmMenu()->SetUpConfirmBuildPhase();
}