#pragma once
#include "Command.h"

class CombatPhaseFileLoadCommand :
	public Command
{
public:
	~CombatPhaseFileLoadCommand();

	virtual void execute() {};
	virtual void execute(State &state);
};

