#pragma once

#include "Command.h"

class TitleLoadFilesCommand : public Command
{
public:
	~TitleLoadFilesCommand();

	virtual void execute() {};
	virtual void execute(State &state);
};

