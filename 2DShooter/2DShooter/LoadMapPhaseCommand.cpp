#include "LoadMapPhaseCommand.h"
#include "State.h"
#include "MapPhase.h"

LoadMapPhaseCommand::~LoadMapPhaseCommand()
{
}

void ::LoadMapPhaseCommand::execute(State &state)
{
	state.GetMapPhase()->SetUpMapPhase();
}