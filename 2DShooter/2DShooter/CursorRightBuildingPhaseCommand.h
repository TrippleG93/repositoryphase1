#pragma once

#include "Command.h"

class CursorRightBuildingPhaseCommand :
	public Command
{
public:
	~CursorRightBuildingPhaseCommand();

	virtual void execute() {};
	virtual void execute(State& state);
};

