#pragma once
#include "Command.h"

class PauseGameCombatPhaseCommand : public Command
{
public:
	~PauseGameCombatPhaseCommand();

	virtual void execute() {};
	virtual void execute(State &state);
};

