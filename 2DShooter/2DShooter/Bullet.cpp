#include "Bullet.h"
#include "UnitManager.h"
#include "Unit.h"

#include "ConfigFileReader.h"

Bullet::Bullet()
{
}

Bullet::Bullet(int player, int x_position, int y_position, int type, float boost, int opp_pos_x, int opp_pos_y)
{
	hit = false;
	which_player = player;

	if (which_player == 1)
	{
		x_velocity = 0;
		y_velocity = -10;
	}
	else
	{
		x_velocity = 0;
		y_velocity = 10;
	}

	bullet_type = type;
	bullet_boost = boost;
	opponent_position_x = opp_pos_x;
	opponent_position_y = opp_pos_y;
	angle = 0;

	SetUpBullet(x_position, y_position);
}

Bullet::Bullet(int player, int x_position, int y_position, int type, float boost)
{
	hit = false;
	which_player = player;

	if (which_player == 1)
	{
		x_velocity = 0;
		y_velocity = -10;
	}
	else
	{
		x_velocity = 0;
		y_velocity = 10;
	}

	bullet_type = type;
	bullet_boost = boost;

	SetUpBullet(x_position, y_position);
}

Bullet::~Bullet()
{
}

void Bullet::SetUpBullet(int su_pos_x, int su_pos_y)
{
	ConfigFileReader file("Content/Text Files/Bullets/Bullets.cfg");

	std::string bullet;
	
	bullet_boost = 1.0;

	switch (bullet_type) {
	case(0): // RED
		if(which_player == 2)
			bullet_image.LoadBitmap("Content/Images/Ships/spr_bullet_strip02.png");
		else
			bullet_image.LoadBitmap("Content/Images/Ships/spr_bullet_strip05.png");
		bullet = "bullet red";
		break;
	case(1): // BIG
		bullet_image.LoadBitmap("Content/Images/Ships/Alien-Bomb-by-MillionthVector/aliendropping0004.png");
		bullet = "bullet big";
		break;
	case(2): // SMALL ROUND
		bullet_image.LoadBitmap("Content/Images/Ships/spr_bullet_strip-BLUE.png");
		bullet = "bullet small round";
		break;
	};
		
	bullet_animation.AddIndividualFrame(file.GetConfigValueIntegers(bullet, "source x"),
		file.GetConfigValueIntegers(bullet, "source y"),
		file.GetConfigValueIntegers(bullet, "width"),
		file.GetConfigValueIntegers(bullet, "height"),
		su_pos_x, su_pos_y,
		file.GetConfigValueDouble(bullet, "scale x"),
		file.GetConfigValueDouble(bullet, "scale y"));

	bullet_animation.CombineFrames(6, true);

	if (which_player == 1) {
		y_velocity = -file.GetConfigValueIntegers(bullet, "velocity y") *   bullet_boost;
	}
	else {
		y_velocity = file.GetConfigValueIntegers(bullet, "velocity y") * bullet_boost;
	}

	x_velocity = file.GetConfigValueIntegers(bullet, "velocity x") * bullet_boost;
	damage = file.GetConfigValueIntegers("bullet", "damage");
}

void Bullet::UpdateBullet()
{
	if (bullet_type == 2) 
	{
		bullet_animation.GetCurrentFrame().SetXPosition(bullet_animation.GetCurrentFrame().GetPositionX() + x_velocity * cos(angle));
		bullet_animation.GetCurrentFrame().SetYPosition(bullet_animation.GetCurrentFrame().GetPositionY() + y_velocity * sin(angle));
	}
	else
	{
		bullet_animation.GetCurrentFrame().SetXPosition(bullet_animation.GetCurrentFrame().GetPositionX() + x_velocity);
		bullet_animation.GetCurrentFrame().SetYPosition(bullet_animation.GetCurrentFrame().GetPositionY() + y_velocity);
	}
}

void Bullet::RenderBullet(float window_scale_x, float window_scale_y)
{
	if (which_player == 1)
	{
		if (bullet_type == 2) // If 'small round bullet', then Support Unit 4 is firing
		{
			float x = ((float)(opponent_position_x - bullet_animation.GetCurrentFrame().GetPositionX()) * window_scale_x +bullet_animation.GetCurrentFrame().GetWidth()*bullet_animation.GetCurrentFrame().GetScaleX());
			float y = ((float)(opponent_position_y -  bullet_animation.GetCurrentFrame().GetPositionY()) * window_scale_y + bullet_animation.GetCurrentFrame().GetHeight()*bullet_animation.GetCurrentFrame().GetScaleY());
			if (angle == 0)
				angle = -(atan2f(y, x));
		}

		bullet_image.DrawScaledImage((float)bullet_animation.GetCurrentFrame().GetSourceX(),
			(float)bullet_animation.GetCurrentFrame().GetSourceY(),
			(float)bullet_animation.GetCurrentFrame().GetWidth(),
			(float)bullet_animation.GetCurrentFrame().GetHeight(),
			((float)bullet_animation.GetCurrentFrame().GetPositionX() - (bullet_animation.GetCurrentFrame().GetWidth() * bullet_animation.GetCurrentFrame().GetScaleX() / 2 ))  * window_scale_x,
			((float)bullet_animation.GetCurrentFrame().GetPositionY() - (bullet_animation.GetCurrentFrame().GetHeight() * bullet_animation.GetCurrentFrame().GetScaleY() / 2)) * window_scale_y,
			(float)bullet_animation.GetCurrentFrame().GetWidth() * bullet_animation.GetCurrentFrame().GetScaleX() * window_scale_x,
			(float)bullet_animation.GetCurrentFrame().GetHeight() * bullet_animation.GetCurrentFrame().GetScaleY() * window_scale_y,
			ALLEGRO_FLIP_VERTICAL);
	}
	else
	{
		if (bullet_type == 2) // If 'small round bullet', then Support Unit 4 is firing
		{
			float x = ((float)(opponent_position_x - bullet_animation.GetCurrentFrame().GetPositionX()) * window_scale_x 
				+ bullet_animation.GetCurrentFrame().GetWidth()*bullet_animation.GetCurrentFrame().GetScaleX());
			float y = ((float)(opponent_position_y - bullet_animation.GetCurrentFrame().GetPositionY()) * window_scale_y 
				+ bullet_animation.GetCurrentFrame().GetHeight()*bullet_animation.GetCurrentFrame().GetScaleY());
			
			if (angle == 0)
				angle = (atan2f(y, x));
		}
		bullet_image.DrawScaledImage((float)bullet_animation.GetCurrentFrame().GetSourceX(),
			(float)bullet_animation.GetCurrentFrame().GetSourceY(),
			(float)bullet_animation.GetCurrentFrame().GetWidth(),
			(float)bullet_animation.GetCurrentFrame().GetHeight(),
			(((float)bullet_animation.GetCurrentFrame().GetPositionX() - bullet_animation.GetCurrentFrame().GetWidth() / 2 * bullet_animation.GetCurrentFrame().GetScaleX())) * window_scale_x,
			((float)bullet_animation.GetCurrentFrame().GetPositionY()) * window_scale_y,
			(float)bullet_animation.GetCurrentFrame().GetWidth() * bullet_animation.GetCurrentFrame().GetScaleX() * window_scale_x,
			(float)bullet_animation.GetCurrentFrame().GetHeight() * bullet_animation.GetCurrentFrame().GetScaleY() * window_scale_y,
			NULL);
	}
}

int Bullet::GetXPosition()
{
	return bullet_animation.GetCurrentFrame().GetPositionX();
}

int Bullet::GetYPosition()
{
	return bullet_animation.GetCurrentFrame().GetPositionY();
}

Sprite Bullet::GetSprite()
{
	return bullet_animation.GetCurrentFrame();
}

bool Bullet::DidBulletHitSomething()
{
	return hit;
}

void Bullet::BulletHitNothing()
{
	hit = false;
}

void Bullet::BulletHitSomething()
{
	hit = true;
}

void Bullet::MoveLeft()
{
	current_x_velocity = -x_velocity;
}

void Bullet::MoveUp()
{
	current_y_velocity = -y_velocity;
}

void Bullet::MoveRight()
{
	current_x_velocity = x_velocity;
}

void Bullet::MoveDown()
{
	current_y_velocity = y_velocity;
}

void Bullet::IdleHorizontal()
{
	current_x_velocity = 0;
}

void Bullet::IdleVerticle()
{
	current_y_velocity = 0;
}

int Bullet::GetXVelocity()
{
	return current_x_velocity;
}

void Bullet::SetXVelocity(int new_x_velocity)
{
	x_velocity = new_x_velocity;
}

void Bullet::SetYVelocity(int new_y_velocity)
{
	y_velocity = new_y_velocity;
}

void Bullet::SetBulletBoost(float set_bullet_boost)
{
	bullet_boost = set_bullet_boost;
}
