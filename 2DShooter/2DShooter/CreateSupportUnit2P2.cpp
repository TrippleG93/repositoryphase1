#include "CreateSupportUnit2P2.h"

#include "State.h"
#include "CombatPhase.h"
#include "UnitManager.h"

CreateSupportUnit2P2::~CreateSupportUnit2P2()
{
}

void CreateSupportUnit2P2::execute(State &state)
{
	state.GetCombatPhase()->GetUnitManager()->CreateSupportUnit2(6, 2);
}