#include "Player.h"
#include <string>

#include "ConfigFileReader.h"

Player::Player()
{
	SetUpPlayer();
}


Player::~Player()
{
	delete(purchases_left);
}

void Player::SetUpPlayer()
{
	ConfigFileReader file("Content/Text Files/Player/player.cfg");

	has_main_ship = false;

	ResetPurchase();

	emulogic_font12.CreateFont("Content/Fonts/Gravedigger.ttf", 12);

	purchases_left = new Render();

	purchases_left->CreateTextRender(emulogic_font12.ObtainFont(), al_map_rgb(255, 255, 255), "Purchases left: " + std::to_string(purchases));
	position_x = file.GetConfigValueIntegers("purchases", "position x");
	position_y = file.GetConfigValueIntegers("purchases", "position y");

	lives = file.GetConfigValueIntegers("lives", "amount");
	max_lives = lives;

	heart_image.LoadBitmap("Content/Images/Others/49-gaming-hearts-lives-game-damage-512.png");
	heart_animation.AddIndividualFrame(file.GetConfigValueIntegers("heart image", "source x"),
		file.GetConfigValueIntegers("heart image", "source y"),
		file.GetConfigValueIntegers("heart image", "width"),
		file.GetConfigValueIntegers("heart image", "height"),
		0, 0, 
		file.GetConfigValueDouble("heart image", "scale x"),
		file.GetConfigValueDouble("heart image", "scale y"));

	lost_animation.AddIndividualFrame(file.GetConfigValueIntegers("lost heart image", "source x"),
		file.GetConfigValueIntegers("lost heart image", "source y"),
		file.GetConfigValueIntegers("lost heart image", "width"),
		file.GetConfigValueIntegers("lost heart image", "height"),
		0, 0, 
		file.GetConfigValueDouble("lost heart image", "scale x"),
		file.GetConfigValueDouble("lost heart image", "scale y"));
}

void Player::DrawPlayerInfo(float window_scale_x, float window_scale_y)
{
	purchases_left->DrawScaledImage(0, 0,
		purchases_left->GetBitmapWidth(),
		purchases_left->GetBitmapHeight(),
		position_x * window_scale_x,
		position_y * window_scale_y,
		purchases_left->GetBitmapWidth() * window_scale_x,
		purchases_left->GetBitmapHeight() * window_scale_y, NULL);
}

void Player::DrawHeart(int position_x, int position_y, float window_scale_x, float window_scale_y)
{
	for (int counter = 0; counter < max_lives; counter++)
	{
		if (counter < lives)
		{
			heart_image.DrawScaledImage(heart_animation.GetCurrentFrame().GetSourceX(),
				heart_animation.GetCurrentFrame().GetSourceY(),
				heart_animation.GetCurrentFrame().GetWidth(),
				heart_animation.GetCurrentFrame().GetHeight(),
				(position_x + (counter * heart_animation.GetCurrentFrame().GetWidth() * heart_animation.GetCurrentFrame().GetScaleX())) * window_scale_x,
				position_y * window_scale_y,
				heart_animation.GetCurrentFrame().GetWidth() * heart_animation.GetCurrentFrame().GetScaleX() * window_scale_x,
				heart_animation.GetCurrentFrame().GetHeight() * heart_animation.GetCurrentFrame().GetScaleY() * window_scale_y,
				NULL);
		}
		else
		{
			heart_image.DrawScaledImage(lost_animation.GetCurrentFrame().GetSourceX(),
				lost_animation.GetCurrentFrame().GetSourceY(),
				lost_animation.GetCurrentFrame().GetWidth(),
				lost_animation.GetCurrentFrame().GetHeight(),
				(position_x + (counter * lost_animation.GetCurrentFrame().GetWidth() * lost_animation.GetCurrentFrame().GetScaleX())) * window_scale_x,
				position_y * window_scale_y,
				lost_animation.GetCurrentFrame().GetWidth() * lost_animation.GetCurrentFrame().GetScaleX() * window_scale_x,
				lost_animation.GetCurrentFrame().GetHeight() * lost_animation.GetCurrentFrame().GetScaleY() * window_scale_y,
				NULL);
		}
	}
}

bool Player::DoesPlayerHaveMainShip()
{
	return has_main_ship;
}

void Player::UpdatePlayer()
{
	if (ready_to_lose_life == true && timer < show_lives_count)
	{
		if (timer > time_to_lose_count && just_lost_life == false)
		{
			just_lost_life = true;
			LoseALife();
		}
		timer += 1;
		if (timer >= show_lives_count)
		{
			done_losing_life = true;
		}
	}
}

void Player::ResetPurchase()
{
	ConfigFileReader file("Content/Text Files/Player/player.cfg");

	if (has_main_ship == false)
	{
		purchases = file.GetConfigValueIntegers("purchases", "no main ship");
	}
	else
	{
		purchases = file.GetConfigValueIntegers("purchases", "yes main ship");
		delete(purchases_left);
		purchases_left = new Render();
		purchases_left->CreateTextRender(emulogic_font12.ObtainFont(), al_map_rgb(255, 255, 255), "Purchases left: " + std::to_string(purchases));
	}
	
}

void Player::Purchased()
{
	purchases -= 1;
	delete(purchases_left);
	purchases_left = new Render();
	purchases_left->CreateTextRender(emulogic_font12.ObtainFont(), al_map_rgb(255, 255, 255), "Purchases left: " + std::to_string(purchases));
}

bool Player::CanPlayerPurchase()
{
	if (purchases - 1 < 0)
	{
		return false;
	}
	else
		return true;
}

void Player::LoseALife()
{
	lives -= 1;
}

bool Player::IsDead()
{
	if (lives <= 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}

void Player::SetTimers()
{
	ConfigFileReader file("Content/Text Files/Player/player.cfg");

	ready_to_lose_life = false;
	just_lost_life = false;
	done_losing_life = false;

	show_lives_count = file.GetConfigValueIntegers("timer", "show lives");
	time_to_lose_count = file.GetConfigValueIntegers("timer", "time to lose");
	timer = 0;
}