#pragma once

#include "SpriteAnimation.h"
#include "Render.h"
#include "Audio.h"
#include "Font.h"
#include <vector>

class Bullet;
class State;
class Command;

class ComputerPlayer
{
private:

	struct ConfigStats {
		int frame_delay_fireP2_min;						// Holds min number of frames must pass till P2 fires	
		int frame_delay_fireP2_max;						// Holds max number of frames must pass until P2 fires
		int wander_min;									// Holds min amount of time Player 2 is wandering 
		int wander_max;									// Holds max amount of time Player 2 is wandering 
		int wander_often;								// Holds frequency of wander. 0=never decide to seek - 10=always decide to seek
		int approach_dist;								// Holds number of pixels around player 1 that player 2 maintains for approach function
		int follow_dist;								// Holds number of pixels around player 1 that player 2 maintains for follow function
		int follow_delay_min;							// Holds min amount of time Player 2 is following 
		int follow_delay_max;							// Holds max amount of time Player 2 is following 
		int evade_min;									// Holds min number of pixels in front of incoming bullet that player 2 evades
		int evade_max;									// Holds max number of pixels in front of incoming bullet that player 2 evades
		int evade_often;								// Holds frequency of evade. 0=never decide to evade - 10=always decide to evade
		int stuck_delay;								// Holds amount of time computer player is stuck to the side
	}ComputerPlayerConfigStats;

public:

	ComputerPlayer();
	~ComputerPlayer();

	/*
	SetUpComputerPlayer
	Loads everything assocaited with the class
	*/
	void SetUpComputerPlayer();

	/*
	UpdateComputerPlayer
	Update animations and any other variables assciated with the class
	*/
	void UpdateComputerPlayer();

	/*
	SetStats
	Update values associated with computer player
	*/
	void SetStats(int building_type);

	/*
	ChooseMainBuildingToBuy
	Select the main building in the build phase and confirm to begin combat
	*/
	void ChooseBuildingsToBuy(State &state);

	/*
	BuyFirstMainBuilding
	Ai FUnctions that buys the compter their first main building
	in order to fight in the combat phase
	*/
	void BuyFirstMainBuilding(State &state);

	/*
		BuyNextBuilding
		
	*/
	void BuyNextBuilding(State &state);

	/*
	Seek
	Traverse towards Player1
	*/
	Command* Seek(State &state);

	/*
	Evade
	Avoid bullets
	*/
	Command* Evade(State &state, int x_position_bullet);

	/*
	Fire
	Traverse towards Player1
	*/
	bool Fire(State &state);

	/*
	Approach
	Coming near to Player1
	*/
	Command* Approach(State &state);

	/*
	Wander
	Decide on where to travel to next randomly
	*/
	Command* Wander(State &state);

	/*
	Follow
	Right above player 1
	*/
	Command* Follow(State &state);

	/*
	AICombatPhaseCommands
	Traverse towards Player1
	*/
	Command* AICombatPhaseCommands(State &state);

	//ComputerPlayer::ConfigStats* GetStruct();

	/*
	ResetPurchase
	resets the purchase to allow the player to make purchases
	AttackSU4
	Deal with the support unit immediately
	*/
	void ResetPurchase();
	Command* AttackSupportUnit(State &state, int unit);


	/*
	Purchased
	Decrease what they can purchase by one
	*/
	void Purchased();

	/*
	CanPlayerPurchase
	Holds if the player can make a purchase or not
	*/
	bool CanPlayerPurchase();

	void Update();

	/*
	LoseALife
	subtracts 1 from the remaining life of the player.
	*/
	void LoseALife();

	/*
	IsDead
	Returns true if the player has no more lives remaining
	*/
	bool IsDead();

	/*
	GetRemainingLives
	Returns the amoung of lives the player has
	*/
	int GetRemainingLives() { return lives; }

	void DrawHeart(int position_x, int position_y, float window_scale_x, float window_scale_y);

	void SetTimers();

	void StartLifeLostTimer() { ready_to_lose_life = true; }

	bool DoneLosingLife() { return done_losing_life; }

	bool ReadyToLoseLife() { return ready_to_lose_life; }

private:
	int SPACING_UNIT = 50;

	int x_position_player_1;									// Holds x position of Player 1 
	int x_position_player_2;									// Holds x position of Player 2 
	int y_position_player_2;									// Holds y position of Player 1 

	bool main_ship_building;									// Holds if the computer has a main ship building

	// TODO: MOVE TO UNIT CLASS
	

													// COMBAT PHASE
													// move
	int frame_count_moveP2;								// Holds how many frames have passed until P2 moves
	int position_diff;									// Holds difference between player1_x and player2_x positions
	bool move_l_r;										// left=false, right=true
	int evade_x;										// Holds x-position of what the computer player is avoiding

	int frame_count_fireP2;								// Holds how many frames have passed until P2 fires
	int frame_delay_fireP2;								// Holds how many frames must pass till P2 fires

														// seek
	bool choose_to_seek;

	// wander
	int frame_count_wander;									// Holds distance of wander passed
	int frame_delay_wander;								// Holds duration of wander

														// follow
	int follow_delay;										// Holds the amount of time Player 2 is following 

															// evade
	int x_position_bullet;
	int evade_dist_to_bullet;
	bool choose_to_evade;
	int stuck_counter;	

	int unit;					// Stores which unit to attack

	int purchases;										// Holds how much the buildings the player can purchase
	bool has_main_ship;									// Holds if they have a main ship or not

	Font emulogic_font12;

	Render purchases_left;								// Image that holds how much purchases they have left. 
	int position_x;
	int position_y;

	Font emulogic_font18;

	Render support_unit_number_image_1G;
	Render support_unit_number_image_2G;
	Render support_unit_number_image_3G;
	Render support_unit_number_image_4G;

	Render action_text;

	Render support_unit_3_image;
	SpriteAnimation support_unit_3_animation;

	std::vector<Bullet*>::size_type i = 0;

	enum Actions
	{
		STAY, SEEK, FLEE, APPROACH, PURSUE, EVADE, FOLLOW, WANDER, ATTACK_SU
	}action;											// Holds current action computer is taking in Combat Phase (seek=true, evade=false)

	int lives;
	int max_lives;

	int show_lives_count;
	int time_to_lose_count;
	int timer;

	bool ready_to_lose_life;
	bool just_lost_life;
	bool done_losing_life;

	Render heart_image;									// Visual representation of lives

	SpriteAnimation heart_animation;					// Visual representation of lives
	SpriteAnimation lost_animation;						// Visual representation of losing  a life
};

