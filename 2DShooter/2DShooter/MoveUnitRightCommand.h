#pragma once

#include "Command.h"

class MoveUnitRightCommand : public Command
{
public:

	~MoveUnitRightCommand();

	virtual void execute() {};
	virtual void execute(State &state);
};

