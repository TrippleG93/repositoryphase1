#include "EndGameMenu.h"

#include "ConfigFileReader.h"

EndGameMenu::EndGameMenu()
{
}

EndGameMenu::EndGameMenu(int winner)
{
	who_won = winner;
	SetUpEndGameMenu();
}


EndGameMenu::~EndGameMenu()
{
}

void EndGameMenu::SetUpEndGameMenu()
{
	ConfigFileReader file("Content/Text Files/Menus/EndGameMenu.cfg");

	emulogic_font.CreateFont("Content/Fonts/Gravedigger.ttf", file.GetConfigValueIntegers("font", "size"));

	background_image.LoadBitmap("Content/Images/Others/73751.png");
	background_animation.AddIndividualFrame(file.GetConfigValueIntegers("Background 1", "source x"),
		file.GetConfigValueIntegers("Background 1", "source y"),
		file.GetConfigValueIntegers("Background 1", "width"),
		file.GetConfigValueIntegers("Background 1", "height"),
		0, 0,
		file.GetConfigValueDouble("Background 1", "scale x"),
		file.GetConfigValueDouble("Background 1", "scale y"));

	background_animation.AddIndividualFrame(file.GetConfigValueIntegers("Background 2", "source x"),
		file.GetConfigValueIntegers("Background 2", "source y"),
		file.GetConfigValueIntegers("Background 2", "width"),
		file.GetConfigValueIntegers("Background 2", "height"),
		0, 0,
		file.GetConfigValueDouble("Background 2", "scale x"),
		file.GetConfigValueDouble("Background 2", "scale y"));

	background_animation.AddIndividualFrame(file.GetConfigValueIntegers("Background 3", "source x"),
		file.GetConfigValueIntegers("Background 3", "source y"),
		file.GetConfigValueIntegers("Background 3", "width"),
		file.GetConfigValueIntegers("Background 3", "height"),
		0, 0,
		file.GetConfigValueDouble("Background 3", "scale x"),
		file.GetConfigValueDouble("Background 3", "scale y"));
	background_animation.AddIndividualFrame(file.GetConfigValueIntegers("Background 4", "source x"),
		file.GetConfigValueIntegers("Background 4", "source y"),
		file.GetConfigValueIntegers("Background 4", "width"),
		file.GetConfigValueIntegers("Background 4", "height"),
		0, 0,
		file.GetConfigValueDouble("Background 4", "scale x"),
		file.GetConfigValueDouble("Background 4", "scale y"));

	background_animation.AddIndividualFrame(file.GetConfigValueIntegers("Background 5", "source x"),
		file.GetConfigValueIntegers("Background 5", "source y"),
		file.GetConfigValueIntegers("Background 5", "width"),
		file.GetConfigValueIntegers("Background 5", "height"),
		0, 0,
		file.GetConfigValueDouble("Background 5", "scale x"),
		file.GetConfigValueDouble("Background 5", "scale y"));

	background_animation.AddIndividualFrame(file.GetConfigValueIntegers("Background 6", "source x"),
		file.GetConfigValueIntegers("Background 6", "source y"),
		file.GetConfigValueIntegers("Background 6", "width"),
		file.GetConfigValueIntegers("Background 6", "height"),
		0, 0,
		file.GetConfigValueDouble("Background 6", "scale x"),
		file.GetConfigValueDouble("Background 6", "scale y"));

	background_animation.CombineFrames(3, true);

	if (who_won == 1)
	{
		who_won_text.CreateTextRender(emulogic_font.ObtainFont(), al_map_rgb(255, 255, 255), "You Won!");
	}
	else
	{
		who_won_text.CreateTextRender(emulogic_font.ObtainFont(), al_map_rgb(255, 255, 255), "You Lost");
	}
	who_won_x = file.GetConfigValueIntegers("who won", "position x");
	who_won_y = file.GetConfigValueIntegers("who won", "position y");

}

void EndGameMenu::LoadBackgroundAnimation()
{

}

void EndGameMenu::UpdateEndGameMenu()
{
	background_animation.Update();
}

void EndGameMenu::DrawEndGameMenu(float window_scale_x, float window_scale_y)
{
	background_image.DrawScaledImage(background_animation.GetCurrentFrame().GetSourceX(),
		background_animation.GetCurrentFrame().GetSourceY(),
		background_animation.GetCurrentFrame().GetWidth(),
		background_animation.GetCurrentFrame().GetHeight(),
		0, 0,
		background_animation.GetCurrentFrame().GetWidth() * background_animation.GetCurrentFrame().GetScaleX() * window_scale_x,
		background_animation.GetCurrentFrame().GetHeight() * background_animation.GetCurrentFrame().GetScaleY() * window_scale_y,
		NULL);

	who_won_text.DrawScaledImage(0, 0,
		who_won_text.GetBitmapWidth(),
		who_won_text.GetBitmapHeight(),
		who_won_x * window_scale_x,
		who_won_y * window_scale_y,
		who_won_text.GetBitmapWidth() * window_scale_x,
		who_won_text.GetBitmapHeight() * window_scale_y,
		NULL);
}