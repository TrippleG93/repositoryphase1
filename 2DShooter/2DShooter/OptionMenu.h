#pragma once

#include "Font.h"
#include "Audio.h"
#include "SpriteAnimation.h"
#include "Render.h"

class OptionMenu
{
public:
	OptionMenu();
	~OptionMenu();

	/*
		SetUpOptionMenu
		Loads everything that is needed for the option menu
	*/
	void SetUpOptionMenu();

	/*
		UpdateOptionMenu
		Updates everything in regards to the option menu
	*/
	void UpdateOptionMenu();

	/*
		RenderOptionMenu
		Draws everything needed in the option menu
	*/
	void RenderOptionMenu(float display_scale_x, float display_scale_y);

	/*
	MoveCursorUp
	Update the choice by subtracting by 1
	*/
	void MoveCursorUp();

	/*
	MoveCursorDown
	Update the choice by adding by 1
	*/
	void MoveCursorDown();

	/*
	GetChoice
	Returns the int value of the choice
	*/
	int GetChoice();

private:
	Font emulogic_font24;
	Font emulogic_font76;

	SpriteAnimation cursor_animation;

	Render cursor_image;

	Render options_text;
	int option_text_x;
	int option_text_y;

	Render start_game_text;
	int start_text_x;
	int start_text_y;

	Render quit_text;
	int quit_text_x;
	int quit_text_y;

	Render main_menu_text;
	int main_text_x;
	int main_text_y;

	Render background_image;

	int choice; 
	int y_position_cursor;
};

