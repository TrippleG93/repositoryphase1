#pragma once

/*
* SpriteAnimation class
* Allows the sense of animation for images
* Draws image to the screen using the display class
* Holds the coordinate of each position of where to draw from the image
* Holds how many frames are in each animation set
* Holds the current frame count of the animation
* Holds an individual frame count to be used for each animation
*/

#include <string>
#include <map>

#include "Sprite.h"

class SpriteAnimation
{
public:
	/*
	* SpriteAnimation (Default Constructor)
	* Is the default constructor
	*/
	SpriteAnimation();

	/*
	* SpriteAnimation (Deconstructor)
	* ...
	*/
	~SpriteAnimation();

	/*
	* AnimationLoopTrue
	* Set the animation loop variable to true so the animation can be played over and over again
	*/
	void AnimationLoopTrue();

	/* AnimationLoopFalse
	Set the animation loop variable to false so the animation can't be looping
	*/
	void AnimationLoopFalse();

	/* CompleteAnimationTrue
	Set the complete animation variable to true to show
	that an animation has been played as leastonce
	*/
	void CompleteAnimationTrue();

	/* CompleteAnimationFalse
	Set the complete animation variable to false to show
	that an animation has not been completed yet
	*/
	void CompleteAnimatinFalse();

	/*
	* update
	* Updates the animated sprite by changing the coordinates of the image
	* it is suppose to use to draw the image
	* This also updates all the animation images at once belonging to the class
	*/
	void Update();

	/*
	* AddIndividualFrame
	* Adds one frame to the animation sequence
	* This contains the starting coordinates of where to locate the portion of the image on the main image
	* Also includes how big that portion is on the main image
	*/
	void AddIndividualFrame(int source_x, int source_y, int width, int height);
	void AddIndividualFrame(int source_x, int source_y, int width, int height, int pos_x, int pos_y);
	void AddIndividualFrame(int source_x, int source_y, int width, int height, int pos_x, int pos_y, float scale_x, float scale_y);
	void AddIndividualFrame(int source_x, int source_y, int width, int height, int pos_x, int pos_y, float scale_x, float scale_y, int x_velocity, int y_velocity);

	/*
	* InsertAnimation
	* Initliazes the rest of variables associated with all the frames so
	* they are together in regards to keeping track of which we are at
	*/
	void CombineFrames(int frame_delays, bool loop);

	/* Destroy
	* Pops out all the sprites in the stack
	*/
	void Destroy();

	/* GetCurrentFrame
	* Returns a sprite in which holds everything in regards to the current frame
	*/
	Sprite & GetCurrentFrame();

private:
	bool animation_loop;															// Holds if the animation will be in a continous loop or not
	bool complete_animation;														// Marks if the animation has been played at least once

	int temp_max_frame;																// Keeps track of the amount of frames being added to one animation before actually adding the animation
	int max_frame;																	// Holds the amount of animation images it has
	int current_frame;																// Keeps track at what Frame we are at with the program
	int frame_count;																// Holds how long it has been on that frame
	int frame_delay;																// Holds when the next animation should be played

	std::map <int, Sprite> animations;												// Holds each scene

};

