#pragma once
#include "Command.h"


class ConfirmPauseMenuCommand :
	public Command
{
public:
	~ConfirmPauseMenuCommand();


	virtual void execute() {};
	virtual void execute(State &state);
};

