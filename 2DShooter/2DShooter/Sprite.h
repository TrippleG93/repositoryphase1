#pragma once
class Sprite
{
public:
	Sprite();
	Sprite(int source_x, int source_y, int width, int height);
	Sprite(int source_x, int source_y, int width, int height, int x_pos, int y_pos);
	Sprite(int source_x, int source_y, int width, int height, int x_pos, int y_pos, float scale_x, float scale_y);
	~Sprite();

	void SetYPosition(int value);
	void SetXPosition(int value);
	void SetScaleX(float value);
	void SetScaleY(float value);

	int GetSourceX() { return source_x; }
	int GetSourceY() { return source_y; }
	int GetWidth() { return width; }
	int GetHeight() { return height; }
	int GetPositionX() { return x_position; }
	int GetPositionY() { return y_position; }
	float GetScaleX() { return scale_x; }
	float GetScaleY() { return scale_y; }

private:

	int x_position;														// Holds the x position of the sprite
	int y_position;														// Holds the y position of the sprite
	int source_x;														// Where from the image you start from x position
	int source_y;														// Where fromt he image you start from y position
	int width;															// How wide the image is
	int height;															// How tall the image is
	float scale_x;														// Holds the scale of the image x direction
	float scale_y;														// Holds the scale of the image y direction
};

