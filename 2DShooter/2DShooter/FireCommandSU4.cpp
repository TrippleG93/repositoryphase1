#include "FireCommandSU4.h"

#include "State.h"

#include "BulletManager.h"

#include "CombatPhase.h"

FireCommandSU4::~FireCommandSU4()
{
}



void FireCommandSU4::execute(State &state)
{
	state.GetCombatPhase()->GetBulletManager()->CreateBullet(1, 2, *state.GetCombatPhase()->GetUnitManager());
}
