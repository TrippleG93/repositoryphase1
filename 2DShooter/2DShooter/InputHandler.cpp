#include "InputHandler.h"

#include "Command.h"

// Title Menu Commands
#include "ExitTitleCommand.h"
#include "TitleLoadFilesCommand.h"

// Main Menu Commands
#include "CursorDownMainMenuCommand.h"
#include "CursorUpMainMenuCommand.h"
#include "BackToTitleCommand.h"
#include "SelectChoiceMainMenuCommand.h"
#include "MenuLoadFileCommand.h"

// Build Phase Commands
#include "CursorUpBuildingPhaseCommand.h"
#include "CursorDownBuildingPhaseCommand.h"
#include "CursorRightBuildingPhaseCommand.h"
#include "CursorLeftBuildingPhaseCommand.h"
#include "GoToBuildMenuCommand.h"
#include "FinishBuildingCommand.h"

// Build menu commands
#include "BackToBuildPhaseCommand.h"
#include "BuyBuildingCommand.h"
#include "CursorUpBuildMenuCommand.h"
#include "CursorDownBuildMenuCommand.h"
#include "CursorLeftBuildMenuCommand.h"
#include "CursorRightBuildMenuCommand.h"
#include "CursorLeftBuildingPhaseCommand.h"
#include "BuildMenuLoadFileCommand.h"

// Confirm Build menu commands
#include "CursorLeftConfirmBuildCommand.h"
#include "CursorRightConfirmBuildCommand.h"
#include "ConfirmDoneBuildingCommand.h"
#include "ConfirmBuildLoadFileCommand.h"

// Option Menu Commands
#include "ConfirmChoiceOptionMenu.h"
#include "CursorDownOptionMenuCommand.h"
#include "CursorUpOptionMenuCommand.h"
#include "OptionLoadFileCommand.h"

// Combat Phase Commands P1
#include "MoveUnitLeftCommand.h"
#include "MoveUnitRightCommand.h"
#include "PauseGameCombatPhaseCommand.h" 
#include "FireCommand.h"
#include "CombatPhaseFileLoadCommand.h"
#include "CreateSupportUnit1P1.h"
#include "CreateSupportUnit2P1.h"
#include "CreateSupportUnit3P1.h"
#include "CreateSupportUnit4P1.h"
#include "GoToPauseMenuCommand.h"

// Combat Phase Commands P2
#include "MoveUnitLeftCommandP2.h"
#include "MoveUnitRightCommandP2.h"
#include "FireCommandP2.h"
#include "CreateSupportUnit1P2.h"
#include "CreateSupportUnit2P2.h"
#include "CreateSupportUnit3P2.h"
#include "CreateSupportUnit4P2.h"

// Map Phase Commands
#include "LoadMapPhaseCommand.h"

// End game commands
#include "ExitEndGameMenu.h"

// Pause Menu Commands
#include "ConfirmPauseMenuCommand.h"
#include "LoadPuaseMenuCommand.h"

InputHandler::InputHandler()
{
	frame_count_build = 0;
	frame_delay_build = 30;
}


InputHandler::~InputHandler()
{
}

void InputHandler::InitTitleHandler()
{
	key_board[KEYCODE_ENTER] = new ExitTitleCommand();
	//key_board[KEYCODE_L] = new TitleLoadFilesCommand();

	controller[AINDEX_START_BUTTON] = new ExitTitleCommand();
	//controller[AINDEX_SELECT_BUTTON] = new TitleLoadFilesCommand();
}

Command* InputHandler::TitleInputHandler()
{
	if (WasKeyPressed(KEYCODE_ENTER) == true &&
		WasKeyHeld(KEYCODE_ENTER) == false)
		return key_board[KEYCODE_ENTER];

	//if (WasKeyPressed(KEYCODE_L) == true &&
	//	WasKeyHeld(KEYCODE_L) == false)
	//	return //key_board[KEYCODE_L];

	if (GetHowManyControllers() != 0)
	{
		if (WasButtonPressed(0, AINDEX_START_BUTTON) == true &&
			WasButtonHeld(0, AINDEX_START_BUTTON) == false)
			return controller[AINDEX_START_BUTTON];

		//if (WasButtonPressed(0, AINDEX_SELECT_BUTTON) == true &&
		//	WasButtonHeld(0, AINDEX_SELECT_BUTTON) == false)
		//	return //controller[AINDEX_SELECT_BUTTON];
	}

	return NULL;
}

void InputHandler::InitMainMenuHandler()
{
	key_board[KEYCODE_BACKSPACE] = new BackToTitleCommand();
	key_board[KEYCODE_W] = new CursorUpMainMenuCommand();
	key_board[KEYCODE_S] = new CursorDownMainMenuCommand();
	key_board[KEYCODE_ENTER] = new SelectChoiceMainMenuCommand();
	//key_board[KEYCODE_L] = new MenuLoadFileCommand();

	controller[AINDEX_B_BUTTON] = new BackToTitleCommand();
	controller[AINDEX_UP_LANALOG] = new CursorUpMainMenuCommand();
	controller[AINDEX_DOWN_LANALOG] = new CursorDownMainMenuCommand();
	controller[AINDEX_A_BUTTON] = new SelectChoiceMainMenuCommand();
	//controller[AINDEX_SELECT_BUTTON] = new MenuLoadFileCommand();
}

Command* InputHandler::MainMenuInputHandler()

{
	if (WasKeyPressed(KEYCODE_BACKSPACE) == true &&
		WasKeyHeld(KEYCODE_BACKSPACE) == false)
	{
		return key_board[KEYCODE_BACKSPACE];
	}

	if (WasKeyPressed(KEYCODE_W) == true &&
		WasKeyHeld(KEYCODE_W) == false)
	{
		return key_board[KEYCODE_W];
	}

	if (WasKeyPressed(KEYCODE_S) == true &&
		WasKeyHeld(KEYCODE_S) == false)
	{
		return key_board[KEYCODE_S];
	}

	if (WasKeyPressed(KEYCODE_ENTER) == true &&
		WasKeyHeld(KEYCODE_ENTER) == false)
	{
		return key_board[KEYCODE_ENTER];
	}

	//if (WasKeyPressed(KEYCODE_L) == true &&
	//	WasKeyHeld(KEYCODE_L) == false)
	//{
	//	return //key_board[KEYCODE_L];
	//}


	if (GetHowManyControllers() != 0)
	{
		//if (WasButtonPressed(0, AINDEX_SELECT_BUTTON) == true &&
		//	WasButtonHeld(0, AINDEX_SELECT_BUTTON) == false)
		//{
		//	return //controller[AINDEX_SELECT_BUTTON];
		//}

		if (WasButtonPressed(0, AINDEX_B_BUTTON) == true &&
			WasButtonHeld(0, AINDEX_B_BUTTON) == false)
		{
			return controller[AINDEX_B_BUTTON];
		}

		if (WasButtonPressed(0, AINDEX_DOWN_LANALOG) == true &&
			WasButtonHeld(0, AINDEX_DOWN_LANALOG) == false)
		{
			return controller[AINDEX_DOWN_LANALOG];
		}

		if (WasButtonPressed(0, AINDEX_UP_LANALOG) == true &&
			WasButtonHeld(0, AINDEX_UP_LANALOG) == false)
		{
			return controller[AINDEX_UP_LANALOG];
		}

		if (WasButtonPressed(0, AINDEX_A_BUTTON) == true &&
			WasButtonHeld(0, AINDEX_A_BUTTON) == false)
		{
			return controller[AINDEX_A_BUTTON];
		}
	}

	return NULL;
}

void InputHandler::InitBuildPhase()
{
	key_board[KEYCODE_W] = new CursorUpBuildingPhaseCommand();
	key_board[KEYCODE_S] = new CursorDownBuildingPhaseCommand();
	key_board[KEYCODE_A] = new CursorLeftBuildingPhaseCommand();
	key_board[KEYCODE_D] = new CursorRightBuildingPhaseCommand();
	key_board[KEYCODE_ENTER] = new GoToBuildMenuCommand();
	key_board[KEYCODE_C] = new FinishBuildingCommand();

	controller[AINDEX_UP_LANALOG] = new CursorUpBuildingPhaseCommand();
	controller[AINDEX_DOWN_LANALOG] = new CursorDownBuildingPhaseCommand();
	controller[AINDEX_LEFT_LANALOG] = new CursorLeftBuildingPhaseCommand();
	controller[AINDEX_RIGHT_LANALOG] = new CursorRightBuildingPhaseCommand();
	controller[AINDEX_A_BUTTON] = new GoToBuildMenuCommand();
	controller[AINDEX_START_BUTTON] = new FinishBuildingCommand();
}

Command* InputHandler::BuildPhaseInputHandler()
{
	// Movement - WASD
	if (WasKeyPressed(KEYCODE_A) == true &&
		WasKeyHeld(KEYCODE_A) == false)
	{
		return key_board[KEYCODE_A];
	}
	if (WasKeyPressed(KEYCODE_D) == true &&
		WasKeyHeld(KEYCODE_D) == false)
	{
		return  key_board[KEYCODE_D];
	}
	if (WasKeyPressed(KEYCODE_W) == true &&
		WasKeyHeld(KEYCODE_W) == false)
	{
		return  key_board[KEYCODE_W];
	}
	if (WasKeyPressed(KEYCODE_S) == true &&
		WasKeyHeld(KEYCODE_S) == false)
	{
		return  key_board[KEYCODE_S];
	}
	// Select
	if (WasKeyPressed(KEYCODE_ENTER) == true &&
		WasKeyHeld(KEYCODE_ENTER) == false)
	{
		return key_board[KEYCODE_ENTER];
	}
	if (WasKeyPressed(KEYCODE_C) == true &&
		WasKeyHeld(KEYCODE_C) == false)
	{
		return key_board[KEYCODE_C];
	}

	if (GetHowManyControllers() != 0)
	{
		// Movement - Left joystick
		if (WasButtonPressed(0, AINDEX_UP_LANALOG) == true &&
			WasButtonHeld(0, AINDEX_UP_LANALOG) == false)
		{
			return controller[AINDEX_UP_LANALOG];
		}
		if (WasButtonPressed(0, AINDEX_DOWN_LANALOG) == true &&
			WasButtonHeld(0, AINDEX_DOWN_LANALOG) == false)
		{
			return controller[AINDEX_DOWN_LANALOG];
		}
		if (WasButtonPressed(0, AINDEX_LEFT_LANALOG) == true &&
			WasButtonHeld(0, AINDEX_LEFT_LANALOG) == false)
		{
			return controller[AINDEX_LEFT_LANALOG];
		}
		if (WasButtonPressed(0, AINDEX_RIGHT_LANALOG) == true &&
			WasButtonHeld(0, AINDEX_RIGHT_LANALOG) == false)
		{
			return controller[AINDEX_RIGHT_LANALOG];
		}
		// Select
		if (WasButtonPressed(0, AINDEX_A_BUTTON) == true &&
			WasButtonHeld(0, AINDEX_A_BUTTON) == false)
		{
			return controller[AINDEX_A_BUTTON];
		}
		if (WasButtonPressed(0, AINDEX_START_BUTTON) == true &&
			WasButtonHeld(0, AINDEX_START_BUTTON) == false)
		{
			return controller[AINDEX_START_BUTTON];
		}
	}
	return NULL;
}

void InputHandler::InitBuildMenu()
{
	key_board[KEYCODE_BACKSPACE] = new BackToBuildPhaseCommand();
	key_board[KEYCODE_W] = new CursorUpBuildMenuCommand();
	key_board[KEYCODE_S] = new CursorDownBuildMenuCommand();
	key_board[KEYCODE_A] = new CursorLeftBuildMenuCommand();
	key_board[KEYCODE_D] = new CursorRightBuildMenuCommand();
	key_board[KEYCODE_ENTER] = new BuyBuildingCommand();
	//key_board[KEYCODE_L] = new BuildMenuLoadFileCommand();

	controller[AINDEX_B_BUTTON] = new BackToBuildPhaseCommand();
	controller[AINDEX_UP_LANALOG] = new CursorUpBuildMenuCommand();
	controller[AINDEX_DOWN_LANALOG] = new CursorDownBuildMenuCommand();
	controller[AINDEX_LEFT_LANALOG] = new CursorLeftBuildMenuCommand();
	controller[AINDEX_RIGHT_LANALOG] = new CursorRightBuildMenuCommand();
	controller[AINDEX_A_BUTTON] = new BuyBuildingCommand();
	//controller[AINDEX_SELECT_BUTTON] = new BuildMenuLoadFileCommand();
}

Command* InputHandler::BuildMenuInputHandler()
{
	if (WasKeyPressed(KEYCODE_BACKSPACE) == true &&
		WasKeyHeld(KEYCODE_BACKSPACE) == false)
	{
		return key_board[KEYCODE_BACKSPACE];
	}
	if (WasKeyPressed(KEYCODE_A) == true &&
		WasKeyHeld(KEYCODE_A) == false)
	{
		return key_board[KEYCODE_A];
	}
	if (WasKeyPressed(KEYCODE_D) == true &&
		WasKeyHeld(KEYCODE_D) == false)
	{
		return key_board[KEYCODE_D];
	}
	if (WasKeyPressed(KEYCODE_S) == true &&
		WasKeyHeld(KEYCODE_S) == false)
	{
		return key_board[KEYCODE_S];
	}
	if (WasKeyPressed(KEYCODE_W) == true &&
		WasKeyHeld(KEYCODE_W) == false)
	{
		return key_board[KEYCODE_W];
	}
	if (WasKeyPressed(KEYCODE_ENTER) == true &&
		WasKeyHeld(KEYCODE_ENTER) == false)
	{
		return key_board[KEYCODE_ENTER];
	}
	//if (WasKeyPressed(KEYCODE_L) == true &&
	//	WasKeyHeld(KEYCODE_L) == false)
	//{
	//	return //key_board[KEYCODE_L];
	//}

	if (GetHowManyControllers() != 0)
	{
		if (WasButtonPressed(0, AINDEX_B_BUTTON) == true &&
			WasButtonHeld(0, AINDEX_B_BUTTON) == false)
		{
			return controller[AINDEX_B_BUTTON];
		}
		if (WasButtonPressed(0, AINDEX_LEFT_LANALOG) == true &&
			WasButtonHeld(0, AINDEX_LEFT_LANALOG) == false)
		{
			return controller[AINDEX_LEFT_LANALOG];
		}
		if (WasButtonPressed(0, AINDEX_RIGHT_LANALOG) == true &&
			WasButtonHeld(0, AINDEX_RIGHT_LANALOG) == false)
		{
			return controller[AINDEX_RIGHT_LANALOG];
		}
		if (WasButtonPressed(0, AINDEX_UP_LANALOG) == true &&
			WasButtonHeld(0, AINDEX_UP_LANALOG) == false)
		{
			return controller[AINDEX_UP_LANALOG];
		}
		if (WasButtonPressed(0, AINDEX_DOWN_LANALOG) == true &&
			WasButtonHeld(0, AINDEX_DOWN_LANALOG) == false)
		{
			return controller[AINDEX_DOWN_LANALOG];
		}
		if (WasButtonPressed(0, AINDEX_A_BUTTON) == true &&
			WasButtonHeld(0, AINDEX_A_BUTTON) == false)
		{
			return controller[AINDEX_A_BUTTON];
		}
		//if (WasButtonPressed(0, AINDEX_SELECT_BUTTON) == true &&
		//	WasButtonHeld(0, AINDEX_SELECT_BUTTON) == false)
		//{
		//	return //controller[AINDEX_SELECT_BUTTON];
		//}
	}
	return NULL;
}

void InputHandler::InitConfirmBuildMenu()
{
	key_board[KEYCODE_A] = new CursorLeftConfirmBuildCommand();
	key_board[KEYCODE_D] = new CursorRightConfirmBuildCommand();
	key_board[KEYCODE_ENTER] = new ConfirmDoneBuildingCommand();
	//key_board[KEYCODE_L] = new ConfirmBuildLoadFileCommand();

	controller[AINDEX_LEFT_LANALOG] = new CursorLeftConfirmBuildCommand();
	controller[AINDEX_RIGHT_LANALOG] = new CursorRightConfirmBuildCommand();
	controller[AINDEX_A_BUTTON] = new ConfirmDoneBuildingCommand();
	//controller[AINDEX_SELECT_BUTTON] = new ConfirmBuildLoadFileCommand();
}

Command* InputHandler::ConfirmBuildMenuInputHandler()
{
	if (WasKeyPressed(KEYCODE_ENTER) == true &&
		WasKeyHeld(KEYCODE_ENTER) == false)
	{
		return key_board[KEYCODE_ENTER];
	}
	if (WasKeyPressed(KEYCODE_A) == true &&
		WasKeyHeld(KEYCODE_A) == false)
	{
		return key_board[KEYCODE_A];
	}
	if (WasKeyPressed(KEYCODE_D) == true &&
		WasKeyHeld(KEYCODE_D) == false)
	{
		return key_board[KEYCODE_D];
	}
	//if (WasKeyPressed(KEYCODE_L) == true &&
	//	WasKeyHeld(KEYCODE_L) == false)
	//{
	//	return //key_board[KEYCODE_L];
	//}

	if (GetHowManyControllers() != 0)
	{
		if (WasButtonPressed(0, AINDEX_A_BUTTON) == true &&
			WasButtonHeld(0, AINDEX_A_BUTTON) == false)
		{
			return controller[AINDEX_A_BUTTON];
		}

		if (WasButtonPressed(0, AINDEX_LEFT_LANALOG) == true &&
			WasButtonHeld(0, AINDEX_LEFT_LANALOG) == false)
		{
			return controller[AINDEX_LEFT_LANALOG];
		}

		if (WasButtonPressed(0, AINDEX_RIGHT_LANALOG) == true &&
			WasButtonHeld(0, AINDEX_RIGHT_LANALOG) == false)
		{
			return controller[AINDEX_RIGHT_LANALOG];
		}

		//if (WasButtonPressed(0, AINDEX_SELECT_BUTTON) == true &&
		//	WasButtonHeld(0, AINDEX_SELECT_BUTTON) == false)
		//{
		//	return //controller[AINDEX_SELECT_BUTTON];
		//}
	}

	return NULL;
}

void InputHandler::InitCombatPhase()
{
	key_board[KEYCODE_A] = new MoveUnitLeftCommand();
	key_board[KEYCODE_D] = new MoveUnitRightCommand();
	key_board[KEYCODE_ENTER] = new FireCommand();
	//key_board[KEYCODE_L] = new CombatPhaseFileLoadCommand();
	key_board[KEYCODE_1] = new CreateSupportUnit1P1();
	key_board[KEYCODE_2] = new CreateSupportUnit2P1();
	key_board[KEYCODE_3] = new CreateSupportUnit3P1();
	key_board[KEYCODE_4] = new CreateSupportUnit4P1();
	key_board[KEYCODE_P] = new GoToPauseMenuCommand();

	controller[AINDEX_LEFT_LANALOG] = new MoveUnitLeftCommand();
	controller[AINDEX_RIGHT_LANALOG] = new MoveUnitRightCommand();
	controller[AINDEX_A_BUTTON] = new FireCommand();
	//controller[AINDEX_SELECT_BUTTON] = new CombatPhaseFileLoadCommand();
	controller[AINDEX_LEFTBUMPER_BUTTON] = new CreateSupportUnit1P1();
	controller[AINDEX_RIGHTBUMPER_BUTTON] = new CreateSupportUnit4P1();
	controller[AINDEX_LEFT_TRIGGER] = new CreateSupportUnit2P1();
	controller[AINDEX_RIGHT_TRIGGER] =  new FireCommand();
	controller[AINDEX_START_BUTTON] = new GoToPauseMenuCommand();
	controller[AINDEX_B_BUTTON] = new CreateSupportUnit3P1();
}

Command* InputHandler::CombatPhaseInputHandler()
{
	// Movement - WASD
	if (WasKeyPressed(KEYCODE_A) == true &&
		WasKeyHeld(KEYCODE_A) == false)
	{
		return key_board[KEYCODE_A];
	}
	if (WasKeyPressed(KEYCODE_D) == true &&
		WasKeyHeld(KEYCODE_D) == false)
	{
		return  key_board[KEYCODE_D];
	}
	if (WasKeyPressed(KEYCODE_W) == true &&
		WasKeyHeld(KEYCODE_W) == false)
	{
		return  key_board[KEYCODE_W];
	}
	if (WasKeyPressed(KEYCODE_S) == true &&
		WasKeyHeld(KEYCODE_S) == false)
	{
		return  key_board[KEYCODE_S];
	}
	//if (WasKeyPressed(KEYCODE_L) == true &&
	//	WasKeyHeld(KEYCODE_L) == false)
	//{
	//	return //key_board[KEYCODE_L];
	//}
	// Fire
	if (WasKeyPressed(KEYCODE_ENTER) == true &&
		WasKeyHeld(KEYCODE_ENTER) == false)
	{
		return key_board[KEYCODE_ENTER];
	}
	// Launch new supports
	if (WasKeyPressed(KEYCODE_1) == true &&
		WasKeyHeld(KEYCODE_1) == false)
	{
		return key_board[KEYCODE_1];
	}
	if (WasKeyPressed(KEYCODE_2) == true &&
		WasKeyHeld(KEYCODE_2) == false)
	{
		return key_board[KEYCODE_2];
	}
	if (WasKeyPressed(KEYCODE_3) == true &&
		WasKeyHeld(KEYCODE_3) == false)
	{
		return key_board[KEYCODE_3];
	}
	if (WasKeyPressed(KEYCODE_4) == true &&
		WasKeyHeld(KEYCODE_4) == false)
	{
		return key_board[KEYCODE_4];
	}
	// Pause
	if (WasKeyPressed(KEYCODE_P) == true &&
		WasKeyHeld(KEYCODE_P) == false)
	{
		return key_board[KEYCODE_P];
	}

	if (GetHowManyControllers() != 0)
	{
		// Movement - Left joystick
		if (WasButtonPressed(0, AINDEX_UP_LANALOG) == true &&
			WasButtonHeld(0, AINDEX_UP_LANALOG) == false)
		{
			return controller[AINDEX_UP_LANALOG];
		}
		if (WasButtonPressed(0, AINDEX_DOWN_LANALOG) == true &&
			WasButtonHeld(0, AINDEX_DOWN_LANALOG) == false)
		{
			return controller[AINDEX_DOWN_LANALOG];
		}
		if (WasButtonPressed(0, AINDEX_LEFT_LANALOG) == true &&
			WasButtonHeld(0, AINDEX_LEFT_LANALOG) == false)
		{
			return controller[AINDEX_LEFT_LANALOG];
		}
		if (WasButtonPressed(0, AINDEX_RIGHT_LANALOG) == true &&
			WasButtonHeld(0, AINDEX_RIGHT_LANALOG) == false)
		{
			return controller[AINDEX_RIGHT_LANALOG];
		}
		// Fire
		if (WasButtonPressed(0, AINDEX_A_BUTTON) == true &&
			WasButtonHeld(0, AINDEX_A_BUTTON) == false)
		{
			return controller[AINDEX_A_BUTTON];
		}
		if (WasButtonPressed(0, AINDEX_B_BUTTON) == true &&
			WasButtonHeld(0, AINDEX_B_BUTTON) == false)
		{
			return controller[AINDEX_B_BUTTON];
		}
		//if (WasButtonPressed(0, AINDEX_SELECT_BUTTON) == true &&
		//	WasButtonHeld(0, AINDEX_SELECT_BUTTON) == false)
		//{
		//	return //controller[AINDEX_SELECT_BUTTON];
		//}
		// Launch supports
		// [2 lt]	[3 rt]
		// [1 lb]	[4 rb]
		if (WasButtonPressed(0, AINDEX_LEFT_TRIGGER) == true &&
			WasButtonHeld(0, AINDEX_LEFT_TRIGGER) == false)
		{
			return controller[AINDEX_LEFT_TRIGGER];
		}
		if (WasButtonPressed(0, AINDEX_LEFTBUMPER_BUTTON) == true &&
			WasButtonHeld(0, AINDEX_LEFTBUMPER_BUTTON) == false)
		{
			return controller[AINDEX_LEFTBUMPER_BUTTON];
		}
		if (WasButtonPressed(0, AINDEX_RIGHT_TRIGGER) == true &&
			WasButtonHeld(0, AINDEX_RIGHT_TRIGGER) == false)
		{
			return controller[AINDEX_RIGHT_TRIGGER];
		}
		if (WasButtonPressed(0, AINDEX_RIGHTBUMPER_BUTTON) == true &&
			WasButtonHeld(0, AINDEX_RIGHTBUMPER_BUTTON) == false)
		{
			return controller[AINDEX_RIGHTBUMPER_BUTTON];
		}
		// Pause
		if (WasButtonPressed(0, AINDEX_START_BUTTON) == true &&
			WasButtonHeld(0, AINDEX_START_BUTTON) == false)
		{
			return controller[AINDEX_START_BUTTON];
		}
	}
	return NULL;

}

void InputHandler::InitCombatPhaseP2()
{
	key_board[KEYCODE_J] = new MoveUnitLeftCommandP2();
	//key_board[KEYCODE_L] = new MoveUnitRightCommandP2();
	key_board[KEYCODE_ENTER] = new FireCommand();
	key_board[KEYCODE_1] = new CreateSupportUnit1P2();
	key_board[KEYCODE_2] = new CreateSupportUnit2P2();
	key_board[KEYCODE_3] = new CreateSupportUnit3P2();
	key_board[KEYCODE_4] = new CreateSupportUnit4P2();

	controller[AINDEX_LEFT_LANALOG] = new MoveUnitLeftCommandP2();
	controller[AINDEX_RIGHT_LANALOG] = new MoveUnitRightCommandP2();
	controller[AINDEX_A_BUTTON] = new FireCommand();
	controller[AINDEX_LEFTBUMPER_BUTTON] = new CreateSupportUnit1P2();
	controller[AINDEX_RIGHTBUMPER_BUTTON] = new CreateSupportUnit2P2();
	controller[AINDEX_LEFT_TRIGGER] = new CreateSupportUnit3P2();
	controller[AINDEX_RIGHT_TRIGGER] = new CreateSupportUnit4P2();
}

Command* InputHandler::CombatPhaseInputHandlerP2()
{
	// Movement - IJKL
	if (WasKeyPressed(KEYCODE_I) == true &&
		WasKeyHeld(KEYCODE_I) == false)
	{
		return key_board[KEYCODE_I];
	}
	if (WasKeyPressed(KEYCODE_J) == true &&
		WasKeyHeld(KEYCODE_J) == false)
	{
		return  key_board[KEYCODE_J];
	}
	if (WasKeyPressed(KEYCODE_K) == true &&
		WasKeyHeld(KEYCODE_K) == false)
	{
		return  key_board[KEYCODE_K];
	}
	//if (WasKeyPressed(KEYCODE_L) == true &&
	//	WasKeyHeld(KEYCODE_L) == false)
	//{
	//	return  //key_board[KEYCODE_L];
	//}
	// Fire
	if (WasKeyPressed(KEYCODE_SPACE) == true &&
		WasKeyHeld(KEYCODE_SPACE) == false)
	{
		return key_board[KEYCODE_SPACE];
	}
	// Launch new supports
	if (WasKeyPressed(KEYCODE_7) == true &&
		WasKeyHeld(KEYCODE_7) == false)
	{
		return key_board[KEYCODE_7];
	}
	if (WasKeyPressed(KEYCODE_8) == true &&
		WasKeyHeld(KEYCODE_8) == false)
	{
		return key_board[KEYCODE_8];
	}
	if (WasKeyPressed(KEYCODE_9) == true &&
		WasKeyHeld(KEYCODE_9) == false)
	{
		return key_board[KEYCODE_9];
	}
	if (WasKeyPressed(KEYCODE_0) == true &&
		WasKeyHeld(KEYCODE_0) == false)
	{
		return key_board[KEYCODE_0];
	}
	return NULL;

}

void InputHandler::InitOptionMenu()
{
	key_board[KEYCODE_S] = new CursorDownOptionMenuCommand();
	key_board[KEYCODE_W] = new CursorUpOptionMenuCommand();
	key_board[KEYCODE_ENTER] = new ConfirmChoiceOptionMenu();
	//key_board[KEYCODE_L] = new OptionLoadFileCommand();

	controller[AINDEX_DOWN_LANALOG] = new CursorDownOptionMenuCommand();
	controller[AINDEX_UP_LANALOG] = new CursorUpOptionMenuCommand();
	controller[AINDEX_A_BUTTON] = new ConfirmChoiceOptionMenu();
	//controller[AINDEX_SELECT_BUTTON] = new OptionLoadFileCommand();
}

Command* InputHandler::OptionMemuInputHandler()
{
	if (WasKeyPressed(KEYCODE_S) == true &&
		WasKeyHeld(KEYCODE_S) == false)
	{
		return key_board[KEYCODE_S];
	}
	if (WasKeyPressed(KEYCODE_W) == true &&
		WasKeyHeld(KEYCODE_W) == false)
	{
		return key_board[KEYCODE_W];
	}
	if (WasKeyPressed(KEYCODE_ENTER) == true &&
		WasKeyHeld(KEYCODE_ENTER) == false)
	{
		return key_board[KEYCODE_ENTER];
	}
	//if (WasKeyPressed(KEYCODE_L) == true &&
	//	WasKeyHeld(KEYCODE_L) == false)
	//{
	//	return //key_board[KEYCODE_L];
	//}

	if (GetHowManyControllers() != 0)
	{
		if (WasButtonPressed(0, AINDEX_UP_LANALOG) == true &&
			WasButtonHeld(0, AINDEX_UP_LANALOG) == false)
		{
			return controller[AINDEX_UP_LANALOG];
		}
		if (WasButtonPressed(0, AINDEX_DOWN_LANALOG) == true &&
			WasButtonHeld(0, AINDEX_DOWN_LANALOG) == false)
		{
			return controller[AINDEX_DOWN_LANALOG];
		}
		if (WasButtonPressed(0, AINDEX_A_BUTTON) == true &&
			WasButtonHeld(0, AINDEX_A_BUTTON) == false)
		{
			return controller[AINDEX_A_BUTTON];
		}
		//if (WasButtonPressed(0, AINDEX_SELECT_BUTTON) == true &&
		//	WasButtonHeld(0, AINDEX_SELECT_BUTTON) == false)
		//{
		//	return //controller[AINDEX_SELECT_BUTTON];
		//}
	}
	return NULL;
}

void InputHandler::InitEndGameCommands()
{
	key_board[KEYCODE_ENTER] = new ExitEndGameMenu();

	controller[AINDEX_A_BUTTON] = new ExitEndGameMenu();
	controller[AINDEX_START_BUTTON] = new ExitEndGameMenu();
}

Command* InputHandler::EndGameInputHandler()
{
	if (WasKeyPressed(KEYCODE_ENTER) == true &&
		WasKeyHeld(KEYCODE_ENTER) == false)
	{
		return key_board[KEYCODE_ENTER];
	}
	if (GetHowManyControllers() != 0)
	{
		if (WasButtonPressed(0, AINDEX_A_BUTTON) == true &&
			WasButtonHeld(0, AINDEX_A_BUTTON) == false)
		{
			return controller[AINDEX_A_BUTTON];
		}
		if (WasButtonPressed(0, AINDEX_START_BUTTON) == true &&
			WasButtonHeld(0, AINDEX_START_BUTTON) == false)
		{
			return controller[AINDEX_START_BUTTON];
		}
	}
	return NULL;
}

Command* InputHandler::RunABuildComputerCommands()
{
	frame_count_build += 1;

	if (frame_count_build >= frame_delay_build)			// If we stayed at that frame longer or equal to the delay to switch frame
	{
		if (computer_commands.size() == 0)
		{
			frame_count_build = 0;						// The count on that frame goes to zero
			return NULL;
		}
		else
		{
			frame_count_build = 0;
			return computer_commands.front();
		}
	}
	else
	{
		return NULL;
	}
}

std::queue <Command*> & InputHandler::GetComputerCommands()
{
	return computer_commands;
}

void InputHandler::DeleteCurrentHandler()
{
	for (auto const& x : key_board)
	{
		delete(x.second);
	}
	key_board.clear();
	for (auto const& x : controller)
	{
		delete(x.second);
	}
	controller.clear();
}

void InputHandler::DeleteComputerCommand()
{
	delete(computer_commands.front());
	computer_commands.pop();
}

void InputHandler::InitMapPhaseCommands()
{
	//key_board[KEYCODE_L] = new LoadMapPhaseCommand();

	//controller[AINDEX_SELECT_BUTTON] = new LoadMapPhaseCommand();
}

Command* InputHandler::MapPhaseInputHandler()
{
	//if (WasKeyPressed(KEYCODE_L) == true &&
	//	WasKeyHeld(KEYCODE_L) == false)
	//{
	//	return //key_board[KEYCODE_L];
	//}

	if (GetHowManyControllers() != 0)
	{
		//if (WasButtonPressed(0, AINDEX_SELECT_BUTTON) == true &&
		//	WasButtonHeld(0, AINDEX_SELECT_BUTTON) == false)
		//{
		//	return //controller[AINDEX_SELECT_BUTTON];
		//}
	}
	return NULL;
}

void InputHandler::InitPauseMenuCommands()
{
	key_board[KEYCODE_ENTER] = new ConfirmPauseMenuCommand();
	key_board[KEYCODE_A] = new CursorLeftConfirmBuildCommand();
	key_board[KEYCODE_D] = new CursorRightConfirmBuildCommand();
	//key_board[KEYCODE_L] = new LoadPuaseMenuCommand();

	controller[AINDEX_A_BUTTON] = new ConfirmPauseMenuCommand();
	controller[AINDEX_START_BUTTON] = new ConfirmPauseMenuCommand();
	controller[AINDEX_LEFT_LANALOG] = new CursorLeftConfirmBuildCommand();
	controller[AINDEX_RIGHT_LANALOG] = new CursorRightConfirmBuildCommand();
	//controller[AINDEX_SELECT_BUTTON] = new LoadPuaseMenuCommand();
}

Command* InputHandler::PauseMenuInputHandler()
{
	if (WasKeyPressed(KEYCODE_ENTER) == true &&
		WasKeyHeld(KEYCODE_ENTER) == false)
	{
		return key_board[KEYCODE_ENTER];
	}
	if (WasKeyPressed(KEYCODE_A) == true &&
		WasKeyHeld(KEYCODE_A) == false)
	{
		return key_board[KEYCODE_A];
	}
	if (WasKeyPressed(KEYCODE_D) == true &&
		WasKeyHeld(KEYCODE_D) == false)
	{
		return key_board[KEYCODE_D];
	}
	//if (WasKeyPressed(KEYCODE_L) == true &&
	//	WasKeyHeld(KEYCODE_L) == false)
	//{
	//	return //key_board[KEYCODE_L];
	//}

	if (GetHowManyControllers() != 0)
	{
		//if (WasButtonPressed(0, AINDEX_SELECT_BUTTON) == true &&
		//	WasButtonHeld(0, AINDEX_SELECT_BUTTON) == false)
		//{
		//	return //controller[AINDEX_SELECT_BUTTON];
		//}
		if (WasButtonPressed(0, AINDEX_A_BUTTON) == true &&
			WasButtonHeld(0, AINDEX_A_BUTTON) == false)
		{
			return controller[AINDEX_A_BUTTON];
		}
		if (WasButtonPressed(0, AINDEX_START_BUTTON) == true &&
			WasButtonHeld(0, AINDEX_START_BUTTON) == false)
		{
			return controller[AINDEX_START_BUTTON];
		}
		if (WasButtonPressed(0, AINDEX_LEFT_LANALOG) == true &&
			WasButtonHeld(0, AINDEX_LEFT_LANALOG) == false)
		{
			return controller[AINDEX_LEFT_LANALOG];
		}
		if (WasButtonPressed(0, AINDEX_RIGHT_LANALOG) == true &&
			WasButtonHeld(0, AINDEX_RIGHT_LANALOG) == false)
		{
			return controller[AINDEX_RIGHT_LANALOG];
		}
	}
	return NULL;
}