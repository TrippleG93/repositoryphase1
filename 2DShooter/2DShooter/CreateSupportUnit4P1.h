#pragma once

#include "Command.h"

class CreateSupportUnit4P1 :  public Command
{
public:
	~CreateSupportUnit4P1();

	virtual void execute() {};
	virtual void execute(State &state);
};

