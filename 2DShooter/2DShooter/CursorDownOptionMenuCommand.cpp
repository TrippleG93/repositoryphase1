#include "CursorDownOptionMenuCommand.h"

#include "State.h"
#include "OptionMenu.h"

CursorDownOptionMenuCommand::~CursorDownOptionMenuCommand()
{
}

void CursorDownOptionMenuCommand::execute(State &state)
{
	state.GetOptionMenu()->MoveCursorDown();
	state.GetAudioManager().PlayCursorMove();
}
