#pragma once
#include "Command.h"
class LoadMapPhaseCommand :
	public Command
{
public:
	~LoadMapPhaseCommand();
	
	virtual void execute() {};
	virtual void execute(State& state);
};

