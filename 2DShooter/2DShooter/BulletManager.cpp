#include "BulletManager.h"

#include "Bullet.h"
#include "UnitManager.h"
#include "Unit.h"
#include "Globals.h"

#include <iostream>

BulletManager::BulletManager()
{
}

BulletManager::~BulletManager()
{

}

void BulletManager::UpdateBullet()
{
	// Update reload counter every frame if needed 
	if (player1_reload_count > 0)
		player1_reload_count += 1;

	for (std::vector<Bullet*>::size_type i = 0; i < player1_bullets.size(); i++)
	{
		player1_bullets.at(i)->UpdateBullet();
		
		if (player1_bullets.at(i)->GetYPosition() < -15 || player1_bullets.at(i)->DidBulletHitSomething() == true)
		{
			delete(player1_bullets.at(i));
			player1_bullets.erase(player1_bullets.begin() + i);
		}
	}

	for (std::vector<Bullet*>::size_type i = 0; i < player2_bullets.size(); i++)
	{
		player2_bullets.at(i)->UpdateBullet();
		
		if (player2_bullets.at(i)->GetYPosition() > globals::GAME_HEIGHT+15 || player2_bullets.at(i)->DidBulletHitSomething() == true)
		{
			delete(player2_bullets.at(i));
			player2_bullets.erase(player2_bullets.begin() + i);
		}
	}
}

void BulletManager::RenderBulletManager(float window_scale_x, float window_scale_y)
{
	for (auto const& x : player1_bullets)
	{
		x->RenderBullet(window_scale_x, window_scale_y);
	}
	for (auto const& x : player2_bullets)
	{
		x->RenderBullet(window_scale_x, window_scale_y);
	}
}

void BulletManager::CreateBullet(int player, int type, UnitManager &unit_manager)
{
	if (player == 1)
	{
		if (type == 2 && unit_manager.GetPlayer1SupportUnit(8) != NULL) // Bullet special to Support Unit 4
		{
			player1_bullets.push_back(new Bullet(player,
				unit_manager.GetPlayer1SupportUnit(8)->GetXPosition() + (unit_manager.GetPlayer1SupportUnit(8)->GetSprite().GetWidth() * unit_manager.GetPlayer1SupportUnit(8)->GetSprite().GetScaleX())/2,
				unit_manager.GetPlayer1SupportUnit(8)->GetYPosition() + (unit_manager.GetPlayer1SupportUnit(8)->GetSprite().GetHeight() * unit_manager.GetPlayer1SupportUnit(8)->GetSprite().GetScaleY()) / 2,
				type,
				unit_manager.GetPlayer1SupportUnit(8)->GetBulletBoost(),
				unit_manager.GetPlayer2Unit()->GetXPosition(),
				unit_manager.GetPlayer2Unit()->GetYPosition()));

		}
		else if (player1_reload_count == 0 && player1_bullets.size() < unit_manager.GetBulletCap())
		{
			player1_bullets.push_back(new Bullet(player,
				unit_manager.GetPlayer1Unit()->GetXPosition() + (unit_manager.GetPlayer1Unit()->GetSprite().GetWidth() * unit_manager.GetPlayer1Unit()->GetSprite().GetScaleX()) / 2,
				unit_manager.GetPlayer1Unit()->GetYPosition(),
				type, 
				unit_manager.GetPlayer1Unit()->GetBulletBoost()));
		}
		else if (player1_reload_count < player1_reload_delay)
		{
			if (player1_reload_count == 0)
				player1_reload_count = 1;
			return;
		}
		else
			player1_reload_count = 0;
	}
	else
	{
		if (type == 2) // Bullet special to Support Unit 4
		{
			player2_bullets.push_back(new Bullet(player,
				unit_manager.Getplayer2SupportUnit(8)->GetXPosition() + (unit_manager.Getplayer2SupportUnit(8)->GetSprite().GetWidth() * unit_manager.Getplayer2SupportUnit(8)->GetSprite().GetScaleX())  / 2,
				unit_manager.Getplayer2SupportUnit(8)->GetYPosition() + (unit_manager.Getplayer2SupportUnit(8)->GetSprite().GetHeight() * unit_manager.Getplayer2SupportUnit(8)->GetSprite().GetScaleY()) / 2,
				type,
				unit_manager.Getplayer2SupportUnit(8)->GetBulletBoost(),
				unit_manager.GetPlayer1Unit()->GetXPosition(),
				unit_manager.GetPlayer1Unit()->GetYPosition()));
		}
		else if (player2_reload_count == 0 && player2_bullets.size() < unit_manager.GetBulletCap())
		{
			player2_bullets.push_back(new Bullet(player,
				unit_manager.GetPlayer2Unit()->GetXPosition() + (unit_manager.GetPlayer2Unit()->GetSprite().GetWidth() * unit_manager.GetPlayer2Unit()->GetSprite().GetScaleX()) / 2,
				unit_manager.GetPlayer2Unit()->GetYPosition() + (unit_manager.GetPlayer2Unit()->GetSprite().GetHeight() * unit_manager.GetPlayer2Unit()->GetSprite().GetScaleY()),
				type,
				unit_manager.GetPlayer2Unit()->GetBulletBoost()));
		}
		else if (player2_reload_count < player2_reload_delay)
		{
			if (player2_reload_count == 0)
				player2_reload_count = 1;
			return;
		}
		else
			player2_reload_count = 0;
	}
}

std::vector <Bullet*> BulletManager::GetPlayer1Bullets()
{
	return player1_bullets;
}

std::vector <Bullet*> BulletManager::GetPlayer2Bullets()
{
	return player2_bullets;
}

int BulletManager::GetPlayer1BulletX(std::vector<Bullet*>::size_type i)
{
	return player1_bullets.at(i)->GetXPosition();
}

int BulletManager::GetPlayer1BulletY(std::vector<Bullet*>::size_type i)
{
	return player1_bullets.at(i)->GetYPosition();
}