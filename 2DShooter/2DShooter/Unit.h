#pragma once

#include "SpriteAnimation.h"
#include "Render.h"
#include "Audio.h"
#include "Font.h"

/*
Class Unit
Holds the basic information of the unit
*/

class Unit
{
public:
	Unit();
	Unit(int type, int player, int position_x, int position_y);
	~Unit();

	void SetUp(int building_type, int player, int position_x, int position_y);

	void RenderUnit(float window_scale_x, float window_scale_y);

	void UpdateUnit(int opp_x, int opp_y);

	/*
	MoveLeft
	Set -2 to x_velocity to move the ship to the left
	*/
	void MoveLeft();

	/*
	MoveRight
	Set 2 to the x_velocity to move the ship to the right
	*/
	void MoveRight();

	/*
	MoveUp
	Set -2 to the y_velocity to move the ship upward
	*/
	void MoveUp();

	/*
	MoveDown
	Set 2 ti the y_velocity to move the ship downward
	*/
	void MoveDown();

	/*
	IdleHorizontal
	Set the x_velocity to 0 it will no longer move in
	the x direction
	*/
	void IdleHorizontal();

	/*
	IdleVerticle
	Set the y_velocity to 0 it will no longer move
	in the y direction
	*/
	void IdleVerticle();

	/*
		GetXPosition
		returns the x position of the unit
	*/
	int GetXPosition();

	/*
	GetXVelocity
	returns the x velocity of the unit
	*/
	int GetXVelocity();

	/*
	SetXVelocity
	sets the x velocity of the unit
	*/
	void SetXVelocity(int new_x_velocity);

	/*
		GetYPosition
		returns the y position of the unit
	*/
	int GetYPosition();

	/*
		CollisionBox
		Checks to see 
	*/
	bool CollisionDetection(Sprite sprite_1, Sprite sprite_2);

	/*
	CollisionRadius
	Checks to see
	*/
	bool CollisionDetectionRadius(Sprite sprite_1, Sprite sprite_2);

	/*
		GetSprite
		Obtains the sprite oject inside the sprite animation class
	*/
	Sprite GetSprite();

	/*
		TakeDamage
		Reduces that unit health based on the damage input
	*/
	void TakeDamage(int damage, int bonus);

	void Heal(int health_boost);

	int GetDamageBoost() { return damage_boost; }
	
	bool IsUnitDead() { return dead; }

	void SuicideAi(int position_x, int position_y);

	void ProtectAi(int position_x, int position_y);
	
	void SentryAi(int position_x, int position_y);

	void HealAi(int position_x, int position_y);
	
	float GetBulletBoost();

	void BoostAttack(int boost);

	void BoostDefense(int boost);

	void BoostHealth(int boost);

	void BoostSpeed(int boost);

private:

	void DrawHealthbar(float window_scale_x, float window_scale_y);

	enum type
	{
		ATTACK = 1, DEFENCE, SPEED, BALANCE, SUPPORT1, SUPPORT2, SUPPORT3, SUPPORT4
	};

	bool dead = false;
	int building_type;								// Holds what building is associated with the unit
	int which_player;								// Holds which player owns that unit
	int health_points;								// Holds the health of the unit
	int max_health;
	int current_x_velocity;							// Holds the current x velocity of the ship
	int current_y_velocity;							// Holds the current y velocity of the ship
	int damage_boost;								// Holds the additionl percentage boost for damage
	int defence_boost;								// Holds the additonal percentage boost for defence 
	float bullet_boost = 1.0;						// Holds the boost for bullet velocity
	int ship_velocity_x;							// Holds the ship velocity y position
	int ship_velocity_y;							// Holds the ship velocity y position

	// SUICIDE AI
	bool suicide_ai_LR_move;
	int wander_count_suicide_ai;
	int wander_delay_suicide_ai = 60;
	int wander_min_suicide_ai;
	int wander_max_suicide_ai;

	int count_fire_S4;
	int delay_fire_S4 = 200;
	
	int frame_count_health;							// Holds how many frames passed showing the hit points
	int frame_delay_health;							// Holds when to stop showing hit points based on frames passed
		
	Render unit_image;								// Holds the image of the unit
	SpriteAnimation unit_animation;					// Holds the animaton of the unit

	Render	bar;									// Holds the image of the health bar
	SpriteAnimation bar_animation;					// Holds just the bar of health
	int bar_x_position;
	int bar_y_position;

	Render health;			
	SpriteAnimation health_animation;				// Holds how much health the unit has
	int health_x_position;
	int health_y_position;

	int opponent_position_x;
	int opponent_position_y;
	int distance_x;									// Holds the distance needed for the support unit PROTECTOR
	int distance_x2;								// Holds the distance needed for the support unit PROTEECTOR
	int distance_y;									// Holds the distance needed for the support unit PROTECTOR

	/*
		Heal Support Unit Ai
	*/
	enum heal_ai_state {WANDER, WAIT, HEAL};
	int state;
	int heal_stops;									// Holds how many times the support ship has to get to a spot to then heal unit
	int stop_amount;								// Holds how many times the unit has stoped at a location
	int time_waiting;								// Holds how long the support unit has been healing
	int time_to_move;								// Holds how much time they must wait till they can move
	int x_pos;
	int y_pos;
};

