#pragma once

#include "Command.h"

class BuildMenuLoadFileCommand : public Command
{
public:
	~BuildMenuLoadFileCommand();

	virtual void execute() {};
	virtual void execute(State &state);
};

