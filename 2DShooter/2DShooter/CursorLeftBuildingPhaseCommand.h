#pragma once

#include "Command.h"

class CursorLeftBuildingPhaseCommand : public Command
{
public:
	~CursorLeftBuildingPhaseCommand();

	virtual void execute() {};
	virtual void execute(State& state);
};

