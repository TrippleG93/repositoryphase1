#include "GoToPauseMenuCommand.h"

#include "State.h"
#include "Globals.h"

GoToPauseMenuCommand::~GoToPauseMenuCommand()
{
}

void GoToPauseMenuCommand::execute(State &state)
{
	state.SetState(PAUSEMENU);
	state.InitPauseMenu();
	state.GetAudioManager().PlayCursorSelect();
}