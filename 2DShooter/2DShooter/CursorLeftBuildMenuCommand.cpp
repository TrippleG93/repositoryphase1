#include "CursorLeftBuildMenuCommand.h"

#include "State.h"
#include "BuildMenu.h"

CursorLeftBuildMenuCommand::~CursorLeftBuildMenuCommand()
{

}

void CursorLeftBuildMenuCommand::execute(State &state)
{
	state.GetBuildMenu()->MoveCursorLeft();
	state.GetAudioManager().PlayCursorMove();
}
