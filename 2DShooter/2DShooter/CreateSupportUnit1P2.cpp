#include "CreateSupportUnit1P2.h"

#include "State.h"
#include "CombatPhase.h"
#include "UnitManager.h"

CreateSupportUnit1P2::~CreateSupportUnit1P2()
{
}

void CreateSupportUnit1P2::execute(State &state)
{
	state.GetCombatPhase()->GetUnitManager()->CreateSupportUnit2(5, 2);
}