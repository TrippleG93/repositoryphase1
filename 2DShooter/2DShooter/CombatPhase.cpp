#include "CombatPhase.h"

#include "UnitManager.h"
#include "BulletManager.h"
#include "State.h"
#include "BuildPhase.h"
#include "ComputerPlayer.h"
#include "BuildingManager.h"
#include "Unit.h"
#include "ConfigFileReader.h"

CombatPhase::CombatPhase()
{
}


CombatPhase::~CombatPhase()
{
	delete(bullet_manager);
	delete(unit_manager);

}

void CombatPhase::SetUpCombatPhase(State &state)
{
	ConfigFileReader file("Content/Text Files/Buildings/Buildings.cfg");
	unit_manager = new UnitManager();
	unit_manager->Init(*state.GetBuildPhase()->GetBuildingManager());
	bullet_manager = new BulletManager();

	ResetCountDown();

	space_image.LoadBitmap("Content/Images/Combat Phase Backgrounds/PC Computer - Space Pirates and Zombies - Backgrounds 22/Building_Background2.png");

	unit_manager->CeateUnit(state.GetBuildPhase()->GetBuildingManager()->GetMainBuildingType(1), 1);
	unit_manager->CeateUnit(state.GetBuildPhase()->GetBuildingManager()->GetMainBuildingType(2), 2);

	for (int i = 9; i < 13; i++)
	{
		if (state.GetBuildPhase()->GetBuildingManager()->DoesBuildingTypeExist(1, i) == true)
		{
			switch (i)
			{
			case 9:
				unit_manager->GetPlayer1Unit()->BoostAttack(file.GetConfigValueIntegers("building type 1", "boost attack"));
				break;
			case 10:
				unit_manager->GetPlayer1Unit()->BoostDefense(file.GetConfigValueIntegers("building type 2", "boost defence"));
				break;
			case 11:
				unit_manager->GetPlayer1Unit()->BoostHealth(file.GetConfigValueIntegers("building type 3", "boost health"));
				break;
			case 12:
				unit_manager->GetPlayer1Unit()->BoostSpeed(file.GetConfigValueIntegers("building type 4", "boost speed"));
				break;
			}
		}
		if (state.GetBuildPhase()->GetBuildingManager()->DoesBuildingTypeExist(2, i) == true)
		{
			switch (i)
			{
			case 9:
				unit_manager->GetPlayer2Unit()->BoostAttack(file.GetConfigValueIntegers("building type 1", "boost attack"));
				break;
			case 10:
				unit_manager->GetPlayer2Unit()->BoostDefense(file.GetConfigValueIntegers("building type 2", "boost defence"));
				break;
			case 11:
				unit_manager->GetPlayer2Unit()->BoostHealth(file.GetConfigValueIntegers("building type 3", "boost health"));
				break;
			case 12:
				unit_manager->GetPlayer2Unit()->BoostSpeed(file.GetConfigValueIntegers("building type 4", "boost speed"));
				break;
			}
		}
	}

	state.GetComputerPlayer()->SetStats(state.GetBuildPhase()->GetBuildingManager()->GetMainBuildingType(2));

	emulogic_font.CreateFont("Content/Fonts/Gravedigger.ttf", 36);

	ready_text.CreateTextRender(emulogic_font.ObtainFont(), al_map_rgb(255, 255, 255), "Get Ready!");
	go_text.CreateTextRender(emulogic_font.ObtainFont(), al_map_rgb(255, 255, 255), "Go!");

	lost_text_1.CreateTextRender(emulogic_font.ObtainFont(), al_map_rgb(255, 255, 255), "Bottom player losing a life");

	lost_text_2.CreateTextRender(emulogic_font.ObtainFont(), al_map_rgb(255, 255, 255), "Top player losing a life");
}

void CombatPhase::UpdateCombatPhase(State &state)
{
	unit_manager->UpdateUnitManager(*bullet_manager, state);
	bullet_manager->UpdateBullet();
	if (state.GetBuildPhase()->DoesComputerExist())
		state.GetComputerPlayer()->UpdateComputerPlayer();

	if (time_passed <= go_time + 1)
	{
		time_passed += 1;
		if (time_passed >= wait_time)
		{
			begin = true;
		}
	}
}

void CombatPhase::RenderCombatPhase(float window_scale_x, float window_scale_y)
{
	space_image.DrawScaledImage(0, 0,
		space_image.GetBitmapWidth(),
		space_image.GetBitmapHeight(),
		0 * window_scale_x,
		0 * window_scale_y,
		space_image.GetBitmapWidth() * window_scale_x,
		space_image.GetBitmapHeight() * window_scale_y,
		NULL);

	if (time_passed < wait_time)
	{
		ready_text.DrawScaledImage(0, 0,
			ready_text.GetBitmapWidth(),
			ready_text.GetBitmapHeight(),
			((1280 / 2) - (ready_text.GetBitmapWidth() / 2) * window_scale_x),
			((720 / 2) - (ready_text.GetBitmapHeight() / 2) * window_scale_y),
			ready_text.GetBitmapWidth() * window_scale_x,
			ready_text.GetBitmapHeight() * window_scale_y,
			NULL);
	}
	else if (time_passed > wait_time && time_passed < go_time)
	{
		go_text.DrawScaledImage(0, 0,
			go_text.GetBitmapWidth(),
			go_text.GetBitmapHeight(),
			((1280 / 2) - (go_text.GetBitmapWidth() / 2) * window_scale_x),
			((720 / 2) - (go_text.GetBitmapHeight() / 2) * window_scale_y),
			go_text.GetBitmapWidth() * window_scale_x,
			go_text.GetBitmapHeight() * window_scale_y,
			NULL);
	}

	unit_manager->RenderUnits(window_scale_x, window_scale_y);
	bullet_manager->RenderBulletManager(window_scale_x, window_scale_y);

	if (DidPlayer1Die() == true)
	{
		lost_text_1.DrawScaledImage(0, 0,
			lost_text_1.GetBitmapWidth(),
			lost_text_1.GetBitmapHeight(),
			lost_text_x * window_scale_x,
			lost_text_y * window_scale_y,
			lost_text_1.GetBitmapWidth() * window_scale_x,
			lost_text_1.GetBitmapHeight() * window_scale_y,
			NULL);
	}
	else if (DidPlayer2Die() == true)
	{
		lost_text_2.DrawScaledImage(0, 0,
			lost_text_2.GetBitmapWidth(),
			lost_text_2.GetBitmapHeight(),
			lost_text_x * window_scale_x,
			lost_text_y * window_scale_y,
			lost_text_2.GetBitmapWidth() * window_scale_x,
			lost_text_2.GetBitmapHeight() * window_scale_y,
			NULL);
	}
}

bool CombatPhase::DidUnitDie()
{
	if (unit_manager->GetPlayer1Unit()->IsUnitDead() == true ||
		unit_manager->GetPlayer2Unit()->IsUnitDead() == true)
	{
		return true;
	}
	else
		return false;
}

bool CombatPhase::DidPlayer1Die()
{
	if (unit_manager->GetPlayer1Unit()->IsUnitDead() == true)
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool CombatPhase::DidPlayer2Die()
{
	if (unit_manager->GetPlayer2Unit()->IsUnitDead() == true)
	{
		return true;
	}
	else
	{
		return false;
	}
}

void CombatPhase::ResetCountDown()
{
	ConfigFileReader file("Content/Text Files/CombatPhase/CombatPhase.cfg");

	begin = false;
	wait_time = file.GetConfigValueIntegers("timer", "start");
	go_time = file.GetConfigValueIntegers("timer", "go");
	time_passed = 0;

	lost_text_x = file.GetConfigValueIntegers("player text", "position x");
	lost_text_y = file.GetConfigValueIntegers("player text", "position y");
}