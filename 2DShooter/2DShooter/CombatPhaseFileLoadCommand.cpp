#include "CombatPhaseFileLoadCommand.h"

#include "State.h"


CombatPhaseFileLoadCommand::~CombatPhaseFileLoadCommand()
{
}

void CombatPhaseFileLoadCommand::execute(State &state)
{
	state.DeleteCombatPhase();
	state.InitCombatPhase();
}
