#pragma once

#include "Render.h"
#include "SpriteAnimation.h"


class Building
{
public:

	Building();
	Building(int type, int row, int column);
	~Building();

	/*
		SetUpBuilding
		Loads everything needed for the class
	*/
	void SetUpBuilding();

	/*
		UpdateBuilding
		Will update the building animations
	*/
	void UpdateBuilding();

	/*
		RenderBuilding
		Draws everything in association with the 
		building class
	*/
	void RenderBuilding(float window_scale_x, float window_scale_y);

	/*
		LoadBuilding
		Reads from a file all the stats of the building
	*/
	void LoadBuilding();

	/*
		GetBuildingType
		Returns the type of building it is
	*/
	int GetBuildingType();

private:

	int building_type;							// Holds the type of building this is
	int row;									// Holds the row position of the building
	int column;									// Holds the column position of the building
	int upgrade_level;							// Holds what level upgrade the building is
	int upgrade_cost;							// Holds how much it cost to upgrade

	bool is_ship;								// Determines if ship or building is drawn

	Render building_image;						// Holds the image of the type of building it is
	SpriteAnimation building_animation;			// Holds the coordinates of the image building

	Render ship_image;							// Holds the image of the type of ship it is
	SpriteAnimation ship_animation;				// Holds the coordinates of the image ship
};

