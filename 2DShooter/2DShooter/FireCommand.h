#pragma once
#include "Command.h"

class FireCommand : public Command
{
public:

	~FireCommand();

	virtual void execute() {};
	virtual void execute(State &state);
};

