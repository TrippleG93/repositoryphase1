#pragma once

#include "SpriteAnimation.h"
#include "Render.h"
#include "Audio.h"
#include "Font.h"

class Player
{
public:
	Player();
	~Player();

	/*
	SetUpPlayer
	Loads everything assocaited with the class
	*/
	void SetUpPlayer();

	/*
	UpdatePlayer
	Update animations and any other variables assciated with the class
	*/
	void UpdatePlayer();

	/*
	DrawPlayerInfo
	Draws all the information in regards to the player
	*/
	void DrawPlayerInfo(float window_scale_x, float window_scale_y);

	/*
	DoesPlayerHaveMainShip
	returns true if the player has a main ship
	false otherwise
	*/
	bool DoesPlayerHaveMainShip();

	/*
		ResetPurchase
		resets the purchase to allow the player to make purchases
	*/
	void ResetPurchase();

	/*
		Purchased 
		Decrease what they can purchase by one
	*/
	void Purchased();

	/*
		CanPlayerPurchase
		Holds if the player can make a purchase or not
	*/
	bool CanPlayerPurchase();

	/*
		LoseALife
		subtracts 1 from the remaining life of the player. 
	*/
	void LoseALife();	

	/*
		IsDead
		Returns true if the player has no more lives remaining
	*/
	bool IsDead();

	/*
		GetRemainingLives
		Returns the amoung of lives the player has
	*/
	int GetRemainingLives() { return lives; }

	void DrawHeart(int position_x, int position_y, float window_scale_x, float window_scale_y);

	void SetTimers();

	void StartLifeLostTimer() { ready_to_lose_life = true; }

	bool DoneLosingLife() { return done_losing_life; }

	bool ReadyToLoseLife() {return ready_to_lose_life;}

	void SetMainShip() { has_main_ship = true; }

private:

	int purchases;										// Holds how much the buildings the player can purchase
	bool has_main_ship;									// Holds if they have a main ship or not

	Font emulogic_font12;

	Render heart_image;									// Visual representation of lives

	SpriteAnimation heart_animation;					// Visual representation of lives
	SpriteAnimation lost_animation;						// Visual representation of losing  a life

	Render* purchases_left;								// Image that holds how much purchases they have left. 
	int position_x;
	int position_y;

	int lives;
	int max_lives;

	int show_lives_count;
	int time_to_lose_count;
	int timer;

	bool ready_to_lose_life;
	bool just_lost_life;
	bool done_losing_life;
};

