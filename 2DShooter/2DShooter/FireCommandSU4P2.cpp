#include "FireCommandSU4P2.h"

#include "State.h"

#include "BulletManager.h"

#include "CombatPhase.h"
FireCommandSU4P2::~FireCommandSU4P2()
{
}



void FireCommandSU4P2::execute(State &state)
{
	state.GetCombatPhase()->GetBulletManager()->CreateBullet(2, 2, *state.GetCombatPhase()->GetUnitManager());
}
