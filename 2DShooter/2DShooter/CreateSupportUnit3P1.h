#pragma once

#include "Command.h"

class CreateSupportUnit3P1 : public Command
{
public:
	~CreateSupportUnit3P1();

	virtual void execute() {};
	virtual void execute(State &state);
};

